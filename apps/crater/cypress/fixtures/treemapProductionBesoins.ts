import type { OptionsChartTreemapDoubleCultures } from '../../src/pages/diagnostic/charts/ChartTreemapDoubleCultures';

export const occitanie: OptionsChartTreemapDoubleCultures = {
    sauTotaleCulturesGaucheHa: 2568467,
    sauTotaleCulturesDroiteHa: 2208764.44,
    titreCulturesGauche: 'Titre gauche',
    titreCulturesDroite: 'Titre droite',
    itemsLegende: [
        {
            libelle: 'Fourrages',
            couleur: 'ce7676'
        },
        {
            libelle: 'Céréales',
            couleur: 'ffba66'
        }
    ],
    donneesTreemapCulturesGauche: [
        {
            code: 'FOU',
            labelCourt: 'FO',
            labelLong: 'Fourrages',
            couleur: '#ce7676',
            sauHa: 1174738.12,
            detailCultures: [
                {
                    codeCulture: 'P-FOA',
                    nomCulture: 'Autres fourr annuels',
                    sauHa: 10047.96
                },
                {
                    codeCulture: 'P-FOP',
                    nomCulture: 'Autres prairies',
                    sauHa: 321670.71
                },
                {
                    codeCulture: 'P-LGF',
                    nomCulture: 'Légumin fourrage',
                    sauHa: 174259.26
                },
                {
                    codeCulture: 'P-MAF',
                    nomCulture: 'Maïs fourrage',
                    sauHa: 34415.63
                },
                {
                    codeCulture: 'P-PRA',
                    nomCulture: 'Prairies permanentes',
                    sauHa: 634344.56
                }
            ]
        },
        {
            code: 'CER',
            labelCourt: 'CE',
            labelLong: 'Céréales',
            couleur: '#ffba66',
            sauHa: 764399.48,
            detailCultures: [
                {
                    codeCulture: 'P-ACR',
                    nomCulture: 'Autres',
                    sauHa: 106223.86
                },
                {
                    codeCulture: 'P-BLD',
                    nomCulture: 'Blé dur',
                    sauHa: 141061
                },
                {
                    codeCulture: 'P-BLT',
                    nomCulture: 'Blé tendre',
                    sauHa: 261454.7
                },
                {
                    codeCulture: 'P-MAG',
                    nomCulture: 'Maïs grain',
                    sauHa: 149794.89
                },
                {
                    codeCulture: 'P-ORG',
                    nomCulture: 'Orge',
                    sauHa: 105865.03
                }
            ]
        },
        {
            code: 'OLP',
            labelCourt: 'OL',
            labelLong: 'Oléoprotéagineux',
            couleur: '#a38566',
            sauHa: 351018.79,
            detailCultures: [
                {
                    codeCulture: 'P-AOP',
                    nomCulture: 'Autres',
                    sauHa: 56106.89
                },
                {
                    codeCulture: 'P-CLZ',
                    nomCulture: 'Colza',
                    sauHa: 40338.12
                },
                {
                    codeCulture: 'P-LGG',
                    nomCulture: 'Légumin graines',
                    sauHa: 48245.42
                },
                {
                    codeCulture: 'P-TRN',
                    nomCulture: 'Tournesol',
                    sauHa: 206328.36
                }
            ]
        },
        {
            code: 'FLC',
            labelCourt: 'FL',
            labelLong: 'Fruits et légumes',
            couleur: '#74a48b',
            sauHa: 50609.46,
            detailCultures: [
                {
                    codeCulture: 'P-FCO',
                    nomCulture: 'Fruits à coque',
                    sauHa: 6917.03
                },
                {
                    codeCulture: 'P-FRU',
                    nomCulture: 'Fruits',
                    sauHa: 31091.24
                },
                {
                    codeCulture: 'P-LEG',
                    nomCulture: 'Légumes',
                    sauHa: 12601.19
                }
            ]
        },
        {
            code: 'DVC',
            labelCourt: 'DV',
            labelLong: 'Autres cultures',
            couleur: '#9195d4',
            sauHa: 227701.62,
            detailCultures: [
                {
                    codeCulture: 'P-AAC',
                    nomCulture: 'Autres',
                    sauHa: 487.36
                },
                {
                    codeCulture: 'P-BET',
                    nomCulture: 'Betterave sucrière',
                    sauHa: 1367.77
                },
                {
                    codeCulture: 'P-OLV',
                    nomCulture: 'Oliviers',
                    sauHa: 3413.85
                },
                {
                    codeCulture: 'P-PAF',
                    nomCulture: 'Plantes à fibres',
                    sauHa: 154.92
                },
                {
                    codeCulture: 'P-PAM',
                    nomCulture: 'Plantes méd et aro, fleurs',
                    sauHa: 2707.16
                },
                {
                    codeCulture: 'P-PDT',
                    nomCulture: 'Pommes de terre',
                    sauHa: 997.66
                },
                {
                    codeCulture: 'P-TAB',
                    nomCulture: 'Tabac',
                    sauHa: 473.47
                },
                {
                    codeCulture: 'P-VIT',
                    nomCulture: 'Vignes',
                    sauHa: 218099.43
                }
            ]
        }
    ],
    donneesTreemapCulturesDroite: [
        {
            code: 'FOU',
            labelCourt: 'FO',
            labelLong: 'Fourrages',
            couleur: '#ce7676',
            sauHa: 1490335.58,
            detailCultures: [
                {
                    codeCulture: 'BA-AFO',
                    nomCulture: 'Fourrages annuels',
                    sauHa: 107315.3
                },
                {
                    codeCulture: 'BA-PRA',
                    nomCulture: 'Prairies',
                    sauHa: 1383020.28
                }
            ]
        },
        {
            code: 'CER',
            labelCourt: 'CE',
            labelLong: 'Céréales',
            couleur: '#ffba66',
            sauHa: 406345.35,
            detailCultures: [
                {
                    codeCulture: 'BA-AAC',
                    nomCulture: 'Alim animale',
                    sauHa: 282312.17
                },
                {
                    codeCulture: 'BH-AHC',
                    nomCulture: 'Alim humaine',
                    sauHa: 124033.18
                }
            ]
        },
        {
            code: 'OLP',
            labelCourt: 'OL',
            labelLong: 'Oléoprotéagineux',
            couleur: '#a38566',
            sauHa: 226270.22,
            detailCultures: [
                {
                    codeCulture: 'BA-AAO',
                    nomCulture: 'Tourteaux',
                    sauHa: 145558.6
                },
                {
                    codeCulture: 'BA-PRO',
                    nomCulture: 'Légumin graines',
                    sauHa: 11915.5
                },
                {
                    codeCulture: 'BH-AHO',
                    nomCulture: 'Alim humaine',
                    sauHa: 68796.11
                }
            ]
        },
        {
            code: 'FLC',
            labelCourt: 'FL',
            labelLong: 'Fruits et légumes',
            couleur: '#74a48b',
            sauHa: 46134.17,
            detailCultures: [
                {
                    codeCulture: 'BH-FCO',
                    nomCulture: 'Fruits à coque',
                    sauHa: 10297.76
                },
                {
                    codeCulture: 'BH-FRU',
                    nomCulture: 'Fruits',
                    sauHa: 16161.57
                },
                {
                    codeCulture: 'BH-LEG',
                    nomCulture: 'Légumes',
                    sauHa: 19674.84
                }
            ]
        },
        {
            code: 'DVC',
            labelCourt: 'DV',
            labelLong: 'Autres cultures',
            couleur: '#9195d4',
            sauHa: 39679.12,
            detailCultures: [
                {
                    codeCulture: 'BH-BET',
                    nomCulture: 'Betterave sucrière',
                    sauHa: 23546.94
                },
                {
                    codeCulture: 'BH-OLV',
                    nomCulture: 'Oliviers',
                    sauHa: 1455.53
                },
                {
                    codeCulture: 'BH-PDT',
                    nomCulture: 'Pommes de terre',
                    sauHa: 14676.65
                }
            ]
        }
    ]
};

export const toulouseMetropole: OptionsChartTreemapDoubleCultures = {
    sauTotaleCulturesGaucheHa: 9597,
    sauTotaleCulturesDroiteHa: 216220.11,
    titreCulturesGauche: 'Titre gauche',
    titreCulturesDroite: 'Titre droite',
    itemsLegende: [
        {
            libelle: 'Fourrages',
            couleur: 'ce7676'
        },
        {
            libelle: 'Céréales',
            couleur: 'ffba66'
        }
    ],
    donneesTreemapCulturesGauche: [
        {
            code: 'FOU',
            labelCourt: 'FO',
            labelLong: 'Fourrages',
            couleur: '#ce7676',
            sauHa: 1021.1,
            detailCultures: [
                {
                    codeCulture: 'P-FOA',
                    nomCulture: 'Autres fourr annuels',
                    sauHa: 4.21
                },
                {
                    codeCulture: 'P-FOP',
                    nomCulture: 'Autres prairies',
                    sauHa: 519.16
                },
                {
                    codeCulture: 'P-LGF',
                    nomCulture: 'Légumin fourrage',
                    sauHa: 200.29
                },
                {
                    codeCulture: 'P-MAF',
                    nomCulture: 'Maïs fourrage',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-PRA',
                    nomCulture: 'Prairies permanentes',
                    sauHa: 297.44
                }
            ]
        },
        {
            code: 'CER',
            labelCourt: 'CE',
            labelLong: 'Céréales',
            couleur: '#ffba66',
            sauHa: 6128.1,
            detailCultures: [
                {
                    codeCulture: 'P-ACR',
                    nomCulture: 'Autres',
                    sauHa: 605.35
                },
                {
                    codeCulture: 'P-BLD',
                    nomCulture: 'Blé dur',
                    sauHa: 2314.59
                },
                {
                    codeCulture: 'P-BLT',
                    nomCulture: 'Blé tendre',
                    sauHa: 1467.16
                },
                {
                    codeCulture: 'P-MAG',
                    nomCulture: 'Maïs grain',
                    sauHa: 1179.23
                },
                {
                    codeCulture: 'P-ORG',
                    nomCulture: 'Orge',
                    sauHa: 561.77
                }
            ]
        },
        {
            code: 'OLP',
            labelCourt: 'OL',
            labelLong: 'Oléoprotéagineux',
            couleur: '#a38566',
            sauHa: 2249.88,
            detailCultures: [
                {
                    codeCulture: 'P-AOP',
                    nomCulture: 'Autres',
                    sauHa: 278.2
                },
                {
                    codeCulture: 'P-CLZ',
                    nomCulture: 'Colza',
                    sauHa: 801.7
                },
                {
                    codeCulture: 'P-LGG',
                    nomCulture: 'Légumin graines',
                    sauHa: 136.71
                },
                {
                    codeCulture: 'P-TRN',
                    nomCulture: 'Tournesol',
                    sauHa: 1033.27
                }
            ]
        },
        {
            code: 'FLC',
            labelCourt: 'FL',
            labelLong: 'Fruits et légumes',
            couleur: '#74a48b',
            sauHa: 165.31,
            detailCultures: [
                {
                    codeCulture: 'P-FCO',
                    nomCulture: 'Fruits à coque',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-FRU',
                    nomCulture: 'Fruits',
                    sauHa: 4.11
                },
                {
                    codeCulture: 'P-LEG',
                    nomCulture: 'Légumes',
                    sauHa: 161.2
                }
            ]
        },
        {
            code: 'DVC',
            labelCourt: 'DV',
            labelLong: 'Autres cultures',
            couleur: '#9195d4',
            sauHa: 32.12,
            detailCultures: [
                {
                    codeCulture: 'P-AAC',
                    nomCulture: 'Autres',
                    sauHa: 0.17
                },
                {
                    codeCulture: 'P-BET',
                    nomCulture: 'Betterave sucrière',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-OLV',
                    nomCulture: 'Oliviers',
                    sauHa: 0.18
                },
                {
                    codeCulture: 'P-PAF',
                    nomCulture: 'Plantes à fibres',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-PAM',
                    nomCulture: 'Plantes méd et aro, fleurs',
                    sauHa: 1.2
                },
                {
                    codeCulture: 'P-PDT',
                    nomCulture: 'Pommes de terre',
                    sauHa: 9.51
                },
                {
                    codeCulture: 'P-TAB',
                    nomCulture: 'Tabac',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-VIT',
                    nomCulture: 'Vignes',
                    sauHa: 21.06
                }
            ]
        }
    ],
    donneesTreemapCulturesDroite: [
        {
            code: 'FOU',
            labelCourt: 'FO',
            labelLong: 'Fourrages',
            couleur: '#ce7676',
            sauHa: 127430.7,
            detailCultures: [
                {
                    codeCulture: 'BA-AFO',
                    nomCulture: 'Fourrages annuels',
                    sauHa: 11962.96
                },
                {
                    codeCulture: 'BA-PRA',
                    nomCulture: 'Prairies',
                    sauHa: 115467.74
                }
            ]
        },
        {
            code: 'CER',
            labelCourt: 'CE',
            labelLong: 'Céréales',
            couleur: '#ffba66',
            sauHa: 48698.74,
            detailCultures: [
                {
                    codeCulture: 'BA-AAC',
                    nomCulture: 'Alim animale',
                    sauHa: 33197.58
                },
                {
                    codeCulture: 'BH-AHC',
                    nomCulture: 'Alim humaine',
                    sauHa: 15501.17
                }
            ]
        },
        {
            code: 'OLP',
            labelCourt: 'OL',
            labelLong: 'Oléoprotéagineux',
            couleur: '#a38566',
            sauHa: 29291.98,
            detailCultures: [
                {
                    codeCulture: 'BA-AAO',
                    nomCulture: 'Tourteaux',
                    sauHa: 18560
                },
                {
                    codeCulture: 'BA-PRO',
                    nomCulture: 'Légumin graines',
                    sauHa: 1542.77
                },
                {
                    codeCulture: 'BH-AHO',
                    nomCulture: 'Alim humaine',
                    sauHa: 9189.2
                }
            ]
        },
        {
            code: 'FLC',
            labelCourt: 'FL',
            labelLong: 'Fruits et légumes',
            couleur: '#74a48b',
            sauHa: 5710.36,
            detailCultures: [
                {
                    codeCulture: 'BH-FCO',
                    nomCulture: 'Fruits à coque',
                    sauHa: 696.09
                },
                {
                    codeCulture: 'BH-FRU',
                    nomCulture: 'Fruits',
                    sauHa: 2303.76
                },
                {
                    codeCulture: 'BH-LEG',
                    nomCulture: 'Légumes',
                    sauHa: 2710.51
                }
            ]
        },
        {
            code: 'DVC',
            labelCourt: 'DV',
            labelLong: 'Autres cultures',
            couleur: '#9195d4',
            sauHa: 5088.33,
            detailCultures: [
                {
                    codeCulture: 'BH-BET',
                    nomCulture: 'Betterave sucrière',
                    sauHa: 3005.41
                },
                {
                    codeCulture: 'BH-OLV',
                    nomCulture: 'Oliviers',
                    sauHa: 185.22
                },
                {
                    codeCulture: 'BH-PDT',
                    nomCulture: 'Pommes de terre',
                    sauHa: 1897.7
                }
            ]
        }
    ]
};

export const metropoleDuGrandParis: OptionsChartTreemapDoubleCultures = {
    sauTotaleCulturesGaucheHa: 1416,
    sauTotaleCulturesDroiteHa: 2657145.78,
    titreCulturesGauche: 'Titre gauche',
    titreCulturesDroite: 'Titre droite',
    itemsLegende: [
        {
            libelle: 'Fourrages',
            couleur: 'ce7676'
        },
        {
            libelle: 'Céréales',
            couleur: 'ffba66'
        }
    ],
    donneesTreemapCulturesGauche: [
        {
            code: 'FOU',
            labelCourt: 'FO',
            labelLong: 'Fourrages',
            couleur: '#ce7676',
            sauHa: 140.34,
            detailCultures: [
                {
                    codeCulture: 'P-FOA',
                    nomCulture: 'Autres fourr annuels',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-FOP',
                    nomCulture: 'Autres prairies',
                    sauHa: 30.64
                },
                {
                    codeCulture: 'P-LGF',
                    nomCulture: 'Légumin fourrage',
                    sauHa: 9.66
                },
                {
                    codeCulture: 'P-MAF',
                    nomCulture: 'Maïs fourrage',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-PRA',
                    nomCulture: 'Prairies permanentes',
                    sauHa: 100.04
                }
            ]
        },
        {
            code: 'CER',
            labelCourt: 'CE',
            labelLong: 'Céréales',
            couleur: '#ffba66',
            sauHa: 894.33,
            detailCultures: [
                {
                    codeCulture: 'P-ACR',
                    nomCulture: 'Autres',
                    sauHa: 8.52
                },
                {
                    codeCulture: 'P-BLD',
                    nomCulture: 'Blé dur',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-BLT',
                    nomCulture: 'Blé tendre',
                    sauHa: 689.64
                },
                {
                    codeCulture: 'P-MAG',
                    nomCulture: 'Maïs grain',
                    sauHa: 68.8
                },
                {
                    codeCulture: 'P-ORG',
                    nomCulture: 'Orge',
                    sauHa: 127.37
                }
            ]
        },
        {
            code: 'OLP',
            labelCourt: 'OL',
            labelLong: 'Oléoprotéagineux',
            couleur: '#a38566',
            sauHa: 198.41,
            detailCultures: [
                {
                    codeCulture: 'P-AOP',
                    nomCulture: 'Autres',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-CLZ',
                    nomCulture: 'Colza',
                    sauHa: 194.23
                },
                {
                    codeCulture: 'P-LGG',
                    nomCulture: 'Légumin graines',
                    sauHa: 4.18
                },
                {
                    codeCulture: 'P-TRN',
                    nomCulture: 'Tournesol',
                    sauHa: 0
                }
            ]
        },
        {
            code: 'FLC',
            labelCourt: 'FL',
            labelLong: 'Fruits et légumes',
            couleur: '#74a48b',
            sauHa: 50.65,
            detailCultures: [
                {
                    codeCulture: 'P-FCO',
                    nomCulture: 'Fruits à coque',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-FRU',
                    nomCulture: 'Fruits',
                    sauHa: 1.12
                },
                {
                    codeCulture: 'P-LEG',
                    nomCulture: 'Légumes',
                    sauHa: 49.53
                }
            ]
        },
        {
            code: 'DVC',
            labelCourt: 'DV',
            labelLong: 'Autres cultures',
            couleur: '#9195d4',
            sauHa: 132.2,
            detailCultures: [
                {
                    codeCulture: 'P-AAC',
                    nomCulture: 'Autres',
                    sauHa: 28.5
                },
                {
                    codeCulture: 'P-BET',
                    nomCulture: 'Betterave sucrière',
                    sauHa: 78.32
                },
                {
                    codeCulture: 'P-OLV',
                    nomCulture: 'Oliviers',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-PAF',
                    nomCulture: 'Plantes à fibres',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-PAM',
                    nomCulture: 'Plantes méd et aro, fleurs',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-PDT',
                    nomCulture: 'Pommes de terre',
                    sauHa: 25.38
                },
                {
                    codeCulture: 'P-TAB',
                    nomCulture: 'Tabac',
                    sauHa: 0
                },
                {
                    codeCulture: 'P-VIT',
                    nomCulture: 'Vignes',
                    sauHa: 0
                }
            ]
        }
    ],
    donneesTreemapCulturesDroite: [
        {
            code: 'FOU',
            labelCourt: 'FO',
            labelLong: 'Fourrages',
            couleur: '#ce7676',
            sauHa: 1825369.77,
            detailCultures: [
                {
                    codeCulture: 'BA-AFO',
                    nomCulture: 'Fourrages annuels',
                    sauHa: 153840.62
                },
                {
                    codeCulture: 'BA-PRA',
                    nomCulture: 'Prairies',
                    sauHa: 1671529.16
                }
            ]
        },
        {
            code: 'CER',
            labelCourt: 'CE',
            labelLong: 'Céréales',
            couleur: '#ffba66',
            sauHa: 463563.98,
            detailCultures: [
                {
                    codeCulture: 'BA-AAC',
                    nomCulture: 'Alim animale',
                    sauHa: 345983.52
                },
                {
                    codeCulture: 'BH-AHC',
                    nomCulture: 'Alim humaine',
                    sauHa: 117580.46
                }
            ]
        },
        {
            code: 'OLP',
            labelCourt: 'OL',
            labelLong: 'Oléoprotéagineux',
            couleur: '#a38566',
            sauHa: 253777,
            detailCultures: [
                {
                    codeCulture: 'BA-AAO',
                    nomCulture: 'Tourteaux',
                    sauHa: 170303.94
                },
                {
                    codeCulture: 'BA-PRO',
                    nomCulture: 'Légumin graines',
                    sauHa: 15938.32
                },
                {
                    codeCulture: 'BH-AHO',
                    nomCulture: 'Alim humaine',
                    sauHa: 67534.75
                }
            ]
        },
        {
            code: 'FLC',
            labelCourt: 'FL',
            labelLong: 'Fruits et légumes',
            couleur: '#74a48b',
            sauHa: 72848.95,
            detailCultures: [
                {
                    codeCulture: 'BH-FCO',
                    nomCulture: 'Fruits à coque',
                    sauHa: 9124.35
                },
                {
                    codeCulture: 'BH-FRU',
                    nomCulture: 'Fruits',
                    sauHa: 31142.67
                },
                {
                    codeCulture: 'BH-LEG',
                    nomCulture: 'Légumes',
                    sauHa: 32581.93
                }
            ]
        },
        {
            code: 'DVC',
            labelCourt: 'DV',
            labelLong: 'Autres cultures',
            couleur: '#9195d4',
            sauHa: 41586.07,
            detailCultures: [
                {
                    codeCulture: 'BH-BET',
                    nomCulture: 'Betterave sucrière',
                    sauHa: 29105.99
                },
                {
                    codeCulture: 'BH-OLV',
                    nomCulture: 'Oliviers',
                    sauHa: 2278.99
                },
                {
                    codeCulture: 'BH-PDT',
                    nomCulture: 'Pommes de terre',
                    sauHa: 10201.08
                }
            ]
        }
    ]
};
