describe('Tests e2e/integration : accueil, recherche territoire puis affichage du diagnostic  ', () => {
    it('Rechercher le territoire France et afficher la page de synthèse du diagnostic', () => {
        cy.visit('/');
        cy.get('input[type="text"]').type('Fra');
        cy.get('menu article').contains('France').click({ force: true });
        cy.verifierAffichagePage('Diagnostic du système alimentaire | France');
    });
});
