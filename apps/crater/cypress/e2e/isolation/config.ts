export function configurerInterceptRequetesAPI() {
    // Par défaut, on retourne une erreur 404 pour etre sur que les tests tournent en isolation, sans appel à l'api externe
    // A faire avant la déclaration des intercepts plus spécifiques
    cy.intercept('GET', 'https://api.resiliencealimentaire.org/dev/crater/api/**', { statusCode: 404, body: 'forced 404' });
    cy.intercept('GET', 'https://api.resiliencealimentaire.org/crater/api/**', { statusCode: 404, body: 'forced 404' });

    // Liste exhaustive des urls interceptées
    cy.intercept('GET', '**/api/territoires?critere=f*', { fixture: 'territoire-liste-suggestions-france.json' });
    cy.intercept('GET', '**/api/territoires?critere=oc*', { fixture: 'territoire-liste-suggestions-occitanie.json' });
    cy.intercept('GET', '**/api/territoires/occitanie', { fixture: 'territoire-occitanie.json' });
    cy.intercept('GET', '**/api/territoires/france', { fixture: 'territoire-france.json' });
    cy.intercept('GET', '**/api/diagnostics/occitanie', { fixture: 'diagnostic-occitanie.json' });
    cy.intercept('GET', '**/api/diagnostics/france', { fixture: 'diagnostic-france.json' });
}
