import { configurerInterceptRequetesAPI } from '../config';

describe('Test de vérification des couleurs des territoires selon leur note sur la page carte', () => {
    const COULEUR_TERRITOIRE_DONNEES_NON_DISPONIBLES = '#d7d7d7';

    beforeEach(() => {
        configurerInterceptRequetesAPI();
    });

    // Fixme : test désactivé car pas stable => il faut refactorer widgetcarte pour coller mieux au cycle de vie leaflet et utiliser des mocks pour les données (éviter le timeout lié au download des contours)
    it.skip('Valider couleur des territoires pour agriculteurs & exploitations', () => {
        // init
        cy.viewport(1500, 1000);
        cy.visit('carte?echelleTerritoriale=DEPARTEMENT');

        // Permet d'attendre que la carte soit chargée avant de changer d'axe
        cy.get('c-carte').find('.carte__id-territoire__D-75').should('have.attr', 'fill', COULEUR_TERRITOIRE_DONNEES_NON_DISPONIBLES);
        cy.get('c-carte').find('.carte__id-territoire__D-38').should('have.attr', 'fill', '#d05c2d');
        // Changer d'axe
        cy.get('#section-choix-indicateur').find('label[for=evaluation-globale-agriculteurs-exploitations]').click({ force: true });

        cy.get('c-carte').find('.carte__id-territoire__D-38').should('have.attr', 'fill', '#e17b3c');
    });
});
