import { configurerInterceptRequetesAPI } from '../config';

describe("Test de vérification des couleurs des territoires après un changement d'échelle territoriale", () => {
    beforeEach(() => {
        configurerInterceptRequetesAPI();
    });

    // Fixme : test désactivé car pas stable => il faut refactorer widgetcarte pour coller mieux au cycle de vie leaflet et utiliser des mocks pour les données (éviter le timeout lié au download des contours)
    it.skip('Valider couleur des territoires pour régions', () => {
        // init
        cy.viewport(1500, 1000);
        cy.visit('carte?echelleTerritoriale=EPCI');

        cy.get('c-barre-menu-territoires').find('#REGION').click({ force: true });

        cy.get('c-carte').find('.carte__id-territoire__R-24').should('have.attr', 'fill', '#9f984e');
    });
});
