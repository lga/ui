import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/commun/piedPage/AllerPlusLoin.js';

import { html } from 'lit';

import imageCouvertureRapport1 from './couverture-rapport1.png';
import imageCouvertureRapport2 from './couverture-rapport2.png';

describe('Test composant AllerPlusLoin', () => {
    it(' Test affichage desktop avec illustration ', () => {
        cy.viewport(800, 500);
        cy.mount<'c-aller-plus-loin'>(
            html`<c-aller-plus-loin>
                <div>Les enjeux du système alimentaire national</div>
                <img src=${imageCouvertureRapport1} width="208" height="300" />
                <div>Leviers d'action à l'échelle des territoires</div>
                <img src=${imageCouvertureRapport2} width="207" height="300" />
            </c-aller-plus-loin>`
        );
        cy.get('c-aller-plus-loin').should('be.visible');
        cy.get('c-aller-plus-loin').find('div').should('be.visible');
        cy.get('c-aller-plus-loin').find('img').should('exist');
    });

    it('Test affichage mobile sans illustration', () => {
        cy.viewport(800, 500);
        cy.mount<'c-aller-plus-loin'>(
            html`<c-aller-plus-loin>
                <div>Les enjeux du système alimentaire national</div>
                <div>Leviers d'action à l'échelle des territoires</div>
            </c-aller-plus-loin>`
        );
        cy.get('c-aller-plus-loin').find('div').should('be.visible');
    });
});
