import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/charts/ChartBarresAvecCourbeTendance.js';

import { html } from 'lit';

import { Serie, SerieMultiple } from '../../../src/pages/diagnostic/charts/chart-outils.js';
import type { OptionsChartBarresAvecCourbeTendance } from '../../../src/pages/diagnostic/charts/ChartBarresAvecCourbeTendance';

describe('Test ChartBarresAvecCourbeTendance', () => {
    context('Mode desktop', () => {
        beforeEach(() => {
            cy.viewport(1024, 768);
        });

        it('chart avec courbe de tendance', () => {
            const options: OptionsChartBarresAvecCourbeTendance = {
                series: new SerieMultiple([new Serie([100, 150, 210, 180, 200], 'Valeurs brutes')], [2010, 2011, 2012, 2013, 2014]),
                nomAxeOrdonnees: 'Nom axe ordonnées'
            };
            cy.mount<'c-chart-barres-avec-courbe-tendance'>(html`
                <c-chart-barres-avec-courbe-tendance .options=${options}></c-chart-barres-avec-courbe-tendance>
            `);

            cy.get('c-chart-barres-avec-courbe-tendance').find('figure').find('.apexcharts-canvas').invoke('height').should('be.equal', 400);
            cy.get('c-chart-barres-avec-courbe-tendance').find('figure').find('.apexcharts-canvas').invoke('width').should('be.equal', 500);
        });
    });
});
