import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/charts/ChartTreemapCultures.js';

import { html } from 'lit';

describe('Test ChartTreemapCulture', () => {
    context('Affichage Desktop', () => {
        beforeEach(() => {
            cy.viewport(1024, 768);
        });

        const options = {
            donnees: [
                {
                    code: 'GC1',
                    labelCourt: 'GC 1',
                    labelLong: 'Groupe de cultures 1',
                    couleur: 'rgb(0, 0, 150)',
                    sauHa: 30,
                    detailCultures: [
                        {
                            codeCulture: 'C1',
                            nomCulture: 'Culture 1-1',
                            sauHa: 20,
                            couleur: 'rgb(0, 0, 150)'
                        },
                        {
                            codeCulture: 'C2',
                            nomCulture: 'Culture 1-2',
                            sauHa: 10
                        }
                    ]
                },
                {
                    code: 'GC2',
                    labelCourt: 'GC 2',
                    labelLong: 'Groupe de cultures 2',
                    couleur: 'rgb(100, 0, 0)',
                    sauHa: 30,
                    detailCultures: [
                        {
                            codeCulture: 'C1',
                            nomCulture: 'Culture 2-1',
                            sauHa: 30
                        }
                    ]
                },
                {
                    code: 'GC3',
                    labelCourt: 'GC 3',
                    labelLong: 'Groupe de cultures 3',
                    couleur: 'rgb(0, 100, 0)',
                    sauHa: 10,
                    detailCultures: [
                        {
                            codeCulture: 'C1',
                            nomCulture: 'Culture 3-1',
                            sauHa: 5
                        },
                        {
                            codeCulture: 'C2',
                            nomCulture: 'Culture 3-2',
                            sauHa: 5
                        }
                    ]
                }
            ],
            hauteur: 300,
            largeur: 200,
            activerFillPattern: false,
            activerLabels: true
        };
        it('affichage nominal', () => {
            cy.mount<'c-chart-treemap-cultures'>(html` <c-chart-treemap-cultures .options=${options}></c-chart-treemap-cultures> `);

            cy.get('c-chart-treemap-cultures').find('figure').find('.apexcharts-canvas').invoke('height').should('be.equal', 300);
            cy.get('c-chart-treemap-cultures').find('figure').find('.apexcharts-canvas').invoke('width').should('be.equal', 200);
        });
    });
});
