import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/charts/ChartTreemapCheptels.js';

import { html } from 'lit';

describe('Test ChartTreemapCheptels', () => {
    context('Affichage Desktop', () => {
        beforeEach(() => {
            cy.viewport(1024, 768);
        });

        const options = {
            donnees: [
                {
                    code: 'C1',
                    couleur: 'rgb(0, 0, 150)',
                    ugb: 10,
                    tetes: 200
                },
                {
                    code: 'C2',
                    couleur: 'rgb(100, 0, 0)',
                    ugb: 30,
                    tetes: 30
                },
                {
                    code: 'C3',
                    couleur: 'rgb(0, 100, 0)',
                    ugb: 50,
                    tetes: 100
                }
            ],
            hauteur: 300,
            largeur: 200
        };
        it('affichage nominal', () => {
            cy.mount<'c-chart-treemap-cheptels'>(html` <c-chart-treemap-cheptels .options=${options}></c-chart-treemap-cheptels> `);

            cy.get('c-chart-treemap-cheptels').find('figure').find('.apexcharts-canvas').invoke('height').should('be.equal', 300);
            cy.get('c-chart-treemap-cheptels').find('figure').find('.apexcharts-canvas').invoke('width').should('be.equal', 200);

            cy.get('c-chart-treemap-cheptels')
                .find('figure')
                .find('.apexcharts-treemap-rect')
                .eq(1)
                .invoke('height')
                .should('be.approximately', 134, 2);
            cy.get('c-chart-treemap-cheptels')
                .find('figure')
                .find('.apexcharts-treemap-rect')
                .eq(1)
                .invoke('width')
                .should('be.approximately', 151, 2);
        });
    });
});
