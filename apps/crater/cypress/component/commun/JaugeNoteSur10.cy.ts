import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/commun/JaugeNoteSur10';

import { html } from 'lit';

import { Note, NOTE_VALEUR_WARNING } from '../../../src/modeles/diagnostics';

describe('Test Jauge note sur 10', () => {
    const couleur_note_0 = '#9b0000';
    const couleur_note_5_and_warning = '#f2994a';
    const couleur_note_10 = '#219653';
    const couleur_pas_note = '#000000';

    it('affiche la note 0 et la couleur correspondante ', () => {
        cy.mount<'c-jauge-note-sur-10'>(html`<c-jauge-note-sur-10 .note=${new Note(0)} couleur=${couleur_note_0}></c-jauge-note-sur-10>`);
        cy.get('c-jauge-note-sur-10').find('.texte-note').should('contain', '0');
        cy.get('c-jauge-note-sur-10').find('.texte-10').should('contain', '/10');
        cy.get('.cercle-partiel').should('have.attr', 'stroke', couleur_note_0);
    });
    it('affiche la note 5 et la couleur correspondante ', () => {
        cy.mount<'c-jauge-note-sur-10'>(html`<c-jauge-note-sur-10 .note=${new Note(5)} couleur=${couleur_note_5_and_warning}></c-jauge-note-sur-10>`);
        cy.get('c-jauge-note-sur-10').find('.texte-note').should('contain', '5');
        cy.get('.cercle-partiel').should('have.attr', 'stroke', couleur_note_5_and_warning);
    });
    it('affiche la note 10 et la couleur correspondante ', () => {
        cy.mount<'c-jauge-note-sur-10'>(html`<c-jauge-note-sur-10 .note=${new Note(10)} couleur=${couleur_note_10}></c-jauge-note-sur-10>`);
        cy.get('c-jauge-note-sur-10').find('.texte-note').should('contain', '10');
        cy.get('.cercle-partiel').should('have.attr', 'stroke', couleur_note_10);
    });
    it('affiche pas de note et affiche la couleur correspondante si note vide', () => {
        cy.mount<'c-jauge-note-sur-10'>(html`<c-jauge-note-sur-10 .note=${new Note(null)} couleur=${couleur_pas_note}></c-jauge-note-sur-10>`);
        cy.get('.cercle-partiel').should('have.attr', 'stroke', couleur_pas_note);
        cy.get('c-jauge-note-sur-10').find('.texte-note').should('contain', '');
    });
    it('affiche pas de note et affiche la couleur correspondante si note null', () => {
        cy.mount<'c-jauge-note-sur-10'>(html`<c-jauge-note-sur-10 n .note=${new Note(null)} couleur=${couleur_pas_note}></c-jauge-note-sur-10>`);
        cy.get('.cercle-partiel').should('have.attr', 'stroke', couleur_pas_note);
        cy.get('c-jauge-note-sur-10').find('.texte-note').should('contain', '');
    });
    it('affiche le warning et la couleur correspondante ', () => {
        cy.mount<'c-jauge-note-sur-10'>(
            html`<c-jauge-note-sur-10 .note="${new Note(NOTE_VALEUR_WARNING)}" couleur=${couleur_note_5_and_warning}></c-jauge-note-sur-10>`
        );
        cy.get('c-jauge-note-sur-10').find('#PathWarning').should('exist');
    });
});
