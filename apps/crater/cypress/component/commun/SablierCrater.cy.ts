import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/commun/SablierCrater.js';

import { html } from 'lit';

describe('Test Sablier', () => {
    it('Desktop + Mobile', () => {
        cy.mount<'c-sablier-crater'>(html`<c-sablier-crater></c-sablier-crater>`);
        cy.get('c-sablier-crater').get('#logo').should('be.visible');
    });
});
