import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../src/pages/diagnostic/chapitres/SyntheseMaillon.js';

import { html } from 'lit';

import { Note } from '../../src/modeles/diagnostics';
import type { OptionsSyntheseMaillon } from '../../src/pages/diagnostic/chapitres/SyntheseMaillon';

describe('c-synthese-maillon', () => {
    const options = {
        note: new Note(10),
        messageSynthese: "je suis un composant synthese maillon avec un message super intéressant à donner à qui veut l'entendre !",
        lienMethodologie: '<a href="url">lien</a>'
    } as OptionsSyntheseMaillon;
    const couleur = '#219653';

    it('Affichage mode desktop', () => {
        cy.viewport(1000, 600);
        cy.mount(html` <c-synthese-maillon .options=${options} couleur=${couleur}></c-synthese-maillon> `);
        cy.get('p').should('contain', 'je suis un composant synthese maillon');
        cy.get('c-jauge-note-sur-10').find('.texte-note').contains('10');
    });

    it('Affichage mode mobile', () => {
        cy.viewport(320, 600);
        cy.mount(html` <c-synthese-maillon .options=${options} couleur=${couleur}></c-synthese-maillon> `);
        cy.get('p').should('contain', 'je suis un composant synthese maillon');
        cy.get('c-jauge-note-sur-10').find('.texte-note').contains('10');
    });
});
