import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/accueil/DescriptionCrater';

import { html } from 'lit';

describe("page d'acceuil : composant description crater", () => {
    it(' Test affichage desktop avec illustrations', () => {
        cy.viewport(1200, 800);
        cy.mount<'c-description-crater'>(html`<c-description-crater></c-description-crater>`);
        cy.get('c-description-crater').invoke('width').should('be.gt', 700);
    });

    it('Test affichage mobile avec illustration', () => {
        cy.viewport(320, 500);
        cy.mount<'c-description-crater'>(html`<c-description-crater></c-description-crater>`);
        cy.get('c-description-crater').invoke('width').should('be.lt', 320);
    });
});
