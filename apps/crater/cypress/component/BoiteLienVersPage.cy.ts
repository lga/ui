import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../src/pages/commun/BoiteLienVersPage.js';

import { html } from 'lit';

import illustration from './systeme-alimentaire.png';

describe('Test BoiteLienPage', () => {
    it('Test affichage desktop', () => {
        // given
        cy.viewport(800, 500);
        cy.mount<'c-boite-lien-vers-page'>(
            html` <c-boite-lien-vers-page titre="Titre" libelleLien="En savoir plus sur ça">
                <img slot="illustration" src="${illustration}" />
                <p slot="texte">Description hyper cool de ce que vous pouvez découvrir en cliquant plus bas !</p>
            </c-boite-lien-vers-page>`
        );
        // then
        cy.get('c-boite-lien-vers-page').find('#illustration').should('exist').should('be.visible');
    });

    it('Test affichage mobile', () => {
        // given
        cy.viewport(300, 500);
        cy.mount<'c-boite-lien-vers-page'>(
            html` <c-boite-lien-vers-page titre="Titre" libelleLien="En savoir plus sur ça">
                <img slot="illustration" src="${illustration}" />
                <p slot="texte">Description hyper cool de ce que vous pouvez découvrir en cliquant plus bas !</p>
            </c-boite-lien-vers-page>`
        );
        // then
        cy.get('c-boite-lien-vers-page').find('#illustration').should('exist').should('not.be.visible');
    });

    it('Test affichage desktop sans illustration', () => {
        // given
        cy.viewport(800, 500);
        cy.mount<'c-boite-lien-vers-page'>(
            html` <c-boite-lien-vers-page titre="Titre" libelleLien="En savoir plus sur ça">
                <p slot="texte">Description hyper cool de ce que vous pouvez découvrir en cliquant plus bas !</p>
            </c-boite-lien-vers-page>`
        );
        // then
        cy.get('c-boite-lien-vers-page').find('#illustration').should('have.css', 'width', '0px');
    });
});
