// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('rechercherTerritoire', (idChampRechercheTerritoire, texteSaisi, tailleListeSuggestion) => {
    cy.get(`[data-cy^=${idChampRechercheTerritoire}]`).find('input').should('be.visible').type(texteSaisi, { force: true });
    cy.get('.autocomplete').should('be.visible').children().should('have.length', tailleListeSuggestion).first().click({ force: true });
});

Cypress.Commands.add('verifierAffichagePage', (contenuH1) => {
    cy.get('h1').should('have.length', 1).contains(contenuH1).should('be.visible');
    cy.window().scrollTo('top');
    cy.window().its('scrollY').should('be.equal', 0);
    cy.window().scrollTo('right');
    // Désactivé car pas stable
    // cy.window().its('scrollX').should('be.equal', 0);
});
