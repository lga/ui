import { defineConfig } from 'cypress';

export default defineConfig({
    includeShadowDom: true,
    experimentalWebKitSupport: true,
    video: false,

    component: {
        supportFile: 'cypress/support/component.ts',
        devServer: {
            // Ignorer les erreurs car on ne renseigne pas la prop "framework" de devServer (framework "lit" non disponible, on utilise cypress-lit en alternative)
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            bundler: 'vite'
        },
        indexHtmlFile: 'cypress/support/component-index.html'
    },

    fixturesFolder: '../../modules/specification-api/fixtures',

    e2e: {
        setupNodeEvents(on, config) {
            // implement node event listeners here
        }
    }
});
