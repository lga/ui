import { ModelePage } from '@lga/commun/build/modeles/pages/ModelePage';
import { ModelePageSimple } from '@lga/commun/build/modeles/pages/ModelePageSimple';
import { getTefBaseUrl } from '@lga/commun/src/env/config-baseurl.js';
import { IDS_MAILLONS } from '@lga/indicateurs';

import type { DonneesPageCarte } from '../../pages/carte/PageCarte';
import type { DonneesPageDiagnostic, DonneesPageDiagnosticDomaine, DonneesPageDiagnosticMaillon } from '../../pages/diagnostic/PageDiagnostic';

export const PAGES_PRINCIPALES = {
    accueil: new ModelePageSimple({
        url: '/',
        titreCourt: 'Accueil',
        metaDescription:
            'CRATer, un outil de diagnostic au service de la transition agro-alimentaire. Évaluez en un clic la résilience et la durabilité du système alimentaire de votre territoire'
    }),
    diagnostic: new ModelePageSimple({
        url: '/diagnostic',
        titreCourt: 'Diagnostic'
    }),
    diagnosticTef: new ModelePageSimple({
        url: '/diagnostic-tef',
        titreCourt: 'Diagnostic'
    }),
    carte: new ModelePageSimple({
        url: `${getTefBaseUrl()}/cartes`,
        titreCourt: 'Carte',
        metaDescription:
            "Visualisez les indicateurs sous forme de cartes : utilisation de pesticides, part de surface agricole en bio, rythme d'artificialisation des sols et bien d'autres"
    }),
    aide: new ModelePageSimple({
        url: `${getTefBaseUrl()}`,
        titreCourt: 'Aide',
        metaDescription: 'Comprendre les enjeux de la transition agro-alimentaire et explorer la méthodologie de calcul des indicateurs de CRATer'
    }),
    projet: new ModelePageSimple({
        url: '/projet',
        titreCourt: 'Projet',
        metaDescription: "Une application libre et gratuite developpée par l'association Les Greniers d'Abondance"
    }),
    mentionsLegales: new ModelePageSimple({ url: '/mentions-legales', titreCourt: 'Mentions légales' }),
    api: new ModelePageSimple({ url: '/api-doc', titreCourt: 'API et données' }),
    contact: new ModelePageSimple({ url: '/contact', titreCourt: 'Contact' }),
    demandeAjoutTerritoire: new ModelePageSimple({ url: '/demande-ajout-territoire', titreCourt: "Demande d'ajout de territoire" }),
    erreur: new ModelePageSimple({ url: '/erreur', titreCourt: 'Erreur' })
};

export const HASH_PROJET_LICENCE = 'licences';

const urlCanoniquePagesDiagnostic = (d: DonneesPageDiagnostic) => `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoireActif}`;
export const PAGES_DIAGNOSTIC = {
    synthese: new ModelePage<DonneesPageDiagnostic>({
        url: (d) => `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoirePrincipal}?echelleterritoriale=${d.idEchelleTerritoriale}`,
        titreCourt: 'Synthèse du diagnostic',
        urlCanonique: urlCanoniquePagesDiagnostic,
        titreLong: (d) => `Diagnostic du système alimentaire pour le territoire ${d.nomTerritoireActif}`,
        metaDescription: (d) => `${d.metaDescription}`
    }),
    territoire: new ModelePage<DonneesPageDiagnostic>({
        url: (d) => `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoirePrincipal}/territoire?echelleterritoriale=${d.idEchelleTerritoriale}`,
        titreCourt: `Présentation du territoire`,
        urlCanonique: urlCanoniquePagesDiagnostic,
        titreLong: (d) => `Présentation du territoire ${d.nomTerritoireActif}`
    }),
    maillons: new ModelePage<DonneesPageDiagnosticMaillon>({
        url: (d) =>
            `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoirePrincipal}/maillons/${d.idMaillon}?echelleterritoriale=${
                d.idEchelleTerritoriale
            }`,
        titreCourt: `Diagnostic du maillon`,
        urlCanonique: urlCanoniquePagesDiagnostic,
        titreLong: (d) => `Diagnostic du maillon ${d.nomMaillon} pour le territoire ${d.nomTerritoireActif}`
    }),
    domaines: new ModelePage<DonneesPageDiagnosticDomaine>({
        url: (d) =>
            `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoirePrincipal}/indicateurs/${d.idDomaine}?echelleterritoriale=${
                d.idEchelleTerritoriale
            }`,
        titreCourt: `Indicateurs`,
        urlCanonique: urlCanoniquePagesDiagnostic,
        titreLong: (d) => `Indicateur ${d.nomDomaine} pour le territoire ${d.nomTerritoireActif}`
    }),
    pdf: new ModelePage<DonneesPageDiagnostic>({
        url: (d) => `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoirePrincipal}/pdf`,
        titreCourt: `PDF`,
        urlCanonique: urlCanoniquePagesDiagnostic,
        titreLong: (d) => `Diagnostic du territoire ${d.nomTerritoirePrincipal} (Version imprimable)`
    })
};

export const PAGES_CARTE = {
    indicateurs: new ModelePage<DonneesPageCarte>({
        url: (d) => `${PAGES_PRINCIPALES.carte.getUrl()}/${d.idIndicateur}/${d.idEchelleTerritoriale}?territoire=${d.idTerritoirePrincipal}`,
        titreCourt: 'Carte',
        urlCanonique: (d) => `${PAGES_PRINCIPALES.carte.getUrl()}/${d.idIndicateur}/${d.idEchelleTerritoriale}`,
        titreLong: (d) => `${d.nomIndicateur} - Carte des ${d.libelleEchelleTerritoriale}`,
        metaDescription: (d) => `Carte des ${d.libelleEchelleTerritoriale} pour l'indicateur ${d.nomIndicateur}`
    })
};

export const PAGES_AIDE = {
    comprendreLeSystemeAlimentaire: new ModelePageSimple({
        url: `${PAGES_PRINCIPALES.aide.getUrl()}/comprendre-le-systeme-alimentaire`,
        titreCourt: 'Comprendre le système alimentaire',
        metaDescription: 'Défaillances et vulnérabilités du système alimentaire actuel, et enjeux de la transition agro-alimentaire à mener'
    }),
    methodologie: new ModelePageSimple({
        url: `${PAGES_PRINCIPALES.aide.getUrl()}/methodologie`,
        titreCourt: 'Méthodologie',
        metaDescription: "Méthodologie de calcul de l'ensemble des indicateurs de CRATer et sources de données utilisées"
    }),
    faq: new ModelePageSimple({ url: `${PAGES_PRINCIPALES.aide.getUrl()}/faq`, titreCourt: 'Foire aux questions' }),
    glossaire: new ModelePageSimple({ url: `${PAGES_PRINCIPALES.aide.getUrl()}/glossaire`, titreCourt: 'Glossaire' })
};

export const PAGE_GLOSSAIRE_HASH = {
    agricultureBiologique: 'agriculture-biologique',
    agroecologie: 'agroecologie',
    autonomieAlimentaire: 'autonomie-alimentaire',
    durabilite: 'durabilite',
    epci: 'epci',
    pat: 'pat',
    menace: 'menace',
    otex: 'otex',
    perturbation: 'perturbation',
    precariteAlimentaire: 'precarite-alimentaire',
    resilience: 'resilience',
    resilienceAlimentaire: 'resilienceAlimentaire',
    sau: 'sau',
    sauProductiveVsPeuProductive: 'sau-productive-peu-productive-vs-productive',
    sauPeuProductiveHorsPraires: 'sau-productive-hors-prairies',
    securiteAlimentaire: 'securite-alimentaire',
    souveraineteAlimentaire: 'souverainete-alimentaire',
    systemeAgroIndustriel: 'systeme-agro-industriel',
    systemeAlimentaire: 'systeme-alimentaire',
    ugb: 'ugb',
    zeroArtificialisation: 'zero-artificialisation'
};

export const PAGES_COMPRENDRE_LE_SA = {
    systemeActuel: new ModelePageSimple({
        url: `${PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrl()}/le-systeme-actuel`,
        titreCourt: 'Un système fait pour produire, pas pour nourrir',
        urlCanonique: PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrlCanonique()
    }),
    defaillancesVulnerabilites: new ModelePageSimple({
        url: `${PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrl()}/defaillances-et-vulnerabilites`,
        titreCourt: 'Nuages à l’horizon'
    }),
    transitionAgricoleAlimentaire: new ModelePageSimple({
        url: `${PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrl()}/la-transition-agricole-et-alimentaire`,
        titreCourt: 'Changement de cap !'
    })
};

export const PAGES_METHODOLOGIE = {
    presentationGenerale: new ModelePageSimple({
        url: `${PAGES_AIDE.methodologie.getUrl()}`,
        titreCourt: 'Présentation générale',
        urlCanonique: PAGES_AIDE.methodologie.getUrlCanonique()
    }),
    terresAgricoles: new ModelePageSimple({
        url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.terresAgricoles}`,
        titreCourt: 'Terres agricoles'
    }),
    agriculteursExploitations: new ModelePageSimple({
        url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.agriculteursExploitations}`,
        titreCourt: 'Agriculteurs & Exploitations'
    }),
    intrants: new ModelePageSimple({ url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.intrants}`, titreCourt: 'Intrants' }),
    production: new ModelePageSimple({ url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.production}`, titreCourt: 'Production' }),
    transformationDistribution: new ModelePageSimple({
        url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.transformationDistribution}`,
        titreCourt: 'Transformation & Distribution'
    }),
    consommation: new ModelePageSimple({ url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.consommation}`, titreCourt: 'Consommation' }),
    sourcesDonnees: new ModelePageSimple({ url: `${PAGES_AIDE.methodologie.getUrl()}/sources-donnees`, titreCourt: 'Sources de données' })
};

export const PAGE_METHODOLOGIE_HASH_OTEX = 'otex';
export const PAGE_METHODOLOGIE_HASH_FLUX_AZOTE = 'flux_azote';
export const PAGE_METHODOLOGIE_HASH_ENERGIE = 'indicateurs-energie';
export const PAGE_METHODOLOGIE_HASH_EAU = 'indicateurs-eau';
export const PAGE_METHODOLOGIE_HASH_PESTICIDES = 'indicateurs-pesticides';

export const LIENS_EXTERNES = {
    lienFaireUnDon: 'https://www.helloasso.com/associations/les-greniers-d-abondance/formulaires/2'
};
