import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import './pages/commun/PageSablierCrater.js';

import { EvenementErreur } from '@lga/commun/build/evenements/EvenementErreur.js';
import { EvenementNaviguer } from '@lga/commun/build/evenements/EvenementNaviguer.js';
import { estActifModeBeta, PARAMETRE_URL_ACTIVER_FONCTIONS_BETA } from '@lga/commun/build/outils/mode-beta';
import { creerDonneesInitialesPageSimple, routePageSimple } from '@lga/commun/build/routeur-utils.js';
import type { DonneesPageSimple } from '@lga/commun/src/modeles/pages/donnees-pages.js';
import { EvenementMajDonneesPage } from '@lga/commun/src/modeles/pages/EvenementMajDonneesPage.js';
import { css, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import pagejs, { type Context as ContextPageJS } from 'page';

import {
    PAGES_AIDE,
    PAGES_CARTE,
    PAGES_COMPRENDRE_LE_SA,
    PAGES_DIAGNOSTIC,
    PAGES_METHODOLOGIE,
    PAGES_PRINCIPALES
} from './configuration/pages/declaration-pages.js';
import { ContexteCraterApp } from './ContexteCraterApp';
import {
    creerDonneesInitialesPageCarte,
    creerDonneesInitialesPageDiagnostic,
    creerDonneesInitialesPageDiagnosticDomaine,
    creerDonneesInitialesPageDiagnosticMaillon,
    routePageCarteIndicateur,
    routePageDiagnostic,
    routePageDiagnosticDomaine,
    routePageDiagnosticMaillon
} from './routeur-utils.js';

import('./pages/diagnostic/PageDiagnostic.js');

declare global {
    interface HTMLElementTagNameMap {
        'c-crater-app': CraterApp;
    }
}

@customElement('c-crater-app')
export class CraterApp extends LitElement {
    static styles = css``;

    private contexteApp: ContexteCraterApp = new ContexteCraterApp();

    constructor() {
        super();
        this.configurerRoutes();
    }

    private configurerRoutes() {
        // Redirections pour retro-compatibilité
        pagejs.redirect('/accueil', PAGES_PRINCIPALES.accueil.getUrl()); // Plus de /accueil, la HP est à la racine

        // Déclaration des routes
        pagejs('*', this.logguerRoute);
        pagejs(`${routePageSimple(PAGES_PRINCIPALES.accueil)}`, this.routerVersPageAccueil, this.routerSiErreur, this.majPostChangementPage);
        this.configurerRoutesDiagnostic();
        this.configurerRoutesCarte();
        this.configurerRoutesAide();
        this.configurerRoutesAutresPages();

        pagejs('*', this.routerVersPageErreurNotFound);
        pagejs();
    }

    private configurerRoutesDiagnostic() {
        // Diagnostic
        pagejs(
            `${routePageDiagnostic(PAGES_DIAGNOSTIC.synthese)}`,
            this.routerVersPageDiagnosticSynthese,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageDiagnostic(PAGES_DIAGNOSTIC.territoire)}`,
            this.routerVersPageDiagnosticTerritoire,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(`${routePageDiagnostic(PAGES_DIAGNOSTIC.pdf)}`, this.routerVersPageDiagnosticPdf, this.routerSiErreur, this.majPostChangementPage);
        pagejs(
            `${routePageDiagnosticMaillon(PAGES_DIAGNOSTIC.maillons)}`,
            this.routerVersPageDiagnosticMaillon,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageDiagnosticDomaine(PAGES_DIAGNOSTIC.domaines)}`,
            this.routerVersPageDiagnosticDomaine,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_PRINCIPALES.diagnostic)}`,
            this.routerVersPageDiagnosticAccueil,
            this.routerSiErreur,
            this.majPostChangementPage
        );
    }

    private configurerRoutesCarte() {
        // Carte
        pagejs(
            `${routePageCarteIndicateur(PAGES_CARTE.indicateurs)}`,
            this.routerVersPageCarteIndicateur,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(`${routePageSimple(PAGES_PRINCIPALES.carte)}`, this.routerVersPageCarte, this.routerSiErreur, this.majPostChangementPage);
    }

    private configurerRoutesAide() {
        // Aide
        pagejs(`${routePageSimple(PAGES_PRINCIPALES.aide)}`, this.routerVersPageAideAccueil, this.routerSiErreur, this.majPostChangementPage);
        // Aide - Comprendre le SA
        pagejs(
            `${routePageSimple(PAGES_AIDE.comprendreLeSystemeAlimentaire)}`,
            this.routerVersPageLeSystemeActuel,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_COMPRENDRE_LE_SA.systemeActuel)}`,
            this.routerVersPageLeSystemeActuel,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites)}`,
            this.routerVersPageDefaillancesVulnerabilites,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire)}`,
            this.routerVersPageTransitionAgricoleAlimentaire,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        // Aide - Methodologie
        pagejs(
            `${routePageSimple(PAGES_AIDE.methodologie)}`,
            this.routerVersPageMethodologiePresentation,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_METHODOLOGIE.presentationGenerale)}`,
            this.routerVersPageMethodologiePresentation,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_METHODOLOGIE.terresAgricoles)}`,
            this.routerVersPageMethodologieTerresAgricoles,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_METHODOLOGIE.agriculteursExploitations)}`,
            this.routerVersPageMethodologieAgriculteursExploitations,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_METHODOLOGIE.intrants)}`,
            this.routerVersPageMethodologieIntrants,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_METHODOLOGIE.production)}`,
            this.routerVersPageMethodologieProduction,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_METHODOLOGIE.transformationDistribution)}`,
            this.routerVersPageMethodologieTransformationDistribution,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_METHODOLOGIE.consommation)}`,
            this.routerVersPageMethodologieConsommation,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        pagejs(
            `${routePageSimple(PAGES_METHODOLOGIE.sourcesDonnees)}`,
            this.routerVersPageMethodologieSourcesDonnees,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        // Aide - FAQ-Glossaire
        pagejs(`${routePageSimple(PAGES_AIDE.faq)}`, this.routerVersPageFAQ, this.routerSiErreur, this.majPostChangementPage);
        pagejs(`${routePageSimple(PAGES_AIDE.glossaire)}`, this.routerVersPageGlossaire, this.routerSiErreur, this.majPostChangementPage);
    }

    private configurerRoutesAutresPages() {
        // Projet
        pagejs(`${routePageSimple(PAGES_PRINCIPALES.projet)}`, this.routerVersPageProjet, this.routerSiErreur, this.majPostChangementPage);
        // Mentions légales
        pagejs(
            `${routePageSimple(PAGES_PRINCIPALES.mentionsLegales)}`,
            this.routerVersPageMentionsLegales,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        // Contact
        pagejs(`${routePageSimple(PAGES_PRINCIPALES.contact)}`, this.routerVersPageContact, this.routerSiErreur, this.majPostChangementPage);
        pagejs(
            `${routePageSimple(PAGES_PRINCIPALES.demandeAjoutTerritoire)}`,
            this.routerVersPageDemandeAjoutTerritoire,
            this.routerSiErreur,
            this.majPostChangementPage
        );
        // API
        pagejs(`${routePageSimple(PAGES_PRINCIPALES.api)}`, this.routerVersPageApi, this.routerSiErreur, this.majPostChangementPage);
    }

    private logguerRoute = (contextPageJS: ContextPageJS, next: () => unknown) => {
        console.log('Routage vers ', contextPageJS.canonicalPath);
        next();
    };

    private routerSiErreur = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerSiErreur();
        next();
    };

    private majPostChangementPage = () => {
        this.contexteApp.majPostChangementPage();
        // request update explicite ici, car asservir le render aux maj d'url captées par pagejs
        this.requestUpdate();
    };

    private routerVersPageAccueil = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageAccueil(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageDiagnosticAccueil = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageDiagnosticAccueil();
        next();
    };

    private routerVersPageDiagnosticSynthese = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageDiagnosticSynthese(creerDonneesInitialesPageDiagnostic(contextPageJS, 'synthese'));
        next();
    };

    private routerVersPageDiagnosticTerritoire = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageDiagnosticTerritoire(creerDonneesInitialesPageDiagnostic(contextPageJS, 'territoire'));
        next();
    };

    private routerVersPageDiagnosticMaillon = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageDiagnosticMaillon(creerDonneesInitialesPageDiagnosticMaillon(contextPageJS));
        next();
    };

    private routerVersPageDiagnosticDomaine = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageDiagnosticDomaine(creerDonneesInitialesPageDiagnosticDomaine(contextPageJS));
        next();
    };

    private routerVersPageDiagnosticPdf = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageDiagnosticPdf(creerDonneesInitialesPageDiagnostic(contextPageJS, 'pdf'));
        next();
    };

    private routerVersPageCarte = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageCarte();
        next();
    };

    private routerVersPageCarteIndicateur = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageCarteIndicateur(creerDonneesInitialesPageCarte(contextPageJS));
        next();
    };

    private routerVersPageAideAccueil = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageAideAccueil();
        next();
    };

    private routerVersPageLeSystemeActuel = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageLeSystemeActuel();
        next();
    };

    private routerVersPageDefaillancesVulnerabilites = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageDefaillancesVulnerabilites();
        next();
    };

    private routerVersPageTransitionAgricoleAlimentaire = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageTransitionAgricoleAlimentaire();
        next();
    };

    private routerVersPageMethodologiePresentation = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageMethodologiePresentation(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };
    private routerVersPageMethodologieTerresAgricoles = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageMethodologieTerresAgricoles(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageMethodologieAgriculteursExploitations = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageMethodologieAgriculteursExploitations(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageMethodologieIntrants = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageMethodologieIntrants(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageMethodologieProduction = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageMethodologieProduction(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageMethodologieTransformationDistribution = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageMethodologieTransformationDistribution(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageMethodologieConsommation = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageMethodologieConsommation(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageMethodologieSourcesDonnees = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageMethodologieSourcesDonnees(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageFAQ = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageFAQ(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageGlossaire = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageGlossaire(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageProjet = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageProjet(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageMentionsLegales = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageMentionsLegales(creerDonneesInitialesPageSimple(contextPageJS));
        next();
    };

    private routerVersPageApi = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageApi();
        next();
    };

    private routerVersPageContact = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageContact();
        next();
    };

    private routerVersPageDemandeAjoutTerritoire = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageDemandeAjoutTerritoire();
        next();
    };

    private routerVersPageErreurNotFound = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.contexteApp.routerVersPageErreurNotFound(contextPageJS.canonicalPath);
        next();
    };

    render() {
        return this.contexteApp.templatePageCourante;
    }

    firstUpdated() {
        document.addEventListener(EvenementNaviguer.ID, (event: Event) => {
            this.actionNaviguer((event as EvenementNaviguer).detail.url, (event as EvenementNaviguer).detail.cible);
        });
        document.addEventListener(EvenementErreur.ID, (event: Event) => {
            this.actionErreur(event);
        });
        document.addEventListener(EvenementMajDonneesPage.ID, (event: Event) => {
            this.actionMajDonneesPage(event);
        });
    }

    private actionErreur(event: Event) {
        const evenementErreur = event as EvenementErreur;
        this.contexteApp.mettreEnErreur(evenementErreur.detail.codeErreur, evenementErreur.detail.messageErreur);
        pagejs(window.location.pathname + window.location.search + window.location.hash);
    }

    private actionMajDonneesPage(event: Event) {
        const evenementMajDonneesPage = event as EvenementMajDonneesPage<Partial<DonneesPageSimple>>;
        this.contexteApp.majDonneesPageCourante(evenementMajDonneesPage.detail.donneesPage);
    }

    actionNaviguer(url: string, cible = '_self') {
        console.log(`Naviger vers url=${url}, cible=${cible}`);
        if (url.startsWith('#')) {
            // lien intern vers hash
            const urlCible = new URL(location.href);
            urlCible.hash = url;
            pagejs(`${urlCible.pathname}${urlCible.search}${urlCible.hash}`);
        } else if (!url.startsWith('http') && cible === '_self') {
            // lien interne, url sans http://hostname
            const urlCible = new URL(`${location.protocol}\\${location.host}${url}`);
            // TODO : modifier ce fonctionnement => stocker l'état du mode beta dans un champ de CraterApp, et ne plus avoir besoin de le faire suivre dans l'url
            if (estActifModeBeta() && !urlCible.searchParams.has(PARAMETRE_URL_ACTIVER_FONCTIONS_BETA)) {
                urlCible.searchParams.set(PARAMETRE_URL_ACTIVER_FONCTIONS_BETA, '');
            }
            console.log(`Naviger vers url=${urlCible.href}, cible=${cible}`);
            pagejs(`${urlCible.pathname}${urlCible.search}${urlCible.hash}`);
        } else {
            // lien externe
            window.open(url, cible);
        }
    }
}
