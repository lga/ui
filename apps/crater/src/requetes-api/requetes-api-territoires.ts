import { fetchRetry } from '@lga/base';
import type { TerritoireApi } from '@lga/specification-api';
import { CategorieTerritoire, HierarchieTerritoires, Territoire } from '@lga/territoires';

export function chargerHierarchieTerritoires(apiBaseUrl: string, idTerritoire: string): Promise<HierarchieTerritoires> {
    return fetchRetry(apiBaseUrl + '/crater/api/territoires/' + idTerritoire)
        .then((response) => response.json())
        .then((donneesTerritoire: TerritoireApi) => {
            return traduireJsonEnHierarchieTerritoires(donneesTerritoire);
        });
}

export function traduireJsonEnTerritoire(donneesTerritoireJson: TerritoireApi): Territoire {
    return new Territoire(donneesTerritoireJson.id, donneesTerritoireJson.nom, CategorieTerritoire.creer(donneesTerritoireJson.categorie));
}

function traduireJsonEnHierarchieTerritoires(territoireApi: TerritoireApi): HierarchieTerritoires {
    return new HierarchieTerritoires(
        territoireApi.id,
        territoireApi.nom,
        territoireApi.categorie,
        territoireApi.sousCategorie ?? null,
        'epci' in territoireApi ? territoireApi.epci?.id : undefined,
        'epci' in territoireApi ? territoireApi.epci?.nom : undefined,
        'departement' in territoireApi ? territoireApi.departement?.id : undefined,
        'departement' in territoireApi ? territoireApi.departement?.nom : undefined,
        'region' in territoireApi ? territoireApi.region?.id : undefined,
        'region' in territoireApi ? territoireApi.region?.nom : undefined,
        'pays' in territoireApi ? territoireApi.pays?.id : undefined,
        'pays' in territoireApi ? territoireApi.pays?.nom : undefined
    );
}
