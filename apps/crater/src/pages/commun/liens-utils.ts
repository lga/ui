import type {
    DefinitionIndicateur,
    DefinitionRapportEtude,
    DefinitionSourceDonnee,
    DefinitionSourceDonneeRetraitee,
    IndicateurCarte,
    SourceIndicateur
} from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html } from 'lit';

import {
    construireUrlPageDiagnosticDomaine,
    construireUrlPageDiagnosticMaillon,
    construireUrlPageMethodologie,
    construireUrlPageMethodologieSourceDonnee,
    construireUrlPageMethodologieSourceDonneeRetraitee
} from '../../configuration/pages/pages-utils.js';

export function construireLienVersMethodologieSourceDonnee(sourceDonnee: DefinitionSourceDonnee, annees?: string | number | undefined) {
    const anneesChoisis = annees ? annees : sourceDonnee.annees;
    return html`<c-lien href="${construireUrlPageMethodologieSourceDonnee(sourceDonnee)}">${sourceDonnee.nom}</c-lien> (${sourceDonnee.fournisseur
            .nom}${anneesChoisis ? ', ' + anneesChoisis : ''})`;
}

export function construireLienVersMethodologieSourceDonneeRetraitee(sourceDonneeRetraitee: DefinitionSourceDonneeRetraitee) {
    return html`<c-lien href="${construireUrlPageMethodologieSourceDonneeRetraitee(sourceDonneeRetraitee)}">${sourceDonneeRetraitee.nom}</c-lien>
        (${sourceDonneeRetraitee.source.fournisseur.nom}, ${sourceDonneeRetraitee.annees}, retraité)`;
}

export function construireLienVersMethodologieIndicateur(indicateur: DefinitionIndicateur) {
    const indicateurDetaille = SA.getIndicateurDetaille(indicateur.id);
    // prettier-ignore
    return html`<c-lien href="${construireUrlPageMethodologie(indicateurDetaille?.idMaillon, indicateurDetaille?.idDomaine)}">${indicateur.libelle}</c-lien>`;
}

export function construireLienVersMethodologieSources(sources: SourceIndicateur[]) {
    const sourcesHtml = sources
        ?.map((s) => {
            let legende;
            if ((s.source as DefinitionSourceDonnee).fournisseur !== undefined) {
                const source = s.source as DefinitionSourceDonnee;
                legende = construireLienVersMethodologieSourceDonnee(source, s.anneesMobilisees);
            } else if ((s.source as DefinitionSourceDonneeRetraitee).source !== undefined) {
                const source = s.source as DefinitionSourceDonneeRetraitee;
                legende = construireLienVersMethodologieSourceDonneeRetraitee(source);
            } else if ((s.source as DefinitionIndicateur).description !== undefined) {
                const source = s.source as DefinitionIndicateur;
                legende = construireLienVersMethodologieIndicateur(source);
            } else if ((s.source as DefinitionRapportEtude).auteur !== undefined) {
                const rapport = s.source as DefinitionRapportEtude;
                legende = html`${rapport.url ? html`<c-lien href="${rapport.url}">${rapport.nom}</c-lien>` : rapport.nom}${', ' +
                rapport.auteur +
                ' (' +
                rapport.annee +
                ')'}`;
            }
            return html`${legende}`;
        })
        .reduce((r, c, index) => html`${r}${index > 0 ? ' ; ' : ''}${c}`, html``);
    return html`Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir des données ${sourcesHtml}`;
}

export function construireLienVersSourcesIndicateur(indicateur: DefinitionIndicateur) {
    return construireLienVersMethodologieSources(indicateur.sources ?? []);
}

export function construireLienVersFournisseurSourceDonnee(sourceDonnee: DefinitionSourceDonnee) {
    return sourceDonnee.url
        ? html`<c-lien href="${sourceDonnee.url}">${sourceDonnee.fournisseur.nom}</c-lien>`
        : html`${sourceDonnee.fournisseur.nom}`;
}

export function construireLienVersPageDomaineDepuisPageCarte(indicateurCarte: IndicateurCarte, idTerritoire: string) {
    return SA.getDomaineParIdDomaineOuIdIndicateurDetaille(indicateurCarte.id)?.type === 'indicateur'
        ? construireUrlPageDiagnosticDomaine(indicateurCarte.idDomaine, idTerritoire)
        : construireUrlPageDiagnosticMaillon(indicateurCarte.idMaillon, idTerritoire);
}

export function remplacerBaliseLien(texte?: string) {
    return texte?.replace('<a', '<c-lien').replace('</a', '</c-lien');
}
