import '@lga/design-system/build/composants/FilAriane.js';
import './TemplatePage.js';
import '@lga/design-system/build/composants/MenuAccordeon.js';

import type { LienFilAriane } from '@lga/design-system/build/composants/FilAriane';
import type { MenuAccordeonItem } from '@lga/design-system/build/composants/MenuAccordeon';
import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { STYLES_CRATER } from '../pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-template-page-avec-sommaire': TemplatePageAvecSommaire;
    }
}

@customElement('c-template-page-avec-sommaire')
export class TemplatePageAvecSommaire extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                display: grid;
                grid-template-columns: 300px 1fr;
            }

            section {
                background-color: var(--couleur-fond-page);
                margin: 0 auto 0;
                max-width: var(--largeur-maximum-contenu);
                padding: calc(2 * var(--dsem));
            }

            nav {
                float: left;
                position: sticky;
                top: var(--hauteur-totale-entete);
                width: 100%;
                height: fit-content;
                min-height: calc(100vh - var(--hauteur-totale-entete));
                padding-top: 2rem;
                background-color: var(--couleur-blanc);
                border-right: 1px solid var(--couleur-neutre-clair);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                main {
                    display: block;
                }
                nav {
                    display: none;
                }
            }
        `
    ];

    @property({ attribute: false })
    liensFilAriane: LienFilAriane[] = [];

    @property({ attribute: false })
    itemsMenuSommaire: MenuAccordeonItem[] = [];

    @property()
    idItemActifMenuSommaire = '';

    @property()
    idItemActifMenuPrincipal = '';

    @property()
    idElementCibleScroll = '';

    render() {
        return html`
            <c-template-page idItemActifMenuPrincipal="${this.idItemActifMenuPrincipal}" idElementCibleScroll=${this.idElementCibleScroll}>
                <main slot="contenu">
                    <nav>${this.renderMenuSommaire()}</nav>
                    <section>
                        <c-fil-ariane .liens=${this.liensFilAriane}> </c-fil-ariane>
                        <slot name="contenu"></slot>
                    </section>
                </main>
                ${this.renderMenuSommaire()}
            </c-template-page>
        `;
    }

    private renderMenuSommaire() {
        return html`<c-menu-accordeon
            slot="contenu-sommaire"
            .idItemSelectionne=${this.idItemActifMenuSommaire}
            .items=${this.itemsMenuSommaire}
        ></c-menu-accordeon>`;
    }
}
