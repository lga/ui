import './TemplatePage.js';
import '../BarreMenuTerritoires.js';

import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import type { OptionsBarreMenuTerritoires } from '../BarreMenuTerritoires';
import { STYLES_CRATER } from '../pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-template-page-avec-menu-territoire': TemplatePageAvecMenuTerritoire;
    }
}

@customElement('c-template-page-avec-menu-territoire')
export class TemplatePageAvecMenuTerritoire extends LitElement {
    static styles = [
        STYLES_CRATER,
        css`
            c-template-page {
                --hauteur-c-barre-menu-territoires: 76px;
                --hauteur-totale-entete: calc(var(--hauteur-c-entete) + var(--hauteur-c-barre-menu-territoires));
            }

            c-barre-menu-territoires {
                position: fixed;
                top: var(--hauteur-c-entete);
                z-index: 1;
                height: var(--hauteur-c-barre-menu-territoires);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                c-template-page {
                    --hauteur-totale-entete: var(--hauteur-c-entete);
                }

                c-barre-menu-territoires {
                    display: none;
                }
            }
        `
    ];

    @property()
    idItemActifMenuPrincipal = '';

    @property()
    idElementCibleScroll = '';

    @property({ attribute: false })
    optionsBarreMenuTerritoires?: OptionsBarreMenuTerritoires;

    render() {
        return html`
            <c-template-page idItemActifMenuPrincipal=${this.idItemActifMenuPrincipal} idElementCibleScroll=${this.idElementCibleScroll}>
                <main slot="contenu">
                    <c-barre-menu-territoires .options=${this.optionsBarreMenuTerritoires}></c-barre-menu-territoires>
                    <slot name="contenu"></slot>
                </main>
                <slot name="contenu-sommaire" slot="contenu-sommaire"></slot>
            </c-template-page>
        `;
    }
}
