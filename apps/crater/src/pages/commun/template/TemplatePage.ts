import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { querySelectorDeep } from 'query-selector-shadow-dom';

import { STYLES_CRATER } from '../pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-template-page': TemplatePage;
    }
}

@customElement('c-template-page')
export class TemplatePage extends LitElement {
    static styles = [
        STYLES_CRATER,
        css`
            :host {
                display: block;
                --hauteur-c-entete: 0px;
                --hauteur-totale-entete: var(--hauteur-c-entete);
                --hauteur-menu-pied-page: 65px;
                background-color: var(--couleur-fond);
            }

            c-entete {
                position: fixed;
                top: 0;
                width: 100%;
                height: var(--hauteur-c-entete);
                z-index: 2;
            }

            c-menu-pied-page {
                height: var(--hauteur-menu-pied-page);
                position: fixed;
                bottom: 0;
                display: none;
                z-index: 3;
            }

            main {
                min-height: calc(100vh - var(--hauteur-totale-entete));
                position: relative;
                z-index: 0;
                margin: var(--hauteur-totale-entete) auto auto auto;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                c-menu-pied-page {
                    display: block;
                }
            }
        `
    ];

    @property()
    idItemActifMenuPrincipal = '';

    @property()
    idElementCibleScroll = '';

    render() {
        return html`
            <main>
                <slot name="contenu"></slot>
            </main>
        `;
    }

    updated() {
        setTimeout(() => {
            this.scrollerVersElement('c-template-page > main', this.idElementCibleScroll);
        }, 250);
    }

    private recupererMarginTop(element: Element) {
        const styleCssMain = window.getComputedStyle(element);
        return parseInt(styleCssMain.getPropertyValue('margin-top'));
    }

    private scrollerVersElement(selecteurContainerScroll: string, idElementCibleScroll: string) {
        if (idElementCibleScroll === '') return;

        const elementContainer = querySelectorDeep(`${selecteurContainerScroll}`);
        const elementCibleScroll = querySelectorDeep(`#${idElementCibleScroll}`);

        if (elementCibleScroll === null || elementContainer === null) return;

        const valeurDecalageHautDePage = this.recupererMarginTop(elementContainer);

        const valeurScrollAvecDecalage =
            elementCibleScroll.getBoundingClientRect().top - this.recupererMarginTop(elementCibleScroll) + window.scrollY - valeurDecalageHautDePage;

        window.scrollTo({
            top: valeurScrollAvecDecalage,
            behavior: 'smooth'
        });
    }
}
