import '@lga/design-system/build/composants/Bouton.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { ICONES_SVG } from '@lga/indicateurs';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-encart-lien-vers-page': EncartLienVersPage;
    }
}

export const POSITION_ILLUSTRATION = {
    droite: 'droite',
    gauche: 'gauche'
};

@customElement('c-encart-lien-vers-page')
export class EncartLienVersPage extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100%;
            }

            article {
                display: flex;
                justify-content: space-between;
            }

            article.droite {
                flex-direction: row;
            }

            article.gauche {
                flex-direction: row-reverse;
            }

            section {
                display: flex;
                flex-direction: column;
            }

            header {
                display: flex;
                flex-direction: column;
                gap: calc(2 * var(--dsem));
                color: var(--couleur-primaire);
            }

            #numero {
                display: flex;
                align-items: center;
                gap: var(--dsem);
            }

            #numero > svg {
                fill: var(--couleur-primaire);
            }

            figure {
                display: flex;
                justify-content: center;
                align-items: center;
                margin: 0 calc(4 * var(--dsem));
            }

            figure ::slotted(*) {
                max-width: 300px;
            }

            figure.gauche {
                transform: rotateY(180deg);
            }

            c-bouton {
                margin-top: var(--dsem);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                article.droite,
                article.gauche {
                    flex-direction: column-reverse;
                    align-items: center;
                    gap: calc(2 * var(--dsem));
                }

                section {
                    align-items: center;
                    text-align: center;
                }

                header {
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                }
                figure {
                    margin: 0;
                }
                figure ::slotted(*) {
                    max-width: 250px;
                }
            }
        `
    ];

    @property()
    numero?: string;

    @property()
    titre?: string;

    @property()
    libelleBouton = 'En savoir plus';

    @property()
    libelleCourtBouton?: string;

    @property()
    href?: string;

    @property()
    positionIllustration: string = POSITION_ILLUSTRATION.droite;

    render() {
        return html`
            <article class=${this.positionIllustration}>
                <section>
                    <header>
                        ${this.numero
                            ? html`<span id="numero" class="titre-large"> ${this.numero} ${unsafeSVG(ICONES_SVG.traitNumeroEncartAccueil)} </span>`
                            : ''}
                        <span class="titre-moyen">${this.titre}</span>
                    </header>
                    <p class="texte-moyen"><slot name="texte"></slot></p>
                    <c-bouton
                        type=${Bouton.TYPE.encadre}
                        themeCouleur=${Bouton.THEME_COULEUR.accent}
                        libelle=${this.libelleBouton}
                        libelleCourt=${ifDefined(this.libelleCourtBouton)}
                        href=${ifDefined(this.href)}
                    ></c-bouton>
                </section>
                <figure class=${this.positionIllustration === 'gauche' ? 'gauche' : 'droite'}>
                    <slot name="illustration"></slot>
                </figure>
            </article>
        `;
    }
}
