import '../commun/template/TemplatePageAccueil.js';
import '../commun/BoiteLienVersPage.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

import { PAGES_AIDE, PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages.js';
import { STYLES_CRATER } from '../commun/pages-styles';
import { ILLUSTRATION_PAGE_AIDE } from './aide-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-aide': PageAide;
    }
}

@customElement('c-page-aide')
export class PageAide extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 10);
            }

            h1 {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 2);
                color: var(--couleur-blanc);
            }

            h1 > svg {
                fill: var(--couleur-primaire);
            }

            section {
                display: flex;
                flex-direction: column;
                gap: 5vh;
                max-width: var(--largeur-maximum-contenu);
            }

            div[slot='illustration'] svg {
                fill: var(--couleur-primaire-clair);
            }
        `
    ];

    render() {
        return html`
      <c-template-page-accueil idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}">
        <main slot="contenu">
          <h1>Centre d'aide</h1>
          <section>
            <c-boite-lien-vers-page 
              id="${PAGES_AIDE.comprendreLeSystemeAlimentaire.getId()}" 
              titre="${PAGES_AIDE.comprendreLeSystemeAlimentaire.getTitreCourt()}"
              libelleLien="En savoir plus" 
              href="${PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrl()}"
              encadrement
            >
              <p slot="texte">Le système alimentaire, c’est l’ensemble des activités qui permettent de produire, transformer, transporter, consommer
                les aliments qui nourrissent quotidiennement la population.</br></br>Quelles sont les défaillances et vulnérabilités du système actuel
                ?</br>Comment le transformer ?</p>
              <div slot="illustration">${ILLUSTRATION_PAGE_AIDE.comprendreLeSystemeAlimentaire}</div>
            </c-boite-lien-vers-page>
            <c-boite-lien-vers-page 
              id="${PAGES_AIDE.methodologie.getId()}" 
              titre="${PAGES_AIDE.methodologie.getTitreCourt()}" 
              libelleLien="Voir la méthodologie" 
              href="${PAGES_AIDE.methodologie.getUrl()}"
              encadrement
            >
              
              <p slot="texte">La méthodologie décrit les grandes principes retenus dans la construction des diagnostics.</br>
                Elle présente également l’ensemble des sources de données utilisées, ainsi que les hypothèses et règles de calcul détaillées pour
                chaque indicateur.</p>
              <div slot="illustration">${ILLUSTRATION_PAGE_AIDE.methodologie}</div>
            </c-boite-lien-vers-page>
            <c-boite-lien-vers-page 
              id="${PAGES_AIDE.faq.getId()}" 
              titre="${PAGES_AIDE.faq.getTitreCourt()}" 
              libelleLien="Voir la FAQ" 
              href="${PAGES_AIDE.faq.getUrl()}"
              encadrement
            >
              <p slot="texte">La FAQ contient des réponses aux questions que vous pouvez vous poser. Si certaines ne trouvent pas réponse,
                contactez-nous !</p>
              <div slot="illustration">${ILLUSTRATION_PAGE_AIDE.faq}</div>
            </c-boite-lien-vers-page>
            <c-boite-lien-vers-page 
              id="${PAGES_AIDE.glossaire.getId()}" 
              titre="${PAGES_AIDE.glossaire.getTitreCourt()}" 
              libelleLien="Voir le glossaire" 
              href="${PAGES_AIDE.glossaire.getUrl()}"
              encadrement
            >
              <p slot="texte">Qu’est ce qu’un EPCI ? Un maillon du système alimentaire ? Et un intrant agricole ?
                </br>
                </br>
                Pour tout comprendre sur les termes utilisés dans l’application, c’est ici !</p>
              <div slot="illustration">${ILLUSTRATION_PAGE_AIDE.glossaire}</div>
            </c-boite-lien-vers-page>
          </section>
        </main>
      </c-template-page-accueil>
    `;
    }
}
