import '../../commun/template/TemplatePageAvecSommaire.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';
import './BoiteEnSavoirPlusSurLeSA.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

import { PAGES_COMPRENDRE_LE_SA, PAGES_PRINCIPALES } from '../../../configuration/pages/declaration-pages.js';
import imageCamions from '../../../ressources/images/camions.jpg';
import imageVigne from '../../../ressources/images/vigne_brulee.jpg';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesComprendreLeSA, ITEMS_MENU_COMPRENDRE_LE_SA } from './comprendre-le-systeme-alimentaire-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-aide-defaillances-vulnerabilites': PageAideDefaillancesVulnerabilites;
    }
}

@customElement('c-page-aide-defaillances-vulnerabilites')
export class PageAideDefaillancesVulnerabilites extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            #vigne {
                width: 350px;
            }
        `
    ];

    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_COMPRENDRE_LE_SA}"
                .liensFilAriane="${construireLiensFilArianePagesComprendreLeSA(
                    'Défaillances et vulnérabilités',
                    `${PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getUrl()}`
                )}"
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    private renderCoeurPage() {
        return html`
            <section class="texte-moyen">
                <h1>${PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getTitreCourt()}</h1>
                <h2>La fin d’une époque</h2>
                <p>
                    D'ici une trentaine d'années, le niveau moyen d'humidité des sols correspondra aux records de sécheresse actuels dans plusieurs
                    régions françaises. L’épuisement des énergies fossiles va directement réduire la disponibilité en engrais, en pesticides et en
                    carburants tout en déstabilisant le système de transport. Il va nous falloir gérer à la fois une diminution tendancielle des
                    rendements et la multiplication des situations de crise (événement climatique extrême, choc énergétique, crise économique...).
                </p>
                <figure>
                    <img src="${imageVigne}" id="vigne" width="400" height="400" alt="Vigne brulée" />
                    <figcaption class="texte-petit">
                        Au cours de l’été 2019, des milliers d’hectares de vignes ont été brûlés par la chaleur dans l’Hérault et dans le Gard 20. Ces
                        vagues de chaleur extrêmes vont augmenter en intensité et en durée. <br />Crédits : © Chai d’Emilien.
                    </figcaption>
                </figure>
                <h2>Pas de plan B !</h2>
                <p>
                    Face à la mise sous tension croissante du modèle agro-industriel, nos vulnérabilités sont criantes. La population agricole ne
                    cesse de décliner, la qualité des sols se dégrade, des dizaines de milliers d’hectares fertiles disparaissent sous le béton chaque
                    année, notre système alimentaire est de plus en plus dépendant de transports longue distance, de chaînes de production
                    mondialisées et de technologies complexes. Du côté des pouvoirs publics, la sécurité alimentaire n’est plus un sujet depuis
                    longtemps et rien n’est prévu en cas de dysfonctionnement de la machine agro-industrielle.
                    <br /><br />
                    Sans changement en profondeur, c’est bien notre sécurité alimentaire qui est en danger.
                </p>
                <figure>
                    <img src="${imageCamions}" width="800" height="533" alt="Camions" />
                    <figcaption class="texte-petit">
                        Le système alimentaire des pays industrialisés est entièrement dépendant du transport routier, et donc, du pétrole. La
                        quasi-totalité des marchandises agricoles produites dans un département français est exportée tandis que la quasi-totalité des
                        aliments qui y sont consommés est importée. <br />
                        Crédits : Pixabay.
                    </figcaption>
                </figure>
                <c-encart-pages-precedente-suivante
                    libellePagePrecedente=${PAGES_COMPRENDRE_LE_SA.systemeActuel.getTitreCourt()}
                    hrefPagePrecedente=${PAGES_COMPRENDRE_LE_SA.systemeActuel.getUrl()}
                    libellePageSuivante=${PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire.getTitreCourt()}
                    hrefPageSuivante=${PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire.getUrl()}
                ></c-encart-pages-precedente-suivante>
                <c-boite-en-savoir-plus-sur-le-sa></c-boite-en-savoir-plus-sur-le-sa>
            </section>
        `;
    }
}
