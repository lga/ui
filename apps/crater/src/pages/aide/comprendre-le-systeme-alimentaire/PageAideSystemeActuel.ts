import '../../commun/template/TemplatePageAvecSommaire.js';
import './BoiteEnSavoirPlusSurLeSA.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

import { PAGES_COMPRENDRE_LE_SA, PAGES_PRINCIPALES } from '../../../configuration/pages/declaration-pages.js';
import imageAlmeria from '../../../ressources/images/almeria.jpg';
import imageSA from '../../../ressources/images/systeme-alimentaire.png';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesComprendreLeSA, ITEMS_MENU_COMPRENDRE_LE_SA } from './comprendre-le-systeme-alimentaire-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-aide-systeme-actuel': PageAideSystemeActuel;
    }
}

@customElement('c-page-aide-systeme-actuel')
export class PageAideSystemeActuel extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER];

    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_COMPRENDRE_LE_SA.systemeActuel.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_COMPRENDRE_LE_SA}"
                .liensFilAriane="${construireLiensFilArianePagesComprendreLeSA('Le système actuel', PAGES_COMPRENDRE_LE_SA.systemeActuel.getUrl())}"
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    private renderCoeurPage() {
        return html`
            <section>
                <h1>${PAGES_COMPRENDRE_LE_SA.systemeActuel.getTitreCourt()}</h1>
                <h2>Manger c’est toute une affaire !</h2>
                <p>
                    L’industrialisation a bouleversé la façon dont nous produisons notre nourriture. Du champ à l’assiette, une multitude de machines,
                    d’usines, de procédés industriels, de camions, de travailleurs et d’institutions interviennent et font tourner le système
                    alimentaire. Derrière les produits les plus banals des grandes surfaces, se cache une organisation extrêmement complexe se
                    déployant à l’échelle mondiale. C’est ce modèle agro-industriel qui aujourd’hui produit l’essentiel de la nourriture d’un pays
                    comme la France.
                </p>
                <figure>
                    <img src="${imageSA}" width="852" height="583" alt="Le système alimentaire" />
                    <figcaption class="texte-petit">
                        Représentation simplifiée du système alimentaire. <br />Certains éléments comme l’environnement et les ressources, les
                        marchés, la recherche et la formation sont absents.
                    </figcaption>
                </figure>
                <h2>Qui se paie au prix fort</h2>
                <p>
                    La généralisation de ce modèle a permis en quelques décennies de réaliser des progrès indéniables en matière de productivité
                    agricole. Jamais l’humanité n’a ainsi produit autant de nourriture. Pourtant, près de la moitié d’entre elle souffre de
                    malnutrition. En cherchant à produire toujours plus à des coûts toujours plus faibles, le modèle agro-industriel se paye en
                    réalité au prix fort. Les prix bas en supermarché ont pour revers une dégradation de la santé publique, la précarité de millions
                    de travailleurs, et des dommages environnementaux d’ampleur planétaire.
                </p>
                <figure>
                    <img src="${imageAlmeria}" width="600" height="400" alt="La péninsule d'Alméria" />
                    <figcaption class="texte-petit">
                        Dans le Sud de l’Espagne, la péninsule d’Alméria est recouverte de serres destinées à la culture intensive de fruits et
                        légumes sur une superficie équivalente à trois fois Paris (31 000 hectares). Dans cette « mer de plastique », la main-d’œuvre
                        se compose principalement de travailleurs saisonniers immigrés d’Afrique du Nord, logés dans des bidonvilles. Ils endurent
                        jusqu’à douze heures par jour des conditions de travail accablantes (températures atteignant 50°C, pauses quasi-inexistantes,
                        exposition aux pesticides et aux particules cancérigènes) et ne bénéficient pas de protection sociale ni du salaire minimum
                        légal. <br />Crédits : © Yann Arthus Bertrand.
                    </figcaption>
                </figure>
                <c-encart-pages-precedente-suivante
                    libellePageSuivante=${PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getTitreCourt()}
                    hrefPageSuivante=${PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getUrl()}
                ></c-encart-pages-precedente-suivante>
                <c-boite-en-savoir-plus-sur-le-sa></c-boite-en-savoir-plus-sur-le-sa>
            </section>
        `;
    }
}
