import '@lga/design-system/build/composants/Lien.js';
import '../../commun/piedPage/AllerPlusLoin.js';

import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';

import imageCouvertureRapport1 from '../../../ressources/images/couverture-rapport1.png';
import imageCouvertureRapport2 from '../../../ressources/images/couverture-rapport2.png';
import { STYLES_CRATER } from '../../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-boite-en-savoir-plus-sur-le-sa': BoiteEnSavoirPlusSurLeSA;
    }
}

@customElement('c-boite-en-savoir-plus-sur-le-sa')
export class BoiteEnSavoirPlusSurLeSA extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            #liens-rapports {
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
            }

            .lien-rapport {
                display: flex;
                flex-grow: 1;
                justify-content: start;
            }

            .lien-rapport img {
                height: 200px;
                width: auto;
                margin-right: 1rem;
            }

            .lien-rapport-texte {
                display: grid;
                align-content: start;
                max-width: 350px;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                .lien-rapport img {
                    display: none;
                }

                .lien-rapport:last-of-type {
                    margin-top: calc(2 * var(--dsem));
                }
            }
        `
    ];

    render() {
        return html`
            <c-aller-plus-loin>
                <div id="liens-rapports" class="texte-moyen">
                    <div class="lien-rapport">
                        <img src="${imageCouvertureRapport2}" width="207" height="300" loading="lazy" />
                        <div class="lien-rapport-texte">
                            <strong>Les enjeux du système alimentaire national</strong>
                            <c-lien href="https://resiliencealimentaire.org/#guide2/"> Lire le rapport</c-lien>
                            <c-lien href="https://resiliencealimentaire.org/qui-veille-au-grain-en-images/"> Regarder l'infographie</c-lien>
                        </div>
                    </div>
                    <div class="lien-rapport">
                        <img src="${imageCouvertureRapport1}" width="208" height="300" loading="lazy" />
                        <div class="lien-rapport-texte">
                            <strong>Leviers d'action à l'échelle des territoires</strong>
                            <c-lien href="https://resiliencealimentaire.org/page-telechargement-guide/"> Lire le rapport</c-lien>
                            <c-lien href="https://resiliencealimentaire.org/infographie-resilience-alimentaire"> Regarder l'infographie</c-lien>
                        </div>
                    </div>
                </div>
            </c-aller-plus-loin>
        `;
    }
}
