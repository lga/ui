import type { LienFilAriane } from '@lga/design-system/build/composants/FilAriane';
import type { MenuAccordeonItem, MenuAccordeonSousItem } from '@lga/design-system/build/composants/MenuAccordeon';

import { PAGES_AIDE, PAGES_PRINCIPALES } from '../../../configuration/pages/declaration-pages';
import { PAGE_FAQ_CONTENU } from './page-faq-contenu';

const construireSousItems = () => {
    const sousItems: MenuAccordeonSousItem[] = [];

    PAGE_FAQ_CONTENU.forEach((section) => {
        sousItems.push({
            id: section.id,
            libelle: section.libelle,
            href: PAGES_AIDE.faq.getUrl() + '#' + section.id
        });
    });

    return sousItems;
};

export const ITEMS_MENU_FAQ: MenuAccordeonItem[] = [
    {
        id: 'faq',
        libelle: PAGES_AIDE.faq.getTitreCourt(),
        sousItems: construireSousItems()
    }
];

export function construireLiensFilArianePagesFAQ(libellePage: string, hrefPage: string): LienFilAriane[] {
    return [
        {
            libelle: PAGES_PRINCIPALES.aide.getTitreCourt(),
            href: PAGES_PRINCIPALES.aide.getUrl()
        },
        {
            libelle: PAGES_AIDE.faq.getTitreCourt(),
            href: PAGES_AIDE.faq.getUrl()
        },
        {
            libelle: libellePage,
            href: hrefPage
        }
    ];
}
