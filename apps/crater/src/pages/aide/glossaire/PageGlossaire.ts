import '../../commun/template/TemplatePageAvecSommaire.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_AIDE, PAGES_PRINCIPALES } from '../../../configuration/pages/declaration-pages.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { PAGE_GLOSSAIRE_CONTENU } from './page-glossaire-contenu.js';
import { construireLiensFilArianePagesGlossaire, ITEMS_MENU_GLOSSAIRE } from './page-glossaire-utils';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-glossaire': PageGlossaire;
    }
}

@customElement('c-page-glossaire')
export class PageGlossaire extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER];

    @property()
    idElementCibleScroll = '';

    render() {
        const sectionGlossaire =
            PAGE_GLOSSAIRE_CONTENU.find((section) => {
                return section.id === this.idElementCibleScroll;
            }) ?? PAGE_GLOSSAIRE_CONTENU[0];

        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${sectionGlossaire.id}"
                .itemsMenuSommaire="${ITEMS_MENU_GLOSSAIRE}"
                .liensFilAriane="${construireLiensFilArianePagesGlossaire(sectionGlossaire)}"
                idElementCibleScroll="${this.idElementCibleScroll}"
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    renderCoeurPage() {
        return html`
            <main>
                <section>
                    <h1 class="titre-large">${PAGES_AIDE.glossaire.getTitreCourt()}</h1>
                    ${PAGE_GLOSSAIRE_CONTENU.map(
                        (section) => html`
                            <h2 id="${section.id}" class="titre-petit">${section.libelle}</h2>
                            <p>${section.definition}</p>
                        `
                    )}
                </section>
            </main>
        `;
    }
}
