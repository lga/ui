import type { LienFilAriane } from '@lga/design-system/build/composants/FilAriane';
import type { MenuAccordeonItem, MenuAccordeonSousItem } from '@lga/design-system/build/composants/MenuAccordeon';

import { PAGES_AIDE, PAGES_PRINCIPALES } from '../../../configuration/pages/declaration-pages';
import { type Glose, PAGE_GLOSSAIRE_CONTENU } from './page-glossaire-contenu';

const construireSousItems = () => {
    const sousItems: MenuAccordeonSousItem[] = [];

    PAGE_GLOSSAIRE_CONTENU.forEach((i) => {
        sousItems.push({
            id: i.id,
            libelle: i.libelle,
            href: PAGES_AIDE.glossaire.getUrl({ idElementCibleScroll: i.id })
        });
    });

    return sousItems;
};

export const ITEMS_MENU_GLOSSAIRE: MenuAccordeonItem[] = [
    {
        id: 'glossaire',
        libelle: PAGES_AIDE.glossaire.getTitreCourt(),
        sousItems: construireSousItems()
    }
];

export function construireLiensFilArianePagesGlossaire(glose: Glose): LienFilAriane[] {
    return [
        {
            libelle: PAGES_PRINCIPALES.aide.getTitreCourt(),
            href: PAGES_PRINCIPALES.aide.getUrl()
        },
        {
            libelle: PAGES_AIDE.glossaire.getTitreCourt(),
            href: PAGES_AIDE.glossaire.getUrl()
        },
        {
            libelle: glose.libelle,
            href: PAGES_AIDE.glossaire.getUrl({ idElementCibleScroll: glose.id })
        }
    ];
}
