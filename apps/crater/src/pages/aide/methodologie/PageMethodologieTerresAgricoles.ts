import '../../commun/template/TemplatePageAvecSommaire.js';
import '@lga/design-system/build/composants/Lien.js';

import { lien } from '@lga/design-system/build/composants/Lien.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { IDS_DOMAINES, SOURCES_DONNEES, SOURCES_DONNEES_RETRAITEES } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_METHODOLOGIE, PAGES_PRINCIPALES } from '../../../configuration/pages/declaration-pages.js';
import { construireUrlPageMethodologieSourceDonneeRetraitee } from '../../../configuration/pages/pages-utils.js';
import { construireLienVersMethodologieSourceDonnee } from '../../commun/liens-utils.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesMethodologie, ITEMS_MENU_METHODOLOGIE, STYLES_METHODOLOGIE } from './methodologie-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-methodologie-terres-agricoles': PageMethodologieTerresAgricoles;
    }
}

@customElement('c-page-methodologie-terres-agricoles')
export class PageMethodologieTerresAgricoles extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_METHODOLOGIE];

    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_METHODOLOGIE.terresAgricoles.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_METHODOLOGIE}"
                .liensFilAriane="${construireLiensFilArianePagesMethodologie(
                    PAGES_METHODOLOGIE.terresAgricoles.getTitreCourt(),
                    PAGES_METHODOLOGIE.terresAgricoles.getUrl()
                )}"
                idElementCibleScroll=${this.idElementCibleScroll}
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    @property()
    idElementCibleScroll = '';

    private renderCoeurPage() {
        return html`
            <section class="texte-moyen">
                <h1>Règles de gestion pour le maillon Terres agricoles</h1>

                <h2 id="${IDS_DOMAINES.sauParHabitant}">Surface agricole utile productive par habitant</h2>
                <h3>Données d’entrées</h3>
                <p>
                    CRATER utilise les données du ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.rpg)} pour obtenir la surface agricole
                    utile productive (voir la
                    ${lien(
                        construireUrlPageMethodologieSourceDonneeRetraitee(SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater),
                        'nomenclature des surfaces agricoles'
                    )})
                    et les données de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.population_totale)}.
                </p>
                <h3>Méthode de calcul</h3>
                <p>L’indicateur est calculé selon la formule suivante :</p>
                <blockquote>
                    <p>sau_par_habitant [en m²/habitant] = surface_agricole_utile_productive * 10 000 / population_totale</p>
                </blockquote>
                <p>avec :</p>
                <ul>
                    <li>surface_agricole_utile_productive : surface agricole utile productive en 2017 [ha]</li>
                    <li>population_totale : population municipale en 2017 [habitants]</li>
                </ul>
                <h3>Limites</h3>
                <p>
                    Comme expliqué dans la
                    ${lien(
                        construireUrlPageMethodologieSourceDonneeRetraitee(SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater),
                        'nomenclature des surfaces agricoles'
                    )},
                    la surface agricole utile productive est sous-estimée ce qui a pour conséquence de sous-estimer la valeur de l’indicateur.
                </p>
                <h3>Message détaillé</h3>
                <p>
                    Un message détaillé est donné en fonction de la valeur de cet indicateur selon des seuils présentés dans la page 141 figure 33 du
                    guide
                    <c-lien href="https://resiliencealimentaire.org/page-telechargement-guide/">Vers la résilience alimentaire</c-lien>
                    basée sur la figure 2 page 31 de
                    <strong
                        >Solagro (2019), Le revers de notre assiette. Changer d’alimentation pour préserver notre santé et notre environnement</strong
                    >.
                </p>
                <blockquote>
                    <ul>
                        <li>
                            valeur &lt; 100 m²/hab: “La surface agricole utile productive par habitant est nulle ou quasiment inexistante, ce qui
                            représente un risque important par manque de ressources pour produire de la nourriture localement. Il est important de
                            mettre en place les politiques adéquates pour libérer des terres agricoles et créer des liens avec les territoires
                            voisins. Il convient également de s'assurer que les surfaces agricoles sont suffisantes pour répondre aux besoins des
                            populations au niveau départemental ou régional.&quot;
                        </li>
                        <li>
                            valeur &lt; 1700 m²/hab : &quot;La surface agricole utile productive par habitant est beaucoup trop faible, même pour un
                            régime alimentaire très végétal.&quot;
                        </li>
                        <li>
                            valeur &lt; 2500 m²/hab : &quot;La surface agricole utile productive par habitant est trop faible pour le régime
                            alimentaire actuel, mais suffisante pour un régime alimentaire alimentaire très végétal.&quot;
                        </li>
                        <li>
                            valeur &lt; 4000 m²/hab : &quot;La surface agricole utile productive par habitant est trop faible pour le régime
                            alimentaire actuel mais suffisante pour un régime alimentaire moins carné type méditerranéen&quot;
                        </li>
                        <li>
                            valeur &gt;= 4000 m²/hab : &quot;La surface agricole utile productive par habitant est suffisante pour le régime
                            alimentaire actuel.”
                        </li>
                    </ul>
                </blockquote>

                <h2 id="${IDS_DOMAINES.politiqueAmenagement}">Politique d'aménagement</h2>
                <h3>Données d’entrées</h3>
                <p>
                    Cet indicateur utilise les données d'${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.artificialisation_sols)}
                    concernant l’artificialisation des sols à des fins d’habitat, d’activité ou mixte, ainsi que la variation du nombre de ménages et
                    d’emplois sur la période 2011-2016, pour chaque commune. Il permet d'appréhender le respect de l'objectif national de
                    <c-lien href="https://www.ecologie.gouv.fr/artificialisation-des-sols">Zéro Artificialisation Nette</c-lien>. Il utilise également
                    la ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.geographie_des_communes)} pour le calcul des superficies des
                    communes.
                </p>
                <h3>Méthode de calcul</h3>
                <p>L’indicateur, qualitatif, est calculé selon la formule suivante :</p>
                <blockquote>
                    <ul>
                        <li>si surface_artificialisée = 0 : &quot;L'objectif Zéro Artificialisation Nette a été atteint entre 2013 et 2018.&quot;</li>
                        <li>
                            si surface_artificialisée &gt; 0 et évolution_menages_emplois &gt; 0 : &quot;L'objectif Zéro Artificialisation Nette n'a
                            pas été atteint entre 2013 et 2018 puisque [surface_artificialisée] ha ont été artificialisés soit
                            [ratio_surface_artificialisée_superficie] % de la superficie totale du territoire.&quot;
                        </li>
                        <li>
                            si surface_artificialisée &gt; 0 et évolution_menages_emplois &lt;= 0 : &quot;L'objectif Zéro Artificialisation Nette n'a
                            pas été atteint entre 2013 et 2018 puisque [surface_artificialisée] ha ont été artificialisés soit
                            [ratio_surface_artificialisée_superficie] % de la superficie totale du territoire, alors que le territoire a perdu
                            [évolution_menages_emplois] ménages et emplois.&quot;
                        </li>
                    </ul>
                </blockquote>
                <p>avec :</p>
                <ul>
                    <li>
                        surface_artificialisée : superficie totale d’espaces naturels, agricoles et forestiers artificialisés entre 2013 et 2018 [ha]
                    </li>
                    <li>superficie : superficie totale du territoire [ha]</li>
                    <li>ratio_surface_artificialisée_superficie : surface_artificialisée / superficie * 100 [%]</li>
                    <li>évolution_menages_emplois = (menages_final + emplois_final) - (menages_initial + emplois_initial)</li>
                    <li>menages_initial : nombre de ménages en 2011</li>
                    <li>menages_final : nombre de ménages en 2016</li>
                    <li>emplois_initial : nombre d’emplois en 2011</li>
                    <li>emplois_final : nombre d’emplois en 2016</li>
                </ul>
                <h3>Limites</h3>
                <p>
                    Comme expliqué dans les limites de la description des données
                    d'${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.artificialisation_sols)}, une incertitude subsiste sur la valeur du
                    flux de surfaces artificialisées du fait de sa méthodologie de calcul.
                </p>

                <h2 id="${IDS_DOMAINES.rythmeArtificialisation}">Rythme d’artificialisation</h2>
                <h3>Données d’entrées</h3>
                <p>
                    Cet indicateur utilise les données d'${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.artificialisation_sols)}
                    concernant l’artificialisation des sols à des fins d’habitat, d’activité ou mixte, ainsi que les données du
                    ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.rpg)} pour obtenir la surface agricole utile productive.
                </p>
                <h3>Méthode de calcul</h3>
                <p>L’indicateur est calculé selon la formule suivante :</p>
                <blockquote>
                    <p>rythme_artificialisation [en % sur une période de 5 ans] = surface_artificialisée / surface_agricole_utile_productive * 100</p>
                </blockquote>
                <p>avec :</p>
                <ul>
                    <li>
                        surface_artificialisée : superficie totale d’espaces naturels, agricoles et forestiers artificialisés entre 2013 et 2018 [ha]
                    </li>
                    <li>surface_agricole_utile_productive : surface agricole utile productive en 2017 [ha]</li>
                </ul>
                <p>
                    Cet indicateur est calculé sur base de la surface agricole utile productive et non de la surface totale d’espaces naturels,
                    agricoles et forestiers pour ne pas tenir compte du capital d’espace naturels et forestiers dans le calcul de l’indicateur car ce
                    dernier doit absolument être conservé en l’état et ne pas légitimer une éventuelle artificialisation (il est en effet courant de
                    voir ces espaces reclassés en zone agricole dans les documents d'urbanisme pour combler l'artificialisation de surfaces
                    agricoles).
                </p>
                <h3>Limites</h3>
                <ul>
                    <li>
                        La surface agricole utile productive est sous-estimée ce qui a pour conséquence de sur-estimer la valeur de l’indicateur ;
                    </li>
                    <li>une incertitude subsiste sur la valeur du flux de surfaces artificialisées du fait de sa méthodologie de calcul ;</li>
                    <li>la surface agricole utile productive utilisée ne correspond pas à la date initiale du flux artificialisé considéré.</li>
                </ul>
                <h3>Message détaillé</h3>
                <p>Un message détaillé est donné en fonction de la valeur de cet indicateur selon les seuils suivants :</p>
                <blockquote>
                    <ul>
                        <li>
                            valeur = 0% : &quot;Aucune terre n'a été artificialisée entre 2013 et 2018, ce qui permet de préserver les terres
                            agricoles existantes. Il convient néanmoins de s'assurer que ces surfaces sont suffisantes pour nourrir la population et
                            qu’elles ne sont pas menacées d’urbanisation.&quot;
                        </li>
                        <li>
                            valeur entre 0% et 80% * [valeur France] : &quot;Le rythme d'artificialisation entre 2013 et 2018 est inférieur à la
                            moyenne française. Ce taux doit néanmoins être examiné au regard des surfaces agricoles disponibles et des besoins en
                            surfaces agricoles au niveau local, départemental ou régional.&quot;
                        </li>
                        <li>
                            valeur entre 80% [valeur France] et 120% [valeur France] : &quot;Le rythme d'artificialisation entre 2013 et 2018
                            correspond à la moyenne française. La politique de préservation des terres agricoles doit être renforcée, et ce d’autant
                            plus si la surface agricole utile productive par habitant est insuffisante localement ou aux échelles de territoires
                            supérieures.&quot;
                        </li>
                        <li>
                            valeur entre 120% * [valeur France] et 1% : &quot;Le rythme d'artificialisation entre 2013 et 2018 est supérieur à la
                            moyenne française. La politique de préservation des terres agricoles doit être très renforcée, et ce d’autant plus si la
                            surface agricole utile productive par habitant est insuffisante localement ou aux échelles de territoires
                            supérieures.&quot;
                        </li>
                        <li>
                            valeur &gt; 1% : &quot;Plus de 1% des terres ont été artificialisées entre 2013 et 2018, ce qui va à l’encontre du
                            principe de préservation des terres agricoles et représente un risque de perte de ressources pour le territoire ou pour
                            d’autres territoires dépendants.&quot;
                        </li>
                    </ul>
                </blockquote>

                <h2 id="${IDS_DOMAINES.logementsVacants}">Part de logements vacants</h2>
                <h3>Données d’entrées</h3>
                <p>
                    CRATER utilise les données du ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.logements_vacants)} et du
                    ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.logements_totaux)}.
                </p>
                <p>Un logement est vacant s’il est inoccupé et :</p>
                <ul>
                    <li>proposé à la vente, à la location,</li>
                    <li>déjà attribué à un acheteur ou un locataire et en attente d'occupation,</li>
                    <li>en attente de règlement de succession,</li>
                    <li>conservé par un employeur pour un usage futur au profit d'un de ses employés,</li>
                    <li>sans affectation précise par le propriétaire (logement vétuste, etc.).</li>
                </ul>
                <h3>Méthode de calcul</h3>
                <p>L’indicateur est calculé selon la formule suivante :</p>
                <blockquote>
                    <p>part_logements_vacants [%] = nombre_logements_vacants / nombre_logements * 100</p>
                </blockquote>
                <p>avec :</p>
                <ul>
                    <li>nombre_logements : nombre total de logements en 2018 (resp. 2013)</li>
                    <li>nombre_logements_vacants : nombre de logements vacants en 2018 (resp. 2013)</li>
                </ul>

                <h2 id="${IDS_DOMAINES.terresAgricoles}">Évaluation globale du maillon Terres agricoles</h2>

                <h3>Note</h3>
                <p>La note est calculée sur la base de deux notes :</p>
                <ul>
                    <li>
                        une note N1 sur 10 calculée sur base de l’indicateur de <strong>surface agricole utile productive par habitant</strong>,
                        bornée entre 0 et 10 et obtenue par interpolation entre :
                        <blockquote>
                            <ul>
                                <li>la note 0 obtenue lorsque l’indicateur vaut 0</li>
                                <li>la note 5 obtenue lorsque l’indicateur vaut 1700 m²/habitant</li>
                                <li>la note 10 obtenue lorsque l’indicateur vaut 4000 m²/habitant</li>
                            </ul>
                        </blockquote>
                        <p>Voir la section <i>message de synthèse</i> pour des explications sur les seuils utilisés.</p>
                    </li>
                    <li>
                        <p>
                            une note N2 sur 10 calculée sur base de l’indicateur
                            <strong>rythme d’artificialisation de la surface agricole utile productive</strong>, bornée entre 0 et 10 et obtenue par
                            interpolation entre :
                        </p>
                        <blockquote>
                            <ul>
                                <li>la note 10 obtenue lorsque l’indicateur vaut 0%</li>
                                <li>la note 0 obtenue lorsque l’indicateur vaut la valeur correspondante à la moyenne française</li>
                            </ul>
                        </blockquote>
                    </li>
                    <li>
                        <p>ces deux notes sont combinées de la façon suivante :</p>
                        <blockquote>N = (N1 + N2) / 2</blockquote>
                    </li>
                </ul>
                <p>De cette manière la note globale prend en compte :</p>
                <ul>
                    <li>pour moitié, l’état des lieux du territoire via la surface agricole disponible par habitant</li>
                    <li>
                        et pour autre moitié, la dynamique d’évolution via la capacité à contenir l’artificialisation, dans un objectif ZAN (Zéro
                        Artificialisation Nette)
                    </li>
                </ul>

                <h3>Message de synthèse</h3>
                <p>
                    Le message donné est généré selon l’indicateur I <strong>surface agricole utile productive par habitant</strong>
                    et la valeur V
                    <strong>surface artificialisée</strong> (voir calcul indicateur rythme d’artificialisation) selon les règles suivantes :
                </p>
                <blockquote>
                    <ul>
                        <li>message = bloc 1 + bloc 2</li>
                        <li>
                            pour le bloc 1 :
                            <ul>
                                <li>si I &lt; 1700 : “la surface agricole par habitant est trop faible”</li>
                                <li>
                                    si 1700 &lt;= I &lt; 2500 : la surface agricole par habitant peut convenir pour un régime alimentaire très
                                    végétal”
                                </li>
                                <li>
                                    si 2500 &lt;= I &lt; 4000 : la surface agricole par habitant peut convenir pour un régime alimentaire moins carné”
                                </li>
                                <li>si I &gt;= 4000 : “la surface agricole par habitant est suffisante pour le régime alimentaire actuel”</li>
                            </ul>
                        </li>
                        <li>
                            pour le bloc 2 :
                            <ul>
                                <li>si V = 0 : “l’objectif ZAN a été atteint entre 2013 et 2018”</li>
                                <li>sinon : “l’objectif ZAN n’a pas été atteint entre 2013 et 2018”</li>
                            </ul>
                        </li>
                    </ul>
                </blockquote>
                <p>
                    La règle du bloc 1 se base sur les seuils de la figure 33 p141 du guide
                    <c-lien href="https://resiliencealimentaire.org/page-telechargement-guide/">Vers la résilience alimentaire</c-lien>
                    basée sur la figure 2 page 31 de
                    <strong
                        >Solagro (2019), Le revers de notre assiette. Changer d’alimentation pour préserver notre santé et notre environnement</strong
                    >.
                </p>
            </section>
        `;
    }
}
