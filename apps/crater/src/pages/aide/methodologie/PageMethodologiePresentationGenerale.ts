import '../../commun/template/TemplatePageAvecSommaire.js';
import '@lga/design-system/build/composants/Lien.js';

import { lien } from '@lga/design-system/build/composants/Lien.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { SOURCES_DONNEES, SOURCES_DONNEES_RETRAITEES } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import {
    PAGE_METHODOLOGIE_HASH_FLUX_AZOTE,
    PAGE_METHODOLOGIE_HASH_OTEX,
    PAGES_METHODOLOGIE,
    PAGES_PRINCIPALES
} from '../../../configuration/pages/declaration-pages.js';
import {
    construireUrlPageMethodologieSourceDonnee,
    construireUrlPageMethodologieSourceDonneeRetraitee
} from '../../../configuration/pages/pages-utils.js';
import SCHEMA_MODELE_MACDYNFS from '../../../ressources/images/schema-modele-macdynfs.png';
import { construireLienVersMethodologieSourceDonnee } from '../../commun/liens-utils.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesMethodologie, ITEMS_MENU_METHODOLOGIE, STYLES_METHODOLOGIE } from './methodologie-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-methodologie-presentation-generale': PageMethodologiePresentationGenerale;
    }
}

@customElement('c-page-methodologie-presentation-generale')
export class PageMethodologiePresentationGenerale extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_METHODOLOGIE];

    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_METHODOLOGIE.presentationGenerale.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_METHODOLOGIE}"
                .liensFilAriane="${construireLiensFilArianePagesMethodologie(
                    PAGES_METHODOLOGIE.presentationGenerale.getTitreCourt(),
                    PAGES_METHODOLOGIE.presentationGenerale.getUrl()
                )}"
                idElementCibleScroll=${this.idElementCibleScroll}
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    @property()
    idElementCibleScroll = '';

    private renderCoeurPage() {
        return html`
            <section class="texte-moyen">
                <h1>Présentation générale</h1>

                <h2>Fonctionnement synthétique</h2>
                <p>
                    L’application CRATer est un outil d’aide au diagnostic de la résilience alimentaire pour les territoires de France métropolitaine.
                </p>
                <p>L’application permet de rechercher une commune, et effectue alors un diagnostic :</p>
                <ul>
                    <li>sur différentes échelles géographiques englobant cette commune (par exemple communauté de communes, département…) ;</li>
                    <li>sur les différents maillons du système alimentaire.</li>
                </ul>
                <p>
                    La synthèse présente le résultat du diagnostic sous forme d’un diagramme de type “radar” et de messages clés associés, pour les 6
                    maillons suivants du système alimentaire :
                </p>
                <ul>
                    <li>Terres agricoles</li>
                    <li>Agriculteurs & Exploitations</li>
                    <li>Intrants</li>
                    <li>Production</li>
                    <li>Transformation & Distribution</li>
                    <li>Consommation</li>
                </ul>
                <p>
                    CRATer fait une évaluation du niveau de résilience sur chacun de ses axes, en construisant une note sur 10. Cette note, obtenue
                    par consolidation de différents indicateurs, permet soit de situer le territoire par rapport à la France entière soit par rapport
                    à un objectif donné selon des règles des calculs détaillées ci-dessous. Aucune note globale n'est calculée, car la résilience d'un
                    système alimentaire ne peut se résumer en une dimension mais doit au contraire être approchée de façon transversale à travers les
                    différents maillons qui composent le système. Chaque note doit être interprétée de façon prudente au regard des hypothèses prises
                    et de la fiabilité des données utilisées sur le territoire considéré.
                </p>
                <p>La synthèse permet ensuite d’accéder aux chapitres du rapport CRATer.</p>
                <p>Il y a un chapitre pour chaque composante du système alimentaire, et chaque chapitre présente de manière détaillée :</p>
                <ul>
                    <li>un message de synthèse</li>
                    <li>un texte expliquant les enjeux associés à ce maillon ;</li>
                    <li>un état des lieux basé sur des indicateurs résumés ;</li>
                    <li>des leviers d'action et ressources externes ;</li>
                    <li>les indicateurs détaillés.</li>
                </ul>
                <h2>Périmètre territorial des calculs</h2>
                <p>Les indicateurs sont donnés pour les échelles géographiques suivantes :</p>
                <ul>
                    <li>commune ;</li>
                    <li>
                        regroupement de communes :
                        <ul>
                            <li>EPCI (Établissement public de coopération intercommunale) ;</li>
                            <li>bassin de vie INSEE 2012 ;</li>
                            <li>parc national ;</li>
                            <li>parc naturel régional ;</li>
                            <li>projet alimentaire territorial ;</li>
                            <li>schéma de cohérence territorial ;</li>
                            <li>autre regroupement sur demande utilisateur ;</li>
                        </ul>
                    </li>
                    <li>département ;</li>
                    <li>région ;</li>
                    <li>France métropolitaine entière.</li>
                </ul>
                <p>
                    Le choix de ne pas offrir de résultats pour les territoires hors France métropolitaine provient du fait que certaines données
                    comme l'${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.hvn)} ou les données sur les
                    ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.consommation_sau)} ne sont pas disponibles pour ces territoires là.
                </p>
                <p>
                    Certaines données ne sont pas disponibles sur toutes les échelles géographiques ou pour tous les indicateurs. Par exemple, les
                    surfaces agricoles utiles en bio ne sont pas rendues publiques sur certaines communes pour des raisons de confidentialité. De
                    même, les problèmes liés à la qualité des données, ou à la cohérence des différentes sources, peuvent conduire à l’absence de
                    données. Dans ces différentes situations CRATer indique autant que possible l’absence de données (par exemple dans les notes et
                    messages de synthèse), et le cas échéant ne présente pas d’information, en particulier dans les histogrammes (quand les données ne
                    sont pas disponibles, alors l’échelle territoriale correspondante n’est pas représentée).
                </p>

                <h3>Cas des mouvements de communes</h3>
                <p>
                    CRATer utilise le référentiel des communes de l'INSEE de l'année 2022. En complément, il prend en compte les mouvements de
                    communes survenus entre l'année 2003 et 2022 pour savoir si une commune existante en 2022 a été affectée par un mouvement de type
                    fusion ou scission.
                </p>
                <p>
                    Cette information est utilisée pour invalider certaines données relatives à la commune en fonction des mouvements : pour une
                    commune qui a subi un mouvement l'année n, les sources de données fournies pour des années inférieures à n ne sont pas prises en
                    compte.
                </p>
                <p>
                    Par exemple en 2019, les communes de Chateau-d'Olonne et Olonne-sur-Mer ont fusionné avec la commune des Sables d'Olonne, ce qui a
                    les conséquences suivantes dans l'application :
                </p>
                <ul>
                    <li>Il n'est pas possible d'afficher un diagnostic pour Chateau d'Olonne ou Olonne-sur-Mer (communes qui n'existent plus)</li>
                    <li>
                        Dans le diagnostic de la commune des Sables d'Olonne, l'indicateur HVN n'est pas disponible au niveau de la commune, car il a
                        été calculé en 2017 c'est à dire avant la fusion en 2019. On considère que son périmètre de calcul n'est plus valable dans le
                        contexte d'utilisation de CRATer
                    </li>
                    <li>
                        Par contre l'indicateur part de SAU Bio est disponible, car la donnée a été calculé en 2019, sur le périmètre actuel de la
                        commune des Sables d'Olonne (post fusion).
                    </li>
                </ul>
                <p>Ce principe est identique pour les cas de scission de communes : les données antérieures à une scission sont invalidées.</p>

                <p>
                    Ce fonctionnement a pour conséquence que les données calculées à l'échelle d'un territoire par agrégation des données des communes
                    composant ce territoire sont réalisés avec les données des seules communes n'ayant pas subi de mouvement.
                </p>

                <h3>Relations entre échelles territoriales</h3>
                <p>
                    CRATer utilise les relations entre territoires pour proposer un diagnostic sur des échelles territoires englobantes à partir d'un
                    territoire donné.
                </p>
                <p>
                    Ainsi à partir du diagnostic d'une commune, il est possible d'accéder au diagnostic de son EPCI, de son département, de sa région,
                    et du pays.
                </p>
                <p>
                    Ces mêmes relations sont utilisées dans certains cas pour consolider les données, en particulier pour consolider les données
                    disponibles au niveau communal sur les autres niveaux. Du fait de l'absence de données pour certaines communes (communes ayant
                    subi un mouvement – voir plus haut –, données indisponibles ou sous secret statistique), les résultats à une échelle territoriale
                    supérieure peuvent être légèrement érronés – sauf lorsque les données sont disponibles directement à l'échelle territoriale
                    voulue.
                </p>
                <p>
                    Le cas de l'EPCI est particulier, car un EPCI peut appartenir à plusieurs départements ou régions. Les règles sont les suivantes :
                </p>
                <ul>
                    <li>Si l'EPCI est inclus dans un seul département, c'est ce département et sa région qui sont associés à l'EPCI</li>
                    <li>
                        Si l'EPCI est à cheval sur plusieurs départements (ce qui concerne environ 7% des EPCIs), CRATer identifie un "département
                        principal d'appartenance" en choisissant le département dans lequel l'EPCI possède le plus de communes. Ce département
                        principal d'appartenance permet également de déterminer la région principale d'appartenance (cas ou l'EPCI est en plus à
                        cheval sur plusieurs régions).
                    </li>
                    <li>
                        Cette règle est une pure convention au sein de l'application CRATer. Elle intervient principalement au niveau de l'interface
                        pour proposer un département et une région au-dessus de l'EPCI quelque soit sa situation
                    </li>
                </ul>

                <h2>Les maillons du système alimentaire</h2>
                <p>
                    Chaque maillon du système alimentaire est représenté au niveau de l’écran de synthèse par un axe sur le radar et un message clé.
                    Il est ensuite détaillé dans un chapitre complet du rapport, à l’aide de textes explicatifs, d’indicateurs et graphiques plus
                    détaillés, et d’informations sur les leviers disponibles. Les règles de gestion détaillées pour le calcul des indicateurs
                    associées sont décrites dans la suite de ce document. A noter que les notes sont parfois calculées par comparaison avec la moyenne
                    nationale, et parfois de façon plus objective quand cela est possible et pertinent. Ainsi, les notes minimale (0), maximale (10)
                    et moyenne (5) ne signifient pas la même chose selon les maillons.
                </p>
                <h2 id="${SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater.id}">Surfaces agricoles</h2>
                <p>
                    Dans CRATer, une nomenclature des surfaces agricoles est employée pour faire la distinction entre surfaces dites
                    <strong>peu productives</strong> et celles dites <strong>productives</strong> afin d'éviter de surestimer les capacités
                    nourricières d'un territoire – par exemple de moyenne ou haute altitude. Cette distinction est utilisée d'une part pour décrire
                    les surfaces agricoles présentes sur un territoire, et d'autre part dans le calcul des différents indicateurs qui se base
                    <strong>exclusivement sur les surfaces dites productives</strong>. Elle est réalisée à partir des catégories de culture du
                    ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.rpg)} de la façon suivante :
                </p>
                <ul>
                    <li>
                        les surfaces agricoles peu productives (dites aussi non cultivées) rassemblent les groupes de cultures “17 Estives et landes”
                        (contenant notamment les pâturages d’altitudes), “11 Gel (surfaces gelées sans production)” (contenant les jachères) et une
                        partie de “28 Divers” ;
                    </li>
                    <li>
                        les surfaces agricoles productives rassemblent tous les autres groupes de cultures du
                        ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.rpg)}.
                    </li>
                </ul>

                <p>
                    Un point de vigilance concerne les
                    <c-lien href="https://www.geoportail.gouv.fr/actualites/les-usages-du-registre-parcellaire-graphique#!"
                        >limites du Recensement Parcellaire Graphique</c-lien
                    >
                    : ce gisement de données n'a connaissance que des parcelles appartenant à un exploitant les ayant déclarées dans sa demande de
                    subvention à la <abbr title="Politique Agricole Commune">PAC</abbr>. En conséquence l'estimation des surfaces agricoles
                    (productive ou peu productive) peut être sous estimée de plusieurs pourcents sur certains territoires (plusieurs dizaines de
                    pourcents dans les cas extrêmes).
                </p>

                <p>
                    Ces orientations sont le résultat de travaux réalisés en partenariat avec la métropole de Nice Côte d'Azur. Le détail de l'étude
                    est accessible <c-lien href="/pdf/Rapport_d_etude_pour_la_metropole_de_Nice_Cote_d_Azur-v1.1.pdf">ici</c-lien>.
                </p>

                <h2 id="${SOURCES_DONNEES_RETRAITEES.cheptels_crater.id}">Cheptels</h2>
                <p>
                    Les données sur les Cheptels sont calculées à partir des données
                    ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.cheptels, SOURCES_DONNEES_RETRAITEES.cheptels_crater.annees)} qui
                    fournissent le nombre de têtes pour différentes catégories d'animaux (Bovins, Ovins, Caprins, Porcins, Volailles, etc.).
                </p>
                <p>
                    Pour certaines communes on ne connait pas le nombre de têtes pour un type de cheptel donné du fait du secret statistique. Pour
                    pallier ce problème, on réalise une estimation de la façon suivante : on calcule d'abord le solde entre le nombre de têtes total
                    du département et celui obtenu en sommant les têtes de toutes les communes du département non touchées par le secret statistique.
                    On répartit ensuite ce solde entre les différentes communes concernées par le secret statistique au prorata des
                    ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.sau_ra)} de ces communes. Le nombre de têtes obtenu est ensuite
                    converti en Unités Gros Bétail via un coefficient moyen par type de cheptel obtenu à partir des données France.
                </p>

                <h2 id="${PAGE_METHODOLOGIE_HASH_OTEX}">Spécialisation des territoires</h2>

                <p>
                    Les exploitations sont classées selon leur spécialisation : l'orientation technico-économique (OTEX). Ce classement se fait à
                    partir des coefficients de production brute standard (PBS). Une exploitation est spécialisée dans un domaine si la PBS de la ou
                    des productions concernées dépasse deux tiers du total.
                </p>
                <p>De la même façon, les communes sont classées selon leur spécialisation sur base du PBS total des exploitations de la commune.</p>

                <p>Les OTEX 12 postes, regroupées en OTEX 5 postes, sont les suivantes :</p>
                <ul>
                    <li>Grandes Cultures (groupe Grandes Cultures)</li>
                    <li>Viticulture (groupe Viticulture)</li>
                    <li>Maraîchage Horticulture (groupe Maraîchage Horticulture ou Fruits)</li>
                    <li>Fruits (groupe Maraîchage Horticulture ou Fruits)</li>
                    <li>Bovin lait (groupe Élevage)</li>
                    <li>Bovin viande (groupe Élevage)</li>
                    <li>Bovin mixte (groupe Élevage)</li>
                    <li>Ovins Caprins et autres Herbivores (groupe Élevage)</li>
                    <li>Porcins Volailles (groupe Élevage)</li>
                    <li>Polyculture Polyélevage (groupe Polyculture Polyélevage)</li>
                    <li>Non Classées (groupe Non définie)</li>
                    <li>Sans Exploitations (groupe Non définie)</li>
                    <li>Non renseignées (groupe Non définie)</li>
                </ul>

                <p>
                    Pour plus d'informations,
                    <c-lien href="${construireUrlPageMethodologieSourceDonnee(SOURCES_DONNEES.otex)}">voir la source de données</c-lien>.
                </p>

                <h2 id="${PAGE_METHODOLOGIE_HASH_FLUX_AZOTE}">Flux d'azote</h2>

                <p>
                    Par essence, l’activité agricole consiste à exporter des produits alimentaires depuis les terres cultivées. Ce faisant, les
                    nutriments présents dans les sols et incorporés par les végétaux sont également exportés. Les sols cultivés s’appauvrissent donc
                    petit à petit et les agriculteurs doivent compenser ces pertes de nutriments afin de maintenir leur fertilité. Cela concerne en
                    particulier l’azote (N), le phosphore (P) et le potassium (K), des éléments souvent limitants dans la croissance des végétaux.
                    L'agriculture s'inscrit ainsi dans les grands cycles biochimiques : de l'azote, du carbone, du phosphore, etc.
                </p>
                <p>La fertilité des sols est assurée par :</p>
                <ul>
                    <li>
                        L'utilisation d’engrais, c’est-à-dire l’apport de nutriments depuis l’extérieur de la parcelle cultivée :
                        <ul>
                            <li>
                                engrais organiques traditionnel, dont les matières utilisées proviennent d’êtres vivants animaux ou végétaux (fumier,
                                lisier, guano, sang et os séchés, compost végétal...) ;
                            </li>
                            <li>engrais minéraux, qui dérivent de matières minérales inertes ou sont synthétisés industriellement ;</li>
                        </ul>
                    </li>
                    <li>L'enrichissement in situ en azote par fixation biologique du diazote atmosphérique grâce aux cultures de légumineuses ;</li>
                    <li>Déposition atmosphérique d'azote.</li>
                </ul>
                <p>
                    Aujourd'hui produits en très grandes quantités, les engrais minéraux sont un élément clé du système agricole industrialisé. Cela
                    marque un contraste fort avec le début du XXe siècle où les matières fécales et les urines étaient fortement valorisées pour
                    fertiliser les sols dans une logique de circularité.
                </p>
                <p>
                    Afin de quantifier les besoins en azote, notamment en engrais de synthèse, CRATer utilise les résultats du modèle MacDyn-FS
                    présenté ci-dessous.
                </p>

                <h3>Le modèle MacDyn-FS</h3>

                <p>
                    Le modèle <c-lien href="https://zenodo.org/records/6350666">MacDyN-FS</c-lien> (Macro Dynamic Nitrogen balance model of a Farming
                    System) a été conçu au cours de la thèse de doctorat de Corentin Pinsard à l'INRAE. Ce macro modèle de système agricole (compris
                    ici comme un ensemble de fermes au sein d'un territoire administratif) décrit temporellement (pas de temps annuel) les flux de
                    biomasse agricole (en masse d'azote) entre les animaux d'élevage (divisés par espèce), les cultures (séparées par espèce), les
                    usages des sols (terres arables et prairies permanentes) et les humains.
                </p>
                <p>
                    Dans sa version pour scénarios de simulation (paramètres forcés), les rendements des cultures dépendent des flux de fertilisants,
                    et la gestion des troupeaux de la nourriture animale disponible. Une description brève du modèle est donnée ci-dessous tandis
                    qu’une description complète de ce modèle (notamment le calcul des pertes de nutriments ou les règles d'allocation des flux de
                    biomasse) est disponible dans le
                    <c-lien href="https://www.theses.fr/2022UPASB021">chapitre 2 de la thèse de Corentin Pinsard</c-lien> et dans les publications
                    scientifiques associées à la thèse (en accès libre dans les
                    <c-lien href="https://scholar.google.com/citations?user=mK87BjUAAAAJ">publications en tant que premier auteur</c-lien>).
                </p>
                <p>Le modèle a été adapté pour les besoins de CRATer notamment pour obtenir des résultats à l’échelle des communes.</p>

                <h4>Principes généraux</h4>
                <p>
                    Le modèle analyse les besoins en azote des cultures pour chaque petite région agricole, et à l’intérieur pour chaque commune, en
                    tenant compte des types de cultures et des rendements, et en déduit une estimation des apports en azote sur base des surfaces
                    cultivées. Il n’y a pas de distinction entre les surfaces en bio et les surfaces en conventionnel. Les données de rendements
                    utilisées intègre néanmoins les surfaces en bio, ce qui n'induit pas de surestimations de l'usage d'engrais de synthèse. Le modèle
                    calcule également les besoins en azote des animaux d’élevage sur base des cheptels et de l’assiette par types d’animaux.
                </p>
                <p>
                    Une fois ces deux besoins évalués, le modèle calcule les besoins d’importation de nourriture animale, par différence entre les
                    besoins des animaux et la production végétale locale destinée aux animaux. Le schéma suivant résume le principe :
                </p>
                <figure>
                    <img src=${SCHEMA_MODELE_MACDYNFS} />
                </figure>
                <p>
                    In fine, le modèle permet d’obtenir une estimation de l’ensemble des flux d’azote liés au système agricole du territoire tels que
                    les besoins en azote de la plante, l’apport nécessaire d’engrais de synthèse, la production locale de fourrages pour les animaux,
                    etc.
                </p>

                <h4>Données mobilisées</h4>
                <p>Le modèle utilise :</p>
                <ul>
                    <li>
                        Des données de cheptel et surfaces agricoles. Dans le cas de CRATer il s’agit :
                        <ul>
                            <li>
                                du
                                ${lien(
                                    construireUrlPageMethodologieSourceDonneeRetraitee(SOURCES_DONNEES_RETRAITEES.cheptels_crater),
                                    "nombre d’animaux par types d'animaux et groupes en UGB au niveau des communes"
                                )}
                            </li>
                            <li>
                                des
                                ${lien(
                                    construireUrlPageMethodologieSourceDonneeRetraitee(SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater),
                                    'surfaces agricoles utiles en hectares par types de culture'
                                )}
                            </li>
                            <li>de la correspondance entre les types de cultures RPG et les types de cultures du modèle MacDyN-FS</li>
                        </ul>
                    </li>
                    <li>
                        Les besoins alimentaires des animaux par catégories d’aliment et par types d'animaux (EFESE - à l’échelle des Petites Régions
                        Agricoles - valeur de 2010) [kgN/UGB]
                    </li>
                    <li>
                        Les rendements en matières brutes de la production végétale par types de culture (AGRESTE - échelle départementale - moyenne
                        annuelle de la période 2016-2018) [tMB/ha]
                    </li>
                    <li>
                        Des coefficients de répartition de la biomasse récoltée selon usages (alimentation humaine, alimentation animale, énergie) par
                        types de culture (EFESE - échelle France - valeur de 2010)
                    </li>
                </ul>
            </section>
        `;
    }
}
