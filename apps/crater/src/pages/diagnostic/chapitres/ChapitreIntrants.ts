import '../../commun/TimeLine.js';
import '../charts/ChartLignes';
import '../charts/ChartLignesDeuxAxesY';
import './composants/CommentAgir.js';
import './composants/ChapitreDomaine.js';
import './composants/EncartResumeDomaine.js';
import './composants/PourquoiCestImportant.js';
import './composants/VoirEgalement.js';
import './SyntheseMaillon.js';
import '@lga/design-system/build/composants/Lien.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { formaterNombreEnNDecimalesString, formaterNombreSelonValeurString } from '@lga/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { INDICATEURS } from '@lga/indicateurs';
import { IDS_MAILLONS } from '@lga/indicateurs';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { construireUrlPageDiagnosticMaillon } from '../../../configuration/pages/pages-utils.js';
import type { Rapport } from '../../../modeles/diagnostics';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import type { DonneesPageDiagnostic } from '../PageDiagnostic.js';
import { creerOptionsSyntheseMaillon } from './chapitres-outils';
import type { OptionsEncartResumeDomaine } from './composants/EncartResumeDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-intrants': ChapitreIntrants;
    }
}

@customElement('c-chapitre-intrants')
export class ChapitreIntrants extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_CHAPITRES_DIAGNOSTIC];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly maillon = SA.getMaillon(IDS_MAILLONS.intrants);

    render() {
        if (!this.rapport) return;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.maillon?.nom}</h1>

                <c-synthese-maillon
                    .options=${creerOptionsSyntheseMaillon(
                        this.rapport,
                        this.rapport.getDiagnosticActif()!.intrants,
                        IDS_MAILLONS.intrants,
                        INDICATEURS.scoreIntrants.id
                    )}
                >
                </c-synthese-maillon>

                <h2 id="pourquoi-cest-important">Pourquoi c'est important ?</h2>
                <c-pourquoi-cest-important .maillon=${this.maillon}></c-pourquoi-cest-important>

                <h2 id="etat-lieux">Quel est l'état des lieux ?</h2>
                <div class="domaines-resumes">
                    <p>
                        <span>Quel est le niveau de dépendance à l'énergie ?</span>
                    </p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineEnergie(this.rapport, this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                    <p>
                        <span>Quel est le niveau de dépendance à l’eau pour l’irrigation ?</span>
                    </p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineIrrigation(this.rapport, this.donneesPage)}
                        ></c-encart-resume-domaine>
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineAlertesSecheresse(this.rapport, this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                    <p>
                        <span>Quel usage de pesticides ?</span>
                    </p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineNoduNormalise(this.rapport, this.donneesPage)}
                        ></c-encart-resume-domaine>
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineQsaNodu(this.rapport, this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                </div>

                <c-voir-egalement
                    .liensDomaines=${[
                        {
                            question: 'Les pratiques agricoles sont-elles sobres en intrants ?',
                            idDomaine: IDS_DOMAINES.bio
                        },
                        {
                            question: 'Quels besoins en surfaces agricoles pour nourrir les animaux d’élevage ?',
                            idDomaine: IDS_DOMAINES.consommationProduitsAnimaux
                        }
                    ]}
                    .donneesPage=${this.donneesPage}
                ></c-voir-egalement>

                <h2 id="leviers-action">Comment ma collectivité peut-elle s&#39;améliorer ?</h2>
                <c-comment-agir
                    .maillon=${this.maillon}
                    idTerritoireParcel=${ifDefined(this.rapport.getDiagnosticActif()?.idTerritoireParcel ?? undefined)}
                ></c-comment-agir>

                <c-encart-pages-precedente-suivante
                    libellePagePrecedente=${ifDefined(SA.getMaillon(IDS_MAILLONS.agriculteursExploitations)?.nom)}
                    hrefPagePrecedente=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.agriculteursExploitations,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                    libellePageSuivante=${ifDefined(SA.getMaillon(IDS_MAILLONS.production)?.nom)}
                    hrefPageSuivante=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.production,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsEncartResumeDomaineEnergie(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.energie)!,
            chiffresCles: [
                {
                    chiffre: `${formaterNombreSelonValeurString(this.rapport?.getDiagnosticActif()?.intrants.energie.energiePrimaireGJ)} GJ`,
                    libelleApresChiffre: `d'energie primaire par an`
                },
                {
                    chiffre: `${formaterNombreSelonValeurString(this.rapport?.getDiagnosticActif()?.intrants.energie.energiePrimaireGJParHa)} GJ/ha`,
                    libelleApresChiffre: `de surface agricole utile`
                }
            ],
            motSeparateurChiffresCles: 'soit',
            note: this.rapport?.getDiagnosticActif()?.intrants.energie.note,
            texteComplementaire: `Le poste de consommation principal est ${
                this.rapport?.getDiagnosticActif()?.intrants.energie.postePrincipal?.libelle
            }`,
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomaineIrrigation(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.irrigation)!,
            chiffresCles: [
                {
                    chiffre: `${formaterNombreSelonValeurString(this.rapport?.getDiagnosticActif()?.intrants.eau.irrigation.irrigationM3)} m³`,
                    libelleApresChiffre: `d'eau prélevé en moyenne par an`
                },
                {
                    chiffre: `${formaterNombreSelonValeurString(
                        this.rapport?.getDiagnosticActif()?.intrants.eau.irrigation.irrigationM3ParHa
                    )} m³/ha`,
                    libelleApresChiffre: `de surface agricole utile productive (hors prairies)`
                }
            ],
            motSeparateurChiffresCles: 'soit',
            note: this.rapport?.getDiagnosticActif()?.intrants.eau.irrigation.note,
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomaineAlertesSecheresse(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.secheresses)!,
            chiffresCles: [
                {
                    chiffre: `${formaterNombreSelonValeurString(
                        rapport.getDiagnosticActif()?.intrants.eau.secheresse.tauxImpactArretesSecheressePourcent
                    )} %`,
                    libelleApresChiffre: `part du territoire en alerte sécheresse en moyenne sur juillet-août`
                }
            ],
            note: this.rapport?.getDiagnosticActif()?.intrants.eau.secheresse.note,
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomaineQsaNodu(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.qsaNodu)!,
            chiffresCles: [
                {
                    chiffre: rapport.getDiagnosticActif()!.intrants.pesticides.qsaTotalAnneeFinale,
                    libelleApresChiffre: 'de substances actives achetées'
                },
                {
                    chiffre: `${rapport.getDiagnosticActif()!.intrants.pesticides.evolutionNodu! > 0 ? `+` : ``}${formaterNombreEnNDecimalesString(
                        rapport.getDiagnosticActif()!.intrants.pesticides.evolutionNodu,
                        0
                    )} %`,
                    libelleApresChiffre: `de doses unités utilisées par an entre ${
                        rapport.getDiagnosticActif()!.intrants.pesticides.indicateursParAnnees.anneeInitiale.annee
                    } et ${rapport.getDiagnosticActif()!.intrants.pesticides.indicateursParAnnees.anneeFinale.annee}`
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomaineNoduNormalise(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.noduNormalise)!,
            chiffresCles: [
                {
                    chiffre: formaterNombreSelonValeurString(
                        rapport.getDiagnosticActif()!.intrants.pesticides.indicateursParAnnees.anneeFinale.noduNormalise
                    ),
                    libelleApresChiffre: 'fois la dose annuelle maximale autorisée pour une substance donnée'
                }
            ],
            note: this.rapport?.getDiagnosticActif()?.intrants.pesticides.note,
            donneesPage: donneesPage
        };
    }
}
