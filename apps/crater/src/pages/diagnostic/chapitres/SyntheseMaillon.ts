import '../../commun/JaugeNoteSur10.js';
import '@lga/design-system/build/composants/Bouton.js';
import '@lga/design-system/build/composants/Boite.js';

import { Boite } from '@lga/design-system/build/composants/Boite.js';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { StyleCarteChoropletheNotes } from '@lga/styles-carte';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import type { Note } from '../../../modeles/diagnostics';

declare global {
    interface HTMLElementTagNameMap {
        'c-synthese-maillon': SyntheseMaillon;
    }
}

export interface OptionsSyntheseMaillon {
    note: Note;
    messageSynthese: string;
    lienMethodologie: string;
    lienCarte?: string;
}

@customElement('c-synthese-maillon')
export class SyntheseMaillon extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
            }

            main {
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: center;
                padding: calc(4 * var(--dsem));
                text-align: left;
            }

            c-jauge-note-sur-10 {
                width: calc(12 * var(--dsem));
                flex-shrink: 0;
            }

            .message {
                margin: auto;
                color: var(--couleur-neutre);
                padding-left: var(--dsem);
                margin-left: var(--dsem);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                :host {
                    padding: 0;
                }

                main {
                    flex-direction: column;
                }

                .message {
                    padding: 0;
                    margin: 0;
                    padding-top: calc(2 * var(--dsem));
                }
            }
        `
    ];

    private styleCarteNotes = new StyleCarteChoropletheNotes();

    @property({ attribute: false })
    options?: OptionsSyntheseMaillon;

    render() {
        if (!this.options) {
            return html``;
        }
        const couleur = this.styleCarteNotes.calculerCouleurFondFeature(this.options.note.valeurNumerique);

        return html`
            <c-boite encadrement arrondi=${Boite.ARRONDI.large}>
                <main>
                    <c-jauge-note-sur-10 .note=${this.options.note} couleur=${couleur}></c-jauge-note-sur-10>
                    <p class="message texte-grand">${unsafeHTML(this.options.messageSynthese)}</p>
                </main>
            </c-boite>
        `;
    }
}
