import '../../commun/TimeLine.js';
import '../charts/ChartBarres.js';
import './composants/CommentAgir.js';
import './composants/ChapitreDomaine.js';
import './composants/EncartResumeDomaine.js';
import './composants/PourquoiCestImportant.js';
import './composants/VoirEgalement.js';
import './SyntheseMaillon.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { calculerQualificatifEvolution, calculerSigneNombre, formaterNombreSelonValeurString, QualificatifEvolution } from '@lga/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import type { Maillon } from '@lga/indicateurs';
import { ICONES_SVG, INDICATEURS } from '@lga/indicateurs';
import { IDS_MAILLONS } from '@lga/indicateurs';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { construireUrlPageDiagnosticMaillon, construireUrlPageDiagnosticTerritoire } from '../../../configuration/pages/pages-utils';
import type { Rapport, TerresAgricoles } from '../../../modeles/diagnostics';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import type { DonneesPageDiagnostic } from '../PageDiagnostic.js';
import { creerOptionsSyntheseMaillon } from './chapitres-outils';
import type { OptionsEncartResumeDomaine } from './composants/EncartResumeDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-terres-agricoles': ChapitreTerresAgricoles;
    }
}

@customElement('c-chapitre-terres-agricoles')
export class ChapitreTerresAgricoles extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_CHAPITRES_DIAGNOSTIC];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly maillon: Maillon = SA.getMaillon(IDS_MAILLONS.terresAgricoles)!;

    render() {
        if (!this.rapport) return;

        const diagTA = this.rapport.getDiagnosticActif()!.terresAgricoles;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.maillon.nom}</h1>

                <c-synthese-maillon
                    .options=${creerOptionsSyntheseMaillon(
                        this.rapport,
                        this.rapport.getDiagnosticActif()!.terresAgricoles,
                        IDS_MAILLONS.terresAgricoles,
                        INDICATEURS.scoreTerresAgricoles.id
                    )}
                >
                </c-synthese-maillon>

                <h2 id="pourquoi-cest-important">Pourquoi c'est important ?</h2>
                <c-pourquoi-cest-important .maillon=${this.maillon}></c-pourquoi-cest-important>

                <h2 id="etat-lieux">Quel est l'état des lieux ?</h2>
                <div class="domaines-resumes">
                    <p>Y a-t-il assez de terres agricoles pour nourrir les habitants ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineSauParHabitant(diagTA, this.donneesPage)}
                            data-cy="encart-resume-domaine-sau-par-habitant"
                        ></c-encart-resume-domaine>
                    </div>
                    <p>Les terres agricoles, naturelles et forestières sont-elles artificialisées ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineRythmeArtificialisation(diagTA, this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                    <p>Est-ce justifié ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomainePolitiqueAmenagement(diagTA, this.donneesPage)}
                        ></c-encart-resume-domaine>
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomainePartLogementsVacants(diagTA, this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                </div>

                <c-voir-egalement
                    .liensDomaines=${[
                        {
                            question: 'Quels agriculteurs cultivent ces terres ?',
                            idDomaine: IDS_DOMAINES.actifsAgricoles
                        },
                        {
                            question: 'Quelles productions sur mon territoire ? Quelle adéquation théorique avec les consommations ?',
                            idDomaine: IDS_DOMAINES.adequationProductionConsommation
                        },
                        {
                            question: 'Quels besoins en surfaces agricoles pour nourrir les animaux d’élevage ?',
                            idDomaine: IDS_DOMAINES.consommationProduitsAnimaux
                        }
                    ]}
                    .donneesPage=${this.donneesPage}
                ></c-voir-egalement>

                <h2 id="leviers-action">Comment ma collectivité peut-elle s&#39;améliorer ?</h2>
                <c-comment-agir
                    .maillon=${this.maillon}
                    idTerritoireParcel=${ifDefined(this.rapport.getDiagnosticActif()?.idTerritoireParcel ?? undefined)}
                ></c-comment-agir>

                <c-encart-pages-precedente-suivante
                    libellePagePrecedente="Présentation du territoire"
                    hrefPagePrecedente=${construireUrlPageDiagnosticTerritoire(
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale ?? ''
                    )}
                    libellePageSuivante=${ifDefined(SA.getMaillon(IDS_MAILLONS.agriculteursExploitations)?.nom)}
                    hrefPageSuivante=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.agriculteursExploitations,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsEncartResumeDomaineSauParHabitant(
        diagTA: TerresAgricoles,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.sauParHabitant)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '',
                    chiffre: formaterNombreSelonValeurString(diagTA.sauParHabitantM2) + ' m²',
                    libelleApresChiffre: 'de surface agricole utile productive par habitant'
                },
                {
                    libelleAvantChiffre: '',
                    chiffre: formaterNombreSelonValeurString(diagTA.besoinsParHabitantAssietteActuelleM2) + ' m²',
                    libelleApresChiffre: 'nécéssaires pour le régime alimentaire actuel'
                }
            ],
            motSeparateurChiffresCles: 'vs',
            donneesPage: donneesPage
        };
    }
    private calculerOptionsEncartResumeDomaineRythmeArtificialisation(
        diagTA: TerresAgricoles,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.rythmeArtificialisation)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: 'l’équivalent de',
                    chiffre: formaterNombreSelonValeurString(diagTA.rythmeArtificialisationSauPourcent) + ' %',
                    libelleApresChiffre: 'de la surface agricole a été artificialisé en 5 ans'
                },
                {
                    libelleAvantChiffre: '&nbsp',
                    chiffre: diagTA.chiffreRythmeArtificialisation.nombre + ' ',
                    icone: ICONES_SVG.terrainFootball,
                    libelleApresChiffre: 'terrains de football ' + diagTA.chiffreRythmeArtificialisation.periode
                }
            ],
            motSeparateurChiffresCles: 'soit',
            donneesPage: donneesPage
        };
    }
    private calculerOptionsEncartResumeDomainePolitiqueAmenagement(
        diagTA: TerresAgricoles,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.politiqueAmenagement)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '',
                    chiffre: `${formaterNombreSelonValeurString(diagTA.artificialisation5ansHa)}`,
                    libelleApresChiffre: 'hectares artificialisés entre 2013 et 2018',
                    evolutionQualificatif: diagTA.rythmeArtificialisationSauPourcent! > 0 ? QualificatifEvolution.rate : QualificatifEvolution.valide,
                    evolutionDescription: `Objectif Zéro Artificialisation ${diagTA.rythmeArtificialisationSauPourcent! > 0 ? ' raté' : ' atteint'}`
                },
                {
                    libelleAvantChiffre: '',
                    chiffre: `${formaterNombreSelonValeurString(diagTA.evolutionMenagesEmplois5ans, true)}`,
                    libelleApresChiffre: 'ménages + emplois sur la même période'
                }
            ],
            motSeparateurChiffresCles: 'vs',
            donneesPage: donneesPage
        };
    }
    private calculerOptionsEncartResumeDomainePartLogementsVacants(
        diagTA: TerresAgricoles,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.logementsVacants)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '',
                    chiffre: formaterNombreSelonValeurString(diagTA.partLogementsVacants2018Pourcent) + ' %',
                    libelleApresChiffre: 'des logements',
                    evolutionSigne: calculerSigneNombre(diagTA.evolutionPartLogementsVacants2013a2018Points),
                    evolutionQualificatif: calculerQualificatifEvolution(
                        calculerSigneNombre(diagTA.evolutionPartLogementsVacants2013a2018Points),
                        'negative'
                    ),
                    evolutionDescription: `${formaterNombreSelonValeurString(
                        diagTA.evolutionPartLogementsVacants2013a2018Points,
                        true
                    )} pts entre 2013 et 2018`
                }
            ],
            donneesPage: donneesPage
        };
    }
}
