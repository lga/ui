import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, type TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { STYLES_CRATER } from '../../../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-figure': Figure;
    }
}

@customElement('c-figure')
export class Figure extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                max-width: 700px;
                margin: auto;
            }

            p {
                padding-top: var(--dsem);
                text-align: center;
                margin-top: 0;
            }
        `
    ];

    @property({ attribute: false })
    titre?: TemplateResult<1>;

    render() {
        return html`
            <main>
                <slot name="chart-figure"></slot>
                <p class="texte-petit">${ifDefined(this.titre)}</p>
            </main>
        `;
    }
}
