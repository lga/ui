import '@lga/design-system/build/composants/Lien.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { SA } from '@lga/indicateurs';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { construireUrlPageDiagnosticMaillon } from '../../../../configuration/pages/pages-utils.js';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-voir-egalement': VoirEgalement;
    }
}

export interface LienDomaine {
    question: string;
    idDomaine: string;
}

@customElement('c-voir-egalement')
export class VoirEgalement extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            #encart {
                padding-bottom: 0.5rem;
            }

            ul {
                padding-left: 2rem;
                list-style: none;
            }

            li:hover {
                color: var(--couleur-accent);
                text-decoration: underline;
            }
        `
    ];

    @property({ attribute: false })
    liensDomaines: LienDomaine[] = [];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    render() {
        return html` <div id="encart">
            <p>Voir également :</p>
            <ul>
                ${this.liensDomaines.map(
                    (l) =>
                        html` <li>
                            <c-lien
                                href="${construireUrlPageDiagnosticMaillon(
                                    SA.getDomaine(l.idDomaine)?.idMaillon ?? '',
                                    this.donneesPage?.idTerritoirePrincipal ?? '',
                                    this.donneesPage?.idEchelleTerritoriale,
                                    `${l.idDomaine}`
                                )}"
                            >
                                → ${l.question}
                            </c-lien>
                        </li>`
                )}
            </ul>
        </div>`;
    }
}
