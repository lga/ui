import '../../charts/ChartBarres.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-politique-amenagement': ChapitrePolitiqueAmenagement;
    }
}

@customElement('c-chapitre-politique-amenagement')
export class ChapitrePolitiqueAmenagement extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html` <c-chapitre-domaine
                .options=${this.calculerOptionsChapitreDomainePolitiqueAmenagement(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="3"
                contenuAdhoc
            >
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomainePolitiqueAmenagement(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.politiqueAmenagement)!,
            message: rapport.getDiagnosticActif()!.terresAgricoles.messagePolitiqueAmenagement,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }
}
