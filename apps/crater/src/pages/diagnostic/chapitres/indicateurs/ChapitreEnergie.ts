import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';
import '../../charts/ChartAnneau.js';
import '@lga/design-system/build/composants/Tableau.js';
import '../../charts/ChartBarres.js';

import { formaterNombreSelonValeurString } from '@lga/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import type { OptionsTableau } from '@lga/design-system/src/composants/Tableau';
import { DEFINITIONS_SOURCES_ENERGIE, IDS_DOMAINES, IDS_MAILLONS, INDICATEURS } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { css, html, LitElement, type TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { PAGE_METHODOLOGIE_HASH_ENERGIE } from '../../../../configuration/pages/declaration-pages.js';
import { construireUrlPageMethodologie } from '../../../../configuration/pages/pages-utils.js';
import type { PosteEnergie } from '../../../../modeles/diagnostics/energie/Energie.js';
import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { construireLienVersSourcesIndicateur } from '../../../commun/liens-utils.js';
import { STYLES_CRATER } from '../../../commun/pages-styles.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { ItemAnneau } from '../../charts/ChartAnneau';
import type { OptionsChartBarres } from '../../charts/ChartBarres';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-energie': ChapitreEnergie;
    }
}

@customElement('c-chapitre-energie')
export class ChapitreEnergie extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            :host {
                --c-barre-serie-1: black;
            }
            .legende {
                text-align: center;
            }
            article {
                padding-top: var(--dsem3);
            }
        `
    ];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${{
                    domaine: SA.getDomaine(IDS_DOMAINES.energie),
                    territoire: this.rapport.getTerritoireActif(),
                    estPagePourTef: this.donneesPage?.estPagePourTef
                } as OptionsDomaineDetaille}
                contenuAdhoc
            >
                <div slot="contenu">
                    <p class="italique">
                        La méthodologie utilisée pour cet indicateur est largement adaptée de la
                        <c-lien
                            href="https://librairie.ademe.fr/produire-autrement/3486-climagri-un-diagnostic-energie-gaz-a-effet-de-serre-au-service-d-une-demarche-de-territoire.html"
                            >méthodologie Climagri</c-lien
                        >
                        développée par l'<c-lien href="https://www.ademe.fr">ADEME</c-lien>. Cette adaptation a été réalisée avec l'aide précieuse de
                        <c-lien href="https://solagro.org/">SOLAGRO</c-lien> (ensemble du périmètre Climagri) et de Corentin Pinsard (docteur agronome
                        INRAE, expertise sur les flux d'azote) sur base des données disponibles dans CRATer. Les données fournies sont des
                        <strong>estimations</strong> calculées à partir d'autres données et peuvent donc s'écarter de la réalité (voir la
                        <c-lien href="${construireUrlPageMethodologie(IDS_MAILLONS.intrants, PAGE_METHODOLOGIE_HASH_ENERGIE)}"
                            >méthodologie de calcul</c-lien
                        >). Le poste chauffage des serres n'apparaît pas pour les échelles infradépartementales faute de données fiables disponibles.
                        D'autres postes tels que la production d'engrais non azotés (Potassium, Phosphore...) ou les produits phytosanitaires ne sont
                        pas évalués. Ils représentent à l'échelle de la France environ 10 % de la consommation totale énergétique du secteur agricole.
                    </p>
                    <article>
                        <p>${unsafeHTML(this.rapport.getDiagnosticActif()!.intrants.energie.messagePrincipal)}</p>
                        <p>${unsafeHTML(this.rapport.getDiagnosticActif()!.intrants.energie.messageSecondaire)}</p>
                        <c-figure .titre="${construireLienVersSourcesIndicateur(INDICATEURS.consommationEnergetiqueParHectare)}">
                            <c-chart-anneau
                                slot="chart-figure"
                                unite="GJ/an"
                                .items=${this.calculerItemsChartPostesEnergie(this.rapport.getDiagnosticActif()?.intrants.energie.postes)}
                            ></c-chart-anneau>
                        </c-figure>
                    </article>
                    <article>
                        <p>
                            Le détail des sources d'énergie (gaz, électricité...) utilisées pour chaque poste est donné dans la figure suivante pour
                            les postes d'énergie directe. L'énergie est dite indirecte quand elle concerne un produit qui n'est pas directement
                            utilisé comme source d'énergie – un carburant par exemple – mais en a nécéssité pour le produire. Les engrais azotés de
                            synthèse sont ainsi une énergie indirecte et désignent l'énergie qu'il a fallu pour les produire. Bien qu'on ne sache pas
                            évaluer exactement la source qui a été utilisée pour les produire, il s'agit quasimement en totalité de gaz naturel.
                        </p>
                        <c-figure .titre="${construireLienVersSourcesIndicateur(INDICATEURS.consommationEnergetiqueParHectare)}">
                            <c-chart-barres slot="chart-figure" .options=${this.calculerOptionsChartSourcesEnergie(this.rapport)}></c-chart-barres>
                        </c-figure>
                    </article>
                    <article>
                        <p>Toutes les informations sont reprises dans ce tableau :</p>
                        <c-tableau slot="chart-figure" .options=${this.calculerOptionsTableauEnergie()}></c-tableau>
                        <p class="legende texte-petit">${construireLienVersSourcesIndicateur(INDICATEURS.consommationEnergetiqueParHectare)}</p>
                    </article>
                    <article>
                        <p>Pour en savoir plus sur l'énergie, n'hésitez pas à consulter ces deux études de Solagro :</p>
                        <ul>
                            <li>
                                <c-lien
                                    href="https://solagro.org/images/imagesCK/files/publications/2019/rapport-agriculture-efficacite-energetique-2019.pdf"
                                    >Agriculture et efficacité énergétique</c-lien
                                >
                                : propositions et recommandations pour améliorer l’efficacité énergétique des exploitations agricoles en France,
                                rapport ADEME février 2019.
                            </li>
                            <li>
                                <c-lien
                                    href="https://solagro.org/travaux-et-productions/references/references-planete-mieux-connaitre-les-consommations-energetiques-des-exploitations-agricoles-francaises"
                                    >Références PLANÈTE 2010</c-lien
                                >
                                : traitement statistiques de 3500 diagnostic d’exploitations agricoles. Mise en avant de profils énergétiques et
                                d’indicateurs de consommation d’énergie primaire par système agricole.
                            </li>
                        </ul>
                    </article>
                </div>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerItemsChartPostesEnergie(postes?: PosteEnergie[]): ItemAnneau[] {
        if (!postes) return [];
        return postes.map((p) => {
            return {
                valeur: p.energiePrimaireGJ ?? 0,
                libelle: `${p.libelleCourt}`,
                contenuTooltip: html`${formaterNombreSelonValeurString(p.energiePrimaireGJ)} GJ/an<br />
                    ${p.sources?.reduce(
                        (a, b) => html`${a}<br />${b.libelle}: ${formaterNombreSelonValeurString(b.partPourcent)}%`,
                        p.estEnergieDirecte ? html`<br /><strong>Détail par source :</strong>` : html``
                    )}`,
                couleur: p.couleur
            };
        });
    }

    private calculerOptionsChartSourcesEnergie(rapport: Rapport): OptionsChartBarres {
        const series = new SerieMultiple(
            DEFINITIONS_SOURCES_ENERGIE.map(
                (s) =>
                    new Serie(
                        rapport
                            .getDiagnosticActif()
                            ?.intrants.energie.postes?.map((p) =>
                                !p.estEnergieDirecte && s.code === 'DIVERS'
                                    ? p.energiePrimaireGJ
                                    : (p.sources?.find((i) => i.code === s.code)?.energiePrimaireGJ ?? 0)
                            ),
                        s.libelle
                    )
            ),
            rapport.getDiagnosticActif()?.intrants.energie.postes.map((p) => p.libelleCourt)
        );
        return {
            series: series,
            nomAxeOrdonnees: 'Consommation énergétique (GJ)',
            empile: true,
            couleurs: DEFINITIONS_SOURCES_ENERGIE.map((i) => i.couleur)
        };
    }

    private calculerOptionsTableauEnergie(): OptionsTableau {
        const contenuTableau: TemplateResult[][] = [];
        const energie = this.rapport?.getDiagnosticActif()?.intrants.energie;
        const postes = energie?.postes;
        postes?.forEach((p) =>
            contenuTableau.push([
                html`${p.libelle}`,
                html`${formaterNombreSelonValeurString(p.energiePrimaireGJ)}`,
                html`${formaterNombreSelonValeurString(p.sources?.find((s) => s.code === 'CARBURANTS')?.partPourcent)}`,
                html`${formaterNombreSelonValeurString(p.sources?.find((s) => s.code === 'GAZ')?.partPourcent)}`,
                html`${formaterNombreSelonValeurString(p.sources?.find((s) => s.code === 'ELECTRICITE')?.partPourcent)}`,
                html`${formaterNombreSelonValeurString(p.sources?.find((s) => s.code === 'BIOMASSE')?.partPourcent)}`
            ])
        );
        const total = [html`Total`, html`${formaterNombreSelonValeurString(energie?.energiePrimaireGJ ?? 0)}`, html``, html``, html``, html``];
        return {
            entetes: [
                html`Poste`,
                html`Consommation <br />d'énergie primaire<br />[GJ]`,
                html`Part<br />carburants<br />[%]`,
                html`Part<br />gaz<br />[%]`,
                html`Part<br />électricité<br />[%]`,
                html`Part<br />biomasse<br />[%]`
            ],
            contenuHtml: contenuTableau,
            ligneFin: total
        };
    }
}
