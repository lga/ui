import '../../charts/ChartBarres.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { OptionsChartBarres } from '../../charts/ChartBarres';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-rythme-artificialisation': ChapitreRythmeArtificialisation;
    }
}

@customElement('c-chapitre-rythme-artificialisation')
export class ChapitreRythmeArtificialisation extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html` <c-chapitre-domaine
                .options=${this.calculerOptionsChapitreDomaineRythmeArtificialisation(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="3"
            >
                <c-chart-barres
                    .options=${this.calculerOptionsChartRythmeArtificialisation(this.rapport)}
                    slot="chart-chapitre-domaine"
                ></c-chart-barres>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomaineRythmeArtificialisation(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.rythmeArtificialisation)!,
            message: rapport.getDiagnosticActif()!.terresAgricoles.messageRythmeArtificialisation,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }

    private calculerOptionsChartRythmeArtificialisation(rapport: Rapport): OptionsChartBarres {
        const labels = rapport.getDiagnostics()!.map((d) => d!.territoire.nom);
        return {
            series: new SerieMultiple(
                [new Serie(rapport.getDiagnostics()!.map((d) => d!.terresAgricoles.rythmeArtificialisationSauPourcent))],
                labels
            ),
            nomAxeOrdonnees: "Rythme d'artificialisation (% / 5 ans)"
        };
    }
}
