import '../../charts/ChartBarres.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { OptionsChartBarres } from '../../charts/ChartBarres';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-sau-bio': ChapitrePartSAUBio;
    }
}

@customElement('c-chapitre-part-sau-bio')
export class ChapitrePartSAUBio extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${this.calculerOptionsChapitreDomainePartSauBio(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="2"
            >
                <c-chart-barres .options=${this.calculerOptionsChartPartSauBio(this.rapport)} slot="chart-chapitre-domaine"></c-chart-barres>
            </c-chapitre-domaine> `;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomainePartSauBio(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.bio)!,
            message: rapport.getDiagnosticActif()!.production.messageSauBio,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }

    private calculerOptionsChartPartSauBio(rapport: Rapport): OptionsChartBarres {
        return {
            series: new SerieMultiple(
                [new Serie(rapport.getDiagnostics()!.map((d) => d!.production.partSauBioPourcent))],
                rapport.getDiagnostics()!.map((d) => d!.territoire.nom)
            ),
            nomAxeOrdonnees: 'Part de la SAU Bio sur la SAU Totale (%)'
        };
    }
}
