import '../../charts/ChartBarres.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';
import '../../charts/ChartDistancesAuxCommerces.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-distances-plus-proches-commerces': ChapitreDistancesPlusProchesCommerces;
    }
}

@customElement('c-chapitre-distances-plus-proches-commerces')
export class ChapitreDistancesPlusProchesCommerces extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine .options=${this.calculerOptionsChapitreDomaineDistancesAuxCommerces(this.rapport, this.donneesPage)}>
                <c-chart-distances-plus-proches-commerces
                    .indicateurs=${this.rapport.getDiagnosticActif()?.transformationDistribution.indicateursProximiteCommercesParTypesCommerces}
                    slot="chart-chapitre-domaine"
                ></c-chart-distances-plus-proches-commerces>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomaineDistancesAuxCommerces(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.distancesPlusProchesCommerces)!,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }
}
