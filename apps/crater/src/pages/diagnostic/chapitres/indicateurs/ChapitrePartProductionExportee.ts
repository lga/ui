import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-production-exportee': ChapitrePartProductionExportee;
    }
}

@customElement('c-chapitre-part-production-exportee')
export class ChapitrePartProductionExportee extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            const optionsDomaineDetaille: OptionsDomaineDetaille = {
                domaine: SA.getDomaine(IDS_DOMAINES.importationExportationConsommationProduction)!,
                territoire: this.rapport.getTerritoireActif(),
                estPagePourTef: this.donneesPage?.estPagePourTef
            };
            return html`<c-chapitre-domaine .options=${optionsDomaineDetaille} contenuAdhoc>
                <p slot="contenu">
                    A l'échelle d'un bassin de vie plus de 90% des produits agricoles locaux sont exportés, et dans le même temps plus de 90% de
                    l'alimentation est composée de produits agricoles importés.
                </p>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }
}
