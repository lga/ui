import '../../charts/ChartLignesDeuxAxesY.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { getVariableCss, Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { OptionsChartLignesDeuxAxesY } from '../../charts/ChartLignesDeuxAxesY';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-qsa-nodu': ChapitreQsaNodu;
    }
}

@customElement('c-chapitre-qsa-nodu')
export class ChapitreQsaNodu extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html` <c-chapitre-domaine
                .options=${this.calculerOptionsChapitreDomaineQsaNodu(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="5"
            >
                <c-chart-lignes-2-axes-y
                    .options=${this.calculerOptionsChartQsaNodu(this.rapport)}
                    slot="chart-chapitre-domaine"
                ></c-chart-lignes-2-axes-y>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomaineQsaNodu(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.qsaNodu)!,
            message: rapport.getDiagnosticActif()!.intrants.pesticides.messageQSANodu,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }

    private calculerOptionsChartQsaNodu(rapport: Rapport): OptionsChartLignesDeuxAxesY {
        return {
            series: new SerieMultiple([
                new Serie(
                    rapport.getDiagnosticActif()!.intrants.pesticides.indicateursParAnnees.quantitesSubstancesTotalesKg,
                    'Quantité Substance Active (QSA) totale [kg]'
                ),
                new Serie(
                    rapport.getDiagnosticActif()!.intrants.pesticides.indicateursParAnnees.quantitesSubstancesAvecDUKg,
                    'Quantité Substance Active (QSA) avec Doses Unités [kg]'
                ),
                new Serie(rapport.getDiagnosticActif()!.intrants.pesticides.indicateursParAnnees.nodusHa, 'Nombre de doses unités (NODU) [ha]')
            ]),
            categoriesXaxis: [2017, 2018, 2019, 2020],
            nomsSeriesYaxis: [
                'Quantité Substance Active (QSA) totale [kg]',
                'Quantité Substance Active (QSA) totale [kg]',
                'Nombre de doses unités (NODU) [ha]'
            ],
            titreYaxisGauche: 'Quantité Substance Active [kg]',
            titreYaxisDroite: 'Nombre de Doses Unités [ha]',
            couleurYaxisGauche: getVariableCss('--c-ligne-serie-1'),
            couleurYaxisGaucheBis: getVariableCss('--c-ligne-serie-1-bis'),
            couleurYaxisDroite: getVariableCss('--c-ligne-serie-2')
        };
    }
}
