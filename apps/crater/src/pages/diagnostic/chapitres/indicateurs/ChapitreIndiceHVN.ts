import '../../charts/ChartBarres.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { SEUIL_LABELLISATION_HVN } from '../../../../modeles/diagnostics/production/MessageHvn.js';
import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { OptionsChartBarres } from '../../charts/ChartBarres';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-indice-hvn': ChapitreIndiceHVN;
    }
}

@customElement('c-chapitre-indice-hvn')
export class ChapitreIndiceHVN extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${this.calculerOptionsChapitreDomaineHVN(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="4"
            >
                <c-chart-barres .options=${this.calculerOptionsChartHvn(this.rapport)} slot="chart-chapitre-domaine"></c-chart-barres>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomaineHVN(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.hvn)!,
            message: rapport.getDiagnosticActif()!.production.messageHvn,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }
    private calculerOptionsChartHvn(rapport: Rapport): OptionsChartBarres {
        return {
            series: new SerieMultiple(
                [
                    new Serie(
                        rapport.getDiagnostics().map((d) => d!.production.indicateurHvn.indice1),
                        '1 : diversité des assolements'
                    ),
                    new Serie(
                        rapport.getDiagnostics().map((d) => d!.production.indicateurHvn.indice2),
                        '2 : pratiques agricoles durables'
                    ),
                    new Serie(
                        rapport.getDiagnostics().map((d) => d!.production.indicateurHvn.indice3),
                        "3 : infrastructures d'intérêt écologique"
                    )
                ],
                rapport.getDiagnostics().map((d) => d!.territoire.nom)
            ),
            nomAxeOrdonnees: 'Score HVN (/30)',
            empile: true,
            minY: 0,
            maxY: 30,
            annotations: {
                yaxis: [
                    {
                        y: SEUIL_LABELLISATION_HVN,
                        borderColor: 'var(--couleur-neutre)',
                        strokeDashArray: 5,
                        label: {
                            style: {
                                color: 'var(--blanc)',
                                background: 'var(--couleur-neutre-20)'
                            },
                            text: 'Seuil de labellisation HVN'
                        }
                    }
                ]
            }
        };
    }
}
