import '../../commun/TimeLine.js';
import '../charts/ChartTreemapConsommation';
import './composants/CommentAgir.js';
import './composants/ChapitreDomaine.js';
import './composants/EncartResumeDomaine.js';
import './composants/PourquoiCestImportant.js';
import './composants/VoirEgalement.js';
import './SyntheseMaillon.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { formaterNombreSelonValeurString, QualificatifEvolution } from '@lga/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { IDS_DOMAINES, IDS_MAILLONS } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { construireUrlPageDiagnosticMaillon } from '../../../configuration/pages/pages-utils.js';
import type { Rapport } from '../../../modeles/diagnostics';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import type { DonneesPageDiagnostic } from '../PageDiagnostic';
import { creerOptionsSyntheseMaillon } from './chapitres-outils';
import type { OptionsEncartResumeDomaine } from './composants/EncartResumeDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-consommation': ChapitreConsommation;
    }
}

@customElement('c-chapitre-consommation')
export class ChapitreConsommation extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_CHAPITRES_DIAGNOSTIC];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly maillon = SA.getMaillon(IDS_MAILLONS.consommation);

    render() {
        if (!this.rapport) return;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.maillon?.nom}</h1>

                <c-synthese-maillon
                    .options=${creerOptionsSyntheseMaillon(this.rapport, this.rapport.getDiagnosticActif()!.consommation, IDS_MAILLONS.consommation)}
                >
                </c-synthese-maillon>

                <h2 id="pourquoi-cest-important">Pourquoi c'est important ?</h2>
                <c-pourquoi-cest-important .maillon=${this.maillon}></c-pourquoi-cest-important>

                <h2 id="etat-lieux">Quel est l'état des lieux ?</h2>
                <div class="domaines-resumes">
                    <p>Quelle surface est nécessaire pour produire l'alimentation d'origine animale ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomainePartAlimentationAnimaleDansConsommation(this.rapport, this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                    <p>Est-ce que notre alimentation est financièrement accessible pour tous ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineTauxPauvrete(this.rapport, this.donneesPage)}
                        ></c-encart-resume-domaine>
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineAideAlimentaire(this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                    <p>Quel est l'impact de notre alimentation sur la santé ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomainePartPopulationObese(this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                </div>

                <c-voir-egalement
                    .liensDomaines=${[
                        {
                            question: 'Est-ce que la production est suffisante pour couvrir la consommation de mon territoire ?',
                            idDomaine: IDS_DOMAINES.adequationProductionConsommation
                        },
                        {
                            question: 'Quelle est la dépendance des consommateurs au pétrole pour faire leurs courses ?',
                            idDomaine: IDS_DOMAINES.dependancePopulationVoiture
                        }
                    ]}
                    .donneesPage=${this.donneesPage}
                ></c-voir-egalement>

                <h2 id="leviers-action">Comment ma collectivité peut-elle s&#39;améliorer ?</h2>
                <c-comment-agir
                    .maillon=${this.maillon}
                    idTerritoireParcel=${ifDefined(this.rapport.getDiagnosticActif()?.idTerritoireParcel ?? undefined)}
                ></c-comment-agir>

                <c-encart-pages-precedente-suivante
                    libellePagePrecedente=${ifDefined(SA.getMaillon(IDS_MAILLONS.transformationDistribution)?.nom)}
                    hrefPagePrecedente=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.transformationDistribution,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsEncartResumeDomainePartAlimentationAnimaleDansConsommation(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.consommationProduitsAnimaux)!,
            chiffresCles: [
                {
                    chiffre: `${formaterNombreSelonValeurString(
                        rapport.getDiagnosticActif()!.consommation.indicateurConsommation!.pourcentageConsommationAnimauxSurConsommationTotale
                    )} %`,
                    libelleApresChiffre: `des surfaces sont utilisées pour la production d’aliments à destination des animaux d'élevage`
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomaineTauxPauvrete(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.pauvrete)!,
            chiffresCles: [
                {
                    chiffre: formaterNombreSelonValeurString(rapport.getDiagnosticActif()!.consommation.tauxPauvrete) + ' %',
                    libelleApresChiffre: 'de la population vit sous le seuil de pauvreté'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomaineAideAlimentaire(donneesPage: DonneesPageDiagnostic | undefined): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.aideAlimentaire)!,
            estPerimetreNational: true,
            motSeparateurChiffresCles: 'soit',
            chiffresCles: [
                {
                    chiffre: '> 5 millions',
                    libelleApresChiffre: 'de personnes',
                    evolutionQualificatif: QualificatifEvolution.declin,
                    evolutionDescription: 'x2 en 10 ans'
                },
                {
                    chiffre: '1 personne sur 10'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomainePartPopulationObese(donneesPage: DonneesPageDiagnostic | undefined): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.obesite)!,
            estPerimetreNational: true,
            chiffresCles: [
                {
                    chiffre: '17 %',
                    libelleApresChiffre: 'des plus de 18 ans',
                    evolutionQualificatif: QualificatifEvolution.declin,
                    evolutionDescription: 'x3 depuis 1992'
                }
            ],
            donneesPage: donneesPage
        };
    }
}
