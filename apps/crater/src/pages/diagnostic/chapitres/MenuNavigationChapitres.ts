import '@lga/design-system/build/composants/MenuAccordeon.js';

import type { MenuAccordeonItem } from '@lga/design-system/build/composants/MenuAccordeon';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { IDS_MAILLONS } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { StyleCarteChoropletheNotes } from '@lga/styles-carte';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import {
    construireUrlPageDiagnosticMaillon,
    construireUrlPageDiagnosticSynthese,
    construireUrlPageDiagnosticTerritoire
} from '../../../configuration/pages/pages-utils';
import { type Diagnostic, NOTE_VALEUR_WARNING, type ValeurNote } from '../../../modeles/diagnostics';
import { STYLES_CRATER } from '../../commun/pages-styles';
import type { DonneesPageDiagnostic } from '../PageDiagnostic';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-navigation-chapitres': MenuNavigationChapitres;
    }
}

@customElement('c-menu-navigation-chapitres')
export class MenuNavigationChapitres extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            :host {
                display: block;
                width: 100%;
                background-color: var(--couleur-blanc);
                overflow-y: auto;
                border-right: 1px solid var(--couleur-neutre-clair);
                padding: var(--dsem);
            }
            .conteneur-menu-bas {
                position: relative;
                margin: 20px auto;
                z-index: 0;
            }
            .menu-bas {
                padding-left: 20px;
            }
            .trait {
                position: absolute;
                border: solid 3px var(--couleur-neutre-20);
                border-radius: 30px;
                top: 0;
                left: -20px;
                width: 60px;
                height: 100%;
            }
        `
    ];

    private styleCarteNotes = new StyleCarteChoropletheNotes();

    @property({ attribute: false })
    diagnostic?: Diagnostic;

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    render() {
        const chapitreSelectionne = SA.getDomaine(this.donneesPage?.chapitre ?? '')
            ? SA.getDomaine(this.donneesPage?.chapitre ?? '')?.idMaillon
            : (this.donneesPage?.chapitre ?? '');
        return html`
            <c-menu-accordeon
                class="menu-haut"
                .idItemSelectionne=${chapitreSelectionne}
                .items=${[
                    {
                        id: 'synthese',
                        libelle: 'Synthèse',
                        href: construireUrlPageDiagnosticSynthese(
                            this.donneesPage?.idTerritoirePrincipal ?? '',
                            this.donneesPage?.idEchelleTerritoriale
                        )
                    },
                    {
                        id: 'territoire',
                        libelle: 'Présentation du territoire',
                        href: construireUrlPageDiagnosticTerritoire(
                            this.donneesPage?.idTerritoirePrincipal ?? '',
                            this.donneesPage?.idEchelleTerritoriale
                        )
                    }
                ] as MenuAccordeonItem[]}
            ></c-menu-accordeon>
            <div class="conteneur-menu-bas">
                <c-menu-accordeon
                    class="menu-bas"
                    .idItemSelectionne="${chapitreSelectionne}"
                    .items=${[
                        this.creerItemMenuMaillon(
                            IDS_MAILLONS.terresAgricoles,
                            this.diagnostic?.terresAgricoles.note.valeur ?? null,
                            this.donneesPage
                        ),
                        this.creerItemMenuMaillon(
                            IDS_MAILLONS.agriculteursExploitations,
                            this.diagnostic?.agriculteursExploitations.note.valeur ?? null,
                            this.donneesPage
                        ),
                        this.creerItemMenuMaillon(IDS_MAILLONS.intrants, this.diagnostic?.intrants.note.valeur ?? null, this.donneesPage),
                        this.creerItemMenuMaillon(IDS_MAILLONS.production, this.diagnostic?.production.note.valeur ?? null, this.donneesPage),
                        this.creerItemMenuMaillon(
                            IDS_MAILLONS.transformationDistribution,
                            this.diagnostic?.transformationDistribution.note.valeur ?? null,
                            this.donneesPage
                        ),
                        this.creerItemMenuMaillon(IDS_MAILLONS.consommation, this.diagnostic?.consommation.note.valeur ?? null, this.donneesPage)
                    ] as MenuAccordeonItem[]}
                ></c-menu-accordeon>
                <div class="trait"></div>
            </div>
        `;
    }

    private creerItemMenuMaillon(idMaillon: string, noteMaillon: ValeurNote, donneesPage: DonneesPageDiagnostic | undefined) {
        return {
            id: idMaillon,
            libelle: SA.getMaillon(idMaillon)?.nom,
            sousLibelle: `Score : ${noteMaillon === null ? '-' : noteMaillon}/10`,
            href: construireUrlPageDiagnosticMaillon(idMaillon, donneesPage?.idTerritoirePrincipal ?? '', donneesPage?.idEchelleTerritoriale),
            sousItems: [
                {
                    id: `${idMaillon}#pourquoi-cest-important`,
                    libelle: "Pourquoi c'est important ?",
                    href: construireUrlPageDiagnosticMaillon(
                        idMaillon,
                        donneesPage?.idTerritoirePrincipal ?? '',
                        donneesPage?.idEchelleTerritoriale,
                        'pourquoi-cest-important'
                    )
                },
                {
                    id: `${idMaillon}#etat-lieux`,
                    libelle: 'Les indicateurs',
                    href: construireUrlPageDiagnosticMaillon(
                        idMaillon,
                        donneesPage?.idTerritoirePrincipal ?? '',
                        donneesPage?.idEchelleTerritoriale,
                        'etat-lieux'
                    )
                },
                {
                    id: `${idMaillon}#leviers-action`,
                    libelle: "Les leviers d'action",
                    href: construireUrlPageDiagnosticMaillon(
                        idMaillon,
                        donneesPage?.idTerritoirePrincipal ?? '',
                        donneesPage?.idEchelleTerritoriale,
                        'leviers-action'
                    )
                }
            ],
            icone: `
                <svg width="20" height="20" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="16" cy="16" r="14" fill="${this.styleCarteNotes.calculerCouleurFondFeature(
                        noteMaillon === NOTE_VALEUR_WARNING ? 5 : noteMaillon
                    )}"/>
                </svg>`
        } as MenuAccordeonItem;
    }
}
