import './synthese/ChapitreSynthese.js';
import '../commun/SablierCrater.js';
import './chapitres/ChapitrePresentationTerritoire.js';
import './chapitres/ChapitreTerresAgricoles';
import './chapitres/ChapitreAgriculteursExploitations.js';
import './chapitres/ChapitreIntrants.js';
import './chapitres/ChapitreProduction.js';
import './chapitres/ChapitreTransformationDistribution.js';
import './chapitres/ChapitreConsommation.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html } from 'lit';
import { customElement } from 'lit/decorators.js';

import { EtatRapport } from '../../modeles/diagnostics';
import { STYLES_CRATER } from '../commun/pages-styles';
import { ControleurDiagnostic } from './ControleurDiagnostic.js';
import { STYLES_CHAPITRES_DIAGNOSTIC } from './diagnostic-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-diagnostic-pdf': PageDiagnosticPdf;
    }
}

@customElement('c-page-diagnostic-pdf')
export class PageDiagnosticPdf extends ControleurDiagnostic {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        STYLES_CHAPITRES_DIAGNOSTIC,
        css`
            :host {
                --displayPDF: none;
                max-width: 800px;
                margin: auto;
            }

            main {
                display: flex;
                flex-direction: column;
                gap: 50px;
            }

            c-identite-site {
                padding: calc(2 * var(--dsem));
                float: left;
            }

            #message {
                display: block;
            }

            @media print {
                @page {
                    size: 25cm 35cm;
                    margin: 10mm 10mm 10mm 10mm;
                }

                c-chapitre-synthese {
                    padding-bottom: 700px;
                }
                c-chapitre-presentation-territoire,
                c-chapitre-terres-agricoles,
                c-chapitre-agriculteurs-exploitations,
                c-chapitre-intrants,
                c-chapitre-production,
                c-chapitre-transformation-distribution,
                c-chapitre-consommation {
                    margin-top: 200px;
                }
            }
        `
    ];

    render() {
        if (this.rapport && this.rapport.getEtat() !== EtatRapport.PRET) {
            return html`<c-sablier-crater></c-sablier-crater>`;
        } else {
            return html`<main>
                <c-chapitre-synthese affichagePDF .rapport=${this.rapport} .donneesPage="${this.donneesPage}"></c-chapitre-synthese>
                <p id="message">
                    Ce contenu est publié par l'association Les Greniers d'Abondance sous la
                    <c-lien href="https://creativecommons.org/licenses/by-nc-sa/4.0/">licence CC BY-NC-SA 4.0</c-lien> (Creative Commons, Attribution,
                    Pas d’utilisation commerciale, Partage dans les mêmes conditions). <br />Généré le ${new Date().toLocaleDateString()}.
                </p>
                <c-chapitre-presentation-territoire
                    affichagePDF
                    .rapport=${this.rapport}
                    .donneesPage="${this.donneesPage}"
                ></c-chapitre-presentation-territoire>
                <c-chapitre-terres-agricoles affichagePDF .rapport=${this.rapport} .donneesPage="${this.donneesPage}"></c-chapitre-terres-agricoles>
                <c-chapitre-agriculteurs-exploitations
                    affichagePDF
                    .rapport=${this.rapport}
                    .donneesPage="${this.donneesPage}"
                ></c-chapitre-agriculteurs-exploitations>
                <c-chapitre-intrants affichagePDF .rapport=${this.rapport} .donneesPage="${this.donneesPage}"></c-chapitre-intrants>
                <c-chapitre-production affichagePDF .rapport=${this.rapport} .donneesPage="${this.donneesPage}"></c-chapitre-production>
                <c-chapitre-transformation-distribution
                    affichagePDF
                    .rapport=${this.rapport}
                    .donneesPage="${this.donneesPage}"
                ></c-chapitre-transformation-distribution>
                <c-chapitre-consommation affichagePDF .rapport=${this.rapport} .donneesPage="${this.donneesPage}"></c-chapitre-consommation>
            </main>`;
        }
    }

    firstUpdated() {
        setTimeout(() => {
            window.print();
        }, 1000);
    }
}
