import { EvenementSelectionnerTerritoire } from '@lga/commun/build/composants/champ-recherche-territoire/EvenementSelectionnerTerritoire';
import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';
import { EvenementErreur } from '@lga/commun/build/evenements/EvenementErreur';
import { EvenementNaviguer } from '@lga/commun/build/evenements/EvenementNaviguer';
import { majDonneesPage } from '@lga/commun/build/modeles/pages/donnees-pages.js';
import { EvenementMajDonneesPage } from '@lga/commun/src/modeles/pages/EvenementMajDonneesPage.js';
import { IDS_DOMAINES, IDS_MAILLONS } from '@lga/indicateurs';
import type { HierarchieTerritoires } from '@lga/territoires';
import { LitElement } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';

import { PAGES_DIAGNOSTIC } from '../../configuration/pages/declaration-pages';
import { construireUrlPageDiagnosticSynthese } from '../../configuration/pages/pages-utils';
import { EvenementSelectionnerCategorieTerritoire } from '../../evenements/EvenementSelectionnerCategorieTerritoire';
import { EtatRapport, Rapport } from '../../modeles/diagnostics';
import { chargerDiagnosticCsv, chargerDiagnostics, traduireReponseApiEnDiagnostic } from '../../requetes-api/requetes-api-diagnostic';
import { chargerHierarchieTerritoires } from '../../requetes-api/requetes-api-territoires';
import type { DonneesPageDiagnostic } from './PageDiagnostic';
import { EvenementExporter, type TypeExport } from './synthese/menu-exporter/EvenementExporter';

declare global {
    interface HTMLElementTagNameMap {
        'c-controleur-diagnostic': ControleurDiagnostic;
    }
}

@customElement('c-controleur-diagnostic')
export class ControleurDiagnostic extends LitElement {
    @property({ attribute: false })
    donneesPage: DonneesPageDiagnostic = {
        idTerritoirePrincipal: '',
        chapitre: 'synthese'
    };

    @state()
    protected rapport?: Rapport;

    willUpdate() {
        this.verifierDonneesPage();
        // Cas 1er affichage ou changement de territoire principal
        if (this.chargementAFaire(this.rapport, this.donneesPage)) {
            this.rapport = Rapport.creerEnChargement();
            this.chargerRapport(Rapport.creerVide(), this.donneesPage.idTerritoirePrincipal, this.donneesPage.idEchelleTerritoriale)
                .then((rapport) => {
                    this.rapport = rapport;
                    this.majDonneesPage(rapport);
                })
                .catch((e) => {
                    this.actionErreur('DIAG-02', 'Erreur technique lors du chargement du rapport', e);
                });
        } else {
            // Cas changement de territoire actif sans nécessite de recharger les données (typiquement back via le navigateur)
            this.rapport = this.rapport?.setTerritoireActifDepuisEchelleTerritoriale(this.donneesPage.idEchelleTerritoriale);
        }
    }

    private chargementAFaire(rapport: Rapport | undefined, donneesPage: DonneesPageDiagnostic): boolean {
        return (
            rapport === undefined ||
            rapport.getEtat() === EtatRapport.VIDE ||
            (rapport.getEtat() === EtatRapport.PRET &&
                rapport.getHierarchieTerritoires()?.territoirePrincipal.id !== donneesPage.idTerritoirePrincipal)
        );
    }

    private chargerRapport(rapport: Rapport, idTerritoire: string, idEchelleTerritoriale?: string): Promise<Rapport> {
        return chargerHierarchieTerritoires(getApiBaseUrl(), idTerritoire).then((hierarchieTerritoires) => {
            return this.traiterReponseHierarchieTerritoires(rapport, hierarchieTerritoires, idEchelleTerritoriale);
        });
    }

    private traiterReponseHierarchieTerritoires = (
        rapport: Rapport,
        hierarchieTerritoires: HierarchieTerritoires,
        idEchelleTerritoriale?: string
    ): Promise<Rapport> => {
        const rapportIntialise = rapport
            .setHierarchieTerritoires(hierarchieTerritoires)
            .setTerritoireActifDepuisEchelleTerritoriale(idEchelleTerritoriale);
        return chargerDiagnostics(getApiBaseUrl(), hierarchieTerritoires).then((d) => {
            return this.traiterReponseDiagnostics(rapportIntialise, d, hierarchieTerritoires);
        });
    };

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private traiterReponseDiagnostics = (rapport: Rapport, donneesDiagnostics: any[], hierarchieTerritoires: HierarchieTerritoires): Rapport => {
        let rapportComplete = rapport;
        donneesDiagnostics.forEach((d) => {
            rapportComplete = rapportComplete.ajouterDiagnostic(
                traduireReponseApiEnDiagnostic(hierarchieTerritoires.getListeTerritoires().find((t) => t.id === d.idTerritoire)!, d)
            );
        });
        return rapportComplete;
    };

    connectedCallback(): void {
        super.connectedCallback();
        this.addEventListener(EvenementSelectionnerTerritoire.ID, (e: Event) => {
            this.actionNouveauTerritoirePrincipal((e as EvenementSelectionnerTerritoire).detail.idTerritoireCrater);
        });
        this.addEventListener(EvenementSelectionnerCategorieTerritoire.ID, (evt: Event) => {
            this.actionModifierEchelleTerritoriale((evt as EvenementSelectionnerCategorieTerritoire).detail.codeCategorieTerritoire);
        });
        this.addEventListener(EvenementExporter.ID, (event: Event) => {
            this.actionExporter((event as EvenementExporter).detail.typeExport);
        });
    }

    disconnectedCallback(): void {
        super.disconnectedCallback();
        this.removeEventListener(EvenementSelectionnerTerritoire.ID, (e: Event) => {
            this.actionNouveauTerritoirePrincipal((e as EvenementSelectionnerTerritoire).detail.idTerritoireCrater);
        });
        this.removeEventListener(EvenementSelectionnerCategorieTerritoire.ID, (evt: Event) => {
            this.actionModifierEchelleTerritoriale((evt as EvenementSelectionnerCategorieTerritoire).detail.codeCategorieTerritoire);
        });
        this.removeEventListener(EvenementExporter.ID, (event: Event) => {
            this.actionExporter((event as EvenementExporter).detail.typeExport);
        });
    }

    actionNouveauTerritoirePrincipal = (idTerritoire: string) => {
        this.dispatchEvent(new EvenementNaviguer(construireUrlPageDiagnosticSynthese(idTerritoire)));
    };

    actionModifierEchelleTerritoriale = (idEchelleTerritoriale: string) => {
        this.rapport = this.rapport?.setTerritoireActifDepuisEchelleTerritoriale(idEchelleTerritoriale);
        this.majDonneesPage(this.rapport);
    };

    actionErreur = (codeErreur: string, messageErreur: string, erreur?: Error) => {
        console.warn(`Erreur : ${codeErreur} ${messageErreur}`, erreur);
        this.dispatchEvent(new EvenementErreur(codeErreur, messageErreur));
    };

    actionExporter(type: TypeExport) {
        if (type === 'CSV') {
            chargerDiagnosticCsv(getApiBaseUrl(), this.rapport!.getDiagnosticActif()!.territoire.id);
        } else if (type === 'PDF') {
            this.dispatchEvent(
                new EvenementNaviguer(
                    PAGES_DIAGNOSTIC.pdf.getUrl({
                        idTerritoirePrincipal: this.donneesPage.idTerritoireActif ?? ''
                    }),
                    '_blank'
                )
            );
        }
    }

    private majDonneesPage(rapport: Rapport | undefined) {
        this.donneesPage = majDonneesPage(this.donneesPage, {
            idEchelleTerritoriale: rapport?.getIdEchelleTerritoriale(),
            nomTerritoirePrincipal: rapport?.getHierarchieTerritoires()?.territoirePrincipal.nom ?? '',
            idTerritoireActif: rapport?.getTerritoireActif()?.id ?? '',
            nomTerritoireActif: rapport?.getTerritoireActif()?.nom ?? '',
            metaDescription: rapport?.getDiagnosticActif()?.messageMetaDescription ?? undefined
        });
        this.dispatchEvent(new EvenementMajDonneesPage(this.donneesPage));
    }

    private verifierDonneesPage() {
        if (!this.donneesPage.idTerritoirePrincipal) {
            this.actionErreur('DIAG-01', 'Chargement du diagnostic impossible : identifiant du territoire principal non renseigné');
        } else {
            if (this.donneesPage.chapitre === undefined) {
                this.donneesPage.chapitre = 'synthese';
            } else if (
                !['pdf', 'synthese', 'territoire', ...Object.values(IDS_MAILLONS), ...Object.values(IDS_DOMAINES)].includes(this.donneesPage.chapitre)
            ) {
                this.actionErreur(
                    'DIAG-03',
                    `Chargement du diagnostic impossible, erreur dans l'url (le nom du maillon ou de l'indicateur est inconnu) : "${this.donneesPage.chapitre}"`
                );
            }
        }
    }
}
