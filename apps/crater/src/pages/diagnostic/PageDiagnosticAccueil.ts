import '../commun/template/TemplatePageAccueil.js';
import './composants/DescriptionDiagnostic.js';
import '@lga/commun/build/composants/champ-recherche-territoire/ChampRechercheTerritoire.js';

import type { EvenementSelectionnerTerritoire } from '@lga/commun/build/composants/champ-recherche-territoire/EvenementSelectionnerTerritoire.js';
import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';
import { EvenementNaviguer } from '@lga/commun/build/evenements/EvenementNaviguer.js';
import { CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';

import { PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages.js';
import { construireUrlPageDiagnosticSynthese } from '../../configuration/pages/pages-utils.js';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-diagnostic-accueil': PageDiagnosticAccueil;
    }
}
@customElement('c-page-diagnostic-accueil')
export class PageDiagnosticAccueil extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(30 * var(--dsem));
            }

            section {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(5 * var(--dsem));
                max-width: var(--largeur-maximum-contenu);
                padding-bottom: calc(5 * var(--dsem));
            }

            h1 > svg {
                fill: var(--couleur-primaire);
            }

            h1 {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 2);
                text-align: center;
                width: 80%;
                color: var(--couleur-blanc);
            }

            c-champ-recherche-territoire {
                width: 50%;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_TABLETTE + 'px')}) {
                main {
                    gap: calc(35 * var(--dsem));
                }

                section {
                    gap: var(--dsem);
                }
            }

            @media screen and (max-width: 600px) {
                c-champ-recherche-territoire {
                    width: 100%;
                }

                h1 {
                    width: 100%;
                }

                main {
                    gap: calc(30 * var(--dsem));
                }
            }
        `
    ];

    render() {
        return html`
            <c-template-page-accueil idItemActifMenuPrincipal=${PAGES_PRINCIPALES.diagnostic.getId()}>
                <main slot="contenu">
                    <section>
                        <h1>Découvrez le diagnostic du système agricole et alimentaire de votre territoire</h1>
                        <c-champ-recherche-territoire
                            apiBaseUrl=${getApiBaseUrl()}
                            data-cy="champ-recherche-territoire"
                            lienFormulaireDemandeAjoutTerritoire="${PAGES_PRINCIPALES.demandeAjoutTerritoire.getUrl()}"
                            @selectionnerTerritoire=${this.actionSelectionnerTerritoire}
                        ></c-champ-recherche-territoire>
                    </section>
                    <section>
                        <c-description-diagnostic></c-description-diagnostic>
                    </section>
                </main>
            </c-template-page-accueil>
        `;
    }

    private actionSelectionnerTerritoire(evt: Event) {
        this.dispatchEvent(
            new EvenementNaviguer(construireUrlPageDiagnosticSynthese((evt as EvenementSelectionnerTerritoire).detail.idTerritoireCrater))
        );
    }
}
