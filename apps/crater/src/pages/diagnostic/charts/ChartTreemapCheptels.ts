import { formaterNombreEnEntierString, fusionner, sommeArray } from '@lga/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import type { ApexOptions } from 'apexcharts';
import ApexCharts from 'apexcharts';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import { APEX_OPTIONS_GLOBALES } from './chart-config';

export interface OptionsTreemapCheptels {
    donnees: DonneesTreemapCheptels[];
    hauteur: number;
    largeur?: number;
}

export interface DonneesTreemapCheptels {
    code: string;
    couleur: string;
    ugb: number;
    tetes: number;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-treemap-cheptels': ChartTreemapCheptels;
    }
}

@customElement('c-chart-treemap-cheptels')
export class ChartTreemapCheptels extends LitElement {
    private chart?: ApexCharts;

    @property({ attribute: false })
    options?: OptionsTreemapCheptels;

    @query('figure')
    private figure?: HTMLElement;

    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            figure {
                margin: auto;
                width: 250px;
                display: flex;
                justify-content: center;
            }

            rect {
                stroke-width: 1px;
            }

            text {
                fill: var(--couleur-neutre);
                font-size: 12px;
            }
        `
    ];

    render() {
        return html`
            <div>
                <figure></figure>
            </div>
        `;
    }

    updated() {
        if (!this.options) return;

        if (this.chart) {
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        } else {
            this.chart = new ApexCharts(this.figure, this.calculerApexOptions(this.options));
            this.chart.render();
            // Le premier render d'apex ne fonctionne pas dans lit (diagramme vide). Contournement : on attend la fin du render, et on fait un update
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        }
    }

    private calculerApexOptions({ donnees, hauteur, largeur }: OptionsTreemapCheptels): ApexOptions {
        const estSurfaceTotaleNulle = sommeArray(donnees.map((i) => i.ugb)) === 0;
        const couleursApex = donnees.map((d) => d.couleur);

        const seriesApex = [
            {
                data: donnees.map((d) => {
                    return { x: d.code.slice(0, 2).toUpperCase(), y: d.ugb };
                })
            }
        ];

        const apexOptionsLocales: ApexOptions = {
            series: seriesApex,
            legend: {
                show: false
            },
            chart: {
                height: estSurfaceTotaleNulle ? 0 : hauteur,
                width: largeur ?? '100%',
                type: 'treemap',
                animations: {
                    enabled: false
                },
                sparkline: {
                    enabled: true
                }
            },
            colors: couleursApex,
            dataLabels: {
                enabled: true
            },
            plotOptions: {
                treemap: {
                    distributed: true,
                    enableShades: false,
                    borderRadius: 0
                }
            },
            tooltip: {
                custom: (contexteApex: { dataPointIndex: number }) => {
                    return ChartTreemapCheptels.calculerHtmlTooltipChartSansDetailCultures(donnees, contexteApex.dataPointIndex);
                }
            }
        };

        return fusionner(APEX_OPTIONS_GLOBALES, apexOptionsLocales) as ApexOptions;
    }

    private static calculerHtmlTooltipChartSansDetailCultures(donnees: DonneesTreemapCheptels[], dataPointIndex: number) {
        return `<div class="texte-titre">${donnees[dataPointIndex].code}
        <br>${formaterNombreEnEntierString(donnees[dataPointIndex].ugb)} Unités Gros Bétail (UGB)
        <br>(soit ${formaterNombreEnEntierString(donnees[dataPointIndex].tetes)} têtes)</div>`;
    }
}
