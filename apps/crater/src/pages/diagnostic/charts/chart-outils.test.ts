import { describe, expect, it } from 'vitest';

import {
    decouperLabelEnPlusieursLignes,
    formaterLabelNombreEleve,
    ModeFiltrageSeries,
    Serie,
    SerieMultiple,
    transformerEnLabelMultiligne
} from './chart-outils';

describe('Tests des outils de chart', () => {
    it('Formatter nombre grands', () => {
        expect(formaterLabelNombreEleve('0,15')).toBe('0,15');
        expect(formaterLabelNombreEleve('0.15')).toBe('0,15');
        expect(formaterLabelNombreEleve('0,33333339')).toBe('0,33');
        expect(formaterLabelNombreEleve('99,6')).toBe('100');
        expect(formaterLabelNombreEleve('123')).toBe('120');
        expect(formaterLabelNombreEleve('1234')).toBe('1 200');
        expect(formaterLabelNombreEleve('12345')).toBe('12k');
        expect(formaterLabelNombreEleve('123456')).toBe('123k');
        expect(formaterLabelNombreEleve('1234567')).toBe('1 235k');
        expect(formaterLabelNombreEleve('12345678')).toBe('12M');
        expect(formaterLabelNombreEleve('123456789')).toBe('123M');
        expect(formaterLabelNombreEleve('1234567891')).toBe('1 235M');
        expect(formaterLabelNombreEleve('12345678912')).toBe('12 346M');
    });

    it('Traiter les labels long', () => {
        expect(transformerEnLabelMultiligne('Commerce spécialisé', 8)).toStrictEqual(['Commerce', 'spécialisé']);
        expect(transformerEnLabelMultiligne('Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson', 15)).toStrictEqual([
            'Saint-Remy-en',
            '-Bouzemont',
            '-Saint-Genest',
            '-et-Isson'
        ]);
        expect(transformerEnLabelMultiligne('Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson', 16)).toStrictEqual([
            'Saint-Remy-en',
            '-Bouzemont-Saint',
            '-Genest-et-Isson'
        ]);
        expect(transformerEnLabelMultiligne('CA Concarneau Cornouaille', 1)).toStrictEqual(['CA', 'Concarneau', 'Cornouaille']);
        expect(transformerEnLabelMultiligne('CA Concarneau Cornouaille', 12)).toStrictEqual(['CA', 'Concarneau', 'Cornouaille']);
        expect(transformerEnLabelMultiligne('CA Concarneau Cornouaille', 13)).toStrictEqual(['CA Concarneau', 'Cornouaille']);
        expect(decouperLabelEnPlusieursLignes(['CA Concarneau Cornouaille', 'Agglomération'], 13)).toStrictEqual([
            'CA Concarneau',
            'Cornouaille',
            'Agglomération'
        ]);
        expect(transformerEnLabelMultiligne('Entre 100 et 200 ha', 9)).toStrictEqual(['Entre 100', 'et 200 ha']);
        expect(transformerEnLabelMultiligne('De 20 à 50 ha', 9)).toStrictEqual(['De 20 à', '50 ha']);
    });

    it('Filtrer les valeurs null dans les SerieMultiple selon les différents modes de filtrage', () => {
        const seriesAvecNull = new SerieMultiple([new Serie([1, 5, 20], 'serie1'), new Serie([null, 10, 30], 'serie2')], ['A', 'B', 'C']);

        expect(seriesAvecNull.filtrerValeursNull(ModeFiltrageSeries.SUPPRIMER_VALEURS_EN_POSITION_X_SI_AU_MOINS_UN_NULL)).toEqual(
            new SerieMultiple([new Serie([5, 20], 'serie1'), new Serie([10, 30], 'serie2')], ['B', 'C'])
        );
        expect(seriesAvecNull.filtrerValeursNull(ModeFiltrageSeries.SUPPRIMER_SERIES_AVEC_AU_MOINS_UNE_VALEUR_NULL)).toEqual(
            new SerieMultiple([new Serie([1, 5, 20], 'serie1')], ['A', 'B', 'C'])
        );

        const seriesSansNull = new SerieMultiple([new Serie([1, 5, 7], 'serie1'), new Serie([5, 10, 25], 'serie2')], ['A', 'B', 'C']);
        expect(seriesSansNull.filtrerValeursNull(ModeFiltrageSeries.SUPPRIMER_SERIES_AVEC_AU_MOINS_UNE_VALEUR_NULL)).toEqual(seriesSansNull);
        expect(seriesSansNull.filtrerValeursNull(ModeFiltrageSeries.SUPPRIMER_VALEURS_EN_POSITION_X_SI_AU_MOINS_UN_NULL)).toEqual(seriesSansNull);
    });
});
