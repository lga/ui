import '@lga/design-system/build/composants/Tooltip.js';
import '@lga/design-system/build/composants/Legende.js';

import { calculerPartsArray, coordonneesPolairesVersCartesiennes, formaterNombreSelonValeurString, sommeArray } from '@lga/base';
import type { ItemLegende } from '@lga/design-system/build/composants/Legende';
import type { Tooltip } from '@lga/design-system/build/composants/Tooltip';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, svg, type TemplateResult, unsafeCSS } from 'lit';
import { customElement, property, query, queryAll, state } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-anneau': ChartAnneau;
    }
}

export interface ItemAnneau {
    valeur: number;
    libelle: string;
    contenuTooltip?: TemplateResult<1>;
    couleur: string;
}

@customElement('c-chart-anneau')
export class ChartAnneau extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100%;
                max-width: 1000px;
                height: 100%;
                padding: var(--dsem);
            }

            figure {
                position: relative;
                margin: auto;
            }

            figure #texte-total {
                position: absolute;
                top: 50%;
                left: 50%;
                width: 40%;
                transform: translate(-50%, -50%);
                text-align: center;
                padding: var(--dsem);
            }

            svg {
                display: block;
                margin: auto;
                max-width: 400px;
            }

            svg path:hover {
                opacity: 0.6;
            }

            c-tooltip {
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                text-align: center;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                :host {
                    padding: 10px;
                }

                figure {
                    width: 80%;
                }
            }
        `
    ];

    @property({ attribute: false })
    items: ItemAnneau[] = [];

    @property()
    unite = '';

    @state()
    parts: number[] = [];

    @query('c-tooltip')
    private tooltip!: Tooltip;

    @query('#texte-total')
    private texteTotal!: HTMLElement;

    @queryAll('path')
    private pathElements!: SVGPathElement[];

    willUpdate() {
        this.parts = calculerPartsArray(
            this.items.map((i) => i.valeur),
            1
        );
    }

    render() {
        const rayon = 3;
        const tailleBoite = 2.5 * rayon;
        return html`
            <figure>
                <svg width="100%" height="100%" viewBox="${0} ${0} ${tailleBoite} ${tailleBoite}" xmlns="http://www.w3.org/2000/svg">
                    ${this.items.map((item, index) => {
                        const coordonnees = this.calculerCoordonneesArc(rayon, this.parts, index, tailleBoite);
                        return svg`
                            <path id="${index}" class="arc" d="M${coordonnees.xDepart} ${coordonnees.yDepart} A ${rayon} ${rayon} 0 ${
                                this.parts[index] > 50 ? 1 : 0
                            } 1 ${coordonnees.xArrivee} ${coordonnees.yArrivee}" fill="none" stroke="${item.couleur}">
                            </path>
                        `;
                    })}
                </svg>
                <div id="texte-total" class="chiffre-grand">
                    ${formaterNombreSelonValeurString(sommeArray(this.items.map((i) => i.valeur))) + ' ' + this.unite}
                </div>
                <c-tooltip></c-tooltip>
            </figure>
            <c-legende
                .items=${this.items.map(
                    (i, index) =>
                        ({
                            couleur: i.couleur,
                            libelle: `${i.libelle} (${this.parts[index]}%)`
                        }) as ItemLegende
                )}
            >
            </c-legende>
        `;
    }

    firstUpdated() {
        this.pathElements.forEach((pathElement) => {
            pathElement.addEventListener('mouseover', () => {
                this.tooltip.style.visibility = 'visible';
                this.texteTotal.style.visibility = 'hidden';

                const item = this.items[pathElement.id as unknown as number];
                this.tooltip.titre = item.libelle;
                this.tooltip.contenu = item.contenuTooltip;
            });

            pathElement.addEventListener('mouseout', () => {
                this.tooltip.style.visibility = 'hidden';
                this.texteTotal.style.visibility = 'visible';
            });
        });
    }

    private calculerCoordonneesArc(rayon: number, parts: number[], index: number, tailleBoite: number) {
        const angleDepart = ((sommeArray(parts.slice(0, index)) ?? 0) / 100) * 2 * Math.PI;
        const angleArrivee = angleDepart + (parts[index] / 100) * 2 * Math.PI;
        const pointDepart = this.transformerCoordonneesPourSVG(coordonneesPolairesVersCartesiennes(rayon, angleDepart), tailleBoite);
        const pointArrivee = this.transformerCoordonneesPourSVG(coordonneesPolairesVersCartesiennes(rayon, angleArrivee), tailleBoite);
        return { xDepart: pointDepart.x, yDepart: pointDepart.y, xArrivee: pointArrivee.x, yArrivee: pointArrivee.y };
    }

    private transformerCoordonneesPourSVG(coordonnees: { x: number; y: number }, tailleBoite: number) {
        return {
            x: coordonnees.x + tailleBoite / 2,
            y: -coordonnees.y + tailleBoite / 2
        };
    }
}
