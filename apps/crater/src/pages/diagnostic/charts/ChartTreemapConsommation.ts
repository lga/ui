import './ChartTreemapCultures.js';
import '@lga/design-system/build/composants/Legende.js';

import { type ItemLegende, Legende, MOTIF_RAYURE } from '@lga/design-system/build/composants/Legende';
import { GroupeCulture } from '@lga/indicateurs';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import type { DonneesTreemapCultures, OptionsChartTreemapCultures } from './ChartTreemapCultures';

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-treemap-consommation': ChartTreemapConsommation;
    }
}

const HAUTEUR_MAX_PIXELS = 350;

const ITEMS_LEGENDE_GROUPES_CULTURES_CONSOMMATION: ItemLegende[] = GroupeCulture.tous
    .filter((gc) => ['FOU', 'CER', 'OLP'].includes(gc.code))
    .map((g) => {
        return { libelle: `${g.nom} alim. animale (${g.nomCourt})`, couleur: g.couleur, motif: MOTIF_RAYURE };
    })
    .concat(
        GroupeCulture.tous.map((g) => {
            return { libelle: `${g.nom} (${g.nomCourt})`, couleur: g.couleur, motif: '' };
        })
    );

export interface OptionsTreemapConsommation {
    consommationAnimauxHa: number;
    consommationHumainsHa: number;
    donneesTreemapConsommationAnimaux: DonneesTreemapCultures[];
    donneesTreemapConsommationHumains: DonneesTreemapCultures[];
}

@customElement('c-chart-treemap-consommation')
export class ChartTreemapConsommation extends LitElement {
    static styles = css`
        figure {
            margin: auto;
            max-width: 600px;
            text-align: center;
        }

        c-legende {
            margin: 1rem auto;
        }
    `;

    @query('c-legende')
    private legende?: Legende;

    @property({ attribute: false })
    options?: OptionsTreemapConsommation;

    render() {
        if (!this.options) return;

        const consommationHa = this.options.consommationAnimauxHa + this.options.consommationHumainsHa;
        return html`
            <figure>
                <div class="chart-treemap">
                    <c-chart-treemap-cultures
                        .options=${this.calculerOptionsTreemapConsommationAnimaux(this.options, consommationHa)}
                        id="chart-treemap-consommation-animaux"
                    ></c-chart-treemap-cultures>
                    <c-chart-treemap-cultures
                        .options=${this.calculerOptionsTreemapConsommationHumains(this.options, consommationHa)}
                        id="chart-treemap-consommation-humains"
                    ></c-chart-treemap-cultures>
                </div>
                <c-legende .items=${ITEMS_LEGENDE_GROUPES_CULTURES_CONSOMMATION} type=${Legende.TYPE_LEGENDE.discrete}></c-legende>
            </figure>
        `;
    }

    private calculerOptionsTreemapConsommationAnimaux(options: OptionsTreemapConsommation, consommationHa: number): OptionsChartTreemapCultures {
        return {
            donnees: options.donneesTreemapConsommationAnimaux,
            hauteur: this.calculerHauteurTreemapConsommation(options.consommationAnimauxHa, consommationHa),
            activerFillPattern: true
        };
    }

    private calculerOptionsTreemapConsommationHumains(options: OptionsTreemapConsommation, consommationHa: number): OptionsChartTreemapCultures {
        return {
            donnees: options.donneesTreemapConsommationHumains,
            hauteur: this.calculerHauteurTreemapConsommation(options.consommationHumainsHa, consommationHa)
        };
    }

    private calculerHauteurTreemapConsommation(consommation: number, consommationHa: number) {
        return (consommation * HAUTEUR_MAX_PIXELS) / consommationHa;
    }
}
