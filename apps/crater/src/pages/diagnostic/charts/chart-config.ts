import './theme-charts.css';

import type { ApexOptions } from 'apexcharts';
import apexFrLocale from 'apexcharts/dist/locales/fr.json';

import { getVariableCss } from './chart-outils';

// APEXCHARTS

export const APEX_OPTIONS_CHART: ApexOptions = {
    chart: {
        fontFamily: getVariableCss('--font-family-texte-ds'),
        foreColor: getVariableCss('--couleur-neutre'),
        toolbar: { show: false },
        locales: [apexFrLocale],
        defaultLocale: 'fr'
    },
    noData: {
        text: `Données indisponibles à cette échelle`,
        verticalAlign: 'middle',
        style: {
            color: getVariableCss('--couleur-neutre'),
            fontSize: '16px',
            fontFamily: undefined
        }
    }
};

const APEX_OPTIONS_COULEURS: ApexOptions = {
    colors: [
        function (param: { value: unknown; seriesIndex: unknown; w: { config: { series: unknown[] } } }) {
            if (param.w.config.series.length === 1) {
                return getVariableCss('--c-barre-serie-1');
            } else {
                return getVariableCss('--c-barre-serie-2');
            }
        },
        getVariableCss('--c-barre-serie-1'),
        getVariableCss('--c-barre-serie-3')
    ]
};

const APEX_OPTIONS_DATA_LABELS: ApexOptions = {
    dataLabels: {
        enabled: false
    }
};

export const APEX_FONT_STYLE_CONFIG = {
    fontSize: '14px',
    colors: [getVariableCss('--couleur-neutre')],
    fontFamily: getVariableCss('--font-family-texte-ds'),
    fontWeight: 'normal'
};

const APEX_OPTIONS_AXES: ApexOptions = {
    xaxis: {
        labels: { style: APEX_FONT_STYLE_CONFIG },
        title: { style: APEX_FONT_STYLE_CONFIG },
        axisBorder: {
            color: getVariableCss('--c-bords-charts')
        }
    },
    yaxis: {
        labels: { style: APEX_FONT_STYLE_CONFIG },
        title: { style: APEX_FONT_STYLE_CONFIG },
        axisBorder: {
            show: true,
            color: getVariableCss('--c-bords-charts')
        }
    }
};

const APEX_OPTIONS_STATES: ApexOptions = {
    states: {
        hover: {
            filter: {
                type: 'darken',
                value: 0.5
            }
        }
    }
};

const APEX_OPTIONS_GRID: ApexOptions = {
    grid: {
        show: true,
        borderColor: getVariableCss('--c-grille-charts'),
        position: 'back',
        xaxis: {
            lines: {
                show: true
            }
        },
        yaxis: {
            lines: {
                show: true
            }
        }
    }
};

export const APEX_OPTIONS_GLOBALES: ApexOptions = {
    ...APEX_OPTIONS_CHART,
    ...APEX_OPTIONS_COULEURS,
    ...APEX_OPTIONS_DATA_LABELS,
    ...APEX_OPTIONS_AXES,
    ...APEX_OPTIONS_STATES,
    ...APEX_OPTIONS_GRID
};
