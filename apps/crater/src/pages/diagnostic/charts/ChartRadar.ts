import { fusionner } from '@lga/base';
import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT, CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints';
import type { ApexOptions } from 'apexcharts';
import ApexCharts from 'apexcharts';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import { APEX_FONT_STYLE_CONFIG, APEX_OPTIONS_CHART, APEX_OPTIONS_GLOBALES } from './chart-config';
import { getVariableCss, type SerieMultiple } from './chart-outils';

export interface OptionsChartRadar {
    series: SerieMultiple;
    couleurRemplissageSeries: string[];
    couleurBordureSeries: string[];
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-radar': ChartRadar;
    }
}

@customElement('c-chart-radar')
export class ChartRadar extends LitElement {
    static styles = css`
        figure {
            margin: auto;
            width: 500px;
            height: 400px;
        }

        @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
            figure {
                width: 400px;
                height: 350px;
            }
        }
        @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_TABLETTE + 'px')}) {
            figure {
                width: 500px;
                height: 400px;
            }
        }

        @media screen and (max-width: 650px) {
            figure {
                width: 260px;
                height: 260px;
            }
        }
    `;

    private chart?: ApexCharts;

    @property({ attribute: false })
    options?: OptionsChartRadar;

    @query('figure')
    private figure?: HTMLElement;

    render() {
        return html`
            <div>
                <figure></figure>
            </div>
        `;
    }

    async updated() {
        if (this.chart) {
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        } else {
            this.chart = new ApexCharts(this.figure, this.calculerApexOptions(this.options));
            await this.chart.render();
        }
    }

    private readonly OPTIONS_PAR_DEFAUT = {
        chart: {
            type: 'radar',
            width: '100%',
            height: '100%',
            dropShadow: {
                enabled: true,
                blur: 1,
                left: 1,
                top: 1
            },
            toolbar: {
                show: false
            },
            zoom: {
                enabled: true
            },
            animations: {
                enabled: true
            }
        },
        plotOptions: {
            radar: {
                polygons: {
                    strokeColors: getVariableCss('--c-chart-radar-lignes-grille'),
                    connectorColors: getVariableCss('--c-chart-radar-lignes-grille')
                }
            }
        },
        responsive: [
            // on met ici la conf generale du xaxis car en dehors du responsive apex bug
            {
                breakpoint: 100000, // très grande valeur => tous les écrans au dela des breakpoints definis ensuite
                options: {
                    xaxis: {
                        labels: {
                            style: {
                                ...fusionner(APEX_FONT_STYLE_CONFIG, {
                                    colors: [
                                        getVariableCss('--couleur-neutre'),
                                        getVariableCss('--couleur-neutre'),
                                        getVariableCss('--couleur-neutre'),
                                        getVariableCss('--couleur-neutre'),
                                        getVariableCss('--couleur-neutre')
                                    ]
                                })
                            }
                        }
                    }
                }
            },
            {
                breakpoint: 600,
                options: {
                    xaxis: {
                        labels: {
                            style: {
                                fontSize: '10px'
                            }
                        }
                    }
                }
            }
        ],
        yaxis: {
            min: 0,
            max: 10,
            tickAmount: 5,
            labels: {
                show: false,
                // Bug apex ? le show false montre quand meme les labels, on utilise un formatter
                formatter: function () {
                    return '';
                }
            }
        },
        legend: {
            show: true,
            // floating: true,
            position: 'bottom',
            markers: {
                width: 20,
                height: 20,
                strokeWidth: 10,
                radius: 20
            }
        },
        stroke: {
            width: 1
        },
        markers: {
            size: 0
        }
    };

    private calculerApexOptions(options: OptionsChartRadar | undefined): ApexOptions {
        if (!options) return APEX_OPTIONS_GLOBALES;

        const optionsLocales = {
            series: options.series.versSerieApexXY(),
            colors: options.couleurRemplissageSeries,
            legend: {
                markers: {
                    strokeColor: options.couleurBordureSeries,
                    fillColors: options.couleurRemplissageSeries
                }
            },
            stroke: {
                colors: options.couleurBordureSeries
            }
        };
        return fusionner(APEX_OPTIONS_CHART, fusionner(this.OPTIONS_PAR_DEFAUT, optionsLocales));
    }
}
