import { fusionner } from '@lga/base';
import ApexCharts from 'apexcharts';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import { APEX_OPTIONS_GLOBALES } from './chart-config';
import { formaterLabelNombreEleve, ModeFiltrageSeries, type SerieMultiple } from './chart-outils';

export interface OptionsChartBarres {
    series: SerieMultiple;
    nomAxeOrdonnees: string;
    modeFiltrage?: ModeFiltrageSeries;
    couleurs?: string[];
    empile?: boolean;
    minY?: number;
    maxY?: number;
    annotations?: ApexAnnotations;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-barres': ChartBarres;
    }
}

@customElement('c-chart-barres')
export class ChartBarres extends LitElement {
    private chart!: ApexCharts;

    @property({ attribute: false })
    options?: OptionsChartBarres;

    @query('figure')
    private figure?: HTMLElement;

    static styles = css`
        figure {
            margin: auto;
            max-width: 800px;
        }
    `;

    render() {
        return html`
            <div>
                <figure></figure>
            </div>
        `;
    }

    async updated() {
        if (!this.options) return;
        if (this.chart) {
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        } else {
            this.chart = new ApexCharts(this.figure, this.calculerApexOptions(this.options));
            await this.chart.render();
            // Le premier render d'apex ne fonctionne pas dans lit (diagramme vide). Contournement : on attend la fin du render, et on fait un update
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        }
    }

    private calculerApexOptions(options: OptionsChartBarres) {
        // Sans cette ligne on a une exception dans les logs sur certaines pages, uniquement sur l'app buildée (ne se produit pas avec npm run start:crater)
        if (!options) return;

        const apexOptionsLocales = {
            series: options.series
                .filtrerValeursNull(options.modeFiltrage ?? ModeFiltrageSeries.SUPPRIMER_VALEURS_EN_POSITION_X_SI_AU_MOINS_UN_NULL)
                .positionnerNomSiVide(options.nomAxeOrdonnees)
                .formaterLabels(15)
                .versSerieApexXY(),
            chart: {
                type: 'bar',
                height: 400,
                stacked: options.empile ?? false
            },
            xaxis: {
                labels: {
                    rotate: -35
                }
            },
            yaxis: {
                tickAmount: 5,
                min: options.minY ?? undefined,
                max: options.maxY ?? undefined,
                title: {
                    text: options.nomAxeOrdonnees
                },
                labels: {
                    formatter: (labelInitial: string) => formaterLabelNombreEleve(labelInitial)
                }
            },
            colors: options.couleurs ? options.couleurs : APEX_OPTIONS_GLOBALES.colors,
            tooltip: {
                y: {
                    formatter: (labelInitial: string) => formaterLabelNombreEleve(labelInitial)
                }
            },
            ...(options.annotations && { annotations: options.annotations })
        };

        return fusionner(APEX_OPTIONS_GLOBALES, apexOptionsLocales);
    }
}
