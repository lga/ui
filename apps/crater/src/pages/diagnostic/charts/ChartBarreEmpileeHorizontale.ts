import '@lga/design-system/build/composants/Tooltip.js';

import { calculerPartsArray, formaterNombreEnEntierString, formaterNombreSelonValeurString, htmlstring } from '@lga/base';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { styleMap } from 'lit/directives/style-map.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-barre-empilee-horizontale': ChartBarreEmpileeHorizontale;
    }
}

export interface ItemBarreEmpileeHorizontale {
    valeur: number;
    libelle: string;
    couleur: string;
}

@customElement('c-chart-barre-empilee-horizontale')
export class ChartBarreEmpileeHorizontale extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                box-sizing: border-box;
                width: 100%;
                height: 100%;
                padding: 30px 100px 15px 100px;
                position: relative;
                overflow: hidden;
            }

            #barre {
                display: inline-flex;
                width: 100%;
                height: 20px;
                border-radius: 5px;
            }

            .barre-item {
                display: block;
            }

            .barre-item c-tooltip {
                top: 0;
            }

            .barre-item:hover c-tooltip {
                visibility: visible;
            }

            figcaption {
                width: 100%;
                padding-top: 10px;
            }

            .legende-item {
                display: inline-flex;
                align-items: center;
                padding-right: 10px;
                position: relative;
            }

            .legende-item c-tooltip {
                left: 0;
                width: 100%;
                text-align: center;
            }

            .legende-item:hover c-tooltip {
                visibility: visible;
            }

            .legende-item-carre-couleur {
                width: 10px;
                height: 10px;
                border-radius: 1px;
                margin-right: 5px;
            }

            .legende-item-texte {
                display: inline-flex;
                align-items: baseline;
                gap: 5px;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                :host {
                    padding: 10px;
                }
            }
        `
    ];

    @property({ attribute: false })
    items: ItemBarreEmpileeHorizontale[] = [];

    @property()
    unite = '';

    @state()
    parts: number[] = [];

    willUpdate() {
        this.parts = calculerPartsArray(
            this.items.map((i) => i.valeur),
            1
        );
    }

    render() {
        return html`
            <div id="barre">
                ${this.items.map(
                    (item, index) => html`
                        <div class="barre-item" style=${styleMap({ width: `${this.parts[index]}%`, backgroundColor: `${item.couleur}` })}>
                            &nbsp;
                            <c-tooltip
                                .contenu=${htmlstring`${item.libelle} (${formaterNombreSelonValeurString(item.valeur)}${
                                    this.unite !== '' ? ' ' + this.unite : ''
                                })`}
                            ></c-tooltip>
                        </div>
                    `
                )}
            </div>
            <figcaption>
                ${this.items.map(
                    (item, index) => html`
                        <div class="legende-item">
                            <div class="legende-item-carre-couleur" style=${styleMap({ backgroundColor: `${item.couleur}` })}></div>
                            <div class="legende-item-texte">
                                <div class="texte-moyen">${item.libelle}</div>
                                <div class="texte-titre">${this.parts[index]}%</div>
                            </div>
                            <c-tooltip
                                .contenu=${htmlstring`${formaterNombreEnEntierString(item.valeur)}${this.unite !== '' ? ' ' + this.unite : ''}`}
                            ></c-tooltip>
                        </div>
                    `
                )}
            </figcaption>
        `;
    }
}
