import './ChartBarresHorizontal.js';
import '@lga/design-system/build/composants/Tableau.js';

import { calculerEtFormaterPourcentageString, formaterNombreEnEntierString, formaterNombreSelonValeurString, sommeArray } from '@lga/base';
import type { OptionsTableau } from '@lga/design-system/build/composants/Tableau';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { getVariableCss, Serie, SerieMultiple } from './chart-outils.js';
import type { OptionsChartBarresHorizontal } from './ChartBarresHorizontal';

export interface OptionsChartDoubleBarresOpposees {
    nomAxeXGauche: string;
    nomAxeXDroite: string;
    nomCategorie: string;
    donneesGauche: (number | null)[];
    donneesDroite: (number | null)[];
    labelsCategories: string[];
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-double-barres-opposees': ChartDoubleBarresOpposees;
    }
}

@customElement('c-chart-double-barres-opposees')
export class ChartDoubleBarresOpposees extends LitElement {
    static styles = css`
        div {
            margin: auto;
            display: flex;
            justify-content: center;
        }

        c-tableau {
            display: none;
        }

        @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
            c-chart-barres-horizontal {
                display: none;
            }
            c-tableau {
                display: block;
            }
        }
    `;

    @property({ attribute: false })
    options?: OptionsChartDoubleBarresOpposees;

    render() {
        if (!this.options) return;
        return html`
            <div>
                <c-chart-barres-horizontal .options=${this.calculerOptionsChartGauche(this.options)}></c-chart-barres-horizontal>
                <c-chart-barres-horizontal .options=${this.calculerOptionsChartDroite(this.options)}></c-chart-barres-horizontal>
                <c-tableau .options=${this.calculerOptionsTableau(this.options)}></c-tableau>
            </div>
        `;
    }

    private calculerOptionsChartDroite(options: OptionsChartDoubleBarresOpposees): OptionsChartBarresHorizontal {
        return {
            series: new SerieMultiple([new Serie(options.donneesDroite, options.nomAxeXDroite)], options.labelsCategories),
            nomAxeX: options.nomAxeXDroite,
            largeur: 340,
            couleur: [getVariableCss('--c-barre-serie-2')]
        };
    }

    private calculerOptionsChartGauche(options: OptionsChartDoubleBarresOpposees): OptionsChartBarresHorizontal {
        return {
            series: new SerieMultiple([new Serie(options.donneesGauche, options.nomAxeXGauche)], options.labelsCategories),
            nomAxeX: options.nomAxeXGauche,
            largeur: 280,
            couleur: [getVariableCss('--c-barre-serie-1')],
            masquerAxeY: true,
            renverserAxeY: true
        };
    }

    private calculerOptionsTableau(options: OptionsChartDoubleBarresOpposees): OptionsTableau {
        return {
            entetes: [html`${options.nomCategorie}`, html`${options.nomAxeXGauche}`, html`Part (%)`, html`${options.nomAxeXDroite}`, html`Part (%)`],
            contenuHtml: options.labelsCategories.map((label, indice) => {
                return [
                    html`${Array.isArray(label) ? label.join(' ') : label}`,
                    html`${formaterNombreEnEntierString(this.options!.donneesGauche[indice])}`,
                    html`${calculerEtFormaterPourcentageString(this.options!.donneesGauche[indice], sommeArray(this.options!.donneesGauche))}`,
                    html`${formaterNombreSelonValeurString(this.options!.donneesDroite[indice])}`,
                    html`${calculerEtFormaterPourcentageString(this.options!.donneesDroite[indice], sommeArray(this.options!.donneesDroite))}`
                ];
            })
        };
    }
}
