import './MessageCle.js';

import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { IDS_MAILLONS } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { construireUrlPageDiagnosticMaillon } from '../../../configuration/pages/pages-utils.js';
import { type IndicateurSynthese, Note, type Rapport } from '../../../modeles/diagnostics';
import { STYLES_CRATER } from '../../commun/pages-styles';
import type { DonneesPageDiagnostic } from '../PageDiagnostic.js';
import type { OptionsMessageCle } from './MessageCle';

declare global {
    interface HTMLElementTagNameMap {
        'c-boite-messages-cle': BoiteMessagesCle;
    }
}

@customElement('c-boite-messages-cle')
export class BoiteMessagesCle extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                height: 100%;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: space-evenly;
            }

            #titre-messages-cles {
                width: 100%;
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                padding: calc(2 * var(--dsem)) calc(4 * var(--dsem));
            }

            h1 {
                margin: 0;
                color: var(--couleur-primaire);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                #titre-messages-cles {
                    flex-direction: column;
                    gap: var(--dsem);
                }
            }
        `
    ];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        return html`
            <main>
                <c-message-cle
                    .options="${this.calculerOptionsMessageCle(
                        IDS_MAILLONS.terresAgricoles,
                        this.donneesPage,
                        this.rapport?.getDiagnosticActif()?.terresAgricoles
                    )}"
                    data-cy="message-cle-terres-agricoles"
                >
                </c-message-cle>
                <c-message-cle
                    .options="${this.calculerOptionsMessageCle(
                        IDS_MAILLONS.agriculteursExploitations,
                        this.donneesPage,
                        this.rapport?.getDiagnosticActif()?.agriculteursExploitations
                    )}"
                    data-cy="message-cle-agriculteurs-exploitations"
                >
                </c-message-cle>
                <c-message-cle
                    .options="${this.calculerOptionsMessageCle(
                        IDS_MAILLONS.intrants,
                        this.donneesPage,
                        this.rapport?.getDiagnosticActif()?.intrants
                    )}"
                    data-cy="message-cle-intrants"
                >
                </c-message-cle>
                <c-message-cle
                    .options="${this.calculerOptionsMessageCle(
                        IDS_MAILLONS.production,
                        this.donneesPage,
                        this.rapport?.getDiagnosticActif()?.production
                    )}"
                    data-cy="message-cle-production"
                >
                </c-message-cle>
                <c-message-cle
                    .options="${this.calculerOptionsMessageCle(
                        IDS_MAILLONS.transformationDistribution,
                        this.donneesPage,
                        this.rapport?.getDiagnosticActif()?.transformationDistribution
                    )}"
                    data-cy="message-cle-transformation-distribution"
                >
                </c-message-cle>
                <c-message-cle
                    .options="${this.calculerOptionsMessageCle(
                        IDS_MAILLONS.consommation,
                        this.donneesPage,
                        this.rapport?.getDiagnosticActif()?.consommation
                    )}"
                    data-cy="message-cle-consommation"
                >
                </c-message-cle>
            </main>
        `;
    }

    private calculerOptionsMessageCle(
        idMaillon: string,
        donneesPage?: DonneesPageDiagnostic,
        indicateurSynthese?: IndicateurSynthese
    ): OptionsMessageCle {
        return {
            icone: html`${unsafeHTML(SA.getMaillon(idMaillon)!.icone)}`,
            titre: SA.getMaillon(idMaillon)!.nom,
            sousTitre: SA.getMaillon(idMaillon)?.informationComplementaire,
            message: html`${unsafeHTML(indicateurSynthese?.messageSynthese)}`,
            note: indicateurSynthese?.note ?? new Note(null),
            href: construireUrlPageDiagnosticMaillon(idMaillon, donneesPage?.idTerritoirePrincipal ?? '', donneesPage?.idEchelleTerritoriale)
        };
    }
}
