import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable';

export type TypeExport = 'CSV' | 'PDF';
export class EvenementExporter extends EvenementPersonnalisable<{ typeExport: TypeExport }> {
    static ID = 'exporter';
    constructor(typeExport: TypeExport) {
        super(EvenementExporter.ID, { typeExport: typeExport });
    }
}
