import '@lga/design-system/build/composants/Bouton.js';
import '@lga/design-system/build/composants/Boite.js';
import '@lga/design-system/build/composants/Lien.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, query } from 'lit/decorators.js';

import { EvenementExporter } from './EvenementExporter';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-exporter': MenuExportDiagnostic;
    }
}

@customElement('c-menu-exporter')
export class MenuExportDiagnostic extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            main {
                position: relative;
            }
            a:hover {
                text-decoration: underline;
            }

            c-boite {
                position: absolute;
                right: 0;
                width: fit-content;
                height: fit-content;
                padding-top: var(--dsem);
            }

            #sous-menu-exporter {
                display: none;
            }
        `
    ];

    @query('#sous-menu-exporter')
    private sousMenuExporter!: HTMLElement;

    render() {
        return html`
            <main>
                <c-bouton
                    @click="${this.gererClicOuvrirMenuExporter}"
                    libelle="Exporter"
                    libelleTooltip="Exporter le résultat du diagnostic"
                    themeCouleur="${Bouton.THEME_COULEUR.accent}"
                    type="${Bouton.TYPE.plat}"
                    tailleReduite
                >
                    ${ICONES_DESIGN_SYSTEM.telecharger}
                </c-bouton>
                <c-boite id="sous-menu-exporter" encadrement>
                    <c-bouton libelle="Imprimer/exporter le rapport (pdf)" type=${Bouton.TYPE.plat} @click="${this.gererClicExporterPdf}">
                        ${ICONES_DESIGN_SYSTEM.flecheDroite}
                    </c-bouton>
                    <c-bouton libelle="Exporter les données (csv)" type=${Bouton.TYPE.plat} @click="${this.gererClicExporterCsv}">
                        ${ICONES_DESIGN_SYSTEM.flecheDroite}
                    </c-bouton>
                </c-boite>
            </main>
        `;
    }

    private gererClicOuvrirMenuExporter = () => {
        this.estMasque() ? this.afficher() : this.masquer();
    };

    private estMasque() {
        return window.getComputedStyle(this.sousMenuExporter).getPropertyValue('display') === 'none';
    }

    private gererClicExporterPdf = () => {
        this.masquer();
        this.dispatchEvent(new EvenementExporter('PDF'));
    };

    private gererClicExporterCsv = () => {
        this.masquer();
        this.dispatchEvent(new EvenementExporter('CSV'));
    };

    // Arrow function car déclarée comme listener au niveau window (besoin de préserver le contexte this)
    private gererClicMasquer = (event: Event) => {
        if (event.type === 'click' && !this.contains(event.target as Node)) {
            this.masquer();
        }
    };

    private afficher() {
        this.sousMenuExporter.style.display = 'block';
        window.addEventListener('click', this.gererClicMasquer, { capture: true });
    }

    private masquer() {
        this.sousMenuExporter.style.display = 'none';
        window.removeEventListener('click', this.gererClicMasquer, { capture: true });
    }
}
