import '../commun/template/TemplatePageAccueil.js';
import '../commun/BoiteLienVersPage.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { INDICATEURS } from '@lga/indicateurs';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { EchelleTerritoriale } from '@lga/territoires';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages.js';
import { construireUrlPageCarte } from '../../configuration/pages/pages-utils.js';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-carte-accueil': PageCarteAccueil;
    }
}
@customElement('c-page-carte-accueil')
export class PageCarteAccueil extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 10);
                margin-bottom: calc(8 * var(--dsem));
            }

            section {
                display: flex;
                flex-direction: column;
                gap: 5vh;
                max-width: var(--largeur-maximum-contenu);
            }

            h1 {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 2);
                text-align: center;
                max-width: 700px;
                color: var(--couleur-blanc);
            }

            h1 > svg {
                fill: var(--couleur-primaire);
            }

            c-boite-lien-vers-page {
                max-width: 800px;
            }

            div[slot='illustration'] svg {
                width: 100px;
                height: 100px;
                fill: var(--couleur-primaire-clair);
                padding: calc(var(--dsem));
            }
        `
    ];

    render() {
        return html`
            <c-template-page-accueil idItemActifMenuPrincipal=${PAGES_PRINCIPALES.carte.getId()}>
                <main slot="contenu">
                    <h1>Explorez les différents indicateurs de CRATer sous forme de cartes</h1>
                    <section>
                        <c-boite-lien-vers-page
                            titre="Dépendance de l'agriculture à l'énergie"
                            libelleLien="EXPLORER LA CARTE"
                            href=${ifDefined(construireUrlPageCarte(INDICATEURS.consommationEnergetiqueParHectare.id, EchelleTerritoriale.Epcis.id))}
                            encadrement
                        >
                            <div slot="illustration">${unsafeSVG(SA.getDomaine(IDS_DOMAINES.energie)?.icone)}</div>
                            <p slot="texte">
                                L'énergie – et en particulier l'énergie fossile – est vitale pour l'agriculture des territoires. <br />
                                Les tensions croissantes sur sa disponibilité et son prix constituent une vulnérabilité très forte pour le secteur.
                            </p>
                        </c-boite-lien-vers-page>
                        <c-boite-lien-vers-page
                            titre="Sécheresses"
                            libelleLien="EXPLORER LA CARTE"
                            href=${ifDefined(construireUrlPageCarte(INDICATEURS.alertesSecheresse.id, EchelleTerritoriale.Epcis.id))}
                            encadrement
                        >
                            <div slot="illustration">${unsafeSVG(SA.getDomaine(IDS_DOMAINES.secheresses)?.icone)}</div>
                            <p slot="texte">
                                Le climat change, avec pour conséquence une augmentation en fréquence et en intensité des épisodes de sécheresse en
                                France. Cela se traduit notamment par la prise de plus en plus régulière d'arrêtés sécheresse par les préfets pour
                                encadrer l'usage de l'eau.
                            </p>
                        </c-boite-lien-vers-page>
                        <c-boite-lien-vers-page
                            titre="Pesticides"
                            libelleLien="EXPLORER LA CARTE"
                            href=${ifDefined(construireUrlPageCarte(INDICATEURS.noduNormalise.id, EchelleTerritoriale.Epcis.id))}
                            encadrement
                        >
                            <div slot="illustration">${unsafeSVG(SA.getDomaine(IDS_DOMAINES.noduNormalise)?.icone)}</div>
                            <p slot="texte">
                                On estime que moins de 0,1% des molécules toxiques employées à l'échelle mondiale atteignent effectivement leurs
                                cibles.
                            </p>
                        </c-boite-lien-vers-page>
                        <c-boite-lien-vers-page
                            titre="Haute Valeur Naturelle"
                            libelleLien="EXPLORER LA CARTE"
                            href=${ifDefined(construireUrlPageCarte(INDICATEURS.hvn.id, EchelleTerritoriale.Epcis.id))}
                            encadrement
                        >
                            <div slot="illustration">${unsafeSVG(SA.getDomaine(IDS_DOMAINES.hvn)?.icone)}</div>
                            <p slot="texte">
                                Un tiers des oiseaux à disparu des milieux agricoles en trente ans et deux tiers des insectes ont déserté les prairies
                                allemandes en seulement dix ans.
                            </p>
                        </c-boite-lien-vers-page>
                        <c-boite-lien-vers-page
                            titre="Superficie moyenne des exploitations"
                            libelleLien="EXPLORER LA CARTE"
                            href=${ifDefined(construireUrlPageCarte(INDICATEURS.superficieMoyenneExploitations.id, EchelleTerritoriale.Epcis.id))}
                            encadrement
                        >
                            <div slot="illustration">${unsafeSVG(SA.getDomaine(IDS_DOMAINES.superficiesExploitations)?.icone)}</div>
                            <p slot="texte">Plus de 50 fermes disparaissent chaque jour.</p>
                        </c-boite-lien-vers-page>
                    </section>
                </main>
            </c-template-page-accueil>
        `;
    }
}
