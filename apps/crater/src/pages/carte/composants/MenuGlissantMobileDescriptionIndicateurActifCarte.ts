import './ListeChoixIndicateursCarte.js';
import '@lga/commun/build/composants/champ-recherche-territoire/ChampRechercheTerritoire.js';
import '@lga/design-system/build/composants/Legende.js';
import '@lga/design-system/build/composants/Bouton.js';
import '@lga/design-system/build/composants/PanneauGlissant.js';
import '../../commun/template/TemplatePanneau.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { type ItemLegende, Legende } from '@lga/design-system/build/composants/Legende';
import { EvenementFermer } from '@lga/design-system/build/evenements/EvenementFermer.js';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import type { IndicateurCarte } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { construireUrlPageMethodologie } from '../../../configuration/pages/pages-utils.js';
import { construireLienVersSourcesIndicateur } from '../../commun/liens-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-glissant-mobile-description-indicateur-actif-carte': MenuGlissantMobileDescriptionIndicateurActifCarte;
    }
}

@customElement('c-menu-glissant-mobile-description-indicateur-actif-carte')
export class MenuGlissantMobileDescriptionIndicateurActifCarte extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                height: 10%;
            }

            main {
                padding: 0 var(--dsem2) calc(var(--hauteur-menu-pied-page) + var(--dsem4));
            }

            div {
                text-align: left;
            }

            p {
                margin: var(--dsem3) var(--dsem) var(--dsem);
            }

            c-legende {
                margin: var(--dsem);
                width: fit-content;
                background-color: rgba(243, 245, 246, 0.8);
                padding: var(--dsem);
                --direction-flex-legende-discrete: column;
            }

            c-bouton {
                margin: auto;
                margin-right: 0;
            }
        `
    ];

    @property({ attribute: false })
    indicateurActif?: IndicateurCarte;

    @query('#bouton-retour')
    private boutonRetour?: Bouton;

    render() {
        const domaineActif = SA.getDomaine(this.indicateurActif?.id ?? '');
        return html`
            <c-panneau-glissant>
                <c-bouton id="bouton-description-indicateur" slot="bouton-ouvrir" .type=${Bouton.TYPE.relief}>
                    ${ICONES_DESIGN_SYSTEM.question}
                </c-bouton>
                <c-template-panneau slot="panneau">
                    <main slot="contenu-panneau">
                        <c-bouton
                            id="bouton-retour"
                            libelle="Retour"
                            libelleTooltip="Retour"
                            type="${Bouton.TYPE.plat}"
                            tailleReduite
                            @click="${this.fermer}"
                            >${ICONES_DESIGN_SYSTEM.flecheGauche}</c-bouton
                        >
                        <div>
                            <p class="titre-petit">${domaineActif?.libelle}</p>
                            <p class="texte-moyen">${unsafeHTML(domaineActif?.description)}</p>
                            <p class="texte-moyen">
                                ${this.indicateurActif
                                    ? construireLienVersSourcesIndicateur(this.indicateurActif)
                                    : html`Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>`}
                            </p>
                            <p class="texte-moyen">Légende :</p>
                            <c-legende
                                .items=${this.indicateurActif?.styleCarte?.seuilsLegende?.map(
                                    (seuil) =>
                                        ({
                                            couleur: seuil.couleur,
                                            libelle: seuil.libelle
                                        }) as ItemLegende
                                )}
                                type=${this.indicateurActif?.styleCarte?.estIndicateurContinu
                                    ? Legende.TYPE_LEGENDE.continue
                                    : Legende.TYPE_LEGENDE.discrete}
                            ></c-legende>
                            <p class="texte-moyen">En savoir plus :</p>
                            <c-lien class="texte-moyen" href=${construireUrlPageMethodologie(this.indicateurActif?.idMaillon, domaineActif?.id)}
                                >→ voir la méthodologie</c-lien
                            >
                        </div>
                    </main>
                </c-template-panneau>
            </c-panneau-glissant>
        `;
    }

    private fermer() {
        this.boutonRetour?.dispatchEvent(new EvenementFermer());
    }
}
