import type { FeatureCollection } from 'geojson';

export interface ValeurIndicateurTerritoire {
    idTerritoire: string;
    valeur: number | null;
}

export class TerritoiresVisiblesCarte {
    public contoursGeojson?: FeatureCollection;
    private valeursIndicateurs: ValeurIndicateurTerritoire[] = [];

    ajouterContours(contoursGeojson: FeatureCollection) {
        this.contoursGeojson ? this.contoursGeojson.features.push(...contoursGeojson.features) : (this.contoursGeojson = contoursGeojson);
    }

    ajouterValeursIndicateurs(valeursIndicateurs: ValeurIndicateurTerritoire[]) {
        this.valeursIndicateurs.push(...valeursIndicateurs);
    }

    getValeurIndicateur(idTerritoire: string): number | null {
        return this.valeursIndicateurs.find((i) => i.idTerritoire === idTerritoire)?.valeur ?? null;
    }

    reset() {
        this.contoursGeojson = undefined;
        this.valeursIndicateurs = [];
    }
}
