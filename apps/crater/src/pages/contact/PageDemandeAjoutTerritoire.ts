import './TemplatePageFormulaire.js';

import { html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-demande-ajout-territoire': PageDemandeAjoutTerritoire;
    }
}

@customElement('c-page-demande-ajout-territoire')
export class PageDemandeAjoutTerritoire extends LitElement {
    render() {
        return html`
            <c-template-page-formulaire
                titre="Formulaire de demande d'ajout de territoire"
                hauteur="1200px"
                lienFormulaire="https://resiliencealimentaire.org/formulaire-ajout-territoire-crater/"
            >
            </c-template-page-formulaire>
        `;
    }
}
