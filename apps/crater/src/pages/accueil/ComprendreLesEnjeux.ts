import '../commun/EncartLienVersPage.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

import { PAGES_COMPRENDRE_LE_SA } from '../../configuration/pages/declaration-pages.js';
import COMPRENDRE_ENJEUX_VISUEL_1 from '../../ressources/illustrations/accueil/comprendre-les-enjeux-1.png';
import COMPRENDRE_ENJEUX_VISUEL_2 from '../../ressources/illustrations/accueil/comprendre-les-enjeux-2.png';
import COMPRENDRE_ENJEUX_VISUEL_3 from '../../ressources/illustrations/accueil/comprendre-les-enjeux-3.png';
import { POSITION_ILLUSTRATION } from '../commun/EncartLienVersPage.js';
import { STYLES_ZONES_COMPLEMENTS_ACCUEIL } from './accueil-utils';

declare global {
    interface HTMLElementTagNameMap {
        'c-comprendre-les-enjeux': ComprendreLesEnjeux;
    }
}

@customElement('c-comprendre-les-enjeux')
export class ComprendreLesEnjeux extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_ZONES_COMPLEMENTS_ACCUEIL];

    render() {
        return html`
            <div id="contenu">
                <header class="titre-large">Comprendre les enjeux</header>
                <main>
                    <c-encart-lien-vers-page
                        titre="Un système fait pour produire, pas pour nourrir"
                        href=${PAGES_COMPRENDRE_LE_SA.systemeActuel.getUrl()}
                        positionIllustration=${POSITION_ILLUSTRATION.droite}
                    >
                        <span slot="texte"
                            >Du champ à l’assiette, une multitude de machines, de travailleurs et d’organisations font tourner un système alimentaire
                            qui produit plus de nourriture que jamais dans notre histoire. Pourtant, près de la moitié de l’humanité souffre de
                            malnutrition. En cherchant à produire toujours plus à des coûts toujours plus faibles, le modèle agro-industriel se paye
                            en réalité au prix fort en termes de dégradation de la santé publique, de précarité des travailleurs et de dommages
                            environnementaux d’ampleur planétaire.</span
                        >
                        <img slot="illustration" src="${COMPRENDRE_ENJEUX_VISUEL_1}" width="451" height="320" loading="lazy" />
                    </c-encart-lien-vers-page>
                    <c-encart-lien-vers-page
                        titre="Nuages à l’horizon"
                        href=${PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getUrl()}
                        positionIllustration=${POSITION_ILLUSTRATION.gauche}
                    >
                        <span slot="texte"
                            >Le fonctionnement du modèle agro-industriel est sérieusement remis en cause par l’aggravation de plusieurs menaces :
                            dérèglement climatique, effondrement de la biodiversité, épuisement des ressources et augmentation des tensions
                            économiques et politiques. Dans ce nouveau contexte, les recettes d’hier deviennent tantôt irréalistes, tantôt sources de
                            vulnérabilités. Sans changement de trajectoire, notre sécurité alimentaire est menacée.</span
                        >
                        <img slot="illustration" src="${COMPRENDRE_ENJEUX_VISUEL_2}" width="451" height="320" loading="lazy" />
                    </c-encart-lien-vers-page>
                    <c-encart-lien-vers-page
                        titre="Changement de cap !"
                        href=${PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire.getUrl()}
                        positionIllustration=${POSITION_ILLUSTRATION.droite}
                    >
                        <span slot="texte"
                            >Les grandes transformations à réaliser sont à notre portée, elles se basent sur des scénarios de transition vers des
                            pratiques et des modèles déjà éprouvés. Nous devons généraliser l’agroécologie, diviser par deux la production et la
                            consommation de produits animaux et renforcer l’ancrage territorial des systèmes alimentaires. Il ne s'agit plus
                            simplement de produire, mais de nourrir, sainement et durablement, toute la population.</span
                        >
                        <img slot="illustration" src="${COMPRENDRE_ENJEUX_VISUEL_3}" width="451" height="294" loading="lazy" />
                    </c-encart-lien-vers-page>
                </main>
            </div>
        `;
    }
}
