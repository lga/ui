import '../commun/EncartLienVersPage.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { INDICATEURS } from '@lga/indicateurs';
import { EchelleTerritoriale } from '@lga/territoires';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { construireUrlPageCarte } from '../../configuration/pages/pages-utils.js';
import ENERGIE from '../../ressources/illustrations/accueil/energie.png';
import PESTICIDES_VISUEL from '../../ressources/illustrations/accueil/pesticides.png';
import SECHERESSES from '../../ressources/illustrations/accueil/secheresses.png';
import { POSITION_ILLUSTRATION } from '../commun/EncartLienVersPage.js';
import { STYLES_ZONES_COMPLEMENTS_ACCUEIL } from './accueil-utils';

declare global {
    interface HTMLElementTagNameMap {
        'c-zoom-sur': ZoomSur;
    }
}

@customElement('c-zoom-sur')
export class ZoomSur extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_ZONES_COMPLEMENTS_ACCUEIL,
        css`
            main {
                padding-top: calc(4 * var(--dsem));
            }
        `
    ];

    render() {
        return html`
            <div id="contenu">
                <header class="titre-large">Zoom sur...</header>
                <main>
                    <c-encart-lien-vers-page
                        titre="L'énergie"
                        href=${ifDefined(construireUrlPageCarte(INDICATEURS.consommationEnergetiqueParHectare.id, EchelleTerritoriale.Epcis.id))}
                        positionIllustration=${POSITION_ILLUSTRATION.droite}
                        libelleBouton="Voir la carte sur l'énergie"
                        libelleCourtBouton="Voir la carte"
                    >
                        <span slot="texte">
                            Carburants pour les tracteurs et machines, production d'engrais minéraux, chauffage des serres et bâtiments d'élevage... :
                            l'énergie – et en particulier l'énergie fossile – est vitale pour l'agriculture des territoires.
                            <strong>Les tensions sur sa disponibilité et son prix constituent une vulnérabilité très forte</strong>.
                        </span>
                        <img slot="illustration" src="${ENERGIE}" width="462" height="288" loading="lazy" />
                    </c-encart-lien-vers-page>
                    <c-encart-lien-vers-page
                        titre="Les sécheresses"
                        href=${ifDefined(construireUrlPageCarte(INDICATEURS.alertesSecheresse.id, EchelleTerritoriale.Epcis.id))}
                        positionIllustration=${POSITION_ILLUSTRATION.gauche}
                        libelleBouton="Voir la carte des arrêtés sécheresse"
                        libelleCourtBouton="Voir la carte"
                    >
                        <span slot="texte">
                            Le climat change, avec pour conséquence une
                            <strong>augmentation en fréquence et en intensité des épisodes de sécheresse en France</strong>. Cela se traduit notamment
                            par la prise de plus en plus régulière d'arrêtés sécheresse par les préfets pour encadrer l'usage de l'eau. Dans ce
                            contexte, les tensions d'accès à cette ressource indispensable à la production agricole vont s'exacerber.
                        </span>
                        <img slot="illustration" src="${SECHERESSES}" width="462" height="288" loading="lazy" />
                    </c-encart-lien-vers-page>
                    <c-encart-lien-vers-page
                        titre="Le fléau des pesticides"
                        href=${ifDefined(construireUrlPageCarte(INDICATEURS.noduNormalise.id, EchelleTerritoriale.Epcis.id))}
                        positionIllustration=${POSITION_ILLUSTRATION.droite}
                        libelleBouton="Voir la carte des pesticides"
                        libelleCourtBouton="Voir la carte"
                    >
                        <span slot="texte">
                            L’utilisation massive des pesticides participe à la mort d’un nombre incalculable d’insectes, de plantes et d’autres êtres
                            vivants non nuisibles aux cultures.
                            <strong
                                >À l'échelle mondiale, on estime que moins de 0,1% des molécules toxiques employées atteignent effectivement leur
                                cible.</strong
                            >
                        </span>
                        <img slot="illustration" src="${PESTICIDES_VISUEL}" width="462" height="288" loading="lazy" />
                    </c-encart-lien-vers-page>
                </main>
            </div>
        `;
    }
}
