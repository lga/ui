import { Bouton } from '@lga/design-system/build/composants/Bouton';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';

import { PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages';
import DESCRIPTION_CRATER_VISUEL_1 from '../../ressources/illustrations/accueil/crater-visuel-1.png';
import DESCRIPTION_CRATER_VISUEL_2 from '../../ressources/illustrations/accueil/crater-visuel-2.png';
import DESCRIPTION_CRATER_VISUEL_3 from '../../ressources/illustrations/accueil/crater-visuel-3.png';

declare global {
    interface HTMLElementTagNameMap {
        'c-description-crater': DescriptionCrater;
    }
}

@customElement('c-description-crater')
export class DescriptionCrater extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            main {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                text-align: center;
            }
            section {
                display: grid;
                grid-template-rows: auto auto;
                grid-column-gap: calc(2 * var(--dsem));
                grid-auto-flow: column;
                align-items: center;
                padding-top: calc(4 * var(--dsem));
            }
            h2 {
                max-width: 600px;
                margin: calc(2 * var(--dsem));
                color: var(--couleur-primaire-sombre);
            }
            main > svg {
                fill: var(--couleur-primaire);
                margin-bottom: calc(2 * var(--dsem));
            }

            figure {
                margin: 0;
            }

            c-bouton {
                padding-top: 1.5rem;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                section {
                    grid-template-rows: auto auto auto auto auto auto;
                }
                section p {
                    max-width: 400px;
                }
            }
        `
    ];

    render() {
        return html`
            <main>
                <h2>CRATer, un outil de diagnostic au service de la transition agro-alimentaire des territoires</h2>
                <section>
                    <figure>
                        <img src="${DESCRIPTION_CRATER_VISUEL_1}" width="200" height="187" />
                    </figure>
                    <p class="texte-moyen">
                        Évaluez et comparez en un clic la résilience et la durabilité du système alimentaire de votre territoire.
                    </p>
                    <figure>
                        <img src="${DESCRIPTION_CRATER_VISUEL_2}" width="200" height="188" />
                    </figure>
                    <p class="texte-moyen">Des indicateurs simples et précis facilitent la réalisation de diagnostics territoriaux.</p>
                    <figure>
                        <img src="${DESCRIPTION_CRATER_VISUEL_3}" width="200" height="179" />
                    </figure>
                    <p class="texte-moyen">
                        Notre approche est scientifique dans le fond, pédagogique dans la forme : CRATer est un outil libre et accessible à toutes et
                        tous.
                    </p>
                </section>
                <c-bouton
                    id="button"
                    href="${PAGES_PRINCIPALES.projet.getUrl()}"
                    libelle="En savoir plus sur le projet"
                    libelleCourt="Le projet"
                    type="${Bouton.TYPE.relief}"
                    themeCouleur="${Bouton.THEME_COULEUR.accent}"
                ></c-bouton>
            </main>
        `;
    }
}
