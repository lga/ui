import '../commun/template/TemplatePageAccueil.js';
import './DescriptionCrater.js';
import './ComprendreLesEnjeux.js';
import './ZoomSur.js';
import './RechercherUnTerritoire.js';

import type { EvenementSelectionnerTerritoire } from '@lga/commun/build/composants/champ-recherche-territoire/EvenementSelectionnerTerritoire';
import { EvenementNaviguer } from '@lga/commun/build/evenements/EvenementNaviguer.js';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages.js';
import { construireUrlPageDiagnosticSynthese } from '../../configuration/pages/pages-utils.js';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-accueil': PageAccueil;
    }
}

@customElement('c-page-accueil')
export class PageAccueil extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            c-template-page-accueil {
                --couleur-fond-accueil: var(--couleur-fond-sombre);
            }

            #bandeau-news {
                background-color: var(--couleur-warning-clair);
                height: 50px;
                width: 100%;
                position: absolute;
                display: grid;
                top: 0;
            }

            #bandeau-news p {
                margin: auto;
                text-align: center;
            }

            main {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                gap: calc(var(--dsem) * 12);
                padding-bottom: calc(var(--dsem) * 8);
            }

            c-description-crater,
            c-comprendre-les-enjeux,
            c-zoom-sur {
                max-width: var(--largeur-maximum-contenu);
                margin: 0 auto;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                main {
                    gap: calc(var(--dsem) * 10);
                    padding-bottom: calc(var(--dsem) * 4);
                }

                #bandeau-news p {
                    font-size: 12px;
                }
            }
        `
    ];

    @property()
    idElementCibleScroll = '';

    render() {
        return html`
            <c-template-page-accueil idItemActifMenuPrincipal=${PAGES_PRINCIPALES.accueil.getId()} idElementCibleScroll=${this.idElementCibleScroll}>
                <main slot="contenu">
                    <div id="bandeau-news">
                        <p class="texte-grand">
                            &#128165; Visitez notre nouvelle plateforme
                            <a href="https://www.territoiresfertiles.fr">Territoires Fertiles</a> &#128165;
                        </p>
                    </div>
                    <c-rechercher-un-territoire @selectionnerTerritoire=${this.actionSelectionnerTerritoire}></c-rechercher-un-territoire>
                    <c-zoom-sur></c-zoom-sur>
                    <c-description-crater></c-description-crater>
                    <c-comprendre-les-enjeux></c-comprendre-les-enjeux>
                </main>
            </c-template-page-accueil>
        `;
    }

    private actionSelectionnerTerritoire(evt: Event) {
        this.dispatchEvent(
            new EvenementNaviguer(construireUrlPageDiagnosticSynthese((evt as EvenementSelectionnerTerritoire).detail.idTerritoireCrater))
        );
    }
}
