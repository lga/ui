import type { ModelePage } from '@lga/commun/build/modeles/pages/ModelePage';
import { creerDonneesInitialesPageSimple } from '@lga/commun/build/routeur-utils';
import { SA } from '@lga/indicateurs';
import type { Context as ContextPageJS } from 'page';

import type { DonneesPageCarte } from './pages/carte/PageCarte';
import type { DonneesPageDiagnostic, DonneesPageDiagnosticDomaine, DonneesPageDiagnosticMaillon } from './pages/diagnostic/PageDiagnostic';

export function routePageDiagnostic(page: ModelePage<DonneesPageDiagnostic>): string {
    return page.getUrl({
        idTerritoirePrincipal: ':territoire'
    });
}

export function creerDonneesInitialesPageDiagnostic(contextPageJS: ContextPageJS, chapitre: string): DonneesPageDiagnostic {
    return {
        ...creerDonneesInitialesPageSimple(contextPageJS),
        idElementCibleScroll: contextPageJS.hash,
        idTerritoirePrincipal: contextPageJS.params.territoire,
        idEchelleTerritoriale: getRequestParamValue(contextPageJS.querystring, 'echelleterritoriale')?.toUpperCase(),
        chapitre: chapitre,
        estPagePourTef: getRequestParamValue(contextPageJS.querystring, 'tef') === '' ? true : false
    };
}

export function routePageDiagnosticMaillon(page: ModelePage<DonneesPageDiagnosticMaillon>): string {
    return page.getUrl({
        idTerritoirePrincipal: ':territoire',
        idMaillon: ':maillon'
    });
}
export function creerDonneesInitialesPageDiagnosticMaillon(contextPageJS: ContextPageJS): DonneesPageDiagnosticMaillon {
    return {
        ...creerDonneesInitialesPageSimple(contextPageJS),
        ...creerDonneesInitialesPageDiagnostic(contextPageJS, contextPageJS.params.maillon),
        idMaillon: contextPageJS.params.maillon,
        nomMaillon: SA.getMaillon(contextPageJS.params.maillon)?.nom ?? ''
    };
}

export function routePageDiagnosticDomaine(page: ModelePage<DonneesPageDiagnosticDomaine>): string {
    return page.getUrl({
        idTerritoirePrincipal: ':territoire',
        idDomaine: ':domaine'
    });
}

export function creerDonneesInitialesPageDiagnosticDomaine(contextPageJS: ContextPageJS): DonneesPageDiagnosticDomaine {
    const domaine = SA.getDomaineParIdDomaineOuIdIndicateurDetaille(contextPageJS.params.domaine);
    return {
        ...creerDonneesInitialesPageSimple(contextPageJS),
        ...creerDonneesInitialesPageDiagnostic(contextPageJS, domaine?.id ?? ''),
        idDomaine: domaine?.id ?? '',
        nomDomaine: domaine?.libelle ?? ''
    };
}

export function routePageCarteIndicateur(page: ModelePage<DonneesPageCarte>): string {
    return page.getUrl({
        idIndicateur: ':indicateur',
        idEchelleTerritoriale: ':echelleTerritoriale?'
    });
}
export function creerDonneesInitialesPageCarte(contextPageJS: ContextPageJS): DonneesPageCarte {
    return {
        ...creerDonneesInitialesPageSimple(contextPageJS),
        idIndicateur: contextPageJS.params.indicateur,
        nomIndicateur: SA.getDomaine(contextPageJS.params.indicateur)?.libelle ?? '',
        idEchelleTerritoriale: contextPageJS.params.echelleterritoriale ? contextPageJS.params.echelleterritoriale.toUpperCase() : undefined,
        idTerritoirePrincipal: getRequestParamValue(contextPageJS.querystring, 'territoire')
    };
}

function getRequestParamValue(queryString: string, requestParamName: string): string | undefined {
    const params = new URLSearchParams(queryString);
    return params.has(requestParamName) ? params.get(requestParamName)! : undefined;
}
