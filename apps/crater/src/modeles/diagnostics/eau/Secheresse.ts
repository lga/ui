import type { Territoire } from '@lga/territoires';

import { AOUT, type IndicateursParAnneesMois, JUILLET } from '../IndicateursParAnneesMois';
import type { Note } from '../Note';
import { calculerMessageArretesSecheresse } from './messages-eau';

export interface SecheresseAnneeMoisN {
    anneeMois: Date;
    partTerritoireEnArretePourcent: number;
}

export class Secheresse {
    public secheressePays?: Secheresse;

    constructor(
        private territoire: Territoire,
        public readonly note: Note,
        public readonly tauxImpactArretesSecheressePourcent: number,
        public readonly indicateursParAnneesMois: IndicateursParAnneesMois<SecheresseAnneeMoisN>
    ) {}

    setSecheressePays(secheressePays: Secheresse) {
        this.secheressePays = secheressePays;
    }

    // FIXME : paramètres anneeMin et anneeMax temporaires, il faudrait qu'ils soient connus du domaine
    calculerMessageImpactArretesSecheresse(anneeMin: number, anneeMax: number): string {
        const tauxEstivalParAnnees = this.indicateursParAnneesMois.valeursMoyennesParAnnees(
            [JUILLET, AOUT],
            (i) => i.partTerritoireEnArretePourcent,
            anneeMin,
            anneeMax
        );
        return calculerMessageArretesSecheresse(
            this.territoire.nom,
            this.tauxImpactArretesSecheressePourcent,
            this.secheressePays?.tauxImpactArretesSecheressePourcent ?? null,
            tauxEstivalParAnnees ? tauxEstivalParAnnees.tendance((i) => i.valeurMoyenne) : null,
            anneeMin,
            anneeMax
        );
    }
}
