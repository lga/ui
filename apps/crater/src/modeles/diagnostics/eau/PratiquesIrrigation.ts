import {
    calculerMessageCultureLaPlusIrriguee,
    calculerMessageCulturesLesPlusIntensementIrriguees,
    calculerMessageSuperficieIrriguee
} from './messages-eau';

export interface PratiquesIrrigationCultureParametres {
    categorieCulture: string;
    culture: string;
    sauHa: number;
    sauIrrigueeHa: number;
}

export class PratiquesIrrigationCulture {
    public readonly categorieCulture: string;
    public readonly culture: string;
    public readonly sauHa: number;
    public readonly sauIrrigueeHa: number;

    constructor(
        parametres: PratiquesIrrigationCultureParametres,
        public readonly sauTotaleHa: number,
        public readonly sauTotaleIrrigueeHa: number
    ) {
        this.categorieCulture = parametres.categorieCulture;
        this.culture = parametres.culture;
        this.sauHa = parametres.sauHa;
        this.sauIrrigueeHa = parametres.sauIrrigueeHa;
    }

    get couleur(): string {
        return COULEURS_CATEGORIES_CULTURES_IRRIGATION.find((cc) => cc.categorieCulture === this.categorieCulture)?.couleur ?? '';
    }

    get sauNonIrrigueeHa(): number | null {
        return this.sauHa > 0 ? this.sauHa - this.sauIrrigueeHa : null;
    }

    get partSauIrrigueePourcent(): number | null {
        return this.sauHa > 0 ? Math.round((this.sauIrrigueeHa / this.sauHa) * 100) : null;
    }

    get poidsCultureSauTotalePourcent(): number | null {
        return this.sauTotaleHa > 0 ? Math.round((this.sauHa / this.sauTotaleHa) * 100) : null;
    }

    get poidsCultureSauIrrigueeTotalePourcent(): number | null {
        return this.sauTotaleIrrigueeHa > 0 ? Math.round((this.sauIrrigueeHa / this.sauTotaleIrrigueeHa) * 100) : null;
    }
}

export const COULEURS_CATEGORIES_CULTURES_IRRIGATION = [
    {
        categorieCulture: 'Céréales',
        couleur: '#ffba66'
    },
    {
        categorieCulture: 'Cultures industrielles',
        couleur: '#a38566'
    },
    {
        categorieCulture: 'Fourrages',
        couleur: '#ce7676'
    },
    {
        categorieCulture: 'Légumes',
        couleur: '#74a47f'
    },
    {
        categorieCulture: 'Cultures permanentes',
        couleur: '#9195d4'
    }
];

export class PratiquesIrrigation {
    public readonly pratiquesIrrigationParCultures: PratiquesIrrigationCulture[];
    public readonly pratiquesIrrigationTotal: PratiquesIrrigationCulture;

    constructor(PratiquesIrrigationCultureParametres: PratiquesIrrigationCultureParametres[]) {
        const sauTotaleHa = PratiquesIrrigationCultureParametres.reduce((a, b) => a + b.sauHa, 0);
        const sauIrrigueeTotaleHa = PratiquesIrrigationCultureParametres.reduce((a, b) => a + b.sauIrrigueeHa, 0);
        this.pratiquesIrrigationTotal = new PratiquesIrrigationCulture(
            {
                categorieCulture: '',
                culture: 'Total',
                sauHa: sauTotaleHa,
                sauIrrigueeHa: sauIrrigueeTotaleHa
            },
            sauTotaleHa,
            sauIrrigueeTotaleHa
        );

        this.pratiquesIrrigationParCultures = PratiquesIrrigationCultureParametres.map((pratiquesIrrigationCulture) => {
            return new PratiquesIrrigationCulture(pratiquesIrrigationCulture, sauTotaleHa, sauIrrigueeTotaleHa);
        });
    }

    get cultureLaPlusIrriguee(): PratiquesIrrigationCulture {
        return this.pratiquesIrrigationParCultures.reduce((a, b) =>
            (a.poidsCultureSauIrrigueeTotalePourcent ?? 0) > (b.poidsCultureSauIrrigueeTotalePourcent ?? 0) ? a : b
        );
    }

    get cultureLaPlusIntensementIrriguee(): PratiquesIrrigationCulture {
        return this.pratiquesIrrigationParCultures.sort((a, b) => {
            return (b.partSauIrrigueePourcent ?? 0) - (a.partSauIrrigueePourcent ?? 0);
        })[0];
    }

    get cultureLaDeuxiemePlusIntensementIrriguee(): PratiquesIrrigationCulture {
        return this.pratiquesIrrigationParCultures.sort((a, b) => {
            return (b.partSauIrrigueePourcent ?? 0) - (a.partSauIrrigueePourcent ?? 0);
        })[1];
    }

    get cultureTotal(): PratiquesIrrigationCulture {
        return this.pratiquesIrrigationTotal;
    }

    get messageSuperficieIrriguee() {
        return calculerMessageSuperficieIrriguee(this.cultureTotal.sauIrrigueeHa, this.cultureTotal.partSauIrrigueePourcent);
    }

    get messageCulturesLesPlusIntensementIrriguees() {
        return calculerMessageCulturesLesPlusIntensementIrriguees(
            this.cultureLaPlusIntensementIrriguee,
            this.cultureLaDeuxiemePlusIntensementIrriguee
        );
    }

    get messageCultureLaPlusIrriguee() {
        return calculerMessageCultureLaPlusIrriguee(this.cultureLaPlusIrriguee);
    }
}
