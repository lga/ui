import { formaterEvolutionPourcentString, formaterNombreSelonValeurString, type HtmlString, htmlstring, regressionLineaire } from '@lga/base';

import type { IndicateursParAnnees } from '../IndicateursParAnnees';
import type { Note } from '../Note';
import type { IrrigationIndicateursAnneeN } from './Irrigation';
import type { PratiquesIrrigationCulture } from './PratiquesIrrigation';

export function calculerMessagePrelevementsIrrigation(
    nomTerritoire: string,
    irrigationNote: Note,
    irrigationM3: number | null,
    irrigationMM: number | null,
    irrigationMMPays: number | null,
    irrigationM3ParHa: number | null,
    irrigationIndicateursParAnnees: IndicateursParAnnees<IrrigationIndicateursAnneeN>
): string {
    const fourchettes = ['haute', 'moyenne', 'basse'];

    const anneeInitiale = irrigationIndicateursParAnnees.annees.at(0)!;
    const anneeFinale = irrigationIndicateursParAnnees.annees.at(-1)!;
    const nbAnneesMoyenneGlissante = 5;

    const rl = regressionLineaire(
        irrigationIndicateursParAnnees.annees,
        irrigationIndicateursParAnnees.valeurs((i) => i.irrigationM3)
    );
    const tendance = irrigationIndicateursParAnnees.tendance((i) => i.irrigationM3);
    let tauxEvolution;
    if (rl.fn(anneeInitiale)! > 0 && rl.fn(anneeFinale)! > 0) {
        tauxEvolution = ' (' + formaterEvolutionPourcentString(Math.round((rl.fn(anneeFinale)! / rl.fn(anneeInitiale)! - 1) * 100)) + ')';
    } else {
        tauxEvolution = '';
    }

    let evolution;
    if (tendance === null) {
        evolution = null;
    } else if (tendance > 0) {
        evolution = 'en hausse' + tauxEvolution;
    } else if (tendance < 0) {
        evolution = 'en baisse' + tauxEvolution;
    } else {
        evolution = 'stables';
    }
    const message1 = `Le territoire <em>${nomTerritoire}</em> a prélevé <strong>${formaterNombreSelonValeurString(
        irrigationM3
    )} m³</strong> d’eau pour l’irrigation en moyenne sur les années ${anneeFinale - nbAnneesMoyenneGlissante + 1} à ${anneeFinale}.`;

    const message2 = ` Cela représente <strong>${formaterNombreSelonValeurString(
        irrigationM3ParHa
    )} m³ </strong> d’eau par an et par hectare de surface agricole utile productive (hors prairies) soit ${formaterNombreSelonValeurString(
        irrigationMM
    )} mm par an. Cette valeur est une moyenne toutes surfaces confondues qui permet de simplifier la comparaison entre territoires. Elle ne reflète pas la réalité de l'irrigation pratiquée sur les différentes cultures, qui est de l'ordre de 100 mm en général avec de fortes variations selon les espèces cultivées et les conditons pédo-climatiques.`;

    const message3 = `<br><br>Ces valeurs correspondent à ${formaterNombreSelonValeurString(
        irrigationMM! / irrigationMMPays!
    )} fois la moyenne de la France métropolitaine. Elles appartiennent à la <strong>fourchette ${irrigationNote.calculerLibelle(
        fourchettes
    )}</strong> des prélèvements en mm comparativement aux valeurs des départements francais.${
        evolution !== null ? `<br><br>Elles sont <em>${evolution}</em> en tendanciel entre ${anneeInitiale} et ${anneeFinale}.` : ``
    }`;

    return message1 + message2 + message3;
}

export function calculerMessageArretesSecheresse(
    nomTerritoire: string,
    tauxImpactArretesSecheressePourcent: number | null,
    tauxImpactArretesSecheressePaysPourcent: number | null,
    tendance: number | null,
    anneeMin: number,
    anneeMax: number
): string {
    let adjectifTendance;
    if (tendance === null) {
        adjectifTendance = 'inconnue';
    } else if (tendance > 0) {
        adjectifTendance = 'en hausse';
    } else if (tendance < 0) {
        adjectifTendance = 'en baisse';
    } else {
        adjectifTendance = 'stable';
    }

    let ratioTauxImpactArretesSecheresse;
    if (tauxImpactArretesSecheressePourcent === null || tauxImpactArretesSecheressePaysPourcent === null) {
        ratioTauxImpactArretesSecheresse = null;
    } else {
        ratioTauxImpactArretesSecheresse = tauxImpactArretesSecheressePourcent / tauxImpactArretesSecheressePaysPourcent;
    }

    // FIXME : voir comment on traite le cas des dates 2018-2022 => peut on les positionner en dynamique ?
    return `<p>La part du territoire <em>${nomTerritoire}</em> sous arrêté sécheresse en période estivale est en moyenne de <strong>${formaterNombreSelonValeurString(
        tauxImpactArretesSecheressePourcent
    )} %</strong> (moyenne des valeurs sur juillet-août entre 2016 et 2020).</p>
            <br>
            <p>Cette valeur correspond à ${formaterNombreSelonValeurString(
                ratioTauxImpactArretesSecheresse
            )} fois la moyenne de la France métropolitaine.</p>
            <br>
            <p>
            Elle est <em>${adjectifTendance}</em> en tendanciel entre ${anneeMin} et ${anneeMax} comme le montre le graphique suivant :
          </p>`;
}

export function calculerMessageSuperficieIrriguee(sauIrrigueeTotaleHa: number, partSauIrrigueeTotalePourcent: number | null): HtmlString {
    return htmlstring`<p>
        Le territoire a une superficie <strong>irriguée</strong> de ${formaterNombreSelonValeurString(sauIrrigueeTotaleHa)} 
        ha${partSauIrrigueeTotalePourcent ? htmlstring` soit ${partSauIrrigueeTotalePourcent}% de la surface totale` : ''}. </p>`;
}

export function calculerMessageCulturesLesPlusIntensementIrriguees(
    cultureLaPlusIntensementIrriguee: PratiquesIrrigationCulture,
    cultureLaDeuxiemePlusIntensementIrriguee: PratiquesIrrigationCulture
): HtmlString {
    return cultureLaPlusIntensementIrriguee.partSauIrrigueePourcent && cultureLaDeuxiemePlusIntensementIrriguee.partSauIrrigueePourcent
        ? htmlstring`
          <p>
            Avec <strong>${cultureLaPlusIntensementIrriguee.partSauIrrigueePourcent}% de ses surfaces qui sont irriguées</strong>, le groupe de cultures
            <strong>${cultureLaPlusIntensementIrriguee.culture}</strong> est le groupe le plus intensément irrigué en proportion de sa superficie, devant
            les <strong>${cultureLaDeuxiemePlusIntensementIrriguee.culture}</strong>
            dont la part s'élève à ${cultureLaDeuxiemePlusIntensementIrriguee.partSauIrrigueePourcent}%.
          </p>`
        : htmlstring``;
}

export function calculerMessageCultureLaPlusIrriguee(cultureLaPlusIrriguee: PratiquesIrrigationCulture): HtmlString {
    return cultureLaPlusIrriguee.poidsCultureSauTotalePourcent && cultureLaPlusIrriguee.poidsCultureSauIrrigueeTotalePourcent
        ? htmlstring`
        <p>
            Les ${cultureLaPlusIrriguee.culture}, qui représentent ${cultureLaPlusIrriguee.poidsCultureSauTotalePourcent}% des surfaces agricoles du
            territoire, représentent <strong>${cultureLaPlusIrriguee.poidsCultureSauIrrigueeTotalePourcent}% de l’ensemble des surfaces irriguées</strong>.
        </p>`
        : htmlstring``;
}
