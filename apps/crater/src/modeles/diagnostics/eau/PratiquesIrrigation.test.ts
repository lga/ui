import { describe, expect, it } from 'vitest';

import { PratiquesIrrigation, PratiquesIrrigationCulture } from './PratiquesIrrigation';

describe("Tests des classes pour les pratiques d'irrigation", () => {
    it('Test de PratiquesIrrigationCulture avec sau', () => {
        const pratiquesIrrigationCulture = new PratiquesIrrigationCulture(
            { categorieCulture: 'Céréales', culture: 'Céréales : blé tendre', sauHa: 20, sauIrrigueeHa: 1 },
            40,
            10
        );

        expect(pratiquesIrrigationCulture.couleur).toEqual('#ffba66');
        expect(pratiquesIrrigationCulture.partSauIrrigueePourcent).toEqual(5);
        expect(pratiquesIrrigationCulture.sauNonIrrigueeHa).toEqual(19);
        expect(pratiquesIrrigationCulture.poidsCultureSauTotalePourcent).toEqual(50);
        expect(pratiquesIrrigationCulture.poidsCultureSauIrrigueeTotalePourcent).toEqual(10);
    });

    it('Test de PratiquesIrrigationCulture sans sau', () => {
        const pratiquesIrrigationCulture = new PratiquesIrrigationCulture(
            { categorieCulture: 'Céréales', culture: 'Céréales : blé tendre', sauHa: 0, sauIrrigueeHa: 1 },
            0,
            0
        );

        expect(pratiquesIrrigationCulture.couleur).toEqual('#ffba66');
        expect(pratiquesIrrigationCulture.partSauIrrigueePourcent).toEqual(null);
        expect(pratiquesIrrigationCulture.sauNonIrrigueeHa).toEqual(null);
        expect(pratiquesIrrigationCulture.poidsCultureSauTotalePourcent).toEqual(null);
        expect(pratiquesIrrigationCulture.poidsCultureSauIrrigueeTotalePourcent).toEqual(null);
    });

    it('Test de PratiquesIrrigation', () => {
        const pratiquesIrrigation = new PratiquesIrrigation([
            { categorieCulture: 'Céréales', culture: 'Céréales : blé tendre', sauHa: 50, sauIrrigueeHa: 10 },
            { categorieCulture: 'Cultures industrielles', culture: 'Cultures industrielles : colza', sauHa: 10, sauIrrigueeHa: 5 },
            { categorieCulture: 'Légumes', culture: 'Légumes, pommes de terre, fleurs', sauHa: 1, sauIrrigueeHa: 1 },
            { categorieCulture: 'Fourrages', culture: 'Fourrages : autres', sauHa: 0, sauIrrigueeHa: 0 }
        ]);

        expect(pratiquesIrrigation.pratiquesIrrigationParCultures.length).toEqual(4);
        expect(pratiquesIrrigation.pratiquesIrrigationParCultures[0].couleur).toEqual('#ffba66');
        expect(pratiquesIrrigation.pratiquesIrrigationParCultures[0].partSauIrrigueePourcent).toEqual(20);
        expect(pratiquesIrrigation.pratiquesIrrigationParCultures[0].poidsCultureSauTotalePourcent).toEqual(82);
        expect(pratiquesIrrigation.pratiquesIrrigationParCultures[0].poidsCultureSauIrrigueeTotalePourcent).toEqual(63);
        expect(pratiquesIrrigation.pratiquesIrrigationTotal).toEqual({
            categorieCulture: '',
            culture: 'Total',
            sauHa: 61,
            sauIrrigueeHa: 16,
            sauTotaleHa: 61,
            sauTotaleIrrigueeHa: 16
        });
        expect(pratiquesIrrigation.cultureLaPlusIrriguee.culture).toEqual('Céréales : blé tendre');
        expect(pratiquesIrrigation.cultureLaPlusIntensementIrriguee.culture).toEqual('Légumes, pommes de terre, fleurs');
        expect(pratiquesIrrigation.cultureLaDeuxiemePlusIntensementIrriguee.culture).toEqual('Cultures industrielles : colza');
    });
});
