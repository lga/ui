import type { Territoire } from '@lga/territoires';

import type { Note } from '../Note';
import type { Irrigation } from './Irrigation';
import type { Secheresse } from './Secheresse';

export class Eau {
    constructor(
        private territoire: Territoire,
        public readonly note: Note,
        public readonly irrigation: Irrigation,
        public readonly secheresse: Secheresse
    ) {}
}
