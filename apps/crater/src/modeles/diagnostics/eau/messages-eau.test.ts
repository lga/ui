import { supprimerEspacesRedondantsEtRetoursLignes } from '@lga/base';
import { describe, expect, it } from 'vitest';

import { IndicateursParAnnees } from '../IndicateursParAnnees';
import { Note } from '../Note';
import type { IrrigationIndicateursAnneeN } from './Irrigation';
import {
    calculerMessageArretesSecheresse,
    calculerMessageCultureLaPlusIrriguee,
    calculerMessageCulturesLesPlusIntensementIrriguees,
    calculerMessagePrelevementsIrrigation,
    calculerMessageSuperficieIrriguee
} from './messages-eau';
import { PratiquesIrrigationCulture } from './PratiquesIrrigation';

describe(`Tests calculs message eau`, () => {
    it('Calculer le message irrigation', () => {
        expect(
            calculerMessagePrelevementsIrrigation(
                'nomTerritoire',
                new Note(7),
                1200,
                23,
                46,
                230,
                new IndicateursParAnnees<IrrigationIndicateursAnneeN>([
                    { annee: 2010, irrigationM3: 1000, irrigationMM: 1 },
                    { annee: 2020, irrigationM3: 2000, irrigationMM: 1.9 }
                ])
            )
        ).toEqual(
            "Le territoire <em>nomTerritoire</em> a prélevé <strong>1 200 m³</strong> d’eau pour l’irrigation en moyenne sur les années 2016 à 2020. Cela représente <strong>230 m³ </strong> d’eau par an et par hectare de surface agricole utile productive (hors prairies) soit 23 mm par an. Cette valeur est une moyenne toutes surfaces confondues qui permet de simplifier la comparaison entre territoires. Elle ne reflète pas la réalité de l'irrigation pratiquée sur les différentes cultures, qui est de l'ordre de 100 mm en général avec de fortes variations selon les espèces cultivées et les conditons pédo-climatiques.<br><br>Ces valeurs correspondent à 0,5 fois la moyenne de la France métropolitaine. Elles appartiennent à la <strong>fourchette basse</strong> des prélèvements en mm comparativement aux valeurs des départements francais.<br><br>Elles sont <em>en hausse (x2)</em> en tendanciel entre 2010 et 2020."
        );
        expect(
            calculerMessagePrelevementsIrrigation(
                'nomTerritoire',
                new Note(1),
                1500000,
                1.5,
                1,
                15,
                new IndicateursParAnnees<IrrigationIndicateursAnneeN>([
                    { annee: 2010, irrigationM3: 2000000, irrigationMM: 2 },
                    { annee: 2020, irrigationM3: 1000000, irrigationMM: 1 }
                ])
            )
        ).toEqual(
            "Le territoire <em>nomTerritoire</em> a prélevé <strong>1,5 millions m³</strong> d’eau pour l’irrigation en moyenne sur les années 2016 à 2020. Cela représente <strong>15 m³ </strong> d’eau par an et par hectare de surface agricole utile productive (hors prairies) soit 1,5 mm par an. Cette valeur est une moyenne toutes surfaces confondues qui permet de simplifier la comparaison entre territoires. Elle ne reflète pas la réalité de l'irrigation pratiquée sur les différentes cultures, qui est de l'ordre de 100 mm en général avec de fortes variations selon les espèces cultivées et les conditons pédo-climatiques.<br><br>Ces valeurs correspondent à 1,5 fois la moyenne de la France métropolitaine. Elles appartiennent à la <strong>fourchette haute</strong> des prélèvements en mm comparativement aux valeurs des départements francais.<br><br>Elles sont <em>en baisse (-50%)</em> en tendanciel entre 2010 et 2020."
        );
    });

    it('Test de calculerMessageSuperficieIrriguee', () => {
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessageSuperficieIrriguee(250, 20))).toEqual(
            '<p> Le territoire a une superficie <strong>irriguée</strong> de 250 ha soit 20% de la surface totale. </p>'
        );

        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessageSuperficieIrriguee(250, null))).toEqual(
            '<p> Le territoire a une superficie <strong>irriguée</strong> de 250 ha. </p>'
        );
    });

    it('Test de calculerMessageCulturesLesPlusIntensementIrriguees', () => {
        expect(
            supprimerEspacesRedondantsEtRetoursLignes(
                calculerMessageCulturesLesPlusIntensementIrriguees(
                    new PratiquesIrrigationCulture({ categorieCulture: '', culture: 'Culture #1', sauHa: 1000, sauIrrigueeHa: 200 }, 0, 0),
                    new PratiquesIrrigationCulture({ categorieCulture: '', culture: 'Culture #2', sauHa: 1000, sauIrrigueeHa: 100 }, 0, 0)
                )
            )
        ).toEqual(
            "<p> Avec <strong>20% de ses surfaces qui sont irriguées</strong>, le groupe de cultures <strong>Culture #1</strong> est le groupe le plus intensément irrigué en proportion de sa superficie, devant les <strong>Culture #2</strong> dont la part s'élève à 10%. </p>"
        );
        expect(
            supprimerEspacesRedondantsEtRetoursLignes(
                calculerMessageCulturesLesPlusIntensementIrriguees(
                    new PratiquesIrrigationCulture({ categorieCulture: '', culture: 'Culture #1', sauHa: 0, sauIrrigueeHa: 1000 }, 0, 0),
                    new PratiquesIrrigationCulture({ categorieCulture: '', culture: 'Culture #2', sauHa: 1000, sauIrrigueeHa: 1000 }, 0, 0)
                )
            )
        ).toEqual('');
    });

    it('Test de calculerMessageCultureLaPlusIrriguee', () => {
        expect(
            supprimerEspacesRedondantsEtRetoursLignes(
                calculerMessageCultureLaPlusIrriguee(
                    new PratiquesIrrigationCulture({ categorieCulture: '', culture: 'Culture A', sauHa: 1000, sauIrrigueeHa: 100 }, 10000, 200)
                )
            )
        ).toEqual(
            '<p> Les Culture A, qui représentent 10% des surfaces agricoles du territoire, représentent <strong>50% de l’ensemble des surfaces irriguées</strong>. </p>'
        );
        expect(
            calculerMessageCultureLaPlusIrriguee(
                new PratiquesIrrigationCulture({ categorieCulture: '', culture: 'Culture A', sauHa: 1000, sauIrrigueeHa: 100 }, 0, 0)
            )
        ).toEqual('');
    });

    it('Calculer le message secheresse', () => {
        expect(calculerMessageArretesSecheresse('nomTerritoire', 50, 100, 2, 2012, 2022)).toEqual(
            `<p>La part du territoire <em>nomTerritoire</em> sous arrêté sécheresse en période estivale est en moyenne de <strong>50 %</strong> (moyenne des valeurs sur juillet-août entre 2016 et 2020).</p>
            <br>
            <p>Cette valeur correspond à 0,5 fois la moyenne de la France métropolitaine.</p>
            <br>
            <p>
            Elle est <em>en hausse</em> en tendanciel entre 2012 et 2022 comme le montre le graphique suivant :
          </p>`
        );
    });
});
