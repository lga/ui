import type { Territoire } from '@lga/territoires';

import type { AgriculteursExploitations } from './agriculteurs-exploitations';
import type { Cheptels } from './Cheptels.js';
import type { Consommation } from './consommation';
import type { Intrants } from './intrants';
import { calculerMessageMetaDescriptionDiagnostic, calculerMessageOtex, calculerMessageTerritoire } from './messages-diagnostic';
import type { OccupationSols } from './OccupationSols';
import type { Otex } from './Otex';
import type { Production } from './production';
import type { SurfaceAgricoleUtile } from './SurfaceAgricoleUtile';
import type { TerresAgricoles } from './terres-agricoles';
import type { TransformationDistribution } from './transformation-distribution';

export class Diagnostic {
    public readonly territoire: Territoire;
    public readonly population: number | null;
    public readonly densitePopulationHabParKM2: number | null;
    public readonly idTerritoireParcel: string | null;
    public readonly nbCommunes: number;
    public readonly otex: Otex;
    public readonly occupationSols: OccupationSols;
    public readonly surfaceAgricoleUtile: SurfaceAgricoleUtile;
    public readonly cheptels: Cheptels;
    public readonly ugbParHa: number | null;
    public readonly terresAgricoles: TerresAgricoles;
    public readonly agriculteursExploitations: AgriculteursExploitations;
    public readonly intrants: Intrants;
    public readonly production: Production;
    public readonly transformationDistribution: TransformationDistribution;
    public readonly consommation: Consommation;
    private diagnosticPays?: Diagnostic;

    constructor(
        territoire: Territoire,
        population: number | null,
        densitePopulationHabParKM2: number | null,
        idTerritoireParcel: string | null,
        nbCommunes: number,
        otex: Otex,
        occupationSols: OccupationSols,
        surfaceAgricoleUtile: SurfaceAgricoleUtile,
        cheptels: Cheptels,
        ugbParHa: number | null,
        terresAgricoles: TerresAgricoles,
        agriculteursExploitations: AgriculteursExploitations,
        intrants: Intrants,
        production: Production,
        proximiteCommerces: TransformationDistribution,
        consommation: Consommation
    ) {
        this.territoire = territoire;
        this.population = population;
        this.densitePopulationHabParKM2 = densitePopulationHabParKM2;
        this.idTerritoireParcel = idTerritoireParcel;
        this.nbCommunes = nbCommunes;
        this.otex = otex;
        this.occupationSols = occupationSols;
        this.surfaceAgricoleUtile = surfaceAgricoleUtile;
        this.cheptels = cheptels;
        this.ugbParHa = ugbParHa;
        this.terresAgricoles = terresAgricoles;
        this.agriculteursExploitations = agriculteursExploitations;
        this.intrants = intrants;
        this.production = production;
        this.transformationDistribution = proximiteCommerces;
        this.consommation = consommation;
    }

    get messageTerritoire() {
        return calculerMessageTerritoire(
            this.territoire.categorie,
            this.densitePopulationHabParKM2,
            this.occupationSols.partSuperficieAgricoleClcPourcent,
            this.otex.libelleOtexMajoritaire5Postes
        );
    }

    get messageOtex() {
        return calculerMessageOtex(this.territoire, this.otex.libelleOtexMajoritaire5Postes);
    }

    get messageMetaDescription() {
        return calculerMessageMetaDescriptionDiagnostic(
            this.territoire.nom,
            this.terresAgricoles.note,
            this.agriculteursExploitations.note,
            this.intrants.note,
            this.production.note,
            this.transformationDistribution.note,
            this.consommation.note
        );
    }

    setDiagnosticPays(diagnosticPays: Diagnostic | undefined) {
        if (diagnosticPays !== undefined) {
            this.diagnosticPays = diagnosticPays;
            this.agriculteursExploitations.setAgriculteursExploitationsPays(diagnosticPays.agriculteursExploitations);
            this.intrants.setIntrantsPays(diagnosticPays.intrants);
            this.intrants.energie.setEnergiePays(diagnosticPays.intrants.energie);
            this.intrants.eau.irrigation.setIrrigationPays(diagnosticPays.intrants.eau.irrigation);
            this.intrants.eau.secheresse.setSecheressePays(diagnosticPays.intrants.eau.secheresse);
            this.production.setProductionPays(diagnosticPays.production);
            this.terresAgricoles.setTerresAgricolesPays(diagnosticPays.terresAgricoles);
            this.transformationDistribution.setTransformationDistributionPays(diagnosticPays.transformationDistribution);
            this.consommation.setConsommationPays(diagnosticPays.consommation);
        }
    }

    get rapportDensitePopulationHabParM2SurPays(): number | null {
        if (
            this.densitePopulationHabParKM2 !== null &&
            this.diagnosticPays?.densitePopulationHabParKM2 &&
            this.diagnosticPays?.densitePopulationHabParKM2 !== 0
        ) {
            return this.densitePopulationHabParKM2 / this.diagnosticPays.densitePopulationHabParKM2;
        } else {
            return null;
        }
    }
}
