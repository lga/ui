import { sommeArray } from '@lga/base';
import { describe, expect, it } from 'vitest';

import { Otex } from './Otex';

describe('Tests de la classe Otex', () => {
    it('Calculer proprietés otex', () => {
        const otex = new Otex(
            {
                grandesCultures: 1,
                maraichageHorticulture: 2,
                viticulture: 0,
                fruits: 0,
                bovinLait: 0,
                bovinViande: 0,
                bovinMixte: 0,
                ovinsCaprinsAutresHerbivores: 0,
                porcinsVolailles: 0,
                polyculturePolyelevage: 0,
                nonClassees: 0,
                sansExploitations: 0,
                nonRenseigne: 0
            },
            'VITICULTURE'
        );

        expect(sommeArray(otex.nbCommunesOtex12Postes.map((e) => e.valeur))!).toEqual(3);
        expect(otex.libelleOtexMajoritaire5Postes).toEqual('Viticulture');
    });

    it('Calculer proprietés otex, cas sans otex 5 postes majoritaire', () => {
        const otex = new Otex(
            {
                grandesCultures: 1,
                maraichageHorticulture: 1,
                viticulture: 0,
                fruits: 0,
                bovinLait: 0,
                bovinViande: 1,
                bovinMixte: 0,
                ovinsCaprinsAutresHerbivores: 0,
                porcinsVolailles: 0,
                polyculturePolyelevage: 0,
                nonClassees: 0,
                sansExploitations: 0,
                nonRenseigne: 0
            },
            'SANS_OTEX_MAJORITAIRE'
        );

        expect(otex.libelleOtexMajoritaire5Postes).toEqual('Non définie ou non majoritaire');
    });
});
