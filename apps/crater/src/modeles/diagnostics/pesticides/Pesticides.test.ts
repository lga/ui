import { describe, expect, it } from 'vitest';

import { IndicateursPesticidesAnneeN, IndicateursPesticidesParAnnees } from './Pesticides';

describe('Tests de la classe Pesticides', () => {
    const indicateursPesticides2015 = new IndicateursPesticidesAnneeN(2015, null, 10, 100, 10);
    const indicateursPesticides2019 = new IndicateursPesticidesAnneeN(2019, 180, 20, 150, 15);
    const indicateursPesticides2020 = new IndicateursPesticidesAnneeN(2020, 360, null, 200, 30);

    it('IndicateursPesticides : recupérer les données par années', () => {
        const indicateurs = new IndicateursPesticidesParAnnees([indicateursPesticides2015, indicateursPesticides2020, indicateursPesticides2019]);
        expect(indicateurs.annees).toEqual([2015, 2019, 2020]);
        expect(indicateurs.quantitesSubstancesSansDUKg).toEqual([null, 180, 360]);
        expect(indicateurs.quantitesSubstancesAvecDUKg).toEqual([10, 20, null]);
        expect(indicateurs.nodusHa).toEqual([100, 150, 200]);
        expect(indicateurs.nodusNormalises).toEqual([10, 15, 30]);

        expect(indicateurs.anneeInitiale).toEqual(indicateursPesticides2015);
        expect(indicateurs.anneeFinale).toEqual(indicateursPesticides2020);
    });

    it('IndicateursPesticides : calculer quantitesSubstancesTotalKg', () => {
        const indicateurParAnneesQSASansDUManquante = new IndicateursPesticidesParAnnees([indicateursPesticides2015, indicateursPesticides2019]);
        expect(indicateurParAnneesQSASansDUManquante.quantitesSubstancesTotalesKg).toEqual([null, 200]);

        const indicateurParAnneesQSAAvecDUManquante = new IndicateursPesticidesParAnnees([indicateursPesticides2019, indicateursPesticides2020]);
        expect(indicateurParAnneesQSAAvecDUManquante.quantitesSubstancesTotalesKg).toEqual([200, null]);
    });
});
