export const NOTE_VALEUR_WARNING = '!';
export type ValeurNote = number | null | typeof NOTE_VALEUR_WARNING;

export const CLASSE_NOTE_BASSE = 1;
export const CLASSE_NOTE_MOYENNE = 2;
export const CLASSE_NOTE_HAUTE = 3;
export type ClasseNote = null | typeof CLASSE_NOTE_BASSE | typeof CLASSE_NOTE_MOYENNE | typeof CLASSE_NOTE_HAUTE;
const SEUIL_CATEGORIE_MOYENNE = 10 / 3;
const SEUIL_CATEGORIE_HAUTE = (2 * 10) / 3;

export class Note {
    public readonly valeur: ValeurNote;

    private valeurArrondie = (valeurNote: ValeurNote) => {
        if (valeurNote === null || valeurNote === NOTE_VALEUR_WARNING) {
            return valeurNote;
        } else {
            return Math.round(valeurNote);
        }
    };

    constructor(private readonly _valeurInitiale: ValeurNote) {
        if (_valeurInitiale !== null && _valeurInitiale !== NOTE_VALEUR_WARNING && (_valeurInitiale < 0 || _valeurInitiale > 10)) {
            throw new Error(
                `ERREUR : valeur ${_valeurInitiale} non valide pour construire un objet Note (la valeur doit être null, ou ${NOTE_VALEUR_WARNING} ou comprise entre 0 et 10)`
            );
        }
        this.valeur = this.valeurArrondie(_valeurInitiale);
    }

    get valeurNumerique(): number | null {
        if (this.valeur === NOTE_VALEUR_WARNING) {
            return 5;
        }
        return this.valeur;
    }

    get classe(): ClasseNote {
        if (this.valeur === null) {
            return null;
        } else if (this.valeur === NOTE_VALEUR_WARNING) {
            return CLASSE_NOTE_MOYENNE;
        } else {
            // this.valeur est un number
            if (this.valeur < SEUIL_CATEGORIE_MOYENNE) {
                return CLASSE_NOTE_BASSE;
            } else if (this.valeur < SEUIL_CATEGORIE_HAUTE) {
                return CLASSE_NOTE_MOYENNE;
            } else {
                return CLASSE_NOTE_HAUTE;
            }
        }
    }

    calculerLibelle(libelles: string[]): string {
        return this.classe === null ? 'inconnue' : libelles[this.classe - 1];
    }
}
