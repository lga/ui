import { describe, expect, it } from 'vitest';

import { Note } from '../Note';
import {
    calculerMessageDetaillePolitiqueAmenagement,
    calculerMessagePartLogementsVacants,
    calculerMessageSyntheseTerresAgricoles,
    MessageRythmeArtificialisation,
    MessageSauParHabitant
} from './messages-terres-agricoles';
import { PolitiqueAmenagement } from './PolitiqueAmenagement';
import { SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE } from './TerresAgricoles';

describe('Tests des fonctions de calcul des messages de Terres agricoles', () => {
    it('Calculer le message de synthese', () => {
        expect(calculerMessageSyntheseTerresAgricoles(new Note(null), null, null, null, SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE)).toEqual(
            `Données non disponibles (voir les échelles supérieures telles que l'EPCI).`
        );
        expect(calculerMessageSyntheseTerresAgricoles(new Note(1), 0, 2000, 1000, SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE)).toEqual(
            `La surface agricole par habitant <strong>est trop faible</strong> mais l’objectif <abbr title="L'artificialisation est la transformation d'un sol naturel, agricole ou forestier pour l'affecter à des fonctions urbaines ou de transport (habitat, activités, commerces, infrastructures, équipements publics…). Cette transformation entraîne une imperméabilisation partielle ou totale et a des répercussions directes sur l’environnement. L'objectif Zéro Artificialisation consiste à stopper la consommation de nouveaux espaces.">Zéro Artificialisation</abbr> <strong>a été atteint entre 2013 et 2018</strong>.`
        );
        expect(calculerMessageSyntheseTerresAgricoles(new Note(1), 1500, 2000, 1000, SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE)).toEqual(
            `La surface agricole par habitant <strong>peut convenir pour un régime alimentaire moins carné</strong> et l’objectif <abbr title="L'artificialisation est la transformation d'un sol naturel, agricole ou forestier pour l'affecter à des fonctions urbaines ou de transport (habitat, activités, commerces, infrastructures, équipements publics…). Cette transformation entraîne une imperméabilisation partielle ou totale et a des répercussions directes sur l’environnement. L'objectif Zéro Artificialisation consiste à stopper la consommation de nouveaux espaces.">Zéro Artificialisation</abbr> <strong>a été atteint entre 2013 et 2018</strong>.`
        );
        expect(calculerMessageSyntheseTerresAgricoles(new Note(1), 2500, 2000, 1000, SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE)).toEqual(
            `La surface agricole par habitant <strong>est suffisante pour le régime alimentaire actuel</strong> et l’objectif <abbr title="L'artificialisation est la transformation d'un sol naturel, agricole ou forestier pour l'affecter à des fonctions urbaines ou de transport (habitat, activités, commerces, infrastructures, équipements publics…). Cette transformation entraîne une imperméabilisation partielle ou totale et a des répercussions directes sur l’environnement. L'objectif Zéro Artificialisation consiste à stopper la consommation de nouveaux espaces.">Zéro Artificialisation</abbr> <strong>a été atteint entre 2013 et 2018</strong>.`
        );
        expect(
            calculerMessageSyntheseTerresAgricoles(new Note(1), 2500, 2000, 1000, SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE + 1000)
        ).toEqual(
            `La surface agricole par habitant <strong>est suffisante pour le régime alimentaire actuel</strong> mais l’objectif <abbr title="L'artificialisation est la transformation d'un sol naturel, agricole ou forestier pour l'affecter à des fonctions urbaines ou de transport (habitat, activités, commerces, infrastructures, équipements publics…). Cette transformation entraîne une imperméabilisation partielle ou totale et a des répercussions directes sur l’environnement. L'objectif Zéro Artificialisation consiste à stopper la consommation de nouveaux espaces.">Zéro Artificialisation</abbr> <strong>n’a pas été atteint entre 2013 et 2018</strong>.`
        );
    });

    it('Calculer le message détaillé pour le domaine SAU par habitant', () => {
        expect(MessageSauParHabitant.construireMessage('NomCommune', null, null, null)).toEqual(
            `Sur le territoire <em>NomCommune</em>, l'indicateur est indisponible.`
        );
        expect(MessageSauParHabitant.construireMessage('NomCommune', 99, 2000, 1000)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageSauParHabitant.messageInferieur100
        );
        expect(MessageSauParHabitant.construireMessage('NomCommune', 500, 2000, 1000)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageSauParHabitant.messageEntre100etDemitarien
        );
        expect(MessageSauParHabitant.construireMessage('NomCommune', 1500, 2000, 1000)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageSauParHabitant.messageEntreDemitarienEtActuel
        );
        expect(MessageSauParHabitant.construireMessage('NomCommune', 2500, 2000, 1000)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageSauParHabitant.messageSuperieurActuel
        );
    });

    it('Calculer le message détaillé pour le domaine Rythme d`artificialisation', () => {
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', null, null)).toEqual(
            `Sur le territoire <em>NomCommune</em>, l'indicateur est indisponible.`
        );
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', 0, 0.1)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageRythmeArtificialisation.messageRythmeArtificialisation0
        );
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', 0.05, 0.1)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageRythmeArtificialisation.messageRythmeArtificialisation0_80
        );
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', 0.09, 0.1)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageRythmeArtificialisation.messageRythmeArtificialisation80_120
        );
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', 0.13, 0.1)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageRythmeArtificialisation.messageRythmeArtificialisation120_1
        );
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', 1.1, 0.1)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageRythmeArtificialisation.messageRythmeArtificialisationSup1
        );
    });

    it(`Calculer le message Politique d'aménagement`, () => {
        expect(calculerMessageDetaillePolitiqueAmenagement('NomCommune', PolitiqueAmenagement.DONNEES_NON_DISPONIBLES, null, null, null)).toEqual(
            "Sur le territoire <em>NomCommune</em>, l'indicateur est indisponible."
        );
        expect(
            calculerMessageDetaillePolitiqueAmenagement('NomCommune', PolitiqueAmenagement.ARTIF_QUASI_NULLE_OU_NEGATIVE, null, null, null)
        ).toEqual("Sur le territoire <em>NomCommune</em>, l'objectif Zéro Artificialisation Nette a été atteint entre 2013 et 2018.");
        expect(
            calculerMessageDetaillePolitiqueAmenagement(
                'NomCommune',
                PolitiqueAmenagement.ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE,
                100,
                null,
                9
            )
        ).toEqual(
            "Sur le territoire <em>NomCommune</em>, l'objectif Zéro Artificialisation Nette n'a pas été atteint entre 2013 et 2018 puisque 100 ha ont été artificialisés soit 9 % de la superficie totale du territoire."
        );
        expect(
            calculerMessageDetaillePolitiqueAmenagement(
                'NomCommune',
                PolitiqueAmenagement.ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE,
                100,
                null,
                9
            )
        ).toEqual(
            "Sur le territoire <em>NomCommune</em>, l'objectif Zéro Artificialisation Nette n'a pas été atteint entre 2013 et 2018 puisque 100 ha ont été artificialisés soit 9 % de la superficie totale du territoire."
        );
        expect(
            calculerMessageDetaillePolitiqueAmenagement(
                'NomCommune',
                PolitiqueAmenagement.ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE,
                100,
                -20,
                9
            )
        ).toEqual(
            "Sur le territoire <em>NomCommune</em>, l'objectif Zéro Artificialisation Nette n'a pas été atteint entre 2013 et 2018 puisque 100 ha ont été artificialisés soit 9 % de la superficie totale du territoire, alors que le territoire a perdu 20 ménages et emplois."
        );
    });

    it('Calculer le message Politique Artificialisation', () => {
        expect(calculerMessagePartLogementsVacants('NomCommune', null, 1)).toEqual(
            "Sur le territoire <em>NomCommune</em>, l'indicateur est indisponible."
        );
        expect(calculerMessagePartLogementsVacants('NomCommune', 1.1, 2)).toEqual(
            'Sur le territoire <em>NomCommune</em>, la part de logements vacants était de 2 % en 2018 (1,1 % en 2013).'
        );
    });
});
