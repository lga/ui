import type { Territoire } from '@lga/territoires';

import type { Eau } from '../eau/Eau';
import type { Energie } from '../energie/Energie.js';
import type { IndicateurSynthese } from '../IndicateurSynthese';
import type { Note } from '../Note';
import type { Pesticides } from '../pesticides';
import { calculerMessageSyntheseIntrants } from './messages-intrants';

export class Intrants implements IndicateurSynthese {
    public intrantsPays?: Intrants;
    public readonly note;

    constructor(
        private territoire: Territoire,
        note: Note,
        public readonly energie: Energie,
        public readonly eau: Eau,
        public readonly pesticides: Pesticides
    ) {
        this.note = note;
    }

    setIntrantsPays(intrantsPays: Intrants) {
        this.intrantsPays = intrantsPays;
    }

    get messageSynthese() {
        return calculerMessageSyntheseIntrants(this.energie.note, this.eau.note, this.pesticides.note);
    }
}
