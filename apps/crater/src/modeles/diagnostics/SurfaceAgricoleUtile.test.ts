import { describe, expect, it } from 'vitest';

import { SauParGroupeCulture, SurfaceAgricoleUtile } from './SurfaceAgricoleUtile';

describe('Tests de la classe SurfaceAgricoleUtile', () => {
    it('Calculer les pourcentages de SAU productive et peu productive', () => {
        const sau = new SurfaceAgricoleUtile(200, 40, 160, 20, []);

        expect(sau.pourcentageSauProductive).toEqual(80);
        expect(sau.pourcentageSauPeuProductive).toEqual(20);
    });

    it('Filtrer et trier les sau par groupe culuture', () => {
        const sau = new SurfaceAgricoleUtile(200, 40, 160, 20, [
            new SauParGroupeCulture('FOU', '', 5, []),
            new SauParGroupeCulture('CER', '', 10, []),
            new SauParGroupeCulture('SNC', '', 20, [])
        ]);

        expect(sau.sauParGroupeCultureHorsSNC).toStrictEqual([new SauParGroupeCulture('FOU', '', 5, []), new SauParGroupeCulture('CER', '', 10, [])]);
    });
});
