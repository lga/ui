import { arrondirANDecimales } from '@lga/base';
import { describe, expect, it } from 'vitest';

import { PosteEnergie } from './Energie.js';
import { calculerMessageEnergiePrincipal, calculerMessageEnergieSecondaire, convertirGJEnMWh } from './messages-energie';

describe(`Tests calculs message énergie`, () => {
    it('Calculer le message principal', () => {
        expect(calculerMessageEnergiePrincipal('nomTerritoire', 10000, 3, 30)).toEqual(
            "Le territoire <em>nomTerritoire</em> consomme 10 000 GJ d'énergie primaire par an pour les besoins de son agriculture (soit 2 800 MWh par an). Cela représente 3 GJ d'énergie primaire par hectare de surface agricole, soit <strong>0,1 fois celui de la France métropolitaine</strong>."
        );
    });

    it('Calculer le message principal si valeurs null', () => {
        expect(calculerMessageEnergiePrincipal('nomTerritoire', null, 3, 30)).toEqual(
            "Les données de consommation d'énergie ne sont pas disponibles."
        );
        expect(calculerMessageEnergiePrincipal('nomTerritoire', 2, null, 30)).toEqual(
            "Les données de consommation d'énergie ne sont pas disponibles."
        );
    });

    it('Calculer le message secondaire', () => {
        const poste = new PosteEnergie('ENGRAIS_AZOTES', 10, false, [], 20);
        expect(calculerMessageEnergieSecondaire(poste)).toEqual(
            "Le poste principal de consommation est <em>la production d'engrais azotés de synthèse</em> qui représente 50 % de la consommation totale. La répartition des différents postes est donnée dans la figure suivante :"
        );
    });

    it('Test fonction convertirGJParAnEnMW', () => {
        expect(arrondirANDecimales(convertirGJEnMWh(1) ?? 0, 3)).toEqual(0.278);
    });
});
