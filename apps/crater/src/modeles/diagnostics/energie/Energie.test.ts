import { CategorieTerritoire } from '@lga/territoires';
import { Territoire } from '@lga/territoires';
import { describe, expect, it } from 'vitest';

import { Note } from '../Note.js';
import { Energie, PosteEnergie, SourceEnergie } from './Energie.js';

describe("Tests des classes pour l'énergie'", () => {
    const sourceEnergieGaz = new SourceEnergie('GAZ', 2);
    const sourceEnergieElectricite = new SourceEnergie('ELECTRICITE', 8);
    const posteEnergieEngraisAzotes = new PosteEnergie('ENGRAIS_AZOTES', 10, false, [sourceEnergieElectricite, sourceEnergieGaz]);
    const posteEnergieTracteurs = new PosteEnergie('TRACTEURS', 10, false, [sourceEnergieElectricite, sourceEnergieGaz]);

    it('Test de SourceEnergie', () => {
        expect(sourceEnergieGaz.code).toEqual('GAZ');
        expect(sourceEnergieGaz.libelle).toEqual('gaz');
        expect(sourceEnergieGaz.energiePrimaireGJ).toEqual(2);
    });

    it('Test de PosteEnergie', () => {
        expect(posteEnergieEngraisAzotes.code).toEqual('ENGRAIS_AZOTES');
        expect(posteEnergieEngraisAzotes.libelle).toEqual("la production d'engrais azotés de synthèse");
        expect(posteEnergieEngraisAzotes.couleur).toEqual('#77229f');
        expect(posteEnergieEngraisAzotes.energiePrimaireGJ).toEqual(10);
        expect(posteEnergieEngraisAzotes.estEnergieDirecte).toEqual(false);
        expect(posteEnergieEngraisAzotes.sources![0].code).toEqual('GAZ');
    });

    it('Test de Energie', () => {
        const energie = new Energie(
            new Territoire('idTerritoire', 'nomTerritoire', CategorieTerritoire.Commune),
            new Note(2),
            'ENGRAIS_AZOTES',
            20,
            2,
            [posteEnergieEngraisAzotes, posteEnergieTracteurs]
        );

        expect(energie.postes[0].code).toEqual('TRACTEURS');
        expect(energie.postes[0].partPourcent).toEqual(50);
        expect(energie.postes[0].sources![0].partPourcent).toEqual(20);
    });
});
