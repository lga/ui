import { arrondirANDecimales, type HtmlString } from '@lga/base';
import { DEFINITIONS_POSTES_ENERGIE, DEFINITIONS_SOURCES_ENERGIE } from '@lga/indicateurs';
import type { CodePosteEnergieApi, CodeSourceEnergieApi } from '@lga/specification-api';
import type { Territoire } from '@lga/territoires';

import type { Note } from '../Note';
import { calculerMessageEnergiePrincipal, calculerMessageEnergieSecondaire } from './messages-energie.js';

export class SourceEnergie {
    public readonly libelle: string;
    public energiePrimaireTotalPosteGJ: number | null = null;

    constructor(
        public readonly code: CodeSourceEnergieApi,
        public readonly energiePrimaireGJ: number | null
    ) {
        if (!DEFINITIONS_SOURCES_ENERGIE.find((i) => i.code === this.code))
            throw new RangeError(`Valeur incorrecte : Le code source d'énergie ${this.code} ne correspond à aucun des codes de l'énumération`);
        this.libelle = DEFINITIONS_SOURCES_ENERGIE.find((s) => s.code === this.code)?.libelle ?? 'CODE_SOURCE_INCONNU';
    }

    comparerAvec(source: SourceEnergie): number {
        return (
            DEFINITIONS_SOURCES_ENERGIE.findIndex((i) => i.code === this.code) - DEFINITIONS_SOURCES_ENERGIE.findIndex((i) => i.code === source.code)
        );
    }

    get partPourcent(): number | null {
        if (this.energiePrimaireGJ === null || this.energiePrimaireTotalPosteGJ === null || this.energiePrimaireTotalPosteGJ === 0) {
            return null;
        } else {
            return arrondirANDecimales((this.energiePrimaireGJ / this.energiePrimaireTotalPosteGJ) * 100);
        }
    }
}

export class PosteEnergie {
    public readonly libelle: string;
    public readonly libelleCourt: string;
    public readonly couleur: string;

    constructor(
        public readonly code: CodePosteEnergieApi,
        public readonly energiePrimaireGJ: number | null,
        public readonly estEnergieDirecte: boolean | null,
        public readonly sources?: SourceEnergie[],
        public readonly energiePrimaireTotalGJ: number | null = null
    ) {
        if (!DEFINITIONS_POSTES_ENERGIE.find((i) => i.code === this.code))
            throw new RangeError(`Valeur incorrecte : Le code poste d'énergie ${this.code} ne correspond à aucun des codes de l'énumération`);
        this.libelle = DEFINITIONS_POSTES_ENERGIE.find((p) => p.code == this.code)?.libelle ?? 'CODE_POSTE_INCONNU';
        this.libelleCourt = DEFINITIONS_POSTES_ENERGIE.find((p) => p.code == this.code)?.libelleCourt ?? 'CODE_POSTE_INCONNU';
        this.couleur = DEFINITIONS_POSTES_ENERGIE.find((p) => p.code == this.code)?.couleur ?? 'CODE_POSTE_INCONNU';
        this.trierSourcesSelonOrdreTableauDefinitions();
        if (this.sources) this.sources.forEach((i) => (i.energiePrimaireTotalPosteGJ = this.energiePrimaireGJ));
    }

    private trierSourcesSelonOrdreTableauDefinitions() {
        this.sources?.sort((a, b) => a.comparerAvec(b));
    }

    get partPourcent(): number | null {
        if (this.energiePrimaireGJ === null || this.energiePrimaireTotalGJ === null || this.energiePrimaireTotalGJ === 0) {
            return null;
        } else {
            return arrondirANDecimales((this.energiePrimaireGJ / this.energiePrimaireTotalGJ) * 100);
        }
    }

    comparerAvec(poste: PosteEnergie): number {
        return DEFINITIONS_POSTES_ENERGIE.findIndex((i) => i.code === this.code) - DEFINITIONS_POSTES_ENERGIE.findIndex((i) => i.code === poste.code);
    }
}

export class Energie {
    public energiePays?: Energie;
    public sources?: SourceEnergie[];
    public readonly postes: PosteEnergie[];

    constructor(
        private territoire: Territoire,
        public readonly note: Note,
        public readonly codePostePrincipal: CodePosteEnergieApi,
        public readonly energiePrimaireGJ: number | null,
        public readonly energiePrimaireGJParHa: number | null,
        _postes: PosteEnergie[]
    ) {
        this.postes = _postes.map((p) => new PosteEnergie(p.code, p.energiePrimaireGJ, p.estEnergieDirecte, p.sources, this.energiePrimaireGJ));
        this.postes.sort((a, b) => a.comparerAvec(b));
    }
    setEnergiePays(energiePays: Energie) {
        this.energiePays = energiePays;
    }

    get messagePrincipal(): HtmlString {
        return calculerMessageEnergiePrincipal(
            this.territoire.nom,
            this.energiePrimaireGJ,
            this.energiePrimaireGJParHa,
            this.energiePays?.energiePrimaireGJParHa ?? undefined
        );
    }

    get postePrincipal() {
        return this.postes.find((p) => p.code === this.codePostePrincipal);
    }

    get messageSecondaire(): HtmlString {
        return calculerMessageEnergieSecondaire(this.postePrincipal);
    }
}
