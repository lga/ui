import { regressionLineaire } from '@lga/base';

interface IndicateurAnneeN {
    annee: number;
}

export class IndicateursParAnnees<I extends IndicateurAnneeN> {
    private listeIndicateursParAnnees: I[] = [];

    constructor(indicateursParAnnees: I[]) {
        this.listeIndicateursParAnnees = indicateursParAnnees.sort((i1, i2) => {
            return i1.annee - i2.annee;
        });
    }

    get annees(): number[] {
        return this.listeIndicateursParAnnees.map((i) => i.annee);
    }

    valeurs(getterAttribut: (i: I) => number | null): (number | null)[] {
        return this.listeIndicateursParAnnees.map((i) => getterAttribut(i));
    }

    tendance(getterAttribut: (i: I) => number | null): number | null {
        const rl = regressionLineaire(this.annees, this.valeurs(getterAttribut));
        return rl.a;
    }
}
