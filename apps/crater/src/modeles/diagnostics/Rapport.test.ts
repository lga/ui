import {
    CategorieTerritoire,
    creerHierarchieCommune,
    creerHierarchiePays,
    creerHierarchieRegion,
    creerHierarchieRegroupementCommunesPnr,
    Territoire
} from '@lga/territoires';
import { describe, expect, it } from 'vitest';

import { creerDiagnosticVide } from './__test__/outils-tests-diagnostic.js';
import { EtatRapport, Rapport } from './Rapport';

describe('Tests de la classe Rapport', () => {
    it('Créer un rapport vide', () => {
        // when
        const rapport = Rapport.creerVide();
        //then
        expect(rapport.getHierarchieTerritoires()).toBeUndefined();
        expect(rapport.getEtat()).toEqual(EtatRapport.VIDE);
        expect(rapport.getDiagnosticActif()).toBeUndefined();
        expect(rapport.getDiagnostics()).toEqual([]);
    });
    it('Créer un rapport en chargement', () => {
        // given
        // when
        const rapport = Rapport.creerEnChargement();
        // then
        expect(rapport.getHierarchieTerritoires()).toBeUndefined();
        expect(rapport.getEtat()).toEqual(EtatRapport.CHARGEMENT_EN_COURS);
    });
    it("Positionner la hiérarchie de territoires d'un rapport pour une commune", () => {
        // given
        const hierarchieCommune = creerHierarchieCommune();
        // when
        const rapport = Rapport.creerVide().setHierarchieTerritoires(hierarchieCommune);
        //then
        expect(rapport.getHierarchieTerritoires()).toEqual(hierarchieCommune);
        expect(rapport.getTerritoireActif()).toEqual(hierarchieCommune.territoirePrincipal);
        expect(rapport.getEtat()).toEqual(EtatRapport.CHARGEMENT_EN_COURS);
    });

    it("Mettre à jour le territoire actif d'un rapport", () => {
        //given
        const hierarchieCommune = creerHierarchieCommune();
        // when
        const rapport = Rapport.creerVide().setHierarchieTerritoires(hierarchieCommune).setTerritoireActif(hierarchieCommune.epci!.id);
        // then
        expect(rapport.getTerritoireActif()).toEqual(hierarchieCommune.epci);
    });

    it("Mettre à jour le territoire actif d'un rapport via une échelle territoriale, cas nominal", () => {
        //given
        const hierarchieCommune = creerHierarchieCommune();
        // when
        let rapport = Rapport.creerVide().setHierarchieTerritoires(hierarchieCommune).setTerritoireActifDepuisEchelleTerritoriale('EPCI');
        // then
        expect(rapport.getTerritoireActif()).toEqual(hierarchieCommune.epci);
        // when
        rapport = rapport.setTerritoireActifDepuisEchelleTerritoriale(undefined);
        // then
        expect(rapport.getTerritoireActif()).toEqual(hierarchieCommune.territoirePrincipal);
    });

    it("Mettre à jour le territoire actif d'un rapport via une échelle territoriale, cas des regroupement commune", () => {
        //given
        const hierarchiePnr = creerHierarchieRegroupementCommunesPnr();
        // when
        let rapport = Rapport.creerVide().setHierarchieTerritoires(hierarchiePnr).setTerritoireActifDepuisEchelleTerritoriale('EPCI');
        rapport = rapport.setTerritoireActifDepuisEchelleTerritoriale('REGROUPEMENT_COMMUNES');
        // then
        expect(rapport.getTerritoireActif()).toEqual(hierarchiePnr.territoirePrincipal);
    });

    it('Ajouter un diagnostic pour un territoire inconnue de la hierarchie territoire retourne une erreur', () => {
        // given
        const hierarchieCommune = creerHierarchieCommune();
        const diagnosticRegionHorsHierarchieCommune = creerDiagnosticVide(new Territoire('R-75', 'Region 75', CategorieTerritoire.Region));
        // when
        const rapport = Rapport.creerVide().setHierarchieTerritoires(hierarchieCommune);
        const ajouterDiagnosticTerritoireInconnu = () => {
            rapport.ajouterDiagnostic(diagnosticRegionHorsHierarchieCommune);
        };
        // then
        expect(ajouterDiagnosticTerritoireInconnu).toThrow(Error);
    });

    it("Faire évoluer l'état du rapport dans le cas de chargement", () => {
        // given
        const hierarchieCommune = creerHierarchieCommune();
        const diagnosticCommune = creerDiagnosticVide(hierarchieCommune.territoirePrincipal);
        const diagnosticEpci = creerDiagnosticVide(hierarchieCommune.epci!);
        const diagnosticDepartement = creerDiagnosticVide(hierarchieCommune.departement!);
        const diagnosticRegion = creerDiagnosticVide(hierarchieCommune.region!);
        const diagnosticPays = creerDiagnosticVide(hierarchieCommune.pays!);
        // when
        const rapport_etat1 = Rapport.creerVide();
        // then
        expect(rapport_etat1.getEtat()).toEqual(EtatRapport.VIDE);
        // when
        const rapport_etat2 = rapport_etat1.setHierarchieTerritoires(hierarchieCommune).ajouterDiagnostic(diagnosticCommune);
        // then
        expect(rapport_etat2.getEtat()).toEqual(EtatRapport.CHARGEMENT_EN_COURS);
        // when
        const rapport_etat3 = rapport_etat2.ajouterDiagnostic(diagnosticRegion).ajouterDiagnostic(diagnosticPays);
        // then
        expect(rapport_etat3.getEtat()).toEqual(EtatRapport.CHARGEMENT_EN_COURS);
        // when
        const rapport_etat4 = rapport_etat3.ajouterDiagnostic(diagnosticDepartement).ajouterDiagnostic(diagnosticEpci);
        // then
        expect(rapport_etat4.getDiagnosticActif()?.terresAgricoles.terresAgricolesPays).toEqual(rapport_etat4.getDiagnosticPays()?.terresAgricoles);
        expect(rapport_etat4.getDiagnosticActif()?.consommation.consommationPays).toEqual(rapport_etat4.getDiagnosticPays()?.consommation);
        expect(rapport_etat4.getEtat()).toEqual(EtatRapport.PRET);
    });

    it('Ajouter des diagnostics au rapport, puis charger une nouvelle hierarchie de territoires', () => {
        // given
        const hierarchieCommune = creerHierarchieCommune();
        const hierarchieRegionR75 = creerHierarchieRegion();
        const diagnosticCommune = creerDiagnosticVide(hierarchieCommune.territoirePrincipal);
        const diagnosticEpci = creerDiagnosticVide(hierarchieCommune.epci!);
        const diagnosticDepartement = creerDiagnosticVide(hierarchieCommune.departement!);
        const diagnosticRegionR1 = creerDiagnosticVide(hierarchieCommune.region!);
        const diagnosticPays = creerDiagnosticVide(hierarchieCommune.pays!);
        const diagnosticRegionR75 = creerDiagnosticVide(hierarchieRegionR75.territoirePrincipal);
        // when
        const rapport_etat1 = Rapport.creerVide().setHierarchieTerritoires(hierarchieCommune).ajouterDiagnostic(diagnosticCommune);
        // then
        expect(rapport_etat1.getDiagnosticActif()?.territoire.id).toEqual(diagnosticCommune.territoire.id);
        expect(rapport_etat1.getDiagnostics()).toEqual([diagnosticCommune]);
        expect(rapport_etat1.getEtat()).toEqual(EtatRapport.CHARGEMENT_EN_COURS);

        // when
        // ajout des diagnostics dans le desordre (france puis departement)
        const rapport_etat2 = rapport_etat1
            .ajouterDiagnostic(diagnosticPays)
            .ajouterDiagnostic(diagnosticDepartement)
            .ajouterDiagnostic(diagnosticEpci)
            .ajouterDiagnostic(diagnosticRegionR1);
        // then
        // la liste des diagnostics doit etre ordonnée
        expect(rapport_etat2.getDiagnostics().map((d) => d!.territoire.id)).toEqual([
            diagnosticCommune.territoire.id,
            diagnosticEpci.territoire.id,
            diagnosticDepartement.territoire.id,
            diagnosticRegionR1.territoire.id,
            diagnosticPays.territoire.id
        ]);
        expect(rapport_etat2.getDiagnosticActif()?.territoire.id).toEqual(diagnosticCommune.territoire.id);
        expect(rapport_etat2.getDiagnosticPays()).toEqual(diagnosticPays);
        expect(rapport_etat2.getEtat()).toEqual(EtatRapport.PRET);

        // when
        const rapport_etat3 = rapport_etat2.setTerritoireActif(hierarchieCommune.departement!.id);
        // then
        expect(rapport_etat3.getDiagnosticActif()).toEqual(diagnosticDepartement);

        // when : nouvelle hierarchie de territoires
        const rapport_etat4 = rapport_etat2.setHierarchieTerritoires(hierarchieRegionR75);
        // then
        expect(rapport_etat4.getTerritoireActif()).toEqual(hierarchieRegionR75.territoirePrincipal);
        expect(rapport_etat4.getDiagnosticActif()).toBeUndefined();
        expect(rapport_etat4.getDiagnosticPays()).toBeUndefined();
        expect(rapport_etat4.getEtat()).toEqual(EtatRapport.CHARGEMENT_EN_COURS);

        // when
        const rapport_etat5 = rapport_etat4.ajouterDiagnostic(diagnosticRegionR75);
        expect(rapport_etat5.getDiagnosticActif()).toEqual(diagnosticRegionR75);
        expect(rapport_etat5.getDiagnosticPays()).toBeUndefined();
    });

    it("Le rapport d'un pays doit être prêt et complet après l'ajout d'un seul diagnostic", () => {
        // given
        const hierarchiePays = creerHierarchiePays();
        const diagnosticPays = creerDiagnosticVide(hierarchiePays.territoirePrincipal);
        // when
        const rapport = Rapport.creerVide().setHierarchieTerritoires(hierarchiePays).ajouterDiagnostic(diagnosticPays);
        // then
        expect(rapport.getDiagnosticPays()).toEqual(diagnosticPays);
        expect(rapport.getDiagnosticActif()).toEqual(diagnosticPays);
        expect(rapport.getHierarchieTerritoires()?.getListeTerritoires()).toEqual([hierarchiePays.territoirePrincipal]);
    });

    it("Récupérer l'échelle territoriale", () => {
        // given
        const hierarchie = creerHierarchieRegion();
        let rapport = Rapport.creerVide().setHierarchieTerritoires(hierarchie);
        expect(rapport.getIdEchelleTerritoriale()).toBeUndefined();

        rapport = rapport.setTerritoireActif('P-FR');
        expect(rapport.getIdEchelleTerritoriale()).toEqual('PAYS');

        rapport = rapport.setTerritoireActif('R-75');
        expect(rapport.getIdEchelleTerritoriale()).toBeUndefined();
    });
});
