import { describe, expect, it } from 'vitest';

import { MessageHvn } from './MessageHvn';

describe('Tests de la classe MessageHvn', () => {
    it('Calculer le message détaillé pour le domaine HVN', () => {
        expect(MessageHvn.construireMessage('', null)).toEqual(`Le territoire <em></em> n'a pas de score HVN disponible.`);
        expect(MessageHvn.construireMessage('Nomcommune', 5)).toEqual(
            `Le territoire <em>Nomcommune</em> ${MessageHvn.messageHVNGlobalInferieurSeuil}`
        );
        expect(MessageHvn.construireMessage('Nomcommune', 20)).toEqual(
            `Le territoire <em>Nomcommune</em> ${MessageHvn.messageHVNGlobalSuperieurSeuil}`
        );
    });
});
