import { describe, expect, it } from 'vitest';

import {
    calculerMessageSyntheseAdequationProductionConsommation,
    calculerMessageSynthesePratiqueAgricoles,
    calculerMessageSyntheseProduction
} from './messages-production';

describe('Tests des fonctions de calcul des messages de Production', () => {
    it('Calculer le message de synthese Production', () => {
        expect(calculerMessageSyntheseProduction(10, 5, 2, null)).toEqual(
            `Données non disponibles (voir les échelles supérieures telles que l'EPCI)`
        );
        expect(calculerMessageSyntheseProduction(10, 5, 5, 5)).toEqual(
            `Production <strong>nettement insuffisante</strong> pour couvrir la consommation et pratiques agricoles <strong>très préjudiciables</strong> à la biodiversité.`
        );
        expect(calculerMessageSyntheseProduction(100, 100, 5, 5)).toEqual(
            `Production <strong>suffisante</strong> pour couvrir la consommation mais pratiques agricoles <strong>très préjudiciables</strong> à la biodiversité.`
        );
    });

    it('Calculer le message de synthese, partie taux adequation', () => {
        expect(calculerMessageSyntheseAdequationProductionConsommation(20, 20).message).toEqual(
            `Production <strong>nettement insuffisante</strong> pour couvrir la consommation`
        );
        expect(calculerMessageSyntheseAdequationProductionConsommation(30, 30).message).toEqual(
            `Production <strong>insuffisante</strong> pour couvrir la consommation`
        );
        expect(calculerMessageSyntheseAdequationProductionConsommation(70, 70).message).toEqual(
            `Production <strong>presque suffisante</strong> pour couvrir la consommation`
        );
        expect(calculerMessageSyntheseAdequationProductionConsommation(100, 90).message).toEqual(
            `Production élevée <strong>mais trop spécialisée</strong> pour couvrir la consommation`
        );
        expect(calculerMessageSyntheseAdequationProductionConsommation(100, 100).message).toEqual(
            `Production <strong>suffisante</strong> pour couvrir la consommation`
        );
    });

    it('Calculer le message de synthese , partie pratiques agricoles', () => {
        expect(calculerMessageSynthesePratiqueAgricoles(3, 3).message).toEqual(
            `pratiques agricoles <strong>très préjudiciables</strong> à la biodiversité.`
        );
        expect(calculerMessageSynthesePratiqueAgricoles(7, 7).message).toEqual(
            `pratiques agricoles <strong>préjudiciables</strong> à la biodiversité.`
        );
        expect(calculerMessageSynthesePratiqueAgricoles(10, 10).message).toEqual(
            `pratiques agricoles <strong>favorables</strong> à la biodiversité.`
        );
        expect(calculerMessageSynthesePratiqueAgricoles(4, 6).message).toEqual(
            `pratiques agricoles <strong>très préjudiciables</strong> à la biodiversité.`
        );
    });
});
