import { beforeEach, describe, expect, it } from 'vitest';

import { ConsommationParCulture, ConsommationParGroupeCulture, IndicateurConsommation } from './IndicateurConsommation';

describe('Tests de la classe IndicateurConsommation', () => {
    let consommation: IndicateurConsommation;

    beforeEach(() => {
        consommation = new IndicateurConsommation(200, [
            new ConsommationParGroupeCulture('CER', 'Groupe 1 surproduction', 170, 10, 200, [
                new ConsommationParCulture('CH1', 'Culture alim humains 1', false, 5),
                new ConsommationParCulture('CH2', 'Culture alim humains 2', false, 7),
                new ConsommationParCulture('CA1', 'Culture alim animaux', true, 20)
            ]),
            new ConsommationParGroupeCulture('OLP', 'Groupe 3 manque tout', 30, 10, 200, []),
            new ConsommationParGroupeCulture('FLC', 'Groupe 2 manque', 30, 10, 200, [])
        ]);
    });

    it('Récupérer les besoins par groupe culture triés', () => {
        expect(consommation.consommationsParGroupeCultures.map((b) => b.codeGroupeCulture)).toStrictEqual(['CER', 'OLP', 'FLC']);
    });

    it('Calculer le champ partDansBesoinsTotaux', () => {
        expect(consommation.consommationsParGroupeCultures[0].partDansBesoinsTotaux).toEqual(85);
    });

    it('Calculer les champs relatifs aux besoins ha', () => {
        expect(consommation.consommationHumainsHa).toEqual(12);
        expect(consommation.consommationAnimauxHa).toEqual(20);
        expect(consommation.consommationHa).toEqual(200);
        expect(consommation.pourcentageConsommationAnimauxSurConsommationTotale).toEqual(10);
    });

    it('Calculer les besoins en nourriture pour les Humains et Animaux par groupe culture, en ha', () => {
        expect(consommation.consommationsParGroupeCultures[0].besoinsHumainsHa).toEqual(12);
        expect(consommation.consommationsParGroupeCultures[0].besoinsAnimauxHa).toEqual(20);
    });

    it('Calculer la part des besoins en nourriture pour les Humains et Animaux par groupe culture, en % des besoins pour ce groupe culture', () => {
        expect(consommation.consommationsParGroupeCultures[0].partBesoinsHumainsHa).toEqual(7.1);
        expect(consommation.consommationsParGroupeCultures[0].partBesoinsAnimauxHa).toEqual(11.8);
    });

    it('Récupérer les besoins humains et animaux par groupe culture', () => {
        expect(consommation.consommationHumainsParGroupeCulture).toEqual([
            {
                consommationHa: 12,
                consommationParCulture: [
                    {
                        alimentationAnimale: false,
                        consommationHa: 5,
                        codeCulture: 'CH1',
                        nomCulture: 'Culture alim humains 1'
                    },
                    {
                        alimentationAnimale: false,
                        consommationHa: 7,
                        codeCulture: 'CH2',
                        nomCulture: 'Culture alim humains 2'
                    }
                ],
                consommationTotalTousGroupesCulturesHa: 200,
                codeGroupeCulture: 'CER',
                nomGroupeCulture: 'Groupe 1 surproduction',
                tauxAdequationBrutPourcent: 10
            }
        ]);
        expect(consommation.consommationAnimauxParGroupeCulture).toEqual([
            {
                consommationHa: 20,
                consommationParCulture: [
                    {
                        alimentationAnimale: true,
                        consommationHa: 20,
                        codeCulture: 'CA1',
                        nomCulture: 'Culture alim animaux'
                    }
                ],
                consommationTotalTousGroupesCulturesHa: 200,
                codeGroupeCulture: 'CER',
                nomGroupeCulture: 'Groupe 1 surproduction',
                tauxAdequationBrutPourcent: 10
            }
        ]);
    });
});
