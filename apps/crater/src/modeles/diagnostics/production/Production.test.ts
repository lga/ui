import { CategorieTerritoire, Territoire } from '@lga/territoires';
import { beforeEach, describe, expect, it } from 'vitest';

import { Note } from '../Note';
import { IndicateurHvn, Production } from './Production';

describe('Tests de la classe Production', () => {
    const territoire = new Territoire('C-1', 'NomCommune', CategorieTerritoire.Commune);
    const territoirePays = new Territoire('P-FR', 'NomPays', CategorieTerritoire.Pays);
    let production: Production;
    let productionSansDonnees: Production;
    let productionPays: Production;

    beforeEach(() => {
        production = new Production(territoire, new Note(5), 56, 34, 5, 5, 5, new IndicateurHvn(0, 0, 0, 0), 1, 10);

        productionPays = new Production(territoirePays, new Note(5), 56, 34, 5, 5, 5, new IndicateurHvn(0, 0, 0, 0), 1, 5);

        production.setProductionPays(productionPays);

        productionSansDonnees = new Production(territoire, new Note(5), 56, 34, 5, 5, 5, new IndicateurHvn(null, null, null, null), null, null);
    });

    it('Calculer le message quand les donnees sont indisponibles', () => {
        expect(productionSansDonnees.messageSauBio).toEqual(
            `Pour le territoire <em>${territoire.nom}</em>, la surface agricole biologique est inconnue.`
        );
    });

    it('Calculer le message quand les donnees sont disponibles', () => {
        expect(production.messageSauBio).toEqual(
            `Pour le territoire <em>${territoire.nom}</em>, la surface agricole biologique est de 1 ha ce qui représente 10 % de sa surface agricole utile productive soit 2 fois la moyenne nationale.`
        );
    });
});
