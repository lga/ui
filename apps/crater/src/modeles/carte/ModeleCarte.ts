import type { IndicateurCarte } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import type { Territoire } from '@lga/territoires';
import { EchelleTerritoriale } from '@lga/territoires';

export enum EtatCarte {
    VIDE = 'Vide',
    PRET = 'Pret'
}

export class ModeleCarte {
    private etat = EtatCarte.VIDE;

    private idTerritoirePrincipal?: string;
    private indicateurActif: IndicateurCarte;
    private echelleTerritorialeActive: EchelleTerritoriale;

    private zoomAutoSurTerritoire = false;
    private idDepartementInitialCommune?: string;

    constructor() {
        this.indicateurActif = SA.indicateursCarte[0];
        this.echelleTerritorialeActive = EchelleTerritoriale.Departements;
    }

    getEtat() {
        return this.etat;
    }

    setEtatVide() {
        this.etat = EtatCarte.VIDE;
    }
    setEtatPret() {
        this.etat = EtatCarte.PRET;
    }

    majEchelleTerritorialeActive(idEchelleTerritoriale: string): void {
        this.echelleTerritorialeActive = EchelleTerritoriale.fromId(idEchelleTerritoriale) ?? EchelleTerritoriale.Departements;
        this.idTerritoirePrincipal = undefined;
        this.desactiverZoomAutoTerritoire();
        this.idDepartementInitialCommune = undefined;
    }

    positionnerTerritoirePrincipal(territoire: Territoire, idDepartementInitialCommune?: string): void {
        this.echelleTerritorialeActive = EchelleTerritoriale.fromId(territoire.categorie.codeCategorie) ?? EchelleTerritoriale.Departements;
        this.idTerritoirePrincipal = territoire.id;
        this.activerZoomAutoTerritoire();
        this.idDepartementInitialCommune = idDepartementInitialCommune;
    }

    getEchelleTerritorialeActive(): EchelleTerritoriale {
        return this.echelleTerritorialeActive;
    }

    private activerZoomAutoTerritoire() {
        this.zoomAutoSurTerritoire = this.idTerritoirePrincipal !== undefined;
    }

    desactiverZoomAutoTerritoire() {
        this.zoomAutoSurTerritoire = false;
    }

    getIdTerritoireZoom() {
        return this.zoomAutoSurTerritoire ? this.idTerritoirePrincipal : undefined;
    }

    getIdDepartementInitialCommune() {
        return this.idDepartementInitialCommune;
    }

    majIndicateurActif(idIndicateur: string): void {
        if (SA.getIndicateurCarte(idIndicateur)) this.indicateurActif = SA.getIndicateurCarte(idIndicateur)!;
    }

    getIndicateurActif(): IndicateurCarte {
        return this.indicateurActif;
    }

    getIdTerritoirePrincipal() {
        return this.idTerritoirePrincipal;
    }
}
