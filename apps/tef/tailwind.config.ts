import type { Config } from 'tailwindcss';
import colors from 'tailwindcss/colors';

const PALETTE_PRIMAIRE = {
    50: '#F4F7F5',
    100: '#E9EFEC',
    200: '#B4C5BE',
    300: '#809C94',
    clair: '#809C94',
    400: '#4C746E',
    500: '#124F4B',
    DEFAULT: '#124F4B',
    600: '#0F4845',
    700: '#0B423F',
    800: '#083B39',
    900: '#063534',
    950: '#042F2E',
    sombre: '#042F2E'
};

const PALETTE_ACCENT = {
    clair: '#FFEFEB',
    50: '#FFEFEB',
    100: '#FFE0D6',
    200: '#FFC0AD',
    300: '#FFA185',
    400: '#FF825C',
    500: '#FF6433',
    DEFAULT: '#FF6433',
    600: '#F53900',
    700: '#B82B00',
    sombre: '#B82B00',
    800: '#7A1D00',
    900: '#3D0E00',
    950: '#1F0700'
};
const PALETTE_JAUNE = {
    50: '#FFFFED',
    100: '#FFFFDB',
    200: '#FDF3C1',
    300: '#FEEEAD',
    clair: '#FEEEAD',
    400: '#FEE99A',
    500: '#FFE487',
    DEFAULT: '#FFE487',
    600: '#FDD858',
    700: '#FACC15',
    800: '#FBC01F',
    900: '#FBB427',
    sombre: '#FBB427',
    950: '#FAA92F'
};

const PALETTE_SABLE = {
    50: '#FCFBF7',
    100: '#F8F5F0',
    200: '#E6E2DC',
    300: '#D5CFC9',
    400: '#C4BCB6',
    500: '#B3AAA4',
    DEFAULT: '#B3AAA4',
    600: '#9E9690',
    700: '#8A827C',
    800: '#776F69',
    900: '#635C57',
    950: '#514A45'
};

export default {
    // "Safelister" certaines classes pour éviter qu'elles ne soient purgées par tailwind.
    // Utile quand on construit dynamiquement des noms de classes, ou quand on injecte du contenu html (via v-html par ex)
    safelist: ['safelisted', 'not-italic'],
    theme: {
        colors: {
            // Couleurs nécessaires pour nuxtui voir https://github.com/nuxt/ui/issues/889
            transparent: 'transparent',
            current: 'currentColor',
            black: colors.black,
            white: colors.white,
            neutral: colors.neutral,
            gris: colors.gray,
            red: colors.red,
            sky: colors.sky,
            teal: colors.teal,
            // Couleurs custom
            primaire: PALETTE_PRIMAIRE,
            accent: PALETTE_ACCENT,
            jaune: PALETTE_JAUNE,
            sable: PALETTE_SABLE
        },
        extend: {
            fontFamily: {
                sans: 'font-family-open-sans',
                serif: 'font-family-eigthies-comeback'
            },
            fontSize: {
                xxs: ['10px', '16px'],
                xs: ['12px', '20px'],
                'data-xs': ['12px', { lineHeight: '20px', fontWeight: '700' }],
                sm: ['14px', '24px'],
                base: ['16px', '26px'],
                lg: ['18px', '30px'],
                'data-lg': ['18px', { lineHeight: '28px', fontWeight: '700' }],
                xl: ['20px', '32px'],
                'data-xl': ['20px', { lineHeight: '28px', fontWeight: '700' }],
                '2xl': ['24px', '36px'],
                'data-2xl': ['24px', { lineHeight: '36px', fontWeight: '700' }],
                '3xl': ['32px', '44px'],
                'data-3xl': ['32px', { lineHeight: '44px', fontWeight: '700' }],
                '4xl': ['40px', '56px'],
                '5xl': ['48px', '64px'],
                '6xl': ['60px', '84px'],
                '7xl': ['72px', '98px'],
                '8xl': ['96px', '130px'],
                '9xl': ['128px', '174px']
            },
            backgroundImage: {
                'gradient-to-dark-blue': 'linear-gradient(to bottom, rgba(17, 24, 39, 0), rgba(17, 24, 39, 1))'
            },
            boxShadow: {
                'ombre-interne-s': 'inset 0 0 8px rgba(0, 0, 0, 0.08)',
                'ombre-interne-m': 'inset 0 0 8px rgba(0, 0, 0, 0.16)',
                'ombre-externe': '0 0 4px rgba(0, 0, 0, 0.24)',
                'ombre-carte-s': '0 0 4px rgba(0, 0, 0, 0.06)',
                'ombre-carte-m': '0 0 16px rgba(0, 0, 0, 0.06)'
            },
            // On ne maitrise pas la largeur en pixel lors d'une impression (fixée par le browser en fonction du DPI qui peut varier)
            // Pour etre sûr du rendu, on assigne en direct les breakpoints à activer lors d'un print => sm à xl
            screens: {
                sm: { raw: 'screen and (min-width: 640px), print' },
                md: { raw: 'screen and (min-width: 768px), print' },
                lg: { raw: 'screen and (min-width: 1024px), print' },
                xl: { raw: 'screen and (min-width: 1280px), print' },
                '2xl': { raw: 'screen and (min-width: 1536px)' }
            },
            translate: {
                '2px': '2px'
            },
            rotate: {
                '1.5': '1.5deg'
            }
        }
    }
} as Partial<Config>;
