import { type RouteLocationRaw, type RouteParams, useRoute } from 'vue-router/auto';

export function useRouteDocumentsAImprimer(idTerritoire?: string): RouteLocationRaw<'documents-a-imprimer-idTerritoire'> {
    const route: RouteLocationRaw<'documents-a-imprimer-idTerritoire'> = {
        name: 'documents-a-imprimer-idTerritoire',
        params: { idTerritoire }
    };
    return route;
}

export function useRouteParamsDocumentsAImprimer(): RouteParams<'documents-a-imprimer-idTerritoire'> {
    return useRoute().params as RouteParams<'documents-a-imprimer-idTerritoire'>;
}
