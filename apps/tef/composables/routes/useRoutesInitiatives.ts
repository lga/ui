import { type RouteLocationRaw, type RouteParams, useRoute } from 'vue-router/auto';

export function useRouteInitiatives(idTerritoire?: string): RouteLocationRaw<'initiatives-idTerritoire'> {
    const route: RouteLocationRaw<'initiatives-idTerritoire'> = {
        name: 'initiatives-idTerritoire',
        params: { idTerritoire }
    };
    return route;
}

export function useRouteParamsInitiatives(): RouteParams<'initiatives-idTerritoire'> {
    return useRoute().params as RouteParams<'initiatives-idTerritoire'>;
}
