import type { RouteLocationRaw } from 'vue-router/auto';

export function useRouteContact(): RouteLocationRaw<'contact'> {
    const route: RouteLocationRaw<'contact'> = {
        name: 'contact'
    };
    return route;
}

export function useRouteDemandeAjoutTerritoire(): RouteLocationRaw<'demande-ajout-territoire'> {
    const route: RouteLocationRaw<'demande-ajout-territoire'> = {
        name: 'demande-ajout-territoire'
    };
    return route;
}

export function useRouteDonnezVotreAvis(): RouteLocationRaw<'formulaire-donnez-votre-avis'> {
    const route: RouteLocationRaw<'formulaire-donnez-votre-avis'> = {
        name: 'formulaire-donnez-votre-avis'
    };
    return route;
}

export function useRouteFormulaireInscriptionNewsletter(email?: string): RouteLocationRaw<'formulaire-inscription-newsletter'> {
    const route: RouteLocationRaw<'formulaire-inscription-newsletter'> = {
        name: 'formulaire-inscription-newsletter',
        ...(email ? { query: { email: email } } : {})
    };
    return route;
}
