import type { RouteLocationRaw } from 'vue-router/auto';

export function useRouteAccueil(): RouteLocationRaw<'index'> {
    const route: RouteLocationRaw<'index'> = {
        name: 'index'
    };
    return route;
}
