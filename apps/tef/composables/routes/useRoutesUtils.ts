import { type RouteLocationRaw, useRoute, useRouter } from 'vue-router/auto';

import { useSiteConfig } from '#imports';

// Nécessaire de définir ce type car le module siteConfig n'exporte pas correctement le sien
interface SiteConfig {
    url: string;
}

export function usePath(route: RouteLocationRaw) {
    const router = useRouter();
    return router.resolve(route).path;
}

export function useEstRouteFille(route: RouteLocationRaw, routeParent: RouteLocationRaw) {
    return usePath(route).startsWith(usePath(routeParent));
}

export function useEstRouteCouranteEgaleA(route: RouteLocationRaw) {
    return usePath(route) === usePath(useRoute());
}
export function useEstRouteCouranteFilleDe(route: RouteLocationRaw) {
    return useEstRouteFille(useRoute(), route);
}

export function useRouteVersUrlAbsolue(route: RouteLocationRaw): string {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    const siteConfig = useSiteConfig() as SiteConfig;
    return `${siteConfig.url}${usePath(route)}`;
}

export function usePathVersUrlAbsolue(path: string) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    const siteConfig = useSiteConfig() as SiteConfig;
    return `${siteConfig.url}${path}`;
}

interface TefParams {
    idTerritoire?: string;
    idTheme?: string;
}
export function useRoutePageCourante() {
    return useRoute();
}

export function useRouteParamsPageCourante(): TefParams {
    return useRoute().params as TefParams;
}
