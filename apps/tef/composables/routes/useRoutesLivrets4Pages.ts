import { type RouteLocationRaw, type RouteParams, useRoute } from 'vue-router/auto';

export function useRouteLivrets4Pages(idTerritoire: string): RouteLocationRaw<'livrets-4-pages-idTerritoire'> {
    return {
        name: 'livrets-4-pages-idTerritoire',
        params: { idTerritoire }
    };
}

export function useRouteParamsLivrets4Pages(): RouteParams<'livrets-4-pages-idTerritoire'> {
    return useRoute().params as RouteParams<'livrets-4-pages-idTerritoire'>;
}
