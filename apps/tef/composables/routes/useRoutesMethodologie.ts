import type { DefinitionSourceDonnee } from '@lga/indicateurs';
import type { RouteLocationRaw } from 'vue-router/auto';

export function useRouteMethodologie(): RouteLocationRaw<'methodologie'> {
    const route: RouteLocationRaw<'methodologie'> = {
        name: 'methodologie'
    };
    return route;
}

export function useRouteMethodologiePresentationGenerale(hash?: string): RouteLocationRaw<'methodologie'> {
    const route: RouteLocationRaw<'methodologie'> = {
        name: 'methodologie',
        hash: hash ? `#${hash}` : ''
    };
    return route;
}

export function useRouteMethodologieDiagnosticsFlash(): RouteLocationRaw<'methodologie-diagnostics-flash'> {
    const route: RouteLocationRaw<'methodologie-diagnostics-flash'> = {
        name: 'methodologie-diagnostics-flash'
    };
    return route;
}

export function useRouteMethodologieSourcesDonnees(source?: DefinitionSourceDonnee): RouteLocationRaw<'methodologie-sources-donnees'> {
    const route: RouteLocationRaw<'methodologie-sources-donnees'> = {
        name: 'methodologie-sources-donnees',
        hash: source ? `#${source.id}` : ''
    };
    return route;
}

export function useRouteMethodologieIndicateurs(): RouteLocationRaw<'methodologie-indicateurs'> {
    const route: RouteLocationRaw<`methodologie-indicateurs`> = {
        name: `methodologie-indicateurs`
    };
    return route;
}

export function useRouteMethodologieTerresAgricoles(): RouteLocationRaw<'methodologie-terres-agricoles'> {
    const route: RouteLocationRaw<'methodologie-terres-agricoles'> = {
        name: 'methodologie-terres-agricoles'
    };
    return route;
}

export function useRouteMethodologieAgriculteursExploitations(): RouteLocationRaw<'methodologie-agriculteurs-exploitations'> {
    const route: RouteLocationRaw<'methodologie-agriculteurs-exploitations'> = {
        name: 'methodologie-agriculteurs-exploitations'
    };
    return route;
}

export function useRouteMethodologieIntrants(idDomaine?: string): RouteLocationRaw<'methodologie-intrants'> {
    const route: RouteLocationRaw<'methodologie-intrants'> = {
        name: 'methodologie-intrants',
        hash: idDomaine ? `#${idDomaine}` : ''
    };
    return route;
}

export function useRouteMethodologieProduction(idDomaine?: string): RouteLocationRaw<'methodologie-production'> {
    const route: RouteLocationRaw<'methodologie-production'> = {
        name: 'methodologie-production',
        hash: idDomaine ? `#${idDomaine}` : ''
    };
    return route;
}

export function useRouteMethodologieTransformationDistribution(idDomaine?: string): RouteLocationRaw<'methodologie-transformation-distribution'> {
    const route: RouteLocationRaw<'methodologie-transformation-distribution'> = {
        name: 'methodologie-transformation-distribution',
        hash: idDomaine ? `#${idDomaine}` : ''
    };
    return route;
}

export function useRouteMethodologieConsommation(idDomaine?: string): RouteLocationRaw<'methodologie-consommation'> {
    const route: RouteLocationRaw<'methodologie-consommation'> = {
        name: 'methodologie-consommation',
        hash: idDomaine ? `#${idDomaine}` : ''
    };
    return route;
}
