import { type RouteLocationRaw, type RouteParams, useRoute } from 'vue-router/auto';

export function useRouteDiagnosticsFlash(idTerritoire?: string, idTheme?: string): RouteLocationRaw<'diagnostics-flash-idTerritoire'> {
    const route: RouteLocationRaw<'diagnostics-flash-idTerritoire'> = {
        name: 'diagnostics-flash-idTerritoire',
        params: { idTerritoire },
        hash: idTheme ? `#${idTheme}` : undefined
    };
    return route;
}

export function useRouteParamsDiagnosticsFlash(): RouteParams<'diagnostics-flash-idTerritoire'> {
    return useRoute().params as RouteParams<'diagnostics-flash-idTerritoire'>;
}
