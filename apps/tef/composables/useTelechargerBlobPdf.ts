import { useFetchAvecGestionErreur } from '#imports';

const URL_API_PDF_PRODUCTION = 'https://api.territoiresfertiles.fr/pdf/api/generate';
//const URL_API_PDF_PRODUCTION = 'http://localhost:3000/api/generate'; // pour tests en local

export interface ConfigPagePdf {
    largeurViewportPx: number;
    hauteurViewportPx: number;
    echelleImpression?: number;
}

export const CONFIG_PDF_SIMPLE_PAGE_A4_PAYSAGE: ConfigPagePdf = {
    largeurViewportPx: 1400,
    hauteurViewportPx: 900, // Il faut fixer un viewport suffisant pour que les images lazy soit chargées par puppeteer lors de l'autoScroll
    echelleImpression: 1
};

export const CONFIG_PDF_DOUBLE_PAGE_A4_PAYSAGE: ConfigPagePdf = {
    largeurViewportPx: 2048,
    hauteurViewportPx: 1448,
    echelleImpression: 0.8227
};

export async function useTelechargerBlobPdf(url: string, options: ConfigPagePdf): Promise<Blob> {
    return await useFetchAvecGestionErreur<Blob>(URL_API_PDF_PRODUCTION, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            url: url,
            printBackground: true,
            scrollPage: true,
            scale: options.echelleImpression ?? 1,
            viewportDimensions: `${options.largeurViewportPx}x${options.hauteurViewportPx}`
        })
    });
}
