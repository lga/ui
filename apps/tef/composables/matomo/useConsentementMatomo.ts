// Voir https://developer.matomo.org/guides/tracking-javascript-guide#optional-creating-a-custom-opt-out-form

interface TrackerMatomo {
    isUserOptedOut: () => boolean;
}

export const useRecupererStatutConsentement = (callbackSetConsentement: (consentement: boolean) => void): void => {
    window._paq?.push([
        function (this: TrackerMatomo) {
            if (this.isUserOptedOut()) {
                callbackSetConsentement(false);
            } else {
                callbackSetConsentement(true);
            }
        }
    ]);
};

export const useEnregistrerRefusConsentement = () => {
    window._paq?.push(['optUserOut']);
};

export const useEnregistrerAccordConsentement = () => {
    window._paq?.push(['forgetUserOptOut']);
};
