import { supprimerSlashFinal } from '@lga/base';

import { useRuntimeConfig } from '#imports';

export function useGetCraterUrl() {
    const config = useRuntimeConfig();
    const craterBaseUrl = config.public.craterBaseUrl;
    return supprimerSlashFinal(craterBaseUrl);
}
