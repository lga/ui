import { useFetchDiagnostic, useFetchTerritoire } from '@/composables';
import type { DonneesApi } from '~/modeles/fonctions-utils.js';

export async function useFetchDonneesApi(idTerritoire: string): Promise<DonneesApi> {
    const [territoireApi, diagnosticApi] = await Promise.all([useFetchTerritoire(idTerritoire), useFetchDiagnostic(idTerritoire)]);
    return { territoireApi: territoireApi, diagnosticApi: diagnosticApi };
}
