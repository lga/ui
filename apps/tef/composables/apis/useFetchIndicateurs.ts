import type { BoundingBoxApi, CodeCategorieTerritoireApi, IndicateurApi } from '@lga/specification-api';

import { useFetchAvecGestionErreur, useGetApiBaseUrl } from '@/composables';

export async function useFetchIndicateurs(
    bbox: BoundingBoxApi,
    codeCategorieTerritoire: CodeCategorieTerritoireApi,
    urlIndicateur: string
): Promise<IndicateurApi[]> {
    const apiBaseUrl = useGetApiBaseUrl();
    const url = `${apiBaseUrl}/crater/api/indicateurs/${urlIndicateur}?categorieTerritoire=${codeCategorieTerritoire}&longitudeMin=${bbox.longitudeMin}&longitudeMax=${bbox.longitudeMax}&latitudeMin=${bbox.latitudeMin}&latitudeMax=${bbox.latitudeMax}`;
    const options = {
        headers: {
            'Accept-Encoding': 'gzip, deflate, br',
            Accept: '*/*'
        }
    };
    return useFetchAvecGestionErreur<IndicateurApi[]>(url, options);
}
