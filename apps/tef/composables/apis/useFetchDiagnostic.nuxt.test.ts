import { describe, expect, it } from 'vitest';

// TODO : pour faire ce test, on peut mocker les endpoints
// Voir https://nuxt.com/docs/getting-started/testing#registerendpoint
// Et la note dans la doc : Note: If your requests in a component go to an external API, you can use baseURL and then make it empty using Nuxt Environment Override Config ($test) so all your requests will go to Nitro server.
// Mais pour l'instant on a une interférence avec nuxt-ssr-lit => tant que l'on n'a pas basculé le champ recherce territoire sur vue, on ne pourra pas faire de tests nuxt
describe('Tests de useFetchDiagnostic', () => {
    it('Tests retour API OK', () => {
        // TODO
        expect(1).toEqual(1);
    });
    it('Tests retour API Erreur 404', () => {
        // TODO
        expect(1).toEqual(1);
    });
    it('Tests retour API Erreur 503', () => {
        // TODO
        expect(1).toEqual(1);
    });
});
