import type { UseFetchOptions } from '#app';
import type { KeysOf, PickFrom } from '#app/composables/asyncData';
import { createError, useFetch } from '#imports';

export async function useFetchAvecGestionErreur<T>(url: string, options?: UseFetchOptions<T>): Promise<PickFrom<T, KeysOf<T>>> {
    const { data, error } = await useFetch<T>(
        url,
        // @ts-expect-error "Disable typescript qui n'arrive pas à faire la correspondante de type (par ex de "POST" vers le type déclaré pour options.method = NitroFetchRequest<R, M>)
        options
    );
    if (!data.value) {
        console.error('Erreur dans useFetchAvecGestionErreur', error.value);
        throw createError(
            error.value ?? {
                message: 'Erreur technique inconnue',
                fatal: true
            }
        );
    }
    return data.value as PickFrom<T, KeysOf<T>>;
}
