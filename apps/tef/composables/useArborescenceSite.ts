import { useRoute } from 'vue-router/auto';

import {
    useEstRouteFille,
    useGetStateTerritoire,
    usePath,
    useRouteAccueil,
    useRouteCartes,
    useRouteContact,
    useRouteDiagnostics,
    useRouteDiagnosticsFlash,
    useRouteDocumentsAImprimer,
    useRouteDonnezVotreAvis,
    useRouteFAQ,
    useRouteFormulaireInscriptionNewsletter,
    useRouteInitiatives,
    useRouteLicencesUtilisation,
    useRouteMethodologie,
    useRouteMonTerritoire,
    useRouteQuiSommesNous,
    useRouteSimulateur,
    useRouteThematiques
} from '@/composables';
import type { ItemLienPage, ItemSousMenu } from '~/modeles/navigation/ItemMenuModele';
import type { ArborescenceSite } from '~/modeles/navigation/MenuModele';
import { calculerEtatActifSousArborescence } from '~/modeles/navigation/navigation-utils';
import { THEMES } from '~/modeles/Theme';

export const useCreerArborescenceSite = (): ArborescenceSite => {
    const routeCourante = useRoute();
    const idTerritoire = useGetStateTerritoire()?.id;

    const lienPageAccueilOuMonTerritoire: ItemLienPage = idTerritoire
        ? {
              type: 'LIEN_PAGE',
              libelle: 'Mon territoire',
              route: useRouteMonTerritoire(idTerritoire),
              estActif: routeCourante.path === usePath(useRouteMonTerritoire(idTerritoire))
          }
        : {
              type: 'LIEN_PAGE',
              libelle: 'Accueil',
              route: useRouteAccueil(),
              estActif: routeCourante.path === usePath(useRouteAccueil())
          };

    const lienPageDiagnosticFlash: ItemLienPage[] = [
        {
            type: 'LIEN_PAGE',
            libelle: 'Diagnostic flash',
            route: useRouteDiagnosticsFlash(idTerritoire),
            estActif: useEstRouteFille(routeCourante, useRouteDiagnosticsFlash(idTerritoire))
        }
    ];

    const liensPagesThematiques: ItemSousMenu[] = [
        {
            type: 'LIBELLE_SANS_LIEN',
            libelle: `Thématiques`
        },
        ...THEMES.map((theme) => {
            const lienPage: ItemLienPage = {
                type: 'LIEN_PAGE',
                libelle: theme.titreCourt,
                route: useRouteThematiques(theme.id, idTerritoire),
                estActif: useEstRouteFille(routeCourante, useRouteThematiques(theme.id, idTerritoire)),
                estSousSection: true
            };
            return lienPage;
        })
    ];

    const liensPagesSousMenuComprendre = [...lienPageDiagnosticFlash, ...liensPagesThematiques];

    const lienPageDiagnostics: ItemLienPage = {
        type: 'LIEN_PAGE',
        libelle: 'Diagnostic détaillé (CRATer)',
        route: useRouteDiagnostics(idTerritoire),
        estActif: useEstRouteFille(routeCourante, useRouteDiagnostics(idTerritoire))
    };

    const lienPageCartes: ItemLienPage = {
        type: 'LIEN_PAGE',
        libelle: 'Cartes',
        route: useRouteCartes(),
        estActif: useEstRouteFille(routeCourante, useRouteCartes())
    };

    const liensPagesSousMenuEvaluer = [lienPageDiagnostics, lienPageCartes];

    const lienPageInitiatives: ItemLienPage = {
        type: 'LIEN_PAGE',
        libelle: 'Initiatives inspirantes',
        route: useRouteInitiatives(useGetStateTerritoire()?.id),
        estActif: useEstRouteFille(routeCourante, useRouteInitiatives())
    };

    const lienPageSimulateur: ItemLienPage = {
        type: 'LIEN_PAGE',
        libelle: "Simulateur d'action (PARCEL)",
        route: useRouteSimulateur(idTerritoire),
        estActif: useEstRouteFille(routeCourante, useRouteSimulateur(idTerritoire))
    };

    const lienPageSupports: ItemLienPage = {
        type: 'LIEN_PAGE',
        libelle: 'Documents à imprimer ou partager',
        route: useRouteDocumentsAImprimer(idTerritoire),
        estActif: useEstRouteFille(routeCourante, useRouteDocumentsAImprimer(idTerritoire))
    };

    const liensPagesSousMenuAgir: ItemLienPage[] = [lienPageInitiatives, lienPageSimulateur, lienPageSupports];

    const liensPagesSousMenuAPropos: ItemLienPage[] = [
        {
            type: 'LIEN_PAGE',
            libelle: 'Qui sommes-nous ?',
            route: useRouteQuiSommesNous(),
            estActif: useEstRouteFille(routeCourante, useRouteQuiSommesNous())
        },
        {
            type: 'LIEN_PAGE',
            libelle: "Licences d'utilisation",
            route: useRouteLicencesUtilisation(),
            estActif: useEstRouteFille(routeCourante, useRouteLicencesUtilisation())
        },
        {
            type: 'LIEN_PAGE',
            libelle: 'Méthodologie',
            route: useRouteMethodologie(),
            estActif: useEstRouteFille(routeCourante, useRouteMethodologie())
        },
        {
            type: 'LIEN_PAGE',
            libelle: 'Contact',
            route: useRouteContact(),
            estActif: useEstRouteFille(routeCourante, useRouteContact())
        },
        {
            type: 'LIEN_PAGE',
            libelle: 'Donnez votre avis',
            route: useRouteDonnezVotreAvis(),
            estActif: useEstRouteFille(routeCourante, useRouteDonnezVotreAvis())
        },
        {
            type: 'LIEN_PAGE',
            libelle: 'Newsletter',
            route: useRouteFormulaireInscriptionNewsletter(),
            estActif: useEstRouteFille(routeCourante, useRouteFormulaireInscriptionNewsletter())
        },
        {
            type: 'LIEN_PAGE',
            libelle: 'Foire aux questions',
            route: useRouteFAQ(),
            estActif: useEstRouteFille(routeCourante, useRouteFAQ())
        }
    ];

    const arborescenceSite: ArborescenceSite = [
        lienPageAccueilOuMonTerritoire,
        {
            type: 'SOUS_MENU',
            libelle: 'Comprendre',
            sousItems: liensPagesSousMenuComprendre,
            estActif: calculerEtatActifSousArborescence(liensPagesSousMenuComprendre)
        },
        {
            type: 'SOUS_MENU',
            libelle: 'Évaluer',
            sousItems: liensPagesSousMenuEvaluer,
            estActif: calculerEtatActifSousArborescence(liensPagesSousMenuEvaluer)
        },
        {
            type: 'SOUS_MENU',
            libelle: 'Agir',
            sousItems: liensPagesSousMenuAgir,
            estActif: calculerEtatActifSousArborescence(liensPagesSousMenuAgir)
        },
        {
            type: 'SOUS_MENU',
            libelle: 'À propos',
            estActif: calculerEtatActifSousArborescence(liensPagesSousMenuAPropos),
            sousItems: liensPagesSousMenuAPropos
        }
    ];
    return arborescenceSite;
};
