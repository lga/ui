import { type Component, defineAsyncComponent } from 'vue';

import type { NomComposantInfographie } from '@/modeles/constats/InfographieModele';

const AsyncInfographieImageMessage = defineAsyncComponent<Component>(
    () => import('@/components/diagnostics/infographies/InfographieImageMessage.vue')
);
const AsyncInfographieImageChiffreCle = defineAsyncComponent<Component>(
    () => import('@/components/diagnostics/infographies/InfographieImageChiffreCle.vue')
);
const AsyncInfographieDoubleImagesChiffresCles = defineAsyncComponent<Component>(
    () => import('@/components/diagnostics/infographies/InfographieDoubleImagesChiffresCles.vue')
);
const AsyncInfographieEvolutionPopulationAgricole = defineAsyncComponent<Component>(
    () => import('@/components/diagnostics/infographies/InfographieEvolutionPopulationAgricole.vue')
);
const AsyncInfographieCartePrecariteAlimentaire = defineAsyncComponent<Component>(
    () => import('@/components/diagnostics/infographies/InfographieCartePrecariteAlimentaire.vue')
);
const AsyncInfographieCarteHVN = defineAsyncComponent<Component>(() => import('@/components/diagnostics/infographies/InfographieCarteHVN.vue'));
const AsyncInfographieAnalogueClimatique = defineAsyncComponent<Component>(
    () => import('@/components/diagnostics/infographies/InfographieAnalogueClimatique.vue')
);
const InfographieSurfacesAgricolesProductionConsommation = defineAsyncComponent<Component>(
    () => import('@/components/diagnostics/infographies/InfographieSurfacesAgricolesProductionConsommation.vue')
);

export function useImporterComposantInfographie(nomComposant: NomComposantInfographie) {
    switch (nomComposant) {
        case 'InfographieSurfacesAgricolesProductionConsommation':
            return InfographieSurfacesAgricolesProductionConsommation;
        case 'InfographieCarteHVN':
            return AsyncInfographieCarteHVN;
        case 'InfographieCartePrecariteAlimentaire':
            return AsyncInfographieCartePrecariteAlimentaire;
        case 'InfographieEvolutionPopulationAgricole':
            return AsyncInfographieEvolutionPopulationAgricole;
        case 'InfographieDoubleImagesChiffresCles':
            return AsyncInfographieDoubleImagesChiffresCles;
        case 'InfographieImageChiffreCle':
            return AsyncInfographieImageChiffreCle;
        case 'InfographieImageMessage':
            return AsyncInfographieImageMessage;
        case 'InfographieAnalogueClimatique':
            return AsyncInfographieAnalogueClimatique;
        default:
            return AsyncInfographieImageMessage;
    }
}
