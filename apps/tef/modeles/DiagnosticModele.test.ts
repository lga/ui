import { describe, expect, it } from 'vitest';

import { creerConstatAgriculteurs, creerConstatRessources, creerConstatTerresAgricoles } from './DiagnosticModele.js';
import { creerDonneesApi } from './tests-utils';

describe('Tests création diagnostic', () => {
    it('Test du choix du constat pour le theme Terres agricoles', () => {
        let constat = creerConstatTerresAgricoles(
            creerDonneesApi({
                codeProfilDiagnostic: 'DEFAUT',
                tauxAdequationAssietteActuelleBrutPourcent: null
            })
        );
        expect(constat.id).toEqual('absence-donnees');

        constat = creerConstatTerresAgricoles(
            creerDonneesApi({
                codeProfilDiagnostic: 'DEFAUT',
                tauxAdequationAssietteActuelleBrutPourcent: 84,
                tauxAdequationAssietteDemitarienneBrutPourcent: 84
            })
        );
        expect(constat.id).toEqual('terres-insuffisantes');

        constat = creerConstatTerresAgricoles(
            creerDonneesApi({
                codeProfilDiagnostic: 'DEFAUT',
                tauxAdequationAssietteActuelleBrutPourcent: 84,
                tauxAdequationAssietteDemitarienneBrutPourcent: 85
            })
        );
        expect(constat.id).toEqual('terres-partiellement-suffisantes');

        constat = creerConstatTerresAgricoles(
            creerDonneesApi({
                codeProfilDiagnostic: 'DEFAUT',
                tauxAdequationAssietteActuelleBrutPourcent: 100,
                tauxAdequationAssietteDemitarienneBrutPourcent: 85
            })
        );
        expect(constat.id).toEqual('terres-suffisantes');
    });

    it('Test du choix du constat pour le theme Agriculteurs', () => {
        let constat = creerConstatAgriculteurs(
            creerDonneesApi({
                codeProfilDiagnostic: 'URBAIN',
                populationAgricole1988: 100,
                populationAgricole2010: 10
            })
        );
        expect(constat.id).toEqual('evolution-population-agricole');

        constat = creerConstatAgriculteurs(
            creerDonneesApi({
                codeProfilDiagnostic: 'ELEVAGE_INTENSIF',
                populationAgricole1988: 150,
                populationAgricole2010: null
            })
        );
        expect(constat.id).toEqual('revenus');
    });

    it('Test du choix du constat pour le theme Ressources', () => {
        let constat = creerConstatRessources(
            creerDonneesApi({
                codeProfilDiagnostic: 'URBAIN'
            })
        );
        expect(constat.id).toEqual('dependances-ressources');

        constat = creerConstatRessources(
            creerDonneesApi({
                codeProfilDiagnostic: 'ELEVAGE_INTENSIF'
            })
        );
        expect(constat.id).toEqual('alimentation-animale-importee');

        constat = creerConstatRessources(
            creerDonneesApi({
                codeProfilDiagnostic: 'VITICULTURE',
                irrigationM32012: 100,
                irrigationM32022: 119
            })
        );
        expect(constat.id).toEqual('dependances-ressources');

        constat = creerConstatRessources(
            creerDonneesApi({
                codeProfilDiagnostic: 'VITICULTURE',
                irrigationM32012: 100,
                irrigationM32022: 120
            })
        );
        expect(constat.id).toEqual('prelevements-eau');

        constat = creerConstatRessources(
            creerDonneesApi({
                codeProfilDiagnostic: 'GRANDES_CULTURES',
                irrigationM32012: 100,
                irrigationM32022: 149
            })
        );
        expect(constat.id).toEqual('dependances-ressources');

        constat = creerConstatRessources(
            creerDonneesApi({
                codeProfilDiagnostic: 'GRANDES_CULTURES',
                irrigationM32012: 100,
                irrigationM32022: 150
            })
        );
        expect(constat.id).toEqual('prelevements-eau');

        constat = creerConstatRessources(
            creerDonneesApi({
                codeProfilDiagnostic: 'FRUITS_ET_LEGUMES',
                irrigationM32012: 100,
                irrigationM32022: 119
            })
        );
        expect(constat.id).toEqual('dependances-ressources');
        constat = creerConstatRessources(
            creerDonneesApi({
                codeProfilDiagnostic: 'FRUITS_ET_LEGUMES',
                irrigationM32012: 100,
                irrigationM32022: 120
            })
        );
        expect(constat.id).toEqual('prelevements-eau');

        constat = creerConstatRessources(
            creerDonneesApi({
                codeProfilDiagnostic: 'DEFAUT',
                irrigationM32012: 100,
                irrigationM32022: 199
            })
        );
        expect(constat.id).toEqual('dependances-ressources');
        constat = creerConstatRessources(
            creerDonneesApi({
                codeProfilDiagnostic: 'DEFAUT',
                irrigationM32012: 100,
                irrigationM32022: 200
            })
        );
        expect(constat.id).toEqual('prelevements-eau');
    });
});
