import { describe, expect, it } from 'vitest';

import { calculerMessageTitreEtTexteConstatDistances } from './constats-autonomie-alimentaire.js';

describe('Tests constats Autonomie alimentaire', () => {
    it('Test de la fonction calculerMessageTitreEtTexteConstatDistances', () => {
        expect(calculerMessageTitreEtTexteConstatDistances('URBAIN').titre).toEqual(
            'L’essentiel de la nourriture consommée a parcouru de longues distances.'
        );
        expect(calculerMessageTitreEtTexteConstatDistances('VITICULTURE').titre).toEqual('Votre territoire ne consomme pas ce qu’il produit.');
    });
});
