import { describe, expect, it } from 'vitest';

import {
    calculerDonneesEvolutionPopulationAgricole,
    calculerMessageTexteConstatEvolutionPopulationAgricole,
    calculerMessageTitreConstatEvolutionPopulationAgricole,
    calculerQuestionConstatPopulationAgricole
} from './constats-agriculteurs.js';

describe('Tests constats Agriculteurs', () => {
    it('Test fonction creerConstatEvolutionPopulationAgricole', () => {
        expect(calculerMessageTitreConstatEvolutionPopulationAgricole(-20)).toEqual('La population agricole est en baisse.');
        expect(calculerMessageTitreConstatEvolutionPopulationAgricole(10)).toEqual('La population agricole est en hausse !');
    });

    it('Test fonction creerConstatEvolutionPopulationAgricole', () => {
        expect(calculerMessageTexteConstatEvolutionPopulationAgricole(-100, 'VITICULTURE')).toEqual(
            'Revenus faibles au vu du travail fourni et investissements très lourds n’incitent pas à se lancer. La spécialisation viticole fait en plus grimper le prix des terres. Difficile dans ces conditions de s’installer avec des projets visant à nourrir la population locale.'
        );
        expect(calculerMessageTexteConstatEvolutionPopulationAgricole(-100, 'URBAIN')).toEqual(
            'Revenus faibles au vu du travail fourni et investissements très lourds n’incitent pas à se lancer. En l’absence de candidats à qui transmettre sa ferme, ce sont souvent les voisins qui récupèrent les terres. Les exploitations s’agrandissent et deviennent encore plus difficiles à transmettre. Un parfait cercle vicieux.'
        );
        expect(calculerMessageTexteConstatEvolutionPopulationAgricole(10, 'VITICULTURE')).toEqual(
            'Votre territoire fait figure d’exception en France où la tendance est à une forte baisse de la population agricole.'
        );
    });

    it('Test fonction calculerQuestionConstatPopulationAgricole', () => {
        expect(calculerQuestionConstatPopulationAgricole(-20)).toEqual('Et si on facilitait l’installation des agriculteurs qui nous nourrissent ?');
        expect(calculerQuestionConstatPopulationAgricole(10)).toEqual('Et si on allait encore plus loin ?');
    });

    it('Test fonction calculerDonneesEvolutionPopulationAgricole', () => {
        const donnees = calculerDonneesEvolutionPopulationAgricole(100, 200);
        expect(donnees.annees).toEqual([1988, 2010, 2040]);
        expect(donnees.nombreAgriculteurs).toEqual([100, 200, 515]);
    });
});
