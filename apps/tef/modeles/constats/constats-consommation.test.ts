import { describe, expect, it } from 'vitest';

import { creerDonneesApi } from '../tests-utils';
import { creerConstatRegimeAlimentaireTropCarne } from './constats-consommation';

describe('Tests constats Consommation', () => {
    it('Test de creerConstatRegimeAlimentaireTropCarne, cas Absence de données', () => {
        const constat = creerConstatRegimeAlimentaireTropCarne(
            creerDonneesApi({
                tauxAdequationAssietteActuelleBrutPourcent: null,
                tauxAdequationAssietteDemitarienneBrutPourcent: null
            })
        );
        expect(constat.id).toEqual('absence-donnees');
    });

    it('Test de creerConstatRegimeAlimentaireTropCarne, cas Terres insuffisantes', () => {
        const constat = creerConstatRegimeAlimentaireTropCarne(
            creerDonneesApi({
                tauxAdequationAssietteActuelleBrutPourcent: 50,
                tauxAdequationAssietteDemitarienneBrutPourcent: 84,
                besoinsHaAssietteActuelle: 1000
            })
        );
        expect(constat.id).toEqual('regime-alimentaire-trop-carne');
        expect(constat.message.titre).toEqual('Notre consommation de viande menace notre souveraineté alimentaire.');
        expect(constat.message.texte).toEqual(
            'Consommer moins de viande permet de nettement diminuer la surface agricole nécessaire pour nous nourrir. Or, d’importantes baisses de production sont prévisibles avec le changement climatique. C’est donc un levier majeur pour construire la sécurité alimentaire de demain.'
        );
        expect(constat.infographie?.message).toEqual('1 000 hectares nécessaires pour nourrir les habitants, dont 85% pour l’élevage');
    });

    it('Test de creerConstatRegimeAlimentaireTropCarne, cas Terres partiellement suffisantes', () => {
        const constat = creerConstatRegimeAlimentaireTropCarne(
            creerDonneesApi({
                tauxAdequationAssietteActuelleBrutPourcent: 50,
                tauxAdequationAssietteDemitarienneBrutPourcent: 85,
                besoinsHaAssietteActuelle: 1000
            })
        );
        expect(constat.id).toEqual('regime-alimentaire-trop-carne');
        expect(constat.message.titre).toEqual(
            'Bonne nouvelle ! Le territoire pourrait nourrir tous les habitants si on réduisait la consommation de viande.'
        );
        expect(constat.message.texte).toEqual(
            'Consommer moins de viande permet quasiment de doubler votre capacité nourricière ! Et en bonus, de supprimer les importations de viande et de nettement baisser votre impact environnemental.'
        );
        expect(constat.infographie?.message).toEqual('1 000 hectares nécessaires pour nourrir les habitants, dont 85% pour l’élevage');
    });

    it('Test de creerConstatRegimeAlimentaireTropCarne, cas Terres suffisantes', () => {
        const constat = creerConstatRegimeAlimentaireTropCarne(
            creerDonneesApi({
                tauxAdequationAssietteActuelleBrutPourcent: 85,
                tauxAdequationAssietteDemitarienneBrutPourcent: 99,
                besoinsHaAssietteActuelle: 1000
            })
        );
        expect(constat.id).toEqual('regime-alimentaire-trop-carne');
        expect(constat.message.titre).toEqual('Notre consommation de viande menace notre souveraineté alimentaire.');
        expect(constat.message.texte).toEqual(
            "D’importantes baisses de production sont prévisibles avec le changement climatique. Or, consommer moins de viande permet de doubler votre capacité nourricière ET de diminuer votre impact sur le climat ! C'est un levier majeur de la sécurité alimentaire de demain."
        );
        expect(constat.infographie?.message).toEqual('1 000 hectares nécessaires pour nourrir les habitants, dont 85% pour l’élevage');
    });
});
