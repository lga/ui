import type { AnaloguesClimatiquesApi } from '@lga/specification-api';
import { describe, expect, it } from 'vitest';

import { creerDonneesApi } from '../tests-utils';
import { creerConstatAnalogueClimatique } from './constats-climat';
import type { InfographieAnalogueClimatiqueModele } from './InfographieModele.js';
const analoguesClimatiques: AnaloguesClimatiquesApi = {
    communeReference: {
        nom: 'Paris',
        idCrater: 'C-75001',
        longitude: 2.3522,
        latitude: 48.8566
    },
    communeAnalogueClimatique2000_2050: {
        nom: 'Montpellier',
        longitude: -0.1276,
        latitude: 51.5072,
        pays: 'France'
    }
};
describe('Tests constats Climat', () => {
    it('Test de creerConstatAnalogueClimatique, profil defaut', () => {
        const constat = creerConstatAnalogueClimatique(
            creerDonneesApi({
                codeProfilDiagnostic: 'DEFAUT',
                analoguesClimatiques: analoguesClimatiques
            })
        );
        expect(constat.message.texte).toContain('et certains choix de cultures remis en question');
        expect(constat.infographie?.message ?? '').toContain("temps à Paris en 2050 qu'à Montpellier");
        expect(constat.infographie?.nomComposant).toEqual('InfographieAnalogueClimatique');
        const infographieAnalogueClimatique = constat.infographie as InfographieAnalogueClimatiqueModele;
        expect(infographieAnalogueClimatique.configFondCarte.url).toEqual('/analogues-climatiques-fonds-cartes/france.svg');
        expect(infographieAnalogueClimatique.pointsAnaloguesClimatiques.communeReference.x).toEqual(261845.70624393807);
        expect(infographieAnalogueClimatique.pointsAnaloguesClimatiques.communeReference.y).toEqual(6250564.349543127);
        expect(infographieAnalogueClimatique.coinsBBox.nordEst.x).toEqual(1424889.4821539018);
        expect(infographieAnalogueClimatique.coinsBBox.nordEst.y).toEqual(6717375.116051745);
    });

    it('Test de creerConstatAnalogueClimatique, profil defaut', () => {
        const constat = creerConstatAnalogueClimatique(
            creerDonneesApi({
                codeProfilDiagnostic: 'VITICULTURE',
                analoguesClimatiques: analoguesClimatiques
            })
        );
        expect(constat.message.texte).toContain('et la teneur en alcool du vin va devenir difficile à maîtriser');
    });
});
