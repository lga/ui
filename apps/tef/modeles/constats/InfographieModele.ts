import type { HtmlString, ImageModele } from '@lga/base';
import type { TerritoireApi } from '@lga/specification-api';

import type { DonneesTreemapCultures } from '~/components/charts/ChartTreemapCultures.vue';

import type { LibelleBoutonInfoMethodologie } from './BoutonInfoMethodologieModele.js';
import type { CoinsBBoxMercator, ConfigFondCarteAnalogueClimatiqueModele, PointsAnaloguesClimatiquesMercator } from './constats-climat';

export type NomComposantInfographie =
    | 'InfographieSurfacesAgricolesProductionConsommation'
    | 'InfographieAnalogueClimatique'
    | 'InfographieAssiette'
    | 'InfographieCartePrecariteAlimentaire'
    | 'InfographieEvolutionPopulationAgricole'
    | 'InfographieCarteHVN'
    | 'InfographieDoubleImagesChiffresCles'
    | 'InfographieImageChiffreCle'
    | 'InfographieImageMessage';

export interface InformationsMethodologieInfographieModele {
    titre: string;
    descriptif: HtmlString | string;
    descriptifDetaille?: HtmlString | string;
    estPerimetreNational?: boolean;
    libelleBouton?: LibelleBoutonInfoMethodologie;
    libelleLienComplementInformation?: string;
    urlComplementInformation?: string;
}

export interface ChiffreCleInfographieModele {
    chiffre?: string;
    unite?: string;
    phrase: string;
}

export interface ImageInfographieModele extends ImageModele {
    position?: 'centre' | 'gauche';
    styleBords?: 'arrondi' | 'carre';
    largeurPrefereePx?: number;
    espacerImageEtMessage?: boolean;
}

export interface InfographieBaseModele {
    nomComposant: NomComposantInfographie;
    informationsMethodologie: InformationsMethodologieInfographieModele;
    accroche?: string;
    variantes?: InfographieModele[];
}

export interface InfographieImageMessageModele extends InfographieBaseModele {
    image: ImageInfographieModele;
    message?: HtmlString | string;
    attributionHtml?: string;
}

export interface InfographieImageChiffreCleModele extends InfographieBaseModele {
    image: ImageInfographieModele;
    chiffreCle?: ChiffreCleInfographieModele;
    theme?: 'orange' | 'jaune';
}

export interface InfographieDoubleImagesChiffresCles extends InfographieBaseModele {
    libelleBoutonInfographie1: string;
    infographie1: InfographieImageChiffreCleModele;
    libelleBoutonInfographie2: string;
    infographie2: InfographieImageChiffreCleModele;
}

export interface InfographieSurfacesAgricolesProductionConsommationModele extends InfographieBaseModele {
    donneesTreemapCulturesProduction: DonneesTreemapCultures[];
    donneesTreemapCulturesConsommation: DonneesTreemapCultures[];
    tauxAdequationAssietteActuelleBrutPourcent: number;
}

export interface InfographieEvolutionPopulationAgricoleModele extends InfographieBaseModele {
    annees: number[];
    nombreAgriculteurs: number[];
    nomTerritoire: string;
}

export interface InfographieCartePrecariteAlimentaireModele extends InfographieBaseModele {
    territoire: TerritoireApi;
}

export interface InfographieCarteHVNModele extends InfographieBaseModele {
    territoire: TerritoireApi;
}

export interface SvgAnalogueClimatique {
    resolutionX: number;
    resolutionY: number;
    largeurPrefereePx?: number;
}

export interface InfographieAnalogueClimatiqueModele extends InfographieBaseModele {
    carteUrl?: string;
    configFondCarte: ConfigFondCarteAnalogueClimatiqueModele;
    pointsAnaloguesClimatiques: PointsAnaloguesClimatiquesMercator;
    coinsBBox: CoinsBBoxMercator;
    message: HtmlString;
    image: SvgAnalogueClimatique;
    theme?: 'orange' | 'jaune';
}

export type InfographieModele =
    | InfographieBaseModele
    | InfographieImageChiffreCleModele
    | InfographieDoubleImagesChiffresCles
    | InfographieCartePrecariteAlimentaireModele
    | InfographieEvolutionPopulationAgricoleModele
    | InfographieAnalogueClimatiqueModele;
