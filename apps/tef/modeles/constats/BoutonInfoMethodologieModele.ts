export type LibelleBoutonInfoMethodologie = 'Chiffre' | 'Chiffres' | 'Méthode de calcul';

export function calculerLibelleMentionNationalBoutonInfoMethodologie(libelleBouton: LibelleBoutonInfoMethodologie): string {
    return libelleBouton === 'Chiffre' ? 'Chiffre national' : libelleBouton === 'Chiffres' ? 'Chiffres nationaux' : '';
}

export function calculerLibelleBoutonInfoMethodologie(libelleBouton: LibelleBoutonInfoMethodologie, estPerimetreNational: boolean): string {
    return libelleBouton === 'Chiffre'
        ? estPerimetreNational
            ? " - d'où vient-il"
            : "D'où vient ce chiffre"
        : libelleBouton === 'Chiffres'
          ? estPerimetreNational
              ? " - d'où viennent-ils"
              : "D'où viennent ces chiffres"
          : libelleBouton;
}
