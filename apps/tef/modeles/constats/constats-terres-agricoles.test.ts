import type { ProfilsApi } from '@lga/specification-api';
import type { BesoinsParGroupeCultureApi } from '@lga/specification-api/build/specification/api/index.js';
import { describe, expect, it } from 'vitest';

import {
    calculerMessageConstatTerresSuffisantes,
    calculerMessageTexteConstatTerresPartiellementSuffisantes,
    calculerMessageTexteCourt,
    calculerMessageTextesEtQuestionConstatTerresInsuffisantes,
    calculerMessageTitreConstatTerresInsuffisantes
} from './constats-terres-agricoles';

const PROFIL_FRANCE: ProfilsApi = {
    codeProfilDiagnostic: 'DEFAUT',
    codeProfilCollectivite: 'PAYS',
    codeProfilAgriculture: 'NON_SPECIALISE'
};

describe('Tests constats du thème Terres Agricoles', () => {
    it('Test calculerMessageTitreConstatTerresInsuffisantes', () => {
        expect(calculerMessageTitreConstatTerresInsuffisantes('URBAIN')).toEqual(
            'Votre territoire est densément peuplé et dépend d’autres territoires pour se nourrir.'
        );
        expect(calculerMessageTitreConstatTerresInsuffisantes('DEFAUT')).toEqual(
            'Les terres agricoles de votre territoire sont insuffisantes pour nourrir toute la population.'
        );
    });

    it('Test calculerMessageTexteEtQuestionConstatTerresInsuffisantes', () => {
        const casArtificialisation = calculerMessageTextesEtQuestionConstatTerresInsuffisantes('DEFAUT', 5, 4000, 4000);
        expect(casArtificialisation.texte).toEqual(
            'Pour ne rien arranger, les constructions humaines détruisent des terres agricoles de façon irréversible chaque année, 4 000 hectares ont ainsi été artificialisés en 5 ans, soit de quoi nourrir 10 000 personnes.'
        );
        expect(casArtificialisation.question).toEqual('Et si on arrêtait de tout bétonner ?');

        const casUrbain = calculerMessageTextesEtQuestionConstatTerresInsuffisantes('URBAIN', null, null, null);
        expect(casUrbain.texte).toEqual(
            "Même si l'agriculture urbaine a du potentiel, en particulier pour les fruits et légumes, les villes dépendront toujours d'autres territoires pour leur alimentation."
        );
        expect(casUrbain.question).toEqual('Et si on protégeait notre patrimoine agricole commun ?');

        const casDefaut = calculerMessageTextesEtQuestionConstatTerresInsuffisantes('DEFAUT', null, null, null);
        expect(casDefaut.texte).toEqual(
            'L’essentiel de l’alimentation est donc à faire venir d’ailleurs : mieux vaut entretenir de bonnes relations avec les territoires qui vous nourrissent et préserver les terres disponibles localement.'
        );
        expect(casDefaut.question).toEqual('Et si on protégeait notre patrimoine agricole commun ?');
    });

    it('Test calculerMessageTexteConstatTerresPartiellementSuffisantes', () => {
        const besoinsParGroupesCultures: BesoinsParGroupeCultureApi[] = [
            {
                codeGroupeCulture: 'CER',
                nomGroupeCulture: 'Céréales',
                tauxAdequationBrutPourcent: 70,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'DVC',
                nomGroupeCulture: 'Autres cultures',
                tauxAdequationBrutPourcent: 10,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'FLC',
                nomGroupeCulture: 'Fruits et légumes',
                tauxAdequationBrutPourcent: 40,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'FOU',
                nomGroupeCulture: 'Fourrages',
                tauxAdequationBrutPourcent: 10,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'OLP',
                nomGroupeCulture: 'Oléoprotéagineux',
                tauxAdequationBrutPourcent: 50,
                besoinsHa: null,
                besoinsParCulture: []
            }
        ];
        expect(calculerMessageTexteConstatTerresPartiellementSuffisantes(100, 66, besoinsParGroupesCultures)).toBe(
            "Mais les fermes de votre territoire ne produisent pas assez <strong>de fruits et légumes et d'oléoprotéagineux</strong>. Vous avez du potentiel pour développer l’alimentation locale sous réserve de diversifier les productions."
        );
        expect(calculerMessageTexteConstatTerresPartiellementSuffisantes(100, 67, besoinsParGroupesCultures)).toBe(
            "L'agriculture du territoire est par ailleurs relativement diversifiée. Vous avez du potentiel pour développer l’alimentation locale !"
        );
        expect(calculerMessageTexteConstatTerresPartiellementSuffisantes(100, 66, [])).toBe(
            "L'agriculture du territoire est par ailleurs relativement diversifiée. Vous avez du potentiel pour développer l’alimentation locale !"
        );
    });

    const BESOINS_PAR_GROUPES_CULTURES_CARENCES: BesoinsParGroupeCultureApi[] = [
        {
            codeGroupeCulture: 'CER',
            nomGroupeCulture: 'Céréales',
            tauxAdequationBrutPourcent: 300,
            besoinsHa: null,
            besoinsParCulture: []
        },
        {
            codeGroupeCulture: 'DVC',
            nomGroupeCulture: 'Autres cultures',
            tauxAdequationBrutPourcent: 1000,
            besoinsHa: null,
            besoinsParCulture: []
        },
        {
            codeGroupeCulture: 'FLC',
            nomGroupeCulture: 'Fruits et légumes',
            tauxAdequationBrutPourcent: 40,
            besoinsHa: null,
            besoinsParCulture: []
        },
        {
            codeGroupeCulture: 'FOU',
            nomGroupeCulture: 'Fourrages',
            tauxAdequationBrutPourcent: 30,
            besoinsHa: null,
            besoinsParCulture: []
        },
        {
            codeGroupeCulture: 'OLP',
            nomGroupeCulture: 'Oléoprotéagineux',
            tauxAdequationBrutPourcent: 20,
            besoinsHa: null,
            besoinsParCulture: []
        }
    ];
    const BESOINS_PAR_GROUPES_CULTURES_SPECIALISATION: BesoinsParGroupeCultureApi[] = [
        {
            codeGroupeCulture: 'CER',
            nomGroupeCulture: 'Céréales',
            tauxAdequationBrutPourcent: 300,
            besoinsHa: null,
            besoinsParCulture: []
        }
    ];
    const BESOINS_PAR_GROUPES_CULTURES_DIVERSIFICATION: BesoinsParGroupeCultureApi[] = [
        {
            codeGroupeCulture: 'CER',
            nomGroupeCulture: 'Céréales',
            tauxAdequationBrutPourcent: 110,
            besoinsHa: null,
            besoinsParCulture: []
        }
    ];

    it('Test du titre calculerMessageConstatTerresSuffisantes', () => {
        expect(calculerMessageConstatTerresSuffisantes(99, [], PROFIL_FRANCE, 100, 4000).titre).toBe(
            'Votre territoire pourrait produire suffisamment pour nourrir la grande majorité des habitants.'
        );
        expect(calculerMessageConstatTerresSuffisantes(99, BESOINS_PAR_GROUPES_CULTURES_CARENCES, PROFIL_FRANCE, 100, 4000).titre).toBe(
            'Votre territoire pourrait produire suffisamment pour nourrir la grande majorité des habitants, mais…'
        );
        expect(calculerMessageConstatTerresSuffisantes(100, [], PROFIL_FRANCE, 100, 4000).titre).toBe(
            'Bonne nouvelle : votre territoire pourrait produire suffisamment pour nourrir tous ses habitants.'
        );
        expect(calculerMessageConstatTerresSuffisantes(100, BESOINS_PAR_GROUPES_CULTURES_CARENCES, PROFIL_FRANCE, 100, 4000).titre).toBe(
            'Votre territoire a suffisamment de terres agricoles pour nourrir ses habitants, mais…'
        );
    });

    it('Test du texte calculerMessageConstatTerresSuffisantes', () => {
        expect(calculerMessageConstatTerresSuffisantes(0, BESOINS_PAR_GROUPES_CULTURES_CARENCES, PROFIL_FRANCE, 100, 4000).texte).toBe(
            "Les fermes de votre territoire ne produisent pas assez <strong>d'oléoprotéagineux et de fruits et légumes</strong>. Vous avez donc un bon potentiel pour développer l’alimentation locale sous réserve de diversifier les productions."
        );
        expect(calculerMessageConstatTerresSuffisantes(0, BESOINS_PAR_GROUPES_CULTURES_SPECIALISATION, PROFIL_FRANCE, 100, 4000).texte).toBe(
            'Bien que spécialisée dans la production <strong>de céréales</strong>, l’agriculture du territoire couvre les principaux besoins en grands groupes d’aliments. Vous avez un bon potentiel pour développer l’alimentation locale !'
        );
        expect(calculerMessageConstatTerresSuffisantes(0, BESOINS_PAR_GROUPES_CULTURES_DIVERSIFICATION, PROFIL_FRANCE, 100, 4000).texte).toBe(
            'L’agriculture du territoire couvre les principaux besoins en grands groupes d’aliments. Vous avez un bon potentiel pour développer l’alimentation locale !'
        );
    });

    it('Test du texte calculerMessageTexteCourt', () => {
        expect(calculerMessageTexteCourt(40000, 4000, 'Et')).toBe(
            'Et <strong>40 000 ha</strong> ont été <strong>artificialisés en 5 ans</strong>, qui auraient pu nourrir 100 000 personnes.'
        );
        expect(calculerMessageTexteCourt(43000, 4000)).toBe(
            '<strong>43 000 ha</strong> ont été <strong>artificialisés en 5 ans</strong>, qui auraient pu nourrir 110 000 personnes.'
        );
        expect(calculerMessageTexteCourt(null, null)).toBe(null);
    });
});
