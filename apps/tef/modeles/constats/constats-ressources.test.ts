import type { SauParGroupeCultureApi } from '@lga/specification-api/build/specification/api/index.js';
import { describe, expect, it } from 'vitest';

import { creerDonneesApi } from '../tests-utils';
import { creerConstatPrelevementsEau } from './constats-ressources';

describe('Tests constats Ressources', () => {
    it('Test de creerConstatPrelevementsEau ', () => {
        const sau: SauParGroupeCultureApi[] = [
            {
                codeGroupeCulture: 'CER',
                nomGroupeCulture: 'Céréales',
                sauHa: 10,
                sauParCulture: [
                    {
                        codeCulture: 'P-MAG',
                        nomCulture: 'Maïs grain',
                        sauHa: 10
                    }
                ]
            }
        ];
        const constat = creerConstatPrelevementsEau(
            creerDonneesApi({
                irrigationM32012: 100,
                irrigationM32022: 120.1,
                sauParGroupeCulture: sau,
                sauProductiveHa: 100
            })
        );
        expect(constat.message.texte).toContain('en particulier du maïs grain');
        expect(constat.infographie?.chiffreCle?.chiffre).toContain('+20');
        expect(constat.infographie?.chiffreCle?.phrase).toContain('en 10 ans');
    });
});
