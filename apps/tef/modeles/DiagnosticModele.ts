import type { HtmlString } from '@lga/base';
import type { IdTheme } from '@lga/indicateurs';
import type { CodeProfilDiagnosticApi, OccupationSolsApi, TerritoireApi } from '@lga/specification-api';
import { CategorieTerritoire, TerritoireBase } from '@lga/territoires';

import type { ConstatModele } from './constats/ConstatModele';
import { creerConstatCommercesAlimentaires, creerConstatPrecariteAlimentaire } from './constats/constats-accessibilite';
import { creerConstatEvolutionPopulationAgricole, creerConstatRevenus } from './constats/constats-agriculteurs';
import { creerConstatDistances } from './constats/constats-autonomie-alimentaire';
import { creerConstatHaies, creerConstatPaysagesAgricoles } from './constats/constats-biodiversite';
import { creerConstatAnalogueClimatique } from './constats/constats-climat';
import { creerConstatRegimeAlimentaireTropCarne } from './constats/constats-consommation';
import { creerConstatAlimentationAnimale, creerConstatDependancesRessources, creerConstatPrelevementsEau } from './constats/constats-ressources';
import {
    creerConstatAbsenceDonneesTerresAgricoles,
    creerConstatTerresInsuffisantes,
    creerConstatTerresPartiellementSuffisantes,
    creerConstatTerresSuffisantes,
    creerInfographieAvecChartSurfacesConsommationProduction,
    type DonneesApiTerresAgricoles
} from './constats/constats-terres-agricoles';
import type { InfographieModele } from './constats/InfographieModele.js';
import { calculerNiveauAutonomie, calculerTendanceEvolutionPrelevementEauPourcent, type DonneesApi } from './fonctions-utils.js';
import {
    calculerMessageIdentiteTerritoire,
    calculerMessageOccupationSolsEtAgricultureTerritoire
} from './indicateurs/messages-presentation-territoire.js';
import { creerThematiqueAccessibilite } from './thematiques/thematique-accessibilite.js';
import { creerThematiqueAgriculteurs } from './thematiques/thematique-agriculteurs.js';
import { creerThematiqueAutonomieAlimentaire } from './thematiques/thematique-autonomie-alimentaire.js';
import { creerThematiqueBiodiversite } from './thematiques/thematique-biodiversite.js';
import { creerThematiqueClimat } from './thematiques/thematique-climat.js';
import { creerThematiqueConsommation } from './thematiques/thematique-consommation.js';
import { creerThematiqueRessources } from './thematiques/thematique-ressources.js';
import { creerThematiqueRisquesSecuriteAlimentaire } from './thematiques/thematique-risques-securite-alimentaire.js';
import { creerThematiqueTerresAgricoles } from './thematiques/thematique-terres-agricoles.js';
import { creerThematiqueTransformationDistribution } from './thematiques/thematique-transformation-distribution.js';
import type { ThematiqueModele as ThematiqueModele } from './thematiques/ThematiqueModele.js';

export interface DescriptionTerritoire {
    messageIdentiteTerritoire: HtmlString;
    messageOccupationSolsEtAgricultureTerritoire: HtmlString;
    occupationSols: OccupationSolsApi;
    tauxAdequationBrutAssietteActuelle: number | null;
    sauTotaleHa: number;
}

export type Liste6Constats = [
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>
];

export type Liste8Constats = [
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>,
    ConstatModele<InfographieModele>
];

export interface DiagnosticFlashModele {
    territoire: TerritoireBase;
    descriptionTerritoire: DescriptionTerritoire;
    codeProfilDiagnostic: CodeProfilDiagnosticApi;
    constatsModeles: Liste8Constats;
}

export interface Livret4PagesModele {
    territoire: TerritoireBase;
    descriptionTerritoire: DescriptionTerritoire;
    codeProfilDiagnostic: CodeProfilDiagnosticApi;
    constatsModeles: Liste6Constats;
}

export interface DiagnosticThematiqueModele {
    territoire: TerritoireBase;
    codeProfilDiagnostic: CodeProfilDiagnosticApi;
    thematiqueModele: ThematiqueModele;
}

export function creerTerritoire(territoireApi: TerritoireApi): TerritoireBase {
    return new TerritoireBase(
        territoireApi.id,
        territoireApi.nom,
        CategorieTerritoire.creer(territoireApi.categorie, territoireApi.sousCategorie),
        territoireApi.genreNombre,
        territoireApi.preposition,
        territoireApi.boundingBox
    );
}

export function creerDescriptionTerritoire(donneesApi: DonneesApi): DescriptionTerritoire {
    const territoire = creerTerritoire(donneesApi.territoireApi);
    return {
        messageIdentiteTerritoire: calculerMessageIdentiteTerritoire(
            territoire.nomTerritoireAvecDeterminant,
            territoire.genreNombre,
            donneesApi.diagnosticApi.nbCommunes,
            donneesApi.diagnosticApi.population,
            donneesApi.diagnosticApi.densitePopulationHabParKm2
        ),
        messageOccupationSolsEtAgricultureTerritoire: calculerMessageOccupationSolsEtAgricultureTerritoire(
            donneesApi.diagnosticApi.occupationSols,
            donneesApi.diagnosticApi.codeOtexMajoritaire5Postes
        ),
        occupationSols: donneesApi.diagnosticApi.occupationSols,
        tauxAdequationBrutAssietteActuelle: donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent,
        sauTotaleHa: donneesApi.diagnosticApi.surfaceAgricoleUtile.sauTotaleHa
    };
}

export function creerDiagnosticFlashModele(donneesApi: DonneesApi): DiagnosticFlashModele {
    return {
        territoire: creerTerritoire(donneesApi.territoireApi),
        descriptionTerritoire: creerDescriptionTerritoire(donneesApi),
        codeProfilDiagnostic: donneesApi.diagnosticApi.profils.codeProfilDiagnostic,
        constatsModeles: creerListe8Constats(donneesApi)
    };
}

export function creerLivret4PagesModele(donneesApi: DonneesApi): Livret4PagesModele {
    return {
        territoire: creerTerritoire(donneesApi.territoireApi),
        descriptionTerritoire: creerDescriptionTerritoire(donneesApi),
        codeProfilDiagnostic: donneesApi.diagnosticApi.profils.codeProfilDiagnostic,
        constatsModeles: creerListe6Constats(donneesApi)
    };
}

function creerListe8Constats(d: DonneesApi): Liste8Constats {
    return [
        creerConstatTerresAgricoles(d),
        creerConstatDistances(d),
        creerConstatRegimeAlimentaireTropCarne(d),
        creerConstatAccessibiliteAlimentaire(d),
        creerConstatAgriculteurs(d),
        creerConstatRessources(d),
        creerConstatBiodiversite(d),
        creerConstatAnalogueClimatique(d)
    ];
}

function creerListe6Constats(d: DonneesApi): Liste6Constats {
    return [
        creerConstatTerresAgricolesAvecInfographieSurfacesAgricoles(d),
        creerConstatDependancesRessources(d),
        creerConstatPrecariteAlimentaire(d),
        creerConstatAgriculteurs(d),
        creerConstatBiodiversite(d),
        creerConstatAnalogueClimatique(d)
    ];
}

export function creerConstatTerresAgricoles(donneesApi: DonneesApi): ConstatModele<InfographieModele> {
    switch (
        calculerNiveauAutonomie(
            donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent,
            donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteDemitarienne.tauxAdequationBrutPourcent
        )
    ) {
        case 'INCONNU':
            return creerConstatAbsenceDonneesTerresAgricoles();
        case 'TERRES_INSUFFISANTES':
            return creerConstatTerresInsuffisantes(donneesApi as DonneesApiTerresAgricoles);
        case 'TERRES_PARTIELLEMENT_SUFFISANTES':
            return creerConstatTerresPartiellementSuffisantes(donneesApi as DonneesApiTerresAgricoles);
        case 'TERRES_SUFFISANTES':
            return creerConstatTerresSuffisantes(donneesApi as DonneesApiTerresAgricoles);
    }
}

export function creerConstatTerresAgricolesAvecInfographieSurfacesAgricoles(donneesApi: DonneesApi): ConstatModele<InfographieModele> {
    const constatTerresAgricoles = creerConstatTerresAgricoles(donneesApi);
    if (donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent !== null) {
        constatTerresAgricoles.infographie = creerInfographieAvecChartSurfacesConsommationProduction(donneesApi as DonneesApiTerresAgricoles);
    }
    return constatTerresAgricoles;
}

export function creerConstatAccessibiliteAlimentaire(donneesApi: DonneesApi): ConstatModele<InfographieModele> {
    if (
        donneesApi.diagnosticApi.proximiteCommerces.partPopulationDependanteVoiturePourcent !== null &&
        donneesApi.diagnosticApi.proximiteCommerces.partPopulationDependanteVoiturePourcent >= 80
    ) {
        return creerConstatCommercesAlimentaires(donneesApi);
    } else {
        return creerConstatPrecariteAlimentaire(donneesApi);
    }
}

export function creerConstatAgriculteurs(donneesApi: DonneesApi): ConstatModele<InfographieModele> {
    if (
        donneesApi.diagnosticApi.populationAgricole.populationAgricole1988 !== null &&
        donneesApi.diagnosticApi.populationAgricole.populationAgricole2010 !== null
    ) {
        return creerConstatEvolutionPopulationAgricole(donneesApi);
    } else {
        return creerConstatRevenus(donneesApi);
    }
}

export function creerConstatRessources(donneesApi: DonneesApi): ConstatModele<InfographieModele> {
    const codeProfilDiagnostic = donneesApi.diagnosticApi.profils.codeProfilDiagnostic;
    const irrigationIndicateursParAnnees = donneesApi.diagnosticApi.eau.irrigation.indicateursParAnnees;
    const tendanceEvolutionPrelevementEau = calculerTendanceEvolutionPrelevementEauPourcent(irrigationIndicateursParAnnees, 2012, 2022);

    if (codeProfilDiagnostic === 'VITICULTURE') {
        if (tendanceEvolutionPrelevementEau !== null && tendanceEvolutionPrelevementEau >= 20) {
            return creerConstatPrelevementsEau(donneesApi);
        } else {
            return creerConstatDependancesRessources(donneesApi);
        }
    } else if (codeProfilDiagnostic === 'ELEVAGE_INTENSIF') {
        return creerConstatAlimentationAnimale(donneesApi);
    } else if (codeProfilDiagnostic === 'GRANDES_CULTURES') {
        if (tendanceEvolutionPrelevementEau !== null && tendanceEvolutionPrelevementEau >= 50) {
            return creerConstatPrelevementsEau(donneesApi);
        } else {
            return creerConstatDependancesRessources(donneesApi);
        }
    } else if (codeProfilDiagnostic === 'FRUITS_ET_LEGUMES') {
        if (tendanceEvolutionPrelevementEau !== null && tendanceEvolutionPrelevementEau >= 20) {
            return creerConstatPrelevementsEau(donneesApi);
        } else {
            return creerConstatDependancesRessources(donneesApi);
        }
    } else if (codeProfilDiagnostic === 'DEFAUT') {
        if (tendanceEvolutionPrelevementEau !== null && tendanceEvolutionPrelevementEau >= 100) {
            return creerConstatPrelevementsEau(donneesApi);
        } else {
            return creerConstatDependancesRessources(donneesApi);
        }
    } else {
        return creerConstatDependancesRessources(donneesApi);
    }
}

export function creerConstatBiodiversite(donneesApi: DonneesApi): ConstatModele<InfographieModele> {
    if (donneesApi.territoireApi.categorie !== 'PAYS' && donneesApi.diagnosticApi.pratiquesAgricoles.indicateurHvn.indiceTotal !== null) {
        return creerConstatPaysagesAgricoles(donneesApi, donneesApi.diagnosticApi.pratiquesAgricoles.indicateurHvn.indiceTotal);
    } else {
        return creerConstatHaies(donneesApi);
    }
}

export function creerDiagnosticThematiqueModele(idTheme: IdTheme, donneesApi: DonneesApi): DiagnosticThematiqueModele {
    return {
        territoire: creerTerritoire(donneesApi.territoireApi),
        codeProfilDiagnostic: donneesApi.diagnosticApi.profils.codeProfilDiagnostic,
        thematiqueModele: creerThematiqueModele(idTheme, donneesApi)
    };
}

function creerThematiqueModele(idTheme: IdTheme, donneesApi: DonneesApi): ThematiqueModele {
    switch (idTheme) {
        case 'risques-securite-alimentaire':
            return creerThematiqueRisquesSecuriteAlimentaire();
        case 'accessibilite':
            return creerThematiqueAccessibilite(donneesApi);
        case 'agriculteurs':
            return creerThematiqueAgriculteurs(donneesApi);
        case 'autonomie-alimentaire':
            return creerThematiqueAutonomieAlimentaire(donneesApi);
        case 'biodiversite':
            return creerThematiqueBiodiversite(donneesApi);
        case 'climat':
            return creerThematiqueClimat(donneesApi);
        case 'consommation':
            return creerThematiqueConsommation();
        case 'ressources':
            return creerThematiqueRessources(donneesApi);
        case 'terres-agricoles':
            return creerThematiqueTerresAgricoles(donneesApi);
        case 'transformation-distribution':
            return creerThematiqueTransformationDistribution();
        default:
            throw new Error(`Thème inconnu: ${idTheme as string}`);
    }
}
