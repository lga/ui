import type { BesoinsParGroupeCultureApi } from '@lga/specification-api/build/specification/api/index.js';
import { describe, expect, it } from 'vitest';

import {
    calculerLibelleGroupeCultureAvecPreposition,
    calculerLibellesPrincipauxGroupesCulturesExcedentaires,
    calculerLibellesPrincipauxGroupesCulturesManquants,
    calculerNiveauAutonomie,
    calculerTendanceEvolutionPrelevementEauPourcent,
    classerBesoinsAssiette,
    classerTauxAdequationParGroupesCultures,
    estimerPopulation
} from './fonctions-utils.js';

describe('Tests fonctions utils', () => {
    it('Test fonction estimerPopulation', () => {
        expect(estimerPopulation(100, 134, 1990, 2005, 1990)).toEqual(100);
        expect(estimerPopulation(100, 134, 1990, 2005, 1999)).toEqual(119);
        expect(estimerPopulation(100, 134, 1990, 2005, 2005)).toEqual(134);
        expect(estimerPopulation(100, 134, 1990, 2005, 2015)).toEqual(163);
    });
    it('Test fonction estimationEvolutionPrelevementEauPourcent', () => {
        expect(
            calculerTendanceEvolutionPrelevementEauPourcent(
                [
                    {
                        annee: 2012,
                        irrigationM3: 40,
                        irrigationMM: 10
                    },
                    {
                        annee: 2017,
                        irrigationM3: 60,
                        irrigationMM: 10
                    }
                ],
                2012,
                2022
            )
        ).toEqual(100);
    });
    it('Test fonction classerTauxAdequationParGroupesCultures', () => {
        expect(
            classerTauxAdequationParGroupesCultures([
                {
                    codeGroupeCulture: 'CER',
                    nomGroupeCulture: 'Surplus 1',
                    tauxAdequationBrutPourcent: 160
                },
                {
                    codeGroupeCulture: 'FOU',
                    nomGroupeCulture: 'Manquant 1',
                    tauxAdequationBrutPourcent: 10
                },
                {
                    codeGroupeCulture: 'FOU',
                    nomGroupeCulture: 'Manquant 2',
                    tauxAdequationBrutPourcent: 30
                },
                {
                    codeGroupeCulture: 'OLP',
                    nomGroupeCulture: 'OK 1',
                    tauxAdequationBrutPourcent: 90
                },
                {
                    codeGroupeCulture: 'CER',
                    nomGroupeCulture: 'Surplus 2',
                    tauxAdequationBrutPourcent: 155
                },
                {
                    codeGroupeCulture: 'OLP',
                    nomGroupeCulture: 'OK 2',
                    tauxAdequationBrutPourcent: 110
                }
            ])
        ).toEqual({
            excedents: [
                {
                    codeGroupeCulture: 'CER',
                    nomGroupeCulture: 'Surplus 1',
                    tauxAdequationBrutPourcent: 160
                },
                {
                    codeGroupeCulture: 'CER',
                    nomGroupeCulture: 'Surplus 2',
                    tauxAdequationBrutPourcent: 155
                }
            ],
            manques: [
                {
                    codeGroupeCulture: 'FOU',
                    nomGroupeCulture: 'Manquant 1',
                    tauxAdequationBrutPourcent: 10
                },
                {
                    codeGroupeCulture: 'FOU',
                    nomGroupeCulture: 'Manquant 2',
                    tauxAdequationBrutPourcent: 30
                }
            ]
        });
    });
    it('Test fonction classerBesoinsAssiette', () => {
        expect(
            classerBesoinsAssiette([
                {
                    codeGroupeCulture: 'CER',
                    nomGroupeCulture: 'Surplus 1',
                    tauxAdequationBrutPourcent: 160,
                    besoinsHa: null,
                    besoinsParCulture: []
                }
            ])
        ).toEqual({
            excedents: [
                {
                    codeGroupeCulture: 'CER',
                    nomGroupeCulture: 'Surplus 1',
                    tauxAdequationBrutPourcent: 160
                }
            ],
            manques: []
        });
    });
    it('Test fonction classerBesoinsAssiette : pas de données', () => {
        expect(classerBesoinsAssiette([])).toEqual({
            excedents: [],
            manques: []
        });
    });

    it('Test fonction calculerLibelleGroupeCultureAvecPreposition', () => {
        expect(calculerLibelleGroupeCultureAvecPreposition('Céréales')).toEqual('de céréales');
        expect(calculerLibelleGroupeCultureAvecPreposition('Oléoprotéagineux')).toEqual("d'oléoprotéagineux");
    });

    it('Test calculerLibellesPrincipauxGroupesCulturesManquants', () => {
        const besoinsParGroupeCultureAssiette: BesoinsParGroupeCultureApi[] = [
            {
                codeGroupeCulture: 'CER',
                nomGroupeCulture: 'Céréales',
                tauxAdequationBrutPourcent: 70,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'FLC',
                nomGroupeCulture: 'Fruits et légumes',
                tauxAdequationBrutPourcent: 30,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'OLP',
                nomGroupeCulture: 'Oléoprotéagineux',
                tauxAdequationBrutPourcent: 20,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'DVC',
                nomGroupeCulture: 'Autres cultures',
                tauxAdequationBrutPourcent: 10,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'FOU',
                nomGroupeCulture: 'Fourrages',
                tauxAdequationBrutPourcent: 10,
                besoinsHa: null,
                besoinsParCulture: []
            }
        ];
        expect(calculerLibellesPrincipauxGroupesCulturesManquants(besoinsParGroupeCultureAssiette)).toEqual(
            "d'oléoprotéagineux et de fruits et légumes"
        );
        besoinsParGroupeCultureAssiette[2].tauxAdequationBrutPourcent = 50;
        expect(calculerLibellesPrincipauxGroupesCulturesManquants(besoinsParGroupeCultureAssiette)).toEqual(
            "de fruits et légumes et d'oléoprotéagineux"
        );
    });

    it('Test calculerLibellesPrincipauxGroupesCulturesExcedentaires', () => {
        const besoinsParGroupeCultureAssiette: BesoinsParGroupeCultureApi[] = [
            {
                codeGroupeCulture: 'DVC',
                nomGroupeCulture: 'Autres cultures',
                tauxAdequationBrutPourcent: 1000,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'CER',
                nomGroupeCulture: 'Céréales',
                tauxAdequationBrutPourcent: 500,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'FLC',
                nomGroupeCulture: 'Fruits et légumes',
                tauxAdequationBrutPourcent: 400,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'FOU',
                nomGroupeCulture: 'Fourrages',
                tauxAdequationBrutPourcent: 300,
                besoinsHa: null,
                besoinsParCulture: []
            },
            {
                codeGroupeCulture: 'OLP',
                nomGroupeCulture: 'Oléoprotéagineux',
                tauxAdequationBrutPourcent: 20,
                besoinsHa: null,
                besoinsParCulture: []
            }
        ];
        expect(calculerLibellesPrincipauxGroupesCulturesExcedentaires(besoinsParGroupeCultureAssiette)).toEqual(
            'de céréales et de fruits et légumes'
        );
    });

    it('Test calculerNiveauAutonomie', () => {
        expect(calculerNiveauAutonomie(null, 100)).toEqual('INCONNU');
        expect(calculerNiveauAutonomie(100, null)).toEqual('INCONNU');
        expect(calculerNiveauAutonomie(85, 999)).toEqual('TERRES_SUFFISANTES');
        expect(calculerNiveauAutonomie(0, 85)).toEqual('TERRES_PARTIELLEMENT_SUFFISANTES');
        expect(calculerNiveauAutonomie(0, 84)).toEqual('TERRES_INSUFFISANTES');
    });
});
