import { formaterNombreEnEntierString, formaterNombreSelonValeurString, type HtmlString, htmlstring, rendreInitialeMajuscule } from '@lga/base';
import type { CodeGenreNombreTerritoireApi, CodeOtexMajoritaire5PostesApi, OccupationSolsApi } from '@lga/specification-api';

export type CodeGenreNombreAccord = {
    [key in CodeGenreNombreTerritoireApi]: string;
};
const MAP_GENRE_NOMBRE_ACCORD: CodeGenreNombreAccord = {
    FEMININ_SINGULIER: 'e',
    MASCULIN_SINGULIER: '',
    FEMININ_PLURIEL: 'es',
    MASCULIN_PLURIEL: 's'
};

export function calculerMessageIdentiteTerritoire(
    nomTerritoireAvecDeterminant: string,
    genreNombre: CodeGenreNombreTerritoireApi,
    nbCommunes: number,
    nbHabitants: number | null,
    densitePopulationHabParKM2: number | null
): HtmlString {
    let message;
    const verbe = genreNombre === 'FEMININ_SINGULIER' || genreNombre === 'MASCULIN_SINGULIER' ? 'est' : 'sont';
    if (nbCommunes > 1) {
        message = htmlstring`${rendreInitialeMajuscule(nomTerritoireAvecDeterminant)} ${verbe} composé${MAP_GENRE_NOMBRE_ACCORD[genreNombre]} de <strong>${formaterNombreEnEntierString(nbCommunes)} communes</strong> et peuplé${MAP_GENRE_NOMBRE_ACCORD[genreNombre]}`;
    } else {
        message = htmlstring`${rendreInitialeMajuscule(nomTerritoireAvecDeterminant)} ${verbe} peuplée`;
    }
    message = htmlstring`${message} de <strong>${formaterNombreSelonValeurString(nbHabitants)} habitants</strong>`;
    if (densitePopulationHabParKM2) message = htmlstring`${message} (${formaterNombreEnEntierString(densitePopulationHabParKM2)} hab./km²)`;
    return htmlstring`${message}.`;
}

const OTEX_5_POSTES = [
    {
        code: 'GRANDES_CULTURES',
        libelle: 'grandes cultures'
    },
    {
        code: 'VITICULTURE',
        libelle: 'viticulture'
    },
    {
        code: 'FRUITS_ET_LEGUMES',
        libelle: 'maraîchage, horticulture ou fruits'
    },
    {
        code: 'ELEVAGE',
        libelle: 'élevage'
    },
    {
        code: 'POLYCULTURE_POLYELEVAGE',
        libelle: 'polyculture polyélevage'
    }
];

export function calculerMessageOccupationSolsEtAgricultureTerritoire(
    occupationSols: OccupationSolsApi,
    codeOtexMajoritaire5Postes: CodeOtexMajoritaire5PostesApi
): HtmlString {
    let qualificatif = '';
    if (occupationSols.superficieArtificialiseeClcHa && occupationSols.superficieArtificialiseeClcHa / occupationSols.superficieTotaleHa > 0.5) {
        qualificatif = 'urbain';
    } else if (occupationSols.superficieAgricoleClcHa && occupationSols.superficieAgricoleClcHa / occupationSols.superficieTotaleHa > 0.5) {
        qualificatif = 'agricole';
    } else if (
        occupationSols.superficieNaturelleOuForestiereClcHa &&
        occupationSols.superficieNaturelleOuForestiereClcHa / occupationSols.superficieTotaleHa > 0.5
    ) {
        qualificatif = 'naturel ou forestier';
    }
    const partie1message = qualificatif === '' ? htmlstring`` : htmlstring`C'est un territoire majoritairement <strong>${qualificatif}</strong>`;

    let partie2message = htmlstring`avec une agriculture`;
    if (OTEX_5_POSTES.map((i) => i.code).includes(codeOtexMajoritaire5Postes)) {
        partie2message = htmlstring`${partie2message} spécialisée en ${OTEX_5_POSTES.find((i) => i.code === codeOtexMajoritaire5Postes)?.libelle ?? ''}.`;
    } else {
        partie2message = htmlstring`${partie2message} non spécialisée.`;
    }
    return htmlstring`${partie1message} ${partie2message}`;
}
