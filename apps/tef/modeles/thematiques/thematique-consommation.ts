import { creerInfographieAssiette } from '../constats/constats-consommation.js';
import type { InfographieBaseModele } from '../constats/InfographieModele.js';
import type { ThematiqueModele } from './ThematiqueModele.js';

export interface ThematiqueConsommationModele extends ThematiqueModele {
    infographieAssiette: InfographieBaseModele;
}

export function creerThematiqueConsommation(): ThematiqueConsommationModele {
    const infographieAssiette = creerInfographieAssiette();
    return {
        idTheme: 'consommation',
        infographieAssiette
    };
}
