import { formaterNombreSelonValeurString } from '@lga/base';
import { INDICATEURS, type ValeurIndicateur } from '@lga/indicateurs';
import type { TerritoireApi } from '@lga/specification-api/build/specification/api/index.js';

import { informationsMethodologieCartePrecariteAlimentaire } from '../constats/constats-accessibilite.js';
import type { InformationsMethodologieInfographieModele } from '../constats/InfographieModele.js';
import type { DonneesApi } from '../fonctions-utils.js';
import type { ThematiqueModele } from './ThematiqueModele.js';

export interface ThematiqueAccessibiliteModele extends ThematiqueModele {
    territoire?: TerritoireApi;
    methodologieCartePrecariteAlimentaire: InformationsMethodologieInfographieModele;
    indicateurPartPopulationDependanteVoiture: ValeurIndicateur;
}

export function creerThematiqueAccessibilite(donneesApi: DonneesApi): ThematiqueAccessibiliteModele {
    return {
        idTheme: 'accessibilite',
        territoire: donneesApi.territoireApi,
        methodologieCartePrecariteAlimentaire: informationsMethodologieCartePrecariteAlimentaire,
        indicateurPartPopulationDependanteVoiture: {
            indicateur: INDICATEURS.partPopulationDependanteVoiture,
            chiffre: formaterNombreSelonValeurString(donneesApi.diagnosticApi.proximiteCommerces.partPopulationDependanteVoiturePourcent),
            unite: '%'
        }
    };
}
