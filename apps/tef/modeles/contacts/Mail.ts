import { z } from 'zod';

export const mailSchema = z.object({
    repondreA: z.string({ message: 'Champ obligatoire' }).email('Email non valide'),
    sujet: z.string({ message: 'Champ obligatoire' }),
    contenuHtml: z.string({ message: 'Champ obligatoire' })
});

export type Mail = z.infer<typeof mailSchema>;
