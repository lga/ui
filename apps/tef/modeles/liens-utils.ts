import { type HtmlString, htmlstring } from '@lga/base';
import {
    type DefinitionIndicateur,
    type DefinitionRapportEtude,
    type DefinitionSourceDonnee,
    type DefinitionSourceDonneeRetraitee,
    INDICATEURS,
    SA
} from '@lga/indicateurs';

export const creerLienExterneHtmlBrut = (url: string, texte: string): HtmlString =>
    htmlstring`<a href="${url}" target="_blank" rel="noopener noreferrer" class='underline hover:no-underline hover:text-accent'>${texte}</a>`;

//FIXME refacto
export function calculerLibelleSourcesIndicateur(indicateur: DefinitionIndicateur, sansMentionTef = false, sansMentionAnnees = false) {
    const sourcesHtml = indicateur.sources
        .map((s) => {
            let legende;
            // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
            if ((s.source as DefinitionSourceDonnee).fournisseur !== undefined) {
                const source = s.source as DefinitionSourceDonnee;
                legende = htmlstring`${source.nom} (${source.fournisseur.nom}${!sansMentionAnnees && s.anneesMobilisees ? ', ' + s.anneesMobilisees : ''})`;
                // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
            } else if ((s.source as DefinitionSourceDonneeRetraitee).source !== undefined) {
                const source = s.source as DefinitionSourceDonneeRetraitee;
                legende = htmlstring`${source.nom} (${source.source.fournisseur.nom}${!sansMentionAnnees && source.annees ? ', ' + source.annees : ''})`;
                // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
            } else if ((s.source as DefinitionIndicateur).description !== undefined) {
                const source = s.source as DefinitionIndicateur;
                legende = htmlstring`${source.libelle}`;
                // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
            } else if ((s.source as DefinitionRapportEtude).auteur !== undefined) {
                const rapport = s.source as DefinitionRapportEtude;
                legende = htmlstring`${rapport.url ? htmlstring`<a href="${rapport.url}">${rapport.nom}</a>` : rapport.nom}, '${rapport.auteur}' (${rapport.annee})`;
            } else {
                legende = htmlstring``;
            }
            return legende;
        })
        .reduce((r, c, index) => htmlstring`${r}${index > 0 ? ' ; ' : ''}${c}`, htmlstring``);

    return sansMentionTef ? htmlstring`${sourcesHtml}` : htmlstring`Territoires Fertiles, à partir des données ${sourcesHtml}`;
}

export function calculerComplementRouteDiagnostic(indicateur: DefinitionIndicateur): string {
    if (indicateur === INDICATEURS.otexMajoritaire5Postes) {
        return `territoire`;
    } else {
        return `indicateurs/${indicateur.id}`;
    }
}

export function calculerComplementUrlCRATer(indicateur: DefinitionIndicateur): string {
    const indicateurDetaille = SA.getIndicateurDetaille(indicateur.id);
    const complementUrl = `${indicateurDetaille?.idMaillon}#${indicateurDetaille?.idDomaine}`;
    return `/methodologie/${complementUrl}`;
}
