import type { ItemSousMenu } from './ItemMenuModele';

export const calculerEtatActifSousArborescence = (items: ItemSousMenu[]): boolean =>
    items.filter((i) => i.type === 'LIEN_PAGE').some((i) => i.estActif);
