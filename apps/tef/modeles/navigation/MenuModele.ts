import type { ItemLibelleSansLien, ItemLienPage, ItemMenuBase, ItemSousMenu } from './ItemMenuModele';

export type SousMenu = ItemMenuBase & {
    type: 'SOUS_MENU';
    sousItems: ItemSousMenu[];
    estActif?: boolean;
};

export type ItemArborescenceSiteType = ItemLienPage['type'] | ItemLibelleSansLien['type'] | SousMenu['type'];

export type ItemArborescenceSite = ItemLienPage | SousMenu;

export type ArborescenceSite = ItemArborescenceSite[];
