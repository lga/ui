describe('Tests de supervision, génération PDF', () => {
    it('Générer le livret 4 page pour Nevez', () => {
        cy.intercept({
            method: 'POST',
            url: 'https://api.territoiresfertiles.fr/pdf/api/generate'
        }).as('apiGenererPdf');

        cy.viewport(1400, 800);
        cy.visit('/documents-a-imprimer/nevez');
        cy.verifierAffichagePage('Documents à imprimer');

        cy.get('button').contains('Télécharger').click();
        cy.wait('@apiGenererPdf').its('response.statusCode').should('eq', 200);
    });
});
