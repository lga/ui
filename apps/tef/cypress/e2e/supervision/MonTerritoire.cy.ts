describe('Tests de supervision, page Mon Territoire', () => {
    it('Rechercher un territoire depuis la HP et afficher la page EPCI de Nevez', () => {
        cy.viewport(1400, 800);
        cy.visit('/');
        cy.verifierAffichagePageAccueil();

        cy.get('[data-cy="bloc-accueil-recherche-territoire"]').find('input[type="text"]').type('neve');
        cy.get('.boite-suggestion').find('.suggestion').first().click();
        cy.verifierAffichagePage('Les enjeux agricoles');
        // Le h1 contient des éléments sur plusieurs lignes, en complément du verifieAffichagePage on vérifie aussi la présence du nom du territoire
        cy.get('h1').contains('Concarneau Cornouaille').should('be.visible');
    });
});
