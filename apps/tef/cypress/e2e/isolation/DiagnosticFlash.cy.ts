describe('Tests e2e/isolation, diagnostic flash', () => {
    it('Naviguer vers le diagnostic flash depuis la page Mon Territoire et revenir en arrière', () => {
        cy.viewport(1400, 800);
        cy.visit('/occitanie');
        cy.verifierAffichagePage('Les enjeux agricoles');

        cy.get('a').contains('Découvrir le diagnostic').click({ force: true });
        cy.verifierAffichagePage('3 minutes pour');

        cy.get('a').contains('Accueil territoire').click({ force: true });
        cy.verifierAffichagePage('Les enjeux agricoles');
    });
});
