describe('Tests e2e/isolation, thématiques', () => {
    it('Afficher une thématique depuis la HP', () => {
        cy.viewport(1400, 800);
        cy.visit('/');
        cy.verifierAffichagePageAccueil();

        cy.get('a').contains('Un territoire nourricier ?').scrollIntoView().click();
        cy.verifierAffichagePage('Terres agricoles');
    });

    it.only('Afficher une thématique sur mobile', () => {
        cy.viewport(360, 600);
        cy.visit('/thematiques/risques-securite-alimentaire');
        cy.verifierAffichagePage('Menaces');
    });

    it('Territorialiser une thématique', () => {
        cy.viewport(1400, 800);
        cy.visit('/thematiques/terres-agricoles');
        cy.verifierAffichagePage('Terres agricoles');

        cy.contains('Personnalisez cette page pour votre territoire').should('be.visible');
        cy.get('input[type="text"]').filter(':visible').first().scrollIntoView().type('occi');
        cy.get('.boite-suggestion').find('.suggestion').first().click();

        cy.contains('Personnalisez cette page pour votre territoire').should('not.exist');
        cy.get('body').contains('Occitanie [Fixture]');
    });

    it('Naviguer entre thématiques', () => {
        cy.viewport(1400, 800);
        cy.visit('/thematiques/terres-agricoles/occitanie');
        cy.verifierAffichagePage('Terres agricoles');

        cy.get('a').contains("D'où vient ce que vous mangez").scrollIntoView().click();
        cy.verifierAffichagePage('Autonomie alimentaire');
    });
});
