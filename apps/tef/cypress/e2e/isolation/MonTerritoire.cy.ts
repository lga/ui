describe('Tests e2e/isolation, page MonTerritoire', () => {
    it('Rechercher un territoire depuis la HP et afficher la page /occitanie', () => {
        cy.viewport(1400, 800);
        cy.visit('/');
        cy.verifierAffichagePageAccueil();

        cy.get('[data-cy="bloc-accueil-recherche-territoire"]').find('input[type="text"]').type('occi');
        cy.get('.boite-suggestion').find('.suggestion').first().click();
        cy.verifierAffichagePage('Les enjeux agricoles');
        // Le h1 contient des éléments sur plusieurs lignes, en complément du verifieAffichagePage on vérifie aussi la présence du nom du territoire
        cy.get('h1').contains('Occitanie [Fixture]').should('be.visible');
    });
});
