describe('Tests e2e/isolation, initiatives', () => {
    it('Afficher la page des initiatives', () => {
        cy.viewport(1400, 800);
        cy.visit('/');
        cy.verifierAffichagePageAccueil();

        cy.get('nav').find('a').contains('Initiatives inspirantes').click({ force: true });
        cy.verifierAffichagePage('Initiatives inspirantes');
        cy.get('body').contains('Occitanie [Fixture]').should('not.exist');
    });

    it('Afficher la page des initiatives en mode territorialisé', () => {
        cy.viewport(1400, 800);
        cy.visit('/occitanie');
        cy.verifierAffichagePage('Occitanie [Fixture]');

        cy.get('nav').find('a').contains('Initiatives inspirantes').click({ force: true });
        cy.verifierAffichagePage('Initiatives inspirantes');
        cy.get('body').contains('Occitanie [Fixture]').should('be.visible');
    });
});
