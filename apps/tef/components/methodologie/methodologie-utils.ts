import { SOURCES_DONNEES, SOURCES_DONNEES_RETRAITEES } from '@lga/indicateurs';

export const ID_PERIMETRE_TERRITORIAL = 'perimetre-territorial';
export const ID_SURFACES_AGRICOLES = SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater.id;
export const ID_CHEPTELS = SOURCES_DONNEES_RETRAITEES.cheptels_crater.id;
export const ID_OTEX = SOURCES_DONNEES.otex.id;
export const ID_FLUX_AZOTE = 'flux-azote';
