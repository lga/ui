<template>
    <div :id="ID_FLUX_AZOTE">
        <TitreH3>Flux d'azote</TitreH3>

        <TexteP>
            Par essence, l’activité agricole consiste à exporter des produits alimentaires depuis les terres cultivées. Ce faisant, les nutriments
            présents dans les sols et incorporés par les végétaux sont également exportés. Les sols cultivés s’appauvrissent donc petit à petit et les
            agriculteurs doivent compenser ces pertes de nutriments afin de maintenir leur fertilité. Cela concerne en particulier l’azote (N), le
            phosphore (P) et le potassium (K), des éléments souvent limitants dans la croissance des végétaux. L'agriculture s'inscrit ainsi dans les
            grands cycles biochimiques : de l'azote, du carbone, du phosphore, etc.
        </TexteP>
        <TexteP
        >La fertilité des sols est assurée par :
            <ul>
                <li>
                    L'utilisation d’engrais, c’est-à-dire l’apport de nutriments depuis l’extérieur de la parcelle cultivée :
                    <ul>
                        <li>
                            engrais organiques traditionnel, dont les matières utilisées proviennent d’êtres vivants animaux ou végétaux (fumier,
                            lisier, guano, sang et os séchés, compost végétal...) ;
                        </li>
                        <li>engrais minéraux, qui dérivent de matières minérales inertes ou sont synthétisés industriellement ;</li>
                    </ul>
                </li>
                <li>L'enrichissement in situ en azote par fixation biologique du diazote atmosphérique grâce aux cultures de légumineuses ;</li>
                <li>Déposition atmosphérique d'azote.</li>
            </ul>
        </TexteP>
        <TexteP>
            Aujourd'hui produits en très grandes quantités, les engrais minéraux sont un élément clé du système agricole industrialisé. Cela marque un
            contraste fort avec le début du XXe siècle où les matières fécales et les urines étaient fortement valorisées pour fertiliser les sols
            dans une logique de circularité.
        </TexteP>
        <TexteP>
            Afin de quantifier les besoins en azote, notamment en engrais de synthèse, Territoires Fertiles utilise les résultats du modèle MacDyn-FS
            présenté ci-dessous.
        </TexteP>

        <TitreH4>Le modèle MacDyn-FS</TitreH4>

        <TexteP>
            Le modèle <TexteLien url="https://zenodo.org/records/6350666">MacDyN-FS</TexteLien> (Macro Dynamic Nitrogen balance model of a Farming
            System) a été conçu au cours de la thèse de doctorat de Corentin Pinsard à l'INRAE. Ce macro modèle de système agricole (compris ici comme
            un ensemble de fermes au sein d'un territoire administratif) décrit temporellement (pas de temps annuel) les flux de biomasse agricole (en
            masse d'azote) entre les animaux d'élevage (divisés par espèce), les cultures (séparées par espèce), les usages des sols (terres arables
            et prairies permanentes) et les humains.
        </TexteP>
        <TexteP>
            Dans sa version pour scénarios de simulation (paramètres forcés), les rendements des cultures dépendent des flux de fertilisants, et la
            gestion des troupeaux de la nourriture animale disponible. Une description brève du modèle est donnée ci-dessous tandis qu’une description
            complète de ce modèle (notamment le calcul des pertes de nutriments ou les règles d'allocation des flux de biomasse) est disponible dans
            le
            <TexteLien url="https://www.theses.fr/2022UPASB021">chapitre 2 de la thèse de Corentin Pinsard</TexteLien> et dans les publications
            scientifiques associées à la thèse (en accès libre dans les
            <TexteLien url="https://scholar.google.com/citations?user=mK87BjUAAAAJ">publications en tant que premier auteur</TexteLien>).
        </TexteP>
        <TexteP
        >Le modèle a été adapté pour les besoins de Territoires Fertiles notamment pour obtenir des résultats à l’échelle des communes.</TexteP
        >

        <TitreH5>Principes généraux</TitreH5>
        <TexteP>
            Le modèle analyse les besoins en azote des cultures pour chaque petite région agricole, et à l’intérieur pour chaque commune, en tenant
            compte des types de cultures et des rendements, et en déduit une estimation des apports en azote sur base des surfaces cultivées. Il n’y a
            pas de distinction entre les surfaces en bio et les surfaces en conventionnel. Les données de rendements utilisées intègre néanmoins les
            surfaces en bio, ce qui n'induit pas de surestimations de l'usage d'engrais de synthèse. Le modèle calcule également les besoins en azote
            des animaux d’élevage sur base des cheptels et de l’assiette par types d’animaux.
        </TexteP>
        <TexteP>
            Une fois ces deux besoins évalués, le modèle calcule les besoins d’importation de nourriture animale, par différence entre les besoins des
            animaux et la production végétale locale destinée aux animaux. Le schéma suivant résume le principe :
        </TexteP>
        <NuxtImg src="/images/schema-modele-macdynfs-800x600.png" width="800" height="600" alt="schéma-flux-azote" />
        <TexteP>
            In fine, le modèle permet d’obtenir une estimation de l’ensemble des flux d’azote liés au système agricole du territoire tels que les
            besoins en azote de la plante, l’apport nécessaire d’engrais de synthèse, la production locale de fourrages pour les animaux, etc.
        </TexteP>

        <TitreH5>Données mobilisées</TitreH5>
        <TexteP
        >Le modèle utilise :
            <ul>
                <li>
                    Des données de cheptel et surfaces agricoles :
                    <ul>
                        <li>
                            <LienMethodologieCheptels
                                libelle="nombre d’animaux par types d'animaux et groupes en UGB au niveau des communes"
                            ></LienMethodologieCheptels>
                        </li>
                        <li>
                            <LienMethodologieSurfacesAgricoles
                                libelle="surfaces agricoles utiles en hectares par types de culture"
                            ></LienMethodologieSurfacesAgricoles>
                        </li>
                        <li>de la correspondance entre les types de cultures RPG et les types de cultures du modèle MacDyN-FS</li>
                    </ul>
                </li>
                <li>
                    Les besoins alimentaires des animaux par catégories d’aliment et par types d'animaux (EFESE - à l’échelle des Petites Régions
                    Agricoles - valeur de 2010) [kgN/UGB]
                </li>
                <li>
                    Les rendements en matières brutes de la production végétale par types de culture (AGRESTE - échelle départementale - moyenne
                    annuelle de la période 2016-2018) [tMB/ha]
                </li>
                <li>
                    Des coefficients de répartition de la biomasse récoltée selon usages (alimentation humaine, alimentation animale, énergie) par
                    types de culture (EFESE - échelle France - valeur de 2010)
                </li>
            </ul>
        </TexteP>
    </div>
</template>

<script setup lang="ts">
import { NuxtImg } from '#components';
import TexteLien from '~/components/base/TexteLien.vue';
import TexteP from '~/components/base/TexteP.vue';
import TitreH3 from '~/components/base/TitreH3.vue';
import TitreH4 from '~/components/base/TitreH4.vue';
import TitreH5 from '~/components/base/TitreH5.vue';

import LienMethodologieCheptels from '../liens/LienMethodologieCheptels.vue';
import LienMethodologieSurfacesAgricoles from '../liens/LienMethodologieSurfacesAgricoles.vue';
import { ID_FLUX_AZOTE } from '../methodologie-utils.js';
</script>
