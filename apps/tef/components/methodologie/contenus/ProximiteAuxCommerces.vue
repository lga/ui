<template>
    <div>
        <TitreH3>Pourquoi des indicateurs sur la proximité aux commerces alimentaires ?</TitreH3>

        <TexteP>
            Note : pour de plus amples détails sur la méthodologie utilisée, vous pouvez consulter
            <TexteLien url="/pdf/Methodologie-Indicateurs accessibilite physique aux points de distribution alimentaire-V3.1.pdf">
                le document d'élaboration de cet méthodologie </TexteLien
            >.
        </TexteP>

        <TexteP>
            Depuis les années 1960, la distance moyenne parcourue par jour et par personne a connu une explosion avec la démocratisation de
            l’automobile individuelle, les politiques d’aménagement l’accompagnant (habitat pavillonnaire, non mixité urbaine) et les nouveaux modes
            de vie en résultant (augmentation de la distance domicile-travail). Cette distance continue aujourd’hui d’augmenter, bien que cette
            tendance soit incompatible avec une réduction de notre dépendance au pétrole et avec une trajectoire d’émissions de +1,5°C.
        </TexteP>
        <TexteP>
            En particulier, les déplacements des consommateurs pour leurs achats alimentaires sont réalisés à 90 % en voiture et s’élèvent en moyenne
            à plus de 60 kilomètres par semaine par foyer. Ce haut niveau de déplacements s’explique notamment par la transformation récente du
            paysage de commerces alimentaires : en l’espace de quelques décennies, la plupart des pays industrialisés sont passés d’un réseau de
            commerces de détail implantés dans les bourgs et centres-villes et accessibles à pied, à une distribution massivement dominée par des
            grandes et moyennes surfaces implantées en périphérie. Cette évolution est allée de pair avec le déclin des commerces spécialisés, dont le
            nombre a été divisé par 4 depuis 1950.
        </TexteP>
        <TexteP>
            Bien que courts au regard de la chaîne logistique complète, ces trajets sont très peu efficaces : ils consistent peu ou prou à déplacer
            une tonne d’acier sur plusieurs kilomètres pour transporter quelques kilogrammes de nourriture. Ils représentent ainsi 28 % de la
            consommation énergétique du transport alimentaire. On comprend ainsi que la disparition des commerces de proximité augmente fortement la
            dépendance énergétique des ménages : en grande surface, le trajet du consommateur représente en moyenne la moitié de la consommation
            énergétique liée aux transports. Cette dépendance à la voiture pour les achats alimentaires est problématique à plus d’un titre :
            <ul>
                <li>
                    elle induit un recours à une ressource finie (le pétrole) pour assurer un besoin vital : manger. La France est dépendante à 100%
                    d’importations depuis l’étranger. Dans un contexte où l’approvisionnement en pétrole est amené à connaître d’importantes
                    contraintes et en l’absence de substitut pouvant être facilement déployé, il s’agit d’un facteur de vulnérabilité des systèmes
                    alimentaires ;
                </li>
                <li>elle aggrave la précarité alimentaire pour les foyers modestes et non véhiculés ;</li>
                <li>elle n’est pas soutenable sur le plan climatique.</li>
            </ul>
        </TexteP>

        <TexteP>
            L'objectif proposé est d'évaluer la nature et la répartition géographique des points de distribution au regard des besoins de la
            population, afin d’approcher le niveau de dépendance théorique à la voiture (et donc aux énergies fossiles) des mangeurs pour leurs achats
            alimentaires.
        </TexteP>

        <TitreH4>Principe de calcul d'indicateurs de proximité aux commerces</TitreH4>
        <TexteP>
            Le calcul de la proximité des habitants aux commerces est réalisé en 3 étapes principales :
            <ul>
                <li>
                    Construction d'une base de données des commerces alimentaires, principalement à partir de la
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.commerces_osm" />, en la complétant avec la
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.base_permanente_equipements" />. La base BPE de l'INSEE contient en
                    effet certains commerces alimentaires absents d'OpenStreetMap. L'assemblage des deux sources et la suppression des doublons permet
                    d'obtenir une base plus représentative de la situation réelle. Chaque commerce est associé à une typologie donnée (commerce
                    généraliste, boulangerie-pâtisserie, boucherie-poissonnerie, autre commerce spécialisé).
                </li>
                <li>
                    Calcul de la distance des habitants à chaque type de points de distribution alimentaire : pour identifier où sont les habitants
                    par rapport aux commerces, nous utilisons les données carroyées de l’INSEE qui donnent la répartition de la population sur un
                    maillage de carreaux de 200m de largeur (soit 2.3M de carreaux). Elles permettent de calculer la distance à vol d'oiseau entre un
                    commerce alimentaire et le centre d’un «carreau » donné. Pour chacun des carreaux, il est ainsi possible de calculer la distance
                    au commerce le plus proche pour chaque type de commerce (ex : distance pour aller à la boulangerie la plus proche).
                </li>
                <li>
                    Agrégation des calculs à l’échelle d’un territoire (= un ensemble de carreaux) : les distances calculées au niveau de chaque
                    carreau sont agrégées à l’échelle des territoires (commune, EPCI, etc.), en retenant l’ensemble des carreaux dont le centre est
                    localisé sur le territoire. Deux méthodes d’agrégation permettent d’obtenir deux familles d’indicateurs :
                    <ul>
                        <li>
                            agrégation en faisant la moyenne des distances de chaque carreau pondérée par la population du carreau (par ex : « sur la
                            commune, les habitants sont en moyenne à 3.6 km d’une boulangerie ») ;
                        </li>
                        <li>
                            agrégation en dénombrant la population des carreaux dont la distance étudiée est inférieure ou supérieure à un seuil donné
                            et en la rapportant à la population totale du territoire (par ex : « sur la commune, 17% des habitants ont accès à une
                            boulangerie à moins de 2 km »).
                        </li>
                    </ul>
                </li>
            </ul>
        </TexteP>

        <TitreH4>Types de commerces alimentaires pris en compte</TitreH4>

        <TexteP>
            Les types de commerces retenus dans l'application sont les suivants : les commerces généralistes (épicerie, supermarché...), les
            boulangeries/pâtisseries, les boucheries/charcuteries/poissonneries et les autres commerces spécialisés (commerces spécialisés autres que
            les 2 précédents, eg. fromagerie, primeur...). Ils pourront être complétés à l'avenir, en particulier pour ajouter des commerces relatifs
            aux circuits courts et aux marchés de plein vent.
        </TexteP>

        <TitreH4 :id="IDS_DOMAINES.distancesPlusProchesCommerces">Distance au plus proche commerce par type de commerce alimentaire</TitreH4>

        <TexteP>
            Cet indicateur mesure la distance moyenne à vol d'oiseau entre le domicile et le plus proche commerce d'un type donné. Pour cela on
            utilise les principes et données énoncés au chapitre plus haut "Principe de calcul d'indicateurs de proximité aux commerces" :
        </TexteP>
        <TexteFormule>
            <ul>
                <li>
                    pour chaque carreau INSEE (associé à une population donnée) on calcule la distance à vol d'oiseau au plus proche commerce pour
                    chaque type de commerce puis on calcule les valeurs minimale, maximale et moyenne (on obtient donc pour chaque carreau plusieurs
                    types de distances : une valeur par type de commerce ; une valeur minimum = plus proche commerce tous types confondus ; une valeur
                    maximum = plus lointain commerce tous types confondus ; une valeur moyenne tous types de commerce) ;
                </li>
                <li>
                    pour chaque type de distance obtenue on fait la moyenne des valeurs obtenues pour chacun des carreaux appartenant au territoire
                    étudié pondérée par la population de chaque carreau.
                </li>
            </ul>
        </TexteFormule>

        <TitreH4 :id="IDS_DOMAINES.dependancePopulationVoiture">
            Part de la population théoriquement dépendante de la voiture pour accéder à un type de commerce
        </TitreH4>
        <TexteP>
            Cet indicateur correspond à la part de la population du territoire qui est située à plus de 2 km à vol d'oiseau (donc difficilement
            atteignable à vélo) du plus proche commerce, pour chaque type de commerce. Pour cela on utilise les principes et données énoncés au
            chapitre plus haut "Principe de calcul d'indicateurs de proximité aux commerces" :
        </TexteP>
        <TexteFormule>
            <ul>
                <li>
                    pour chaque carreau INSEE (associé à une population donnée) on calcule la distance à vol d'oiseau au plus proche commerce pour
                    chaque type de commerce puis on calcule les valeurs minimale, maximale et moyenne (on obtient donc pour chaque carreau plusieurs
                    types de distances : une valeur par type de commerce ; une valeur minimum = plus proche commerce tous types confondus ; une valeur
                    maximum = plus lointain commerce tous types confondus ; une valeur moyenne tous types de commerce) ;
                </li>
                <li>
                    pour chaque type de distance obtenue :
                    <ul>
                        <li>
                            on somme les populations de tous les carreaux appartenant au territoire étudié pour lequels la distance obtenue est
                            supérieure à 2 km ;
                        </li>
                        <li>
                            on rapporte cette somme à la population totale du territorie étudié (somme des populations de tous les carreaux
                            constitutifs du territoire).
                        </li>
                    </ul>
                </li>
            </ul>
        </TexteFormule>

        <TitreH4>Part de la population théoriquement dépendante de la voiture pour ses achats alimentaires</TitreH4>
        <TexteP>
            Cet indicateur évalue la part de la population théoriquement dépendante de la voiture pour accéder à un ensemble représentatif de
            commerces alimentaires, c'est à dire la part de la population ne disposant pas de 3 commerces de type différent accessibles à vélo depuis
            le domicile. Il est calculé de la même manière que l'indicateur "Part de la population théoriquement dépendante de la voiture pour accéder
            à un type de commerce", en retenant comme distance celle qui permet d'accéder à 3 types de commerces différents sur les 4 types
            identifiés, ceci afin de retenir un ensemble de commerces couvrant une part significative des besoins des habitants en terme de diversité.
        </TexteP>

        <TitreH4 :id="IDS_DOMAINES.dependanceTerritoireVoiture">
            Part du territoire dont la population est en majorité théoriquement dépendante de la voiture pour ses achats alimentaires
        </TitreH4>
        <TexteP>
            Cet indicateur évalue la part du territoire dont la population est en majorité théoriquement dépendante de la voiture pour accéder à un
            ensemble représentatif de commerces alimentaires, c'est à dire ne disposant pas de 3 commerces de type différent accessibles à vélo depuis
            le domicile. Pour les communes, il est obtenu en calculant la part de carreaux INSEE dont la population n'a pas accès à 3 commerces de
            type différent à vélo depuis le domicile. Pour les territoires supracommunaux, il est obtenu en calculant la part des communes pour
            lesquelles plus de 50% des habitants ne disposent pas de ces 3 commerces de type différent accesibles à vélo depuis le domicile (ce qui
            correspond à l'indicateur précedent).
        </TexteP>

        <TitreH4 :id="IDS_DOMAINES.transformationDistribution">Évaluation globale du maillon Transformation & Distribution</TitreH4>
        <TexteP>
            La note et le message de synthèse sont basés sur l'indicateur i "Part de la population dépendante de la voiture pour ses achats
            alimentaires". La note est calculée de la façon suivante :
        </TexteP>
        <TexteFormule>N = ( 100 - i ) / 10</TexteFormule>

        <TitreH4>Limites</TitreH4>
        <TexteP
        >Les indicateurs définis présentent différents biais qu’il est important de garder en tête, notamment :
            <ul>
                <li>
                    La base des commerces alimentaires consolidée à partir d'OSM et BPE contient un nombre significatifs d'erreurs (commerces
                    manquants, commerces présents alors qu'ils sont fermés). Il est possible de vérifier les commerces connus de OSM à
                    <TexteLien url="https://www.openstreetmap.org">cette adresse</TexteLien>. En cas d'erreur, nous vous encourageons à proposer une
                    mise à jour sur ce même site ce qui nous permettra de prendre en compte ces corrections lors du prochain import des données.
                </li>
                <li>
                    Les pratiques alternatives (circuits-courts...) dont les points de vente sont plus difficilement identifiables, ou
                    l’auto-consommation (qui limite le besoin en points de distribution alimentaire de proximité) ne sont pas intégrées. Ces dernières
                    pourraient être intégrées via l’utilisation d’autres données (localisation des AMAP, des potagers privés ou partagés...). De même,
                    les marchés de plein vent ne sont pour l'instant pas pris en compte.
                </li>
                <li>
                    Les calculs ne prennent pas en compte que les achats peuvent être fait lors de déplacements domicile-travail ou domicile-loisirs
                    puisque tout est pensé depuis le domicile.
                </li>
                <li>
                    Aucune différence n’est faite entre une situation où tous les achats sont faits dans un seul point de distribution (ex : un
                    supermarché), et une situation où ils sont fait dans plusieurs points de distribution (ex : plusieurs points de distribution de
                    détails). La 2e situation implique probablement plus de déplacements et de kilomètres parcourus, mais il paraît difficile
                    d’évaluer ce surplus via un modèle abordable et fiable.
                </li>
                <li>
                    Les pratiques de mobilité et les infrastructures de transport, notamment les transports en commun, ne sont pas prises en compte :
                    l’approche par calcul de distance « à vol d’oiseau » est simpliste, car il ne prend pas en compte les infrastructures existantes
                    (présence/absence de route, de transports en communs), ou les obstacles naturels (ex : vallée de montagne).
                </li>
            </ul>
        </TexteP>
    </div>
</template>

<script setup lang="ts">
import { IDS_DOMAINES, SOURCES_DONNEES } from '@lga/indicateurs';

import TexteFormule from '~/components/base/TexteFormule.vue';
import TexteLien from '~/components/base/TexteLien.vue';
import TexteP from '~/components/base/TexteP.vue';
import TitreH3 from '~/components/base/TitreH3.vue';
import TitreH4 from '~/components/base/TitreH4.vue';

import LienMethodologieSourceDonnee from '../liens/LienMethodologieSourceDonnee.vue';
</script>
