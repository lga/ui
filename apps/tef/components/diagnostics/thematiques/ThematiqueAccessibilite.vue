<template>
    <TemplateThematique :id-theme="modele.idTheme" :sectionsMenu="SECTIONS">
        <template #bandeauTitre>
            <TexteP>
                Produire, on sait faire. Mais ce n’est pas parce que la nourriture est disponible, qu’elle est accessible à tous. Elle peut être trop
                chère, éloignée géographiquement, ou ne pas correspondre aux habitudes et préférences alimentaires. En réalité, l’accès à
                l’alimentation est marqué, en France comme ailleurs, par de profondes inégalités. L’<GloseAvecPopoverDefinition
                    cleGlose="insecuriteAlimentaire"
                />
                n’a pas disparu, au contraire, elle progresse.
            </TexteP>
        </template>

        <template #[ID_SECTION_1]>
            <TexteP>
                A l’instar d’autres pays occidentaux, un nombre croissant de citoyens français sont en situation de précarité alimentaire.
                C'est-à-dire qu’ils rencontrent régulièrement des difficultés pour s’alimenter : nourriture insuffisante, de mauvaise qualité, choix
                contraint.
                <TexteAvecPopoverSource
                    numero="1"
                    :source="
                        htmlstring`Le Morvan F. et Wanecq T. (2019) La lutte contre la précarité alimentaire. Évolution du soutien public à une politique sociale, agricole et de santé publique. Rapport IGAS n°2019-069R.`
                    "
                >Le recours à l’aide alimentaire a ainsi presque doublé en dix ans et concerne aujourd’hui plus de cinq millions de personnes en
                    France</TexteAvecPopoverSource
                >. Et c’est sans compter le nombre de personnes qui, sans recourir à l’aide alimentaire, se contraignent dans leur alimentation.
                Difficile d’avoir des chiffres précis mais
                <TexteAvecPopoverSource
                    numero="2"
                    :source="
                        htmlstring`${creerLienExterneHtmlBrut('https://www.secourspopulaire.fr/barometre-pauvrete-ipsos/', 'Ipsos et Secours Populaire Français (2021) Résultats du 15e baromètre IPSOS / SPF sur la perception de la pauvreté et la précarité par les Françaises et les Français')} | ${creerLienExterneHtmlBrut('https://www.credoc.fr/publications/en-forte-hausse-la-precarite-alimentaire-sajoute-a-dautres-fragilites', 'Marianne Bléhaut, Mathilde Gressier 2023 En forte hausse, la précarité alimentaire s’ajoute à d’autres fragilités')}.`
                    "
                >on estime qu’entre 15 et 30&nbsp;% des Français ne mangent pas autant qu’ils le voudraient pour des raisons financières, et
                    qu’entre 40 et 60&nbsp;% se restreignent sur la qualité des produits</TexteAvecPopoverSource
                >.
            </TexteP>

            <TexteP v-if="useGetStateIdTerritoire().value">
                Quelle est la situation sur votre territoire&nbsp;?
                <Encadre
                    type="focus-territoire"
                    titre="Carte de la précarité alimentaire"
                    :informationsMethodologie="modele.methodologieCartePrecariteAlimentaire"
                >
                    <CartePrecariteAlimentaire v-if="modele.territoire" class="m-auto" :territoire="modele.territoire"></CartePrecariteAlimentaire>
                </Encadre>
            </TexteP>
        </template>

        <template #[ID_SECTION_2]>
            <TexteP>
                Cette situation de précarité alimentaire est d’autant plus préoccupante qu’elle peut rapidement s'aggraver en période de crise.
                <TexteAvecPopoverSource
                    numero="3"
                    :source="
                        htmlstring`Chiffre de la Direction de la recherche, des études, de l’évaluation et des statistiques (DREES) et de l’INSEE. ${creerLienExterneHtmlBrut('https://drees.solidarites-sante.gouv.fr/communique-de-presse/aide-alimentaire-une-hausse-prononcee-des-volumes-distribues-par-les', 'Voir sur le site internet de la DREES')}`
                    "
                >Ainsi a-t-on observé durant la pandémie de Covid-19 une hausse de 10&nbsp;% des volumes de nourriture distribués par les
                    associations d’aide alimentaire</TexteAvecPopoverSource
                >
                (qui, rappelons-le, n’est que la partie la plus visible de la précarité alimentaire). En cause notamment, la perte de revenu pour de
                nombreuses personnes du fait de la crise sanitaire.
            </TexteP>

            <TexteP>
                Même constat en 2022 dans un contexte de forte inflation.
                <TexteAvecPopoverSource
                    numero="4"
                    :source="
                        htmlstring`${creerLienExterneHtmlBrut('https://www.credoc.fr/publications/en-forte-hausse-la-precarite-alimentaire-sajoute-a-dautres-fragilites', 'Marianne Bléhaut, Mathilde Gressier 2023 En forte hausse, la précarité alimentaire s’ajoute à d’autres fragilités')}.`
                    "
                >L’augmentation des prix de la nourriture a atteint 15&nbsp;% et s’est accompagnée d’une hausse de 30&nbsp;% de la précarité
                    alimentaire</TexteAvecPopoverSource
                >.
            </TexteP>

            <TexteP>
                Le risque que des crises semblables ou plus graves surviennent dans les années à venir se renforce à mesure que les ressources
                stratégiques (énergies fossiles, matières premières) se raréfient, que le dérèglement climatique s’intensifie et que les tensions
                géopolitiques et sociales se multiplient.
            </TexteP>
        </template>

        <template #[ID_SECTION_3]>
            <TexteP>
                Pour de nombreux français, un véhicule en panne peut rapidement se transformer en jeûne forcé. L’étalement urbain et le développement
                des supermarchés ont eu raison de la plupart des petits commerces alimentaires.
                <TexteAvecPopoverSource
                    numero="5"
                    :source="
                        htmlstring`Ritzenthaler A. (2016) Les circuits de distribution des produits alimentaires. Avis du Conseil Économique Social et Environnemental.`
                    "
                >Leur nombre a été divisé par quatre depuis 1950</TexteAvecPopoverSource
                >
                et aujourd’hui,
                <TexteAvecPopoverSource
                    numero="6"
                    :source="
                        htmlstring`INSEE (2019) Dénombrement des équipements en 2018. Catégories « Hypermarchés », « Supermarchés », « Supérettes » et « Épiceries ».`
                    "
                >sept communes sur dix ne disposent plus d’aucun magasin d’alimentation générale</TexteAvecPopoverSource
                >. Résultat,
                <TexteAvecPopoverSource
                    numero="7"
                    :source="
                        htmlstring`Les courses alimentaires et la restauration hors domicile engendrent 1 360 km/an/personne, soit 78 km par semaine pour un foyer de trois personnes (dont 63 km pour les courses et 15 km pour la restauration) ; voir Barbier C. et al. (2019) L’empreinte énergétique et carbone de l’alimentation en France`
                    "
                >les déplacements des consommateurs pour leurs achats alimentaires sont réalisés à 90&nbsp;% en voiture et s’élèvent en moyenne à
                    plus de 60 kilomètres par semaine pour un foyer de trois personnes</TexteAvecPopoverSource
                >.
            </TexteP>

            <TexteP>
                Dans un monde où le pétrole devient une ressource rare, les commerces alimentaires de proximité vont de nouveau être les meilleurs
                amis du garde-manger.
            </TexteP>

            <TexteP v-if="useGetStateIdTerritoire().value">
                Et sur votre territoire, la voiture est-elle indispensable ?
                <Encadre
                    type="focus-territoire"
                    :url-en-savoir-plus="useRouteDiagnosticsIndicateur(modele.indicateurPartPopulationDependanteVoiture.indicateur)"
                >
                    <IndicateurFocusTerritoire
                        :chiffre="modele.indicateurPartPopulationDependanteVoiture.chiffre"
                        :unite="modele.indicateurPartPopulationDependanteVoiture.unite"
                        complement="de la population du territoire est théoriquement dépendante de la voiture pour faire ses courses alimentaires."
                    >
                    </IndicateurFocusTerritoire>
                </Encadre>
            </TexteP>
        </template>

        <template #[ID_SECTION_4]>
            <TexteP>
                Que plus d’une personne sur cinq n’ait pas les moyens de manger correctement en France, septième puissance mondiale, pays de la
                gastronomie et forte d’une agriculture moderne et productive devrait nous interpeller. Malgré l’engagement des bénévoles, l’aide
                alimentaire montre ses limites, que ce soit au niveau de la qualité des produits disponibles (souvent des invendus de la grande
                distribution) ou de sa capacité à toucher tous les publics concernés par l’insécurité alimentaire. Pourtant il n’y a là aucune
                fatalité, notre pays est tout à fait capable de garantir un véritable droit à l’alimentation à l’ensemble de ces citoyens.
            </TexteP>

            <TexteP>
                Dans cette perspective, la création d’une sécurité sociale de l’alimentation constitue un projet à la hauteur des enjeux. Le principe
                est simple : il consiste à octroyer à chaque citoyen un budget mensuel (par exemple 150 euros) consacré à l’achat de produits
                alimentaires conventionnés. Les critères de conventionnement des produits et leur prix de vente (prix plafond et plancher) seraient
                choisis par une ou plusieurs instances démocratiques représentatives.
            </TexteP>

            <TexteP>
                En créant un marché dédié à prix encadrés, protégé de la concurrence internationale par son côté captif, un tel système offre des
                débouchés massifs et sécurisés aux produits issus de l’agroécologie et permet à de nombreux producteurs de s’engager dans une
                transformation de leur système. Il garantit en parallèle un accès universel à une alimentation choisie, saine et durable. Ce faisant,
                la sécurité sociale de l’alimentation propose une réponse globale pour dépasser les obstacles qui empêchent la transformation du
                modèle actuel.
            </TexteP>

            <Encadre
                type="ils-l-ont-fait"
                localisation="Montpellier"
                url-en-savoir-plus="https://reporterre.net/A-Montpellier-on-teste-une-caisse-alimentaire-facon-Secu"
            >Ce territoire a lancé depuis 2023 une des premières expérimentations de caisse commune alimentaire du pays. 450 habitants cotisent en
                fonction de leurs moyens et reçoivent 100 euros par mois pour acheter des produits alimentaires dans des lieux décidés
                collectivement.</Encadre
            >

            <TexteP v-if="useGetStateIdTerritoire().value">
                De nombreuses initiatives se lancent partout en France. Et chez vous ?
                <Encadre type="focus-territoire">
                    <div class="sm:grid grid-cols-2 gap-4">
                        <div class="col-start-1 col-end-1">
                            <div>
                                Pour le savoir, consultez la
                                <TexteLien
                                    url="https://securite-sociale-alimentation.org/les-dynamiques-locales/carte-des-initiatives-locales-de-la-ssa/"
                                >carte des initiatives locales</TexteLien
                                >
                                !
                            </div>
                            <div class="pt-2">
                                Et pour en apprendre plus sur la sécurité sociale de l’alimentation, regardez le
                                <TexteLien url="https://securite-sociale-alimentation.org/"
                                >site du Collectif pour une sécurité sociale de l’alimentation</TexteLien
                                >.
                            </div>
                        </div>
                        <Image
                            src="/images/thematiques/carte-vitalim-768x484.jpg"
                            alt="Carte Vitalim"
                            :width="768"
                            :height="484"
                            class="col-start-2 col-end-2 row-start-1 max-w-[300px] m-auto"
                        ></Image>
                    </div>
                </Encadre>
            </TexteP>

            <ARetenir>
                <ListLiAvecIcone>
                    Malgré une production de nourriture largement suffisante dans notre pays, environ cinq millions de Français ont recours à l’aide
                    alimentaire.
                </ListLiAvecIcone>
                <ListLiAvecIcone
                >Cette partie la plus visible de la précarité alimentaire ne permet pas de mesurer l’ampleur du phénomène. On estime que 15 à
                    30&nbsp;% des Français se restreignent dans leur alimentation pour des raisons financières.</ListLiAvecIcone
                >
                <ListLiAvecIcone>
                    Les crises récentes ont aggravé la précarité alimentaire, cela doit nous alerter face aux risques de perturbations futures.
                </ListLiAvecIcone>
                <ListLiAvecIcone
                >Dans certains territoires, il est difficile de faire ses courses sans voiture. Une situation problématique face aux incertitudes
                    pesant sur l’approvisionnement en pétrole du pays dans les années à venir.</ListLiAvecIcone
                >
                <ListLiAvecIcone
                >La mise en place d’une Sécurité Sociale de l‘Alimentation est une réponse globale aux problèmes de précarité alimentaire et à la
                    nécessité de transformer nos modes de production de nourriture.</ListLiAvecIcone
                >
            </ARetenir>
        </template>
    </TemplateThematique>
</template>

<script setup lang="ts">
import { htmlstring } from '@lga/base';

import { useGetStateIdTerritoire, useRouteDiagnosticsIndicateur } from '@/composables';
import ListLiAvecIcone from '~/components/base/ListLiAvecIcone.vue';
import TexteLien from '~/components/base/TexteLien.vue';
import TexteP from '~/components/base/TexteP.vue';
import { creerLienExterneHtmlBrut } from '~/modeles/liens-utils.js';
import type { ItemLienHash } from '~/modeles/navigation/ItemMenuModele.js';
import type { ThematiqueAccessibiliteModele } from '~/modeles/thematiques/thematique-accessibilite.js';

import CartePrecariteAlimentaire from '../commun/CartePrecariteAlimentaire.vue';
import ARetenir from './commun/ARetenir.vue';
import Encadre from './commun/Encadre.vue';
import GloseAvecPopoverDefinition from './commun/GloseAvecPopoverDefinition.vue';
import Image from './commun/Image.vue';
import IndicateurFocusTerritoire from './commun/IndicateurFocusTerritoire.vue';
import TexteAvecPopoverSource from './commun/TexteAvecPopoverSource.vue';
import TemplateThematique from './TemplateThematique.vue';

defineProps<{
    modele: ThematiqueAccessibiliteModele;
}>();

const ID_SECTION_1 = 'plus-de-deux-français-sur-dix-n-ont-pas-les-moyens-de-bien-manger';
const ID_SECTION_2 = 'l-insecurite-alimentaire-s-accroît-en-periode-de-crise';
const ID_SECTION_3 = 'en-voiture-tout-le-monde';
const ID_SECTION_4 = 'garantir-a-tous-une-alimentation-saine-et-durable';

const SECTIONS: ItemLienHash[] = [
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_1,
        libelle: 'Plus de deux Français sur dix n’ont pas les moyens de bien manger'
    },
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_2,
        libelle: 'L’insécurité alimentaire s’accroît en période de crise'
    },
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_3,
        libelle: 'En voiture tout le monde !'
    },
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_4,
        libelle: 'Garantir à tous une alimentation saine et durable : la sécurité sociale de l’alimentation ?'
    }
];
</script>
