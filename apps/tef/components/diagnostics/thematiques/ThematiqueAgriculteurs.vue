<template>
    <TemplateThematique :id-theme="modele.idTheme" :sectionsMenu="SECTIONS">
        <template #bandeauTitre>
            <TexteP>
                Agriculteur, une espèce en voie de disparition ? Si la tendance actuelle se poursuit, il n’y aura bientôt plus grand monde pour
                produire notre nourriture. Pourtant le métier continue de séduire. Alors ? Prêt à s’engager pour la transmission des fermes&nbsp;?
            </TexteP>
        </template>

        <template #[ID_SECTION_1]>
            <TexteP>
                En 1860, l’agriculture occupait plus d’un actif sur deux en France. Ce qu’on appelait encore la paysannerie concernait même une partie
                encore plus large de la population, car les femmes et les enfants sollicités pour travailler la terre ne figuraient pas toujours dans
                les statistiques. Puis, l’industrialisation de l’agriculture et les gains de productivité phénoménaux qui l’accompagnèrent ont
                entraîné en un siècle la quasi-disparition de ce monde paysan, si bien qu’aujourd’hui
                <TexteAvecPopoverSource
                    numero="1"
                    :source="
                        htmlstring`Agreste (2023) ${creerLienExterneHtmlBrut('https://agreste.agriculture.gouv.fr/agreste-web/disaron/GraFra2023Integral/detail/', 'Graph’Agri 2023 Emploi')}.`
                    "
                >moins de 3&nbsp;% des actifs travaillent dans l’agriculture</TexteAvecPopoverSource
                >. Et ce déclin se poursuit encore de nos jours.
                <TexteAvecPopoverSource
                    numero="2"
                    :source="
                        htmlstring`Caraes et Vauthier (2022) Recensement agricole 2020 : 416 054 exploitations agricoles en activité sur le territoire national. Analyses et Perspectives n°2207.`
                    "
                >Entre 2010 et 2020, la France a perdu près de 100 000 agriculteurs et autant de fermes (soit 20&nbsp;% du
                    total)</TexteAvecPopoverSource
                >.
            </TexteP>

            <TexteP>
                Cette situation n’est pas prête de s’améliorer car
                <TexteAvecPopoverSource
                    numero="3"
                    :source="
                        htmlstring`Article « Recensement agricole 2020. Âge des exploitants et devenir des exploitations. » dans le numéro de juillet 2022 du journal Primeur de l’Agreste.`
                    "
                >43&nbsp;% des chefs d’exploitation ont 55&nbsp;ans ou plus</TexteAvecPopoverSource
                >
                et quitteront donc leur activité d’ici une dizaine d’années.
                <TexteAvecPopoverSource
                    numero="4"
                    :source="htmlstring`Hervieu et Purseigle (2022) Le problème agricole français. S.E.R. Études 2022/7.`"
                >Au rythme actuel de trois départs pour deux installations</TexteAvecPopoverSource
                >, on s’attend donc à ce que la chute du nombre d’agriculteurs se poursuive.
            </TexteP>

            <TexteP v-if="useGetStateIdTerritoire().value">
                Et chez vous ?
                <Encadre
                    type="focus-territoire"
                    titre="Evolution du nombre d'agriculteurs"
                    :informationsMethodologie="
                        calculerInformationsMethodologieConstatEvolutionPopulationAgricole(modele.evolutionAgriculteurs.annees)
                    "
                >
                    <ChartEvolutionPopulationAgricole
                        v-if="modele.evolutionAgriculteurs.nombreAgriculteurs[0] > 0"
                        class="m-auto"
                        :annees="modele.evolutionAgriculteurs.annees"
                        :nombre-agriculteurs="modele.evolutionAgriculteurs.nombreAgriculteurs"
                    ></ChartEvolutionPopulationAgricole>
                </Encadre>
            </TexteP>

            <TexteP>
                Pour les territoires ruraux, la diminution du nombre d'agriculteurs est une mauvaise nouvelle. Elle signifie moins d’activité
                économique, moins d’emplois induits, moins d’attractivité. Pour les agriculteurs qui restent, l’isolement social se renforce.
                L’agrandissement des exploitations entraîne une charge de travail plus importante, un plus haut niveau d’endettement et des
                difficultés supplémentaires à transmettre son outil de travail. Il est par ailleurs peu favorable à la diffusion des pratiques
                agroécologiques car il s’accompagne souvent d’une simplification technique, d'une homogénéisation des paysages, et d’une
                spécialisation accrue dans un type de production.
            </TexteP>

            <Encadre
                v-if="useGetStateIdTerritoire().value"
                type="simulez-vos-actions"
                :url-en-savoir-plus="useRouteSimulateur(useGetStateIdTerritoire().value, 'Emplois')"
            >
                <p>Combien d’emplois agricoles seraient nécessaires si on consommait plus local ?</p>
            </Encadre>
        </template>

        <template #[ID_SECTION_2]>
            <TexteP>
                Les obstacles au renouvellement des générations en agriculture sont nombreux, en particulier pour les installations hors cadre
                familial, et encore plus pour les personnes non issues du monde agricole. On peut citer le
                <TexteAvecPopoverSource
                    numero="5"
                    :source="
                        htmlstring`Voir notamment l’avis du Conseil économique, social et environnemental (CESE) sur la question : Coly B. (2020) Entre transmettre et s’installer, l’avenir de l’agriculture !`
                    "
                >manque d’adéquation entre les exploitations mises sur le marché et les souhaits des candidats, le manque d’information concernant
                    les opportunités de reprise, les difficultés pour trouver un logement, le temps à consacrer à la transmission pour le cédant et le
                    risque de ne pas aboutir malgré son investissement</TexteAvecPopoverSource
                >. Mais le coût prohibitif de la plupart des exploitations à reprendre est probablement l’un des éléments les plus déterminants.
            </TexteP>

            <TexteP>
                S’installer en agriculture coûte cher ! Après des décennies d’agrandissement, les fermes ont atteint un niveau de capitalisation
                considérable. D’un point de vue comptable,
                <TexteAvecPopoverSource
                    numero="6"
                    :source="
                        htmlstring`Agreste (2023) ${creerLienExterneHtmlBrut('https://agreste.agriculture.gouv.fr/agreste-web/disaron/GraFra2023Integral/detail/', 'Graph’Agri 2023</a> Résultats des exploitations')}.`
                    "
                >l’actif moyen d’une exploitation française s’élève à près de 500 000 euros, dont 280 000 d’immobilisations (foncier, bâtiments,
                    matériel, animaux reproducteurs)</TexteAvecPopoverSource
                >. Mais les valeurs de reprise sont souvent bien supérieures :
                <TexteAvecPopoverDefinition
                    :definition="
                        htmlstring`Note : Contrairement à la valeur comptable qui tient compte de la dépréciation d'un bien (à travers le calcul de son amortissement), sa valeur de marché (ou vénale) correspond au prix constaté pour des biens similaires. Pour de nombreux biens agricoles, le prix du marché est supérieur à la valeur comptable, son propriétaire réalisant alors une plus-value au moment de la vente.`
                    "
                >valorisation des immobilisations au prix « du marché »</TexteAvecPopoverDefinition
                >, rachat de la maison d’habitation,
                <TexteAvecPopoverDefinition
                    :definition="
                        htmlstring`Ce type de pratique, bien qu'illégale, est répandue dans certaines régions de grandes cultures et les montants peuvent être significatifs (de 3 000 à 6 000 euros l’hectare pour une transmission familiale jusqu’à plus de 10 000 euros l’hectare en dehors de ce cadre). Voir Lallau B. et al. (2015) L’analyse du pas-de-porte en agriculture et le renouvellement des générations en zone de fermage majoritaire. Rapport de synthèse.`
                    "
                >monnayage de la transmission des baux dans certaines régions</TexteAvecPopoverDefinition
                >… Ainsi, il n’est pas rare de voir des montants de reprise dépassant le million d’euros sur le site d’annonces des
                <GloseAvecPopoverDefinition cleGlose="safer" />. De quoi en refroidir plus d’un.
            </TexteP>

            <TexteP>
                Même en cas de transmission familiale,
                <TexteAvecPopoverSource
                    numero="7"
                    :source="
                        htmlstring`Voir notamment Solidarité Paysans (2013) Dans l’engrenage de l’endettement. Dans Miramap (ed.) Une autre finance pour une autre agriculture (pp. 53–60). Éditions Yves Michel.`
                    "
                >le recours à l'endettement est quasi-systématique et pèse durablement sur la trésorerie de l’exploitation</TexteAvecPopoverSource
                >.
                <TexteAvecPopoverSource
                    numero="8"
                    :source="
                        htmlstring`Sur la période 2012-2021, 41 % de l’excédent brut d’exploitation (une mesure du bénéfice de l’exploitation agricole) est en moyenne consacré au remboursement des emprunts (capital et intérêts) ; Agreste (2023) ${creerLienExterneHtmlBrut('https://agreste.agriculture.gouv.fr/agreste-web/disaron/GraFra2023Integral/detail/', 'Graph’Agri 2023 Résultats des exploitations. Endettement.')}`
                    "
                >Le bénéfice de l’activité agricole est consacré en grande partie au remboursement des emprunts et donc à la constitution d’un
                    capital au détriment du revenu et de la protection sociale</TexteAvecPopoverSource
                >. On en arrive à ce paradoxe :
                <TexteAvecPopoverSource
                    numero="9"
                    :source="
                        htmlstring`Le patrimoine médian net d’emprunts d’un ménage comprenant au moins un exploitant agricole est de 438 000 euros contre 117 000 pour l’ensemble de la population française, le taux de pauvreté monétaire est quant à lui de 16 % contre 14 % pour l’ensemble de la population ; données issues de la publication « Transformations de l'agriculture et des consommations alimentaires » de l’INSEE (2024).`
                    "
                >les agriculteurs constituent la catégorie professionnelle qui affiche à la fois le plus haut niveau de patrimoine et le plus haut
                    taux de pauvreté</TexteAvecPopoverSource
                >
                !
            </TexteP>

            <TexteP>
                La capitalisation est une obligation de métier, mais elle est accentuée par l’environnement réglementaire et professionnel.
                <TexteAvecPopoverSource
                    numero="10"
                    :source="
                        htmlstring`Voir notamment Delaire G. et al. (2011) La fiscalité du bénéfice réel agricole doit-elle continuer de subventionner l’accumulation des moyens de production ? Économie rurale 323.`
                    "
                >Les
                    <TexteAvecPopoverDefinition :definition="htmlstring`Déduction pour investissement, exonération des plus-values.`"
                    >dispositifs fiscaux</TexteAvecPopoverDefinition
                    >
                    qui encadrent les investissements permettent de réduire l’assiette des prélèvements des impôts et des cotisations
                    sociales</TexteAvecPopoverSource
                >. Cette optimisation fiscale basée sur le suréquipement devient parfois une fin en soi, au détriment de la protection sociale et de
                la transmission de l’exploitation.
            </TexteP>

            <TexteP>
                L’endettement structurel de la profession contribue aux faibles revenus et en particulier, aux faibles retraites.
                <TexteAvecPopoverSource
                    numero="11"
                    :source="htmlstring`DREES (2020) Le niveau des pensions. Dans « Les retraités et les retraites » édition 2020.`"
                >Celles-ci s’élèvent en moyenne à 860 euros par mois pour un agriculteur ayant fait une carrière complète</TexteAvecPopoverSource
                >. La situation est encore pire pour de nombreuses femmes qui, ayant travaillé toute leur vie sans statut professionnel ni revenu
                auprès de leur compagnon,
                <TexteAvecPopoverSource
                    numero="12"
                    :source="
                        htmlstring`Chassaigne A. et al. (2021) Proposition de loi visant à assurer la revalorisation des pensions de retraites agricoles les plus faibles. Proposition de loi n° 4137 présentée à l’Assemblée Nationale.`
                    "
                >touchent la plupart du temps des pensions inférieures à 600 euros mensuels</TexteAvecPopoverSource
                >. Vendre sa ferme à bon prix en fin de carrière est donc un enjeu crucial, sous peine pour certains de basculer dans la pauvreté. Et
                le cycle endettement-capitalisation peut recommencer pour la génération suivante, jusqu’à l’impasse dans laquelle se trouvent la
                plupart des fermes aujourd’hui.
            </TexteP>
        </template>

        <template #[ID_SECTION_3]>
            <TexteP>
                <TexteAvecPopoverSource
                    numero="13"
                    :source="
                        htmlstring`Voir leur livre « Une agriculture sans agriculteurs : la révolution indicible » publié en 2022 aux édition Les Presses de Science Po.`
                    "
                >Cette formule proposée par les sociologues Bertrand Hervieu et François Purseigle</TexteAvecPopoverSource
                >
                fait écho à la disparition du modèle familial traditionnel et à la montée en puissance d’une agriculture « aux allures de firme ».
                Deux grandes tendances sont à l'œuvre.
            </TexteP>

            <TexteP>
                On constate d’un côté une augmentation de la délégation du travail auprès d’entreprises spécialisées. Cette sous-traitance peut-être
                occasionnelle (par exemple pour réaliser un chantier de récolte) ou intégrale (conduite d’une culture de A à Z). Parfois, c’est
                également la gestion administrative et financière de l’exploitation qui est sous-traitée. Le « chef d’exploitation » officiel, parfois
                très éloigné du monde agricole, se contente dans les cas les plus extrêmes d’encaisser le chèque et les subventions.
            </TexteP>

            <TexteP>
                En parallèle, on assiste à une complexification des formes juridiques et des participations aux sociétés agricoles. Des holdings
                émergent qui regroupent plusieurs entreprises agricoles en apparence indépendantes. Les exploitations s’ouvrent à des investisseurs
                extérieurs, parfois des industriels qui y voient un moyen de sécuriser leurs approvisionnements en limitant les risques liés à
                l’endettement de leurs fournisseurs. Ce phénomène de concentration par le biais des formes sociétaires est à la fois difficile à
                mesurer et impossible à réguler en l’état actuel, notamment sur les questions d’accès au foncier.
            </TexteP>

            <TexteP>
                Si ces évolutions répondent en partie aux problèmes soulevés par la diminution du nombre d’exploitants agricoles et par la difficulté
                à transmettre des fermes fortement capitalisées, elles posent néanmoins question. Sans action forte sur le renouvellement des
                générations, le scénario d’une agriculture sans agriculteurs aura toutes les chances de se réaliser. L’agriculture deviendra un
                secteur industriel comme les autres, poursuivant la logique de diminution des coûts de production et privilégiant la rentabilité pour
                les investisseurs. Les chances que cela contribue à une meilleure souveraineté alimentaire sont assez maigres.
            </TexteP>
        </template>

        <template #[ID_SECTION_4]>
            <TexteP>
                L’ensemble des obstacles à la transmission des exploitations agricoles dépassent souvent ce qui est du ressort des cédants ou des
                repreneurs, peu importe leur bonne volonté. Seule une politique globale, s’appuyant sur des moyens et des outils adaptés, permettra de
                dépasser ces obstacles.
            </TexteP>

            <TexteP>
                Considérer que les agriculteurs doivent à tout prix devenir propriétaires de leur outil de travail n’a rien d’une évidence. Ce n’est
                pas le fait en soi d’être propriétaire qui importe, mais la sécurité, la liberté d’entreprendre et l’indépendance vis-à-vis des tiers
                que cela est censé procurer. Les niveaux d’endettement actuels rendent ces promesses très lointaines. En rachetant et en mettant à
                disposition des terres et du bâti agricole à des paysans,
                <TexteAvecPopoverDefinition
                    :definition="
                        htmlstring`Mouvement national dont l’objet est d’accompagner l’installation paysanne et de protéger le foncier agricole. ${creerLienExterneHtmlBrut('https://terredeliens.org/', 'Voir leur site internet')}.`
                    "
                >le mouvement Terre de Liens</TexteAvecPopoverDefinition
                >
                a fait la démonstration qu’il était possible de casser le cercle vicieux de l’endettement tout en garantissant la pérennité des outils
                de production agricoles. Nous pouvons nous appuyer sur ces initiatives réussies pour passer à l’échelle supérieure.
            </TexteP>

            <TexteP>
                Une mesure emblématique consisterait à créer des offices fonciers chargés d’acquérir pour
                <span class="italic">une durée indéfinie</span> et de mettre à disposition des biens immobiliers (terres, bâtiments d’exploitation,
                équipements, logement) nécessaires à la production agricole. Cette idée avait été proposée par Edgar Pisani – ex-ministre de
                l’agriculture – à la fin des années 1970 pour répondre à des enjeux déjà semblables à ceux d’aujourd’hui. Le principe est simple :
                toute personne avec un projet d’installation agricole pourra <span class="italic">s’il le souhaite</span> solliciter l’office foncier
                local pour qu’il assure l’acquisition de certains biens. Ces derniers seront mis à disposition par l’office grâce à des baux de
                carrière, transmissibles à la descendance, et dont les modalités (montant des loyers, conditions à respecter en termes de pratiques
                agricoles) seront précisées dans une charte définie par les usagers et des représentants du territoire. Les loyers des usagers
                couvriront les dépenses de fonctionnement des offices (salaires des employés, travaux sur le bâti) tandis que les acquisitions seront
                essentiellement financées par l’investissement public.
            </TexteP>

            <TexteP>
                En diminuant fortement les besoins en capitaux lors de l’installation, le système des offices fonciers permet de lever un obstacle
                majeur au renouvellement agricole. Il va au-delà des mécanismes de portage foncier en affranchissant définitivement l’agriculteur
                usager de la nécessité de devenir propriétaire de son outil de travail. Ainsi, la part du bénéfice dédiée à la constitution d’un
                patrimoine professionnel diminue fortement, ce qui permet d’améliorer la rémunération directe et la protection sociale des
                agriculteurs. L’attractivité des métiers agricoles s’en trouve renforcée, favorisant davantage la dynamique d’installation.
            </TexteP>

            <Encadre type="ils-l-ont-fait" localisation="Pays de la Loire" url-en-savoir-plus="https://passeursdeterres.org/"
            >Fondée en 2018 à l’initiative de l’association Terre de Liens Pays de la Loire, la SCIC Passeurs de Terres préfigure ce que
                pourraient être les offices fonciers. Cette coopérative réunit habitants et agriculteurs pour acquérir et gérer collectivement du
                foncier sur le territoire.</Encadre
            >

            <ARetenir>
                <ListLiAvecIcone>
                    Jamais le nombre de travailleurs agricoles n’a été aussi faible en France, la tendance à la diminution du nombre de fermes et à
                    l’agrandissement de celles qui restent se poursuit.
                </ListLiAvecIcone>
                <ListLiAvecIcone
                >Parmi les nombreux obstacles à la transmission des fermes, le coût de reprise est probablement le plus difficile à dépasser. Nous
                    payons le prix de décennies d’agrandissement des outils de production.</ListLiAvecIcone
                >
                <ListLiAvecIcone>
                    L’endettement structurel de la profession agricole est une autre conséquence de la capitalisation des fermes. Il pèse sur le
                    revenu et la protection sociale des agriculteurs, réduit leur autonomie et s’oppose aux changements de pratiques.
                </ListLiAvecIcone>
                <ListLiAvecIcone
                >Face à la disparition du modèle historique d’agriculture familiale se développent de nouvelles formes d’agriculture aux allures
                    de firme. Elles recourent à la sous-traitance (travaux et/ou gestion) ainsi qu’à des formes sociétaires complexes et ouvertes à
                    des acteurs multiples.</ListLiAvecIcone
                >
                <ListLiAvecIcone>
                    La création d’offices fonciers locaux chargés d’acquérir des moyens de production (terres, bâtiments, équipements, logements) pour
                    les agriculteurs qui le souhaitent constitue une réponse aux difficultés de transmission. Ces outils peuvent être mis en place dès
                    aujourd’hui par des acteurs volontaires.
                </ListLiAvecIcone>
            </ARetenir>
        </template>
    </TemplateThematique>
</template>

<script setup lang="ts">
import { htmlstring } from '@lga/base';

import { useGetStateIdTerritoire, useRouteSimulateur } from '@/composables';
import ListLiAvecIcone from '~/components/base/ListLiAvecIcone.vue';
import TexteP from '~/components/base/TexteP.vue';
import { calculerInformationsMethodologieConstatEvolutionPopulationAgricole } from '~/modeles/constats/constats-agriculteurs.js';
import { creerLienExterneHtmlBrut } from '~/modeles/liens-utils.js';
import type { ItemLienHash } from '~/modeles/navigation/ItemMenuModele.js';
import type { ThematiqueAgriculteursModele } from '~/modeles/thematiques/thematique-agriculteurs.js';

import ChartEvolutionPopulationAgricole from '../commun/ChartEvolutionPopulationAgricole.vue';
import ARetenir from './commun/ARetenir.vue';
import Encadre from './commun/Encadre.vue';
import GloseAvecPopoverDefinition from './commun/GloseAvecPopoverDefinition.vue';
import TexteAvecPopoverDefinition from './commun/TexteAvecPopoverDefinition.vue';
import TexteAvecPopoverSource from './commun/TexteAvecPopoverSource.vue';
import TemplateThematique from './TemplateThematique.vue';

defineProps<{
    modele: ThematiqueAgriculteursModele;
}>();

const ID_SECTION_1 = 'une-population-d-agriculteurs-vieillissante-et-en-fort-declin';
const ID_SECTION_2 = 'ce-n-est-pas-pres-de-changer';
const ID_SECTION_3 = 'vers-une-agriculture-sans-agriculteurs';
const ID_SECTION_4 = 'creons-des-offices-fonciers-pour-accompagner-la-transmission-des-fermes';

const SECTIONS: ItemLienHash[] = [
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_1,
        libelle: 'Une population d’agriculteurs vieillissante et en fort déclin'
    },
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_2,
        libelle: 'Ce n’est pas près de changer'
    },
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_3,
        libelle: 'Vers une agriculture sans agriculteurs'
    },
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_4,
        libelle: 'Créons des offices fonciers pour accompagner la transmission des fermes'
    }
];
</script>
