import type { BoundingBoxApi, CodeCategorieTerritoireApi, IndicateurApi } from '@lga/specification-api';

import { useFetchIndicateurs } from '~/composables/apis/useFetchIndicateurs.js';

export const BOUNDING_BOX_FRANCE = {
    longitudeMin: -5.14,
    longitudeMax: 9.56,
    latitudeMin: 41.33,
    latitudeMax: 51.09
};

export type MapIndicateurs = Map<string, { valeur: string | number | null }>;

export interface DonneesCercle {
    idTerritoire: string;
    nomTerritoire: string;
    longitude: number;
    latitude: number;
    valeur: number | null;
    valeurCouleur?: number | null;
}

export const recupererIndicateursTerritoires = async (
    bbox: BoundingBoxApi,
    codeCategorieTerritoire: CodeCategorieTerritoireApi,
    urlIndicateur: string
): Promise<MapIndicateurs> => {
    const mapIndicateurs: MapIndicateurs = new Map<string, { valeur: string | number | null }>();
    const indicateursApi = await useFetchIndicateurs(bbox, codeCategorieTerritoire, urlIndicateur);
    indicateursApi.forEach((indicateur: IndicateurApi) => {
        mapIndicateurs.set(indicateur.idTerritoire, {
            valeur: indicateur.valeur as number | string | null
        });
    });
    return mapIndicateurs;
};

//TODO : homogénéiser avec recupererIndicateursTerritoires qui lui utilise une map pour stocker le résultat ?
export const recupererIndicateursCercles = async (
    bbox: BoundingBoxApi,
    codeCategorieTerritoire: CodeCategorieTerritoireApi,
    urlIndicateur: string,
    urlIndicateurCouleurs?: string
): Promise<DonneesCercle[]> => {
    const [indicateurs, indicateursCouleurs, boundingBoxes, nomsTerritoires] = await Promise.all([
        useFetchIndicateurs(bbox, codeCategorieTerritoire, urlIndicateur),
        urlIndicateurCouleurs ? await useFetchIndicateurs(bbox, codeCategorieTerritoire, urlIndicateurCouleurs) : undefined,
        (await useFetchIndicateurs(bbox, codeCategorieTerritoire, 'boundingBoxTerritoire')).map((i) => i.valeur as BoundingBoxApi),
        useFetchIndicateurs(bbox, codeCategorieTerritoire, 'nomTerritoire')
    ]);

    return indicateurs.map((i, index) => {
        return {
            idTerritoire: i.idTerritoire,
            nomTerritoire: nomsTerritoires[index].valeur as string,
            longitude: (boundingBoxes[index].longitudeMin + boundingBoxes[index].longitudeMax) / 2,
            latitude: (boundingBoxes[index].latitudeMin + boundingBoxes[index].latitudeMax) / 2,
            valeur: i.valeur as number | null,
            valeurCouleur: indicateursCouleurs ? (indicateursCouleurs[index].valeur as number | null) : undefined
        };
    });
};
