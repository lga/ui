import type { RouterConfig } from '@nuxt/schema';
import type { RouteLocationNormalized } from 'vue-router';

export default {
    scrollBehavior: async (to) => {
        // Positionnes le scroll selon le hash
        // Gérer un décalage lors d'une navigation vers un hash, pour prendre en compte l'entete
        // Ce mécanisme suppose de l'élement conteneur du coeur de page qu'il est scrollable, est défini avec l'id id-conteneur-coeur-de-page, à une marge égale à l'entete
        if (to.hash) {
            // Promise car sinon le scroll ne fonctionne pas lors d'une nav client side. Voir https://router.vuejs.org/guide/advanced/scroll-behavior#Delaying-the-scroll
            // Permet aussi de laisser au DOM le temps de se charger pour récupérer l'élement 'id-conteneur-coeur-de-page'
            return new Promise((resolve) => {
                setTimeout(() => {
                    resolve({
                        el: to.hash,
                        top: calculerDecalage(to),
                        behavior: 'smooth'
                    });
                }, 500);
            });
        }
        return {
            top: 0
        };
    }
} as RouterConfig;

function calculerDecalage(to: RouteLocationNormalized): number {
    const elementCoeurDePage = document.getElementById('id-conteneur-coeur-de-page');
    const elementHash = document.getElementById(to.hash.substring(1));
    let decalage = 0;
    if (elementCoeurDePage && elementHash) {
        const styleCssElementCoeurDePage = window.getComputedStyle(elementCoeurDePage);
        const styleCssElementHash = window.getComputedStyle(elementHash);
        decalage =
            parseInt(styleCssElementCoeurDePage.getPropertyValue('margin-top')) + parseInt(styleCssElementHash.getPropertyValue('margin-top')) - 1; // -1 pour corriger précision
    }
    return decalage;
}
