import { defineEventHandler, sendRedirect } from 'h3';

export default defineEventHandler((event) => {
    // URL de redirection en dur car depuis server/routes, on n'a pas accès à vue-router (ie impossible d'appeler useRouteXxx())
    return sendRedirect(event, '/documents-a-imprimer', 301);
});
