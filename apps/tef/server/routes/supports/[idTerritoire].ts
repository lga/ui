import { defineEventHandler, getRouterParam, sendRedirect } from 'h3';

export default defineEventHandler((event) => {
    // URL de redirection en dur car depuis server/routes, on n'a pas accès à vue-router (ie impossible d'appeler useRouteXxx())
    const idTerritoire = getRouterParam(event, 'idTerritoire');
    if (idTerritoire) {
        return sendRedirect(event, `/documents-a-imprimer/${idTerritoire}`, 301);
    } else {
        return sendRedirect(event, '/documents-a-imprimer', 301);
    }
});
