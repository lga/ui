import { defineAppConfig } from '#imports';

export default defineAppConfig({
    nuxtIcon: {}, // Pour corriger une erreur lord du build, voir https://github.com/nuxt/ui/issues/1045,
    ui: {
        primary: 'primaire',
        gray: 'neutral'
    },
    tef: {
        idNomFrance: 'france'
    }
});
