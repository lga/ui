import VueMatomo from 'vue-matomo';

import { defineNuxtPlugin } from '#app';
import { useRuntimeConfig } from '#imports';

export default defineNuxtPlugin((nuxtApp) => {
    if (import.meta.dev) {
        console.warn('PLUGIN MATOMO DESACTIVÉ : Exécution en mode développement. Le plugin vue-matomo ne sera pas chargé.');
        return;
    }
    const runtimeConfig = useRuntimeConfig();
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    nuxtApp.vueApp.use(VueMatomo, {
        router: nuxtApp.$router,
        host: 'https://matomo.resiliencealimentaire.org',
        siteId: runtimeConfig.public.matomo.siteId,
        enableLinkTracking: true,
        requireConsent: false,
        trackInitialView: true,
        disableCookies: true,
        requireCookieConsent: true,
        enableHeartBeatTimer: true,
        heartBeatTimerInterval: 15,
        preInitActions: [
            ['trackVisibleContentImpressions'] // Pour activer le content tracking sur les éléments qui sont visibles dans le viewport (voir https://matomo.org/guide/reports/content-tracking/)
        ]
    });
});
