#!/bin/bash

# Lancer ce script depuis le dossier apps/tef

URL_INFOS_COMPLEMENTAIRES=$1 #Permet de passer en param le lien vers le contexte de lancement du test (par ex la page du job gitlab)

cypress run --spec "cypress/e2e/supervision" --browser firefox --config 'baseUrl=https://territoiresfertiles.fr/'

if [ $? -ne 0 ]; then
    FICHIER_TMP_MESSAGE=$(mktemp)
    printf "ALERTE - TEST DE SUPERVISION KO :\n$URL_INFOS_COMPLEMENTAIRES" > "$FICHIER_TMP_MESSAGE"

    # Chemin relatif p/r à apps/tef d'ou est lancé ce script
    ../../deploiement/ovh/scripts/commun/healthchecks-mattermost/post-mattermost.sh "$FICHIER_TMP_MESSAGE"

    rm -f "$FICHIER_TMP_MESSAGE"
fi


