/* eslint-disable */
// Script qui permet d'extraire un style adapté à partir du sytle de base gris fournit par plan ign
// Le fichier original est disponible ici https://data.geopf.fr/annexes/ressources/vectorTiles/styles/PLAN.IGN/gris.json
// Une copie est conservée dans ce dossier : ./ign-plan-gris-original.json
// Ce script utilise cette copie et génère le fichier ign-plan-gris-fond-et-toponymes.json avec seulement les couches nécessaires pour TEF

import fs from 'fs';

const LISTE_LAYERS_A_EXTRAIRE = ['fond_opaque', 'hydro_surf', 'toponyme_localite_ponc'];

fs.readFile('./ign-plan-gris-original.json', 'utf8', (err, data) => {
    if (err) {
        console.error(err);
        return;
    }

    const jsonStyleJson = JSON.parse(data);

    jsonStyleJson.layers = jsonStyleJson.layers.filter((layer) => {
        return LISTE_LAYERS_A_EXTRAIRE.includes(layer['source-layer']);
    });

    fs.writeFile('ign-plan-gris-fond-et-toponymes.json', JSON.stringify(jsonStyleJson, null, 2), 'utf8', (err) => {
        if (err) {
            console.error(err);
            return;
        }

        console.log('File successfully written!');
    });
});
