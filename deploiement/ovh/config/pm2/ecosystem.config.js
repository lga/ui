module.exports = {
    apps: [
        {
            name: 'api-prod',
            cwd: '/home/ubuntu/lga/api/prod/server',
            exec_mode: 'cluster',
            instances: 'max',
            script: 'server.cjs',
            cron_restart: '0 2 * * *',
            env: {
                PORT: 3000
            }
        },
        {
            name: 'api-dev',
            cwd: '/home/ubuntu/lga/api/dev/server',
            exec_mode: 'cluster',
            instances: 1,
            script: 'server.cjs',
            cron_restart: '10 2 * * *',
            env: {
                PORT: 3111
            }
        },
        {
            name: 'tef-prod',
            exec_mode: 'cluster',
            instances: 'max',
            cwd: '/home/ubuntu/lga/tef/prod/server',
            script: 'index.mjs',
            cron_restart: '20 2 * * *',
            env: {
                NITRO_PORT: 4000,
                NUXT_CLE_API_BREVO: '__CLE_API_BREVO__', // Cle privée de l'API Brevo, est injectée par gitlab-ci
                NUXT_PUBLIC_API_BASE_URL: 'https://api.resiliencealimentaire.org',
                NUXT_PUBLIC_CRATER_BASE_URL: 'https://crater-tef-prod.resiliencealimentaire.org',
                NUXT_PUBLIC_SITE_ENV: 'production', // Utilisé par nuxt-site-config, nuxt-seo et estEnvironnementProd. nuxt-seo impose la valeur "production" (par ex pour généere le robots.txt de prod)
                NUXT_PUBLIC_SITE_URL: 'https://territoiresfertiles.fr', // Override de nuxt.config.ts/site.url
                NUXT_PUBLIC_SITE_NAME: 'Territoires Fertiles', // Override de nuxt.config.ts/site.name
                NUXT_PUBLIC_BREVO_ID_LISTE_SOUSCRIPTION: 61, // ID de la liste de prod, voir https://app.brevo.com/contact/list-listing
                NUXT_PUBLIC_BREVO_ID_TEMPLATE_MAIL: 203, // https://my.brevo.com/camp/template/203/message-setup
                NUXT_PUBLIC_BREVO_URL_PAGE_CONFIRMATION: 'https://territoiresfertiles.fr/confirmation-inscription-newsletter',
                NUXT_PUBLIC_MATOMO_SITE_ID: '11'
            }
        },
        {
            name: 'tef-dev',
            exec_mode: 'cluster',
            instances: 1,
            cwd: '/home/ubuntu/lga/tef/dev/server',
            script: 'index.mjs',
            cron_restart: '30 2 * * *',
            env: {
                NITRO_PORT: 4111,
                NUXT_CLE_API_BREVO: '__CLE_API_BREVO__', // Cle privée de l'API Brevo, est injectée par gitlab-ci
                NUXT_PUBLIC_SITE_ENV: 'developpement', // Utilisé par nuxt-site-config, nuxt-seo et estEnvironnementProd
                NUXT_PUBLIC_SITE_URL: 'https://dev.territoiresfertiles.fr', // Override de nuxt.config.ts/site.url
                NUXT_PUBLIC_SITE_NAME: 'Territoires Fertiles - DEV', // Override de nuxt.config.ts/site.name
                NUXT_PUBLIC_API_BASE_URL: 'https://api.resiliencealimentaire.org/dev',
                NUXT_PUBLIC_CRATER_BASE_URL: 'https://crater-tef-dev.resiliencealimentaire.org',
                NUXT_PUBLIC_MATOMO_SITE_ID: '10'
            }
        },
        {
            name: 'api-pdf',
            instances: 1,
            cwd: '/home/ubuntu/lga/pdf/puppeteer-html-to-pdf-converter',
            script: 'src/index.js',
            cron_restart: '40 2 * * * *'
        }
    ]
};
