#!/bin/bash
# Installer chrome pour puppeteer (si pas déjà présent)


if ! command -v google-chrome &> /dev/null
then
    echo "Google Chrome n'est pas disponible sur ce serveur. Lancement de l'installation..."

  # Installer la dernière version de Google Chrome ains
  sudo apt-get update
  sudo apt-get install -y wget gnupg
  sudo wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
  sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
  sudo apt-get update
  sudo apt-get install -y google-chrome-stable fonts-freefont-ttf libxss1 --no-install-recommends
  sudo  rm -rf /var/lib/apt/lists/*

else
    echo "Google Chrome est déjà installé, aucune action requise."
fi