#!/bin/bash

# Prérequis pour puppeteer : chrome doit être installé
./installer-chrome.sh

NOM_BRANCHE=master

echo "Installation de l'api pdf depuis github, à partir de la branche $NOM_BRANCHE"

rm -rf /home/ubuntu/lga/pdf
mkdir -p /home/ubuntu/lga/pdf

cd /home/ubuntu/lga/pdf
git clone https://github.com/Les-Greniers-d-Abondance/puppeteer-html-to-pdf-converter.git --depth 1 --branch $NOM_BRANCHE --single-branch

cd puppeteer-html-to-pdf-converter

# Création du fichier de config
# Au dela de n requetes (RATE_LIMIT_DELAY_AFTER) pour x minutes (RATE_LIMIT_WINDOW), on retarde les requetes de T millisecondes (RATE_LIMIT_DELAY_MS)
# Si T >= au timeout du proxy apache, ces requetes seront interrompues (voir config apache)
cat <<EOF > config.json
{
	"PORT": 5000,
	"TRUST_PROXY": true,
	"RATE_LIMIT_WINDOW_MS": 10000,
	"RATE_LIMIT_GLOBAL_REJECT_AFTER": 3,
	"URL_ALLOWLIST": "https://dev.territoiresfertiles.fr,https://territoiresfertiles.fr"
}
EOF

# Pour ne pas exposer la page html de démo
rm -rf demo

npm install