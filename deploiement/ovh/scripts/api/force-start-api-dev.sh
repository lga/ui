# script a utiliser si les process pm2 ne se relancent pas correctement
# (sur certaines modifications du fichiers ecosystem.config.js, un pm2 stop puis pm2 start ne suffit pas)
pm2 delete api-dev
pm2 start ../../config/pm2/ecosystem.config.js --only api-dev