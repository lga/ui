#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage: $0 \"message_à_poster\" OR $0 /chemin/vers/fichier_message_à_poster"
    exit 1
fi


# Lire le message depuis ligne de commande ou fichier
if [[ -f "$1" ]]; then
    MESSAGE=$(cat "$1")
else
    MESSAGE="$1"
fi

URL_HOOK_MATTERMOST_CANAL_SUPERVISION=https://framateam.org/hooks/5iw81ubnjt8b7gokbfyh83u6za

# Ajouter les \n et les ""
MESSAGE_FORMATE=$(printf "$MESSAGE" | jq -Rs .)

curl -i -X POST -d "payload={\"text\": $MESSAGE_FORMATE, \"username\":\"BOT VPS\"}" $URL_HOOK_MATTERMOST_CANAL_SUPERVISION
