#!/bin/bash

# SEUILS D'ALERTE
SEUIL_UTILISATION_MEMOIRE_POURCENT=80
SEUIL_UTILISATION_CPU_POURCENT=80


# Utilisation mémoire
MEMOIRE_TOTALE_MO=$(free -m | awk '/^Mem:/ {print $2}')
MEMOIRE_DISPONIBLE_MO=$(free -m | awk '/^Mem:/ {print $7}')
MEMOIRE_UTILISEE_MO=$((MEMOIRE_TOTALE_MO - MEMOIRE_DISPONIBLE_MO))
MEMOIRE_UTILISEE_POURCENT=$(( MEMOIRE_UTILISEE_MO * 100 / MEMOIRE_TOTALE_MO))

# Utilisation CPU
CPU_UTILISATION_TOTALE=$(LC_NUMERIC="en_US.UTF-8" awk '{print $1 * 100}' /proc/loadavg)
CPU_NOMBRE_COEURS=$(nproc)
CPU_UTILISATION_MOYENNE=$((CPU_UTILISATION_TOTALE / CPU_NOMBRE_COEURS))

if [ "$MEMOIRE_UTILISEE_POURCENT" -gt "$SEUIL_UTILISATION_MEMOIRE_POURCENT" ] || [ "$CPU_UTILISATION_MOYENNE" -gt "$SEUIL_UTILISATION_CPU_POURCENT" ]; then
    FICHIER_TMP_MESSAGE=$(mktemp)
    printf "ALERTE - SEUIL D'UTILISATION MEMOIRE ou CPU DEPASSE :\n  - Utilisation mémoire=$MEMOIRE_UTILISEE_POURCENT%% (seuil max=$SEUIL_UTILISATION_MEMOIRE_POURCENT%%)\n  - Utilisation CPU=$CPU_UTILISATION_MOYENNE%% (seuil max=$SEUIL_UTILISATION_CPU_POURCENT%%)\n" > "$FICHIER_TMP_MESSAGE"

    ./post-mattermost.sh "$FICHIER_TMP_MESSAGE"

    rm -f "$FICHIER_TMP_MESSAGE"
fi

