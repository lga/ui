#!/bin/bash

#
# Configurer crontab pour le healthcheck healthcheck-erreurs-journalctl
#
SCRIPT_HEALTHCHECK_ERREURS_JOURNALCTL="/home/ubuntu/lga/ovh/scripts/commun/healthchecks-mattermost/healthcheck-erreurs-journalctl.sh"
CRON_JOB="*/59 * * * * $SCRIPT_HEALTHCHECK_ERREURS_JOURNALCTL"  # Toutes les 59 minutes
# Ajout dans la crontab (en évitant les doublons si déjà présent)
(crontab -l 2>/dev/null | grep -v -F "$SCRIPT_HEALTHCHECK_ERREURS_JOURNALCTL"; echo "$CRON_JOB") | crontab -

#
# Configurer crontab pour le healthcheck healthcheck-process-pm2
#
SCRIPT_HEALTHCHECK_PROCESS_PM2="/home/ubuntu/lga/ovh/scripts/commun/healthchecks-mattermost/healthcheck-process-pm2.sh"
CRON_JOB="*/10 * * * * $SCRIPT_HEALTHCHECK_PROCESS_PM2"  # Toutes les 10 minutes
# Ajout dans la crontab (en évitant les doublons si déjà présent)
(crontab -l 2>/dev/null | grep -v -F "$SCRIPT_HEALTHCHECK_PROCESS_PM2"; echo "$CRON_JOB") | crontab -


#
# Configurer crontab pour le healthcheck utilisation mémoire et cpu et
#
SCRIPT_HEALTHCHECK_UTILISATION_MEMOIRE_ET_CPU="/home/ubuntu/lga/ovh/scripts/commun/healthchecks-mattermost/healthcheck-utilisation-memoire-cpu.sh"
CRON_JOB="*/10 * * * * $SCRIPT_HEALTHCHECK_UTILISATION_MEMOIRE_ET_CPU"  # Toutes les 10 minutes
# Ajout dans la crontab (en évitant les doublons si déjà présent)
(crontab -l 2>/dev/null | grep -v -F "$SCRIPT_HEALTHCHECK_UTILISATION_MEMOIRE_ET_CPU"; echo "$CRON_JOB") | crontab -

