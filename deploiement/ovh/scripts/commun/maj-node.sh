#!/bin/bash

## Script pour mettre à jour node (et pm2) sur le serveur

# pour récuperer l'env node (défini dans bashrc, mais pas lu lors de l'execution du script a distance)
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

# installer la version de node
nvm install 20

# utiliser node 20 par defaut dans tous les shells
nvm alias default 20

#installer pm2 (gestion et monitoring des process en prod)
npm install pm2 -g

# configurer pm2 pour qu'il redémarre a chaque fois (commande obtenue en faisant "pm2 startup")
NODE_SCRIPT_PATH=$(which node)
NODE_BIN_DIR=$(dirname "${NODE_SCRIPT_PATH}")
NODE_VERSION_DIR=$(dirname "${NODE_BIN_DIR}")
sudo env PATH=$PATH:$NODE_BIN_DIR $NODE_VERSION_DIR/lib/node_modules/pm2/bin/pm2 startup systemd -u ubuntu --hp /home/ubuntu

