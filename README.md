Ce repo contient les applications web et composants maintenus par l'équipe CRATer :
* l'[application web de CRATer](https://crater.resiliencealimentaire.org)
* l'application [Territoires Fertiles (TeF)](https://territoiresfertiles.fr)
* l'[api](https://api.resiliencealimentaire.org/crater/api/docs)
* des modules communs

Toutes les contributions sont les bienvenues sur le projet !

Cette page décrit les étapes à suivre pour contribuer :

[[_TOC_]]

# 0) Pré requis : compte framagit

Avant de contribuer il faut disposer d'un compte framagit, si besoin [le créer ici](https://framagit.org/users/sign_up) (attention un délai de 1 à 5 jours peut être nécessaire)

# 1) Installer l'environnement

Mode opératoire valable pour linux, mac et windows.

Pré-requis pour Windows :
* il est recommandé d'installer [Windows Subsytem for Linux (WSL)](https://learn.microsoft.com/fr-fr/windows/wsl/about), ce qui permet de disposer simplement d'un environnement linux sous windows, et d'utiliser directement les commandes et configuration décrites ci-dessous
* si pas possible d'installer WSL, 
  * il faut disposer directement dans windows de git et nodejs
  * et il faut modifier les fichiers package.json pour remplacer partout `${npm_config_env}` par `%npm_config_env%`

Pre-requis pour toutes les plateformes :
* disposer de git
* installer [nodejs](https://nodejs.org/en/download/). Il faut installer la version LTS 20.x (actuellement c'est la [20.11.0](https://nodejs.org/download/release/v20.11.0/)). Les pipelines et les env de prod utilisent la même version de base (voir .gitlab-ci.yml). Pour installer et gérer facilement les versions de node, on peut utiliser  [nvm](https://github.com/nvm-sh/nvm)
  * Installer nvm
  * Puis : `nvm install 20.11.0`, `nvm use 20.11.0`, `nvm alias default 20.11.0`

Puis :

1. Cloner le dépôt.
2. Installer les dépendances : `npm install`  
3. Lancer le build initial du projet : `npm run build`
4. Lancer l'application web crater pour vérifier qu'elle fonctionne : `npm run start:crater` pour ouvrir la page http://localhost:8066
5. Lancer l'application web Territoires Fertiles pour vérifier qu'elle fonctionne : `npm run start:tef` pour ouvrir la page http://localhost:8077


# 2) Se positionner sur une issue 

* La [liste des tâches ouvertes à contribution](https://framagit.org/lga/ui/-/boards/18553?label_name[]=Contribution) est disponible sur framagit
* Les issues sur lesquelles il est possible de se positionner sont celle de la colonne "Pret pour dev"
* Déplacer l'issue dans la colonne "En cours" et se l'affecter en indiquant son user dans le champ assignee
* Créer une merge request et une branche depuis l'issue via le bouton "Create merge request" présent sur l'issue, puis récupérer la branche en local (`git fetch` puis `git checkout <nom de la branche>`)

# 3) Développer en local

## 3.1) Pour les app web

Valable pour les évolutions concernant :  apps/*

Voir une description de l'architecture du projet et des apps webs [dans les guides correspondants](https://framagit.org/lga/crater/-/tree/main/guides).

Il faut respecter les standards de développement du projet, voir pour cela les [règles de codage](https://framagit.org/lga/crater/-/blob/main/guides/regles-de-codage.md)


Pour la phase de développement :

* `npm run start:crater` ou `npm run start -w apps/crater/` : lancer l'app crater en mode développement (démarrage rapide, sans build ni tests, ni formatage, avec prise en compte à chaud des modifications)
* `npm run start:tef` ou `npm run start -w apps/tef/` : lancer l'app tef en mode développement
* si on souhaite modifier en même temps le code des modules et le code des apps, il est possible de lancer un build en watch mode sur les modules. Par exemple avec `npm run build:watch -w modules/design-system`
* `npm run cypress:app -w apps/crater/` : Lancer cypress sur le périmètre de l'application crater. Permet d'accéder à l'outil cypress de tests des composants (choisir "component test", puis un navigateur "Firefox" ou "Chrome"). Remplacer `apps/crater` par une autre app ou un module pour tester d'autres périmètres (par ex `npm run cypress:app -w modules/design-system/` pour le module design-system)

Pour lancer le build de l'application :

* `npm run build` : build de toutes les apps et modules
* `npm run build:crater:dev` : build de crater et des modules nécessaires (par ex composants/commun), en utilisant l'api [crater de dev](https://dev.resiliencealimentaire.org/crater/api)
* `npm run build:tef:dev` : build de tef et des modules nécessaires (par ex composants/commun), en utilisant l'api [crater de dev](https://dev.resiliencealimentaire.org/crater/api)
* `npm run build:crater:localhost` et `npm run build:territoires-fertiles:localhost` : comme précédemment, mais en utilisant une api [crater sur localhost](http://localhost:3000/crater/api) (instance de crater-api lancée en local)

Pour servir et tester une app après le build :
* `npm run serve -w apps/crater/` : servir l'app crater et la tester en local (test de la version réellement buildée)
* `npm run serve -w apps/tef/` : servir l'app TerritoiresFertiles et la tester en local (test de la version réellement buildée)

Intégration avec Brevo et test en local :
* certaines pages de TeF (par ex la page d'inscription à la newsletter) nécessitent de faire appel à l'api Brevo
* Pour tester la bonne intégration avec Brevo de ces pages en local, il faut créer un fichier ui/apps/tef/.env définissant la clé de l'api comme suit :
```
NUXT_CLE_API_BREVO='__VALEUR_DE_LA_CLE__'
```
* La valeur de la clé peut être récupérée dans la configuration framagit, dans cette page https://framagit.org/lga/ui/-/settings/ci_cd

## 3.2) Pour l'api

Pour travailler avec l'api en local, il est nécessaire de disposer d'un clone du dépot `crater-data-resultat` : si pas déjà fait, clôner le dépôt `crater-data-resultats` (`https://framagit.org/lga/crater-data-resultats.git`) dans `../crater-data-resultats` par rapport au dossier `ui`.

Puis :

* Pour lancer le lint du code de l'api : `npm run lint -w api`
* Pour builder l'api (+ TU) : `npm run build -w api`
* Pour lancer les tests d'integration : `npm run integration-tests -w api`
* Pour faire l'ensemble lint-build-tests intégration : `npm run validate -w api`
* Pour lancer l'api en local : `npm run serve -w api`
   L'api est accessible sur [localhost:3000](http://localhost:3000), par exemple à cette
   adresse http://localhost:3000/crater/api/territoires/toulouse


# 4) S'assurer que le code est valide

Voir le paragraphe correspondant dans [ce guide](https://framagit.org/lga/crater/-/blob/main/guides/processus-de-developpement.md)

En complément, lancer avant de pousser une branche la commande : `npm run validate` pour valider toutes les apps et modules.
Pour valider une app ou un module ciblé (ici crater) : `npm run validate -w apps/crater/`

* vérifie le code et fait certaines corrections automatique : formatage avec prettier, lint avec eslint
* build
* passe l'ensemble des tests (TU + tests cypress)

En cas d'échec, pour lancer spécifiquement les tests (exemple sur l'app crater) :
* `npm run build:test -w apps/crater/` pour lancer les tests unitaires
* `npm run cypress:app -w apps/crater/` pour lancer les tests cypress en mode interactif

## 5) Faire relire son code et le fusionner 

Cette étape consiste à demander une revue de code via une merge request framagit et la merger quand elle est approuvée. Pour cela :

* pousser le code de sa branche sur le repo git distant
* via framagit, compléter la merge request attachée a cette branche pour demander une revue de code
  * Marquer la merge request comme prête à être relue ("Mark as ready")
  * Positionner un relecteur dans le champ Reviewer (et éventuellement le/la contacter en direct pour lui demander de relire)
* prendre en compte les différents commentaires, qui sont faits avec la convention suivante :
  * préfixe [FIX] : il s'agit d'un point à corriger
  * préfixe [OPT] : il s'agit d'un point optionnel, le/la développeur choisit la solution qui lui semble la plus approprié
  * autres préfixes [QUESTION], [REMARQUES], [PROPOSITION], ou pas de préfixe : point de moindre importance, optionnel lui aussi
* quand les commentaires sont traités :
  * marquer les threads correspondant comme "Resolved" dans framagit
  * pousser la modification sur la branche, dans un nouveau commit
  * si besoin prévenir le relecteur directement pour lui demander un complément de revue
  * si tout est OK le relecteur approuve la merge request (Approved framagit)
* dernière étape, une fois la merge request approuvée, il faut la merger 
  * se fait via framagit
  * s'il y a eu du délai dans le process de review, peut nécessiter un nouveau rebase
  * le merge a pour effet de cloture la merge request, et l'issue associée
* le résultat du merge est livré automatiquement sur l'environnement de dev https://dev.resiliencealimentaire.org
   

# Informations complémentaires sur le CI/CD : Livrer sur les environnements dev et crater

Les apps sont déployées automatiquement sur les environnements de développement et de production via des pipelines gitlab définis dans le fichier [.gitlab-ci.yml](.gitlab-ci.yml)

L'avancement et le résultat des pipelines sont visible ici https://framagit.org/lga/ui/pipelines

Ces pipelines s'exécutent de manière automatique : Lors de chaque commit sur la branche develop, le code correspondant est poussé sur l'hébergeur o2switch (pour les app web), et sur le serveur OVH (pour l'api), et après quelques secondes / minutes, les mises à jour sont disponibles à cette adresse https://dev.resiliencealimentaire.org (crater) et celle-ci https://ta-dev.resiliencealimentaire.org, et pour l'api à cette adresse https://api.resiliencealimentaire.org/dev/crater/api/territoires/toulouse ou https://dev.resiliencealimentaire.org/dev/crater/api/diagnostics/toulouse

Même fonctionnement sur la branche master, ou les mises à jour sont livrées sur les environnement de production : https://crater.resiliencealimentaire.org , https://territoires-fertiles.org et  https://api.resiliencealimentaire.org/crater/api/territoires/toulouse


Rq :
-  La branche master est protégée, il n'est pas possible pour les contributeurs de commiter directement dessus, il faut passer par une merge request gitlab
-  Lors de son exécution, crater-api utilise les données de crater-data-resultats. Ces données sont récupérées lors de l'exécution des pipelines gitlab, le git clone est fait automatiquement.