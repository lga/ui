import { ajouterIndicateursAnneeMoisN, ajouterIndicateursAnneeN } from '@lga/specification-api/build/outils/modelesDiagnosticsUtils';
import type {
    ArretesSecheresseAnneeMoisNApi,
    BesoinsParCultureApi,
    BesoinsParGroupeCultureApi,
    CodeCategorieTerritoireApi,
    CodeGroupeCultureApi,
    CodeNiveauRisquePrecariteAlimentaireApi,
    CodeOtexMajoritaire5PostesApi,
    CodePosteEnergieApi,
    CodeProfilAgricultureApi,
    CodeProfilCollectiviteApi,
    CodeProfilDiagnosticApi,
    CodeSourceEnergieApi,
    CommuneAnalogueClimatique2000_2050Api,
    CommuneReferenceApi,
    DiagnosticApi,
    IrrigationIndicateursAnneeNApi,
    PesticidesIndicateursAnneeNApi,
    SauParCultureApi
} from '@lga/specification-api/build/specification/api';
import {
    valeursCodeCategorieTerritoire,
    valeursCodeGroupeCulture,
    valeursCodeNiveauRisquePrecariteAlimentaire,
    valeursCodeOtexMajoritaire5Postes,
    valeursCodePosteEnergie,
    valeursCodeProfilAgriculture,
    valeursCodeProfilCollectivite,
    valeursCodeProfilDiagnostic,
    valeursCodeSourceEnergie
} from '@lga/specification-api/build/specification/valeurs-codes';

import {
    InjecteurCsv,
    lireCelluleBoolean,
    lireCelluleBooleanOuNull,
    lireCelluleEnum,
    lireCelluleFloat,
    lireCelluleFloatOuNull,
    lireCelluleInt,
    lireCelluleIntOuNull,
    lireCelluleString,
    lireCelluleStringOuNull,
    type TraiterCellules
} from '../communs/injecteursUtils';
import { MapIdsTerritoires } from '../communs/MapIdsTerritoires';
import type { ServicesDiagnostics } from './ServicesDiagnostics';

export class InjecteursDiagnostics {
    private readonly idsTerritoires = new MapIdsTerritoires();

    constructor(private readonly servicesDiagnostics: ServicesDiagnostics) {}

    private traiterCellulesReferentielTerritoires: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.idDepartement = this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_departement') ?? undefined;
        diagnostic.idTerritoireParcel = lireCelluleStringOuNull(mapCellules, 'id_territoire_parcel');
        diagnostic.nbCommunes = lireCelluleInt(mapCellules, 'nb_communes');
    };

    private traiterCellulesPopulation: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.population = lireCelluleIntOuNull(mapCellules, 'population_totale_2017');
        diagnostic.densitePopulationHabParKm2 = lireCelluleFloatOuNull(mapCellules, 'densite_population_hab_par_km2');
    };

    private traiterCellulesOccupationSols: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.occupationSols.superficieTotaleHa = lireCelluleInt(mapCellules, 'superficie_ha');
        diagnostic.occupationSols.superficieArtificialiseeClcHa = lireCelluleIntOuNull(mapCellules, 'superficie_artificialisee_clc_ha');
        diagnostic.occupationSols.superficieAgricoleClcHa = lireCelluleIntOuNull(mapCellules, 'superficie_agricole_clc_ha');
        diagnostic.occupationSols.superficieNaturelleOuForestiereClcHa = lireCelluleIntOuNull(
            mapCellules,
            'superficie_naturelle_ou_forestiere_clc_ha'
        );
        diagnostic.occupationSols.superficieAgricoleRA2020Ha = lireCelluleIntOuNull(mapCellules, 'sau_ra_2020_ha');

        diagnostic.surfaceAgricoleUtile.sauTotaleHa = lireCelluleInt(mapCellules, 'sau_ha');
        diagnostic.surfaceAgricoleUtile.sauProductiveHa = lireCelluleInt(mapCellules, 'sau_productive_ha');
        diagnostic.surfaceAgricoleUtile.sauPeuProductiveHa = lireCelluleInt(mapCellules, 'sau_peu_productive_ha');
    };

    private traiterCellulesOtexTerritoires: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.nbCommunesParOtex.grandesCultures = lireCelluleInt(mapCellules, 'nb_communes_otex_grandes_cultures');
        diagnostic.nbCommunesParOtex.maraichageHorticulture = lireCelluleInt(mapCellules, 'nb_communes_otex_maraichage_horticulture');
        diagnostic.nbCommunesParOtex.viticulture = lireCelluleInt(mapCellules, 'nb_communes_otex_viticulture');
        diagnostic.nbCommunesParOtex.fruits = lireCelluleInt(mapCellules, 'nb_communes_otex_fruits');
        diagnostic.nbCommunesParOtex.bovinLait = lireCelluleInt(mapCellules, 'nb_communes_otex_bovin_lait');
        diagnostic.nbCommunesParOtex.bovinViande = lireCelluleInt(mapCellules, 'nb_communes_otex_bovin_viande');
        diagnostic.nbCommunesParOtex.bovinMixte = lireCelluleInt(mapCellules, 'nb_communes_otex_bovin_mixte');
        diagnostic.nbCommunesParOtex.ovinsCaprinsAutresHerbivores = lireCelluleInt(mapCellules, 'nb_communes_otex_ovins_caprins_autres_herbivores');
        diagnostic.nbCommunesParOtex.porcinsVolailles = lireCelluleInt(mapCellules, 'nb_communes_otex_porcins_volailles');
        diagnostic.nbCommunesParOtex.polyculturePolyelevage = lireCelluleInt(mapCellules, 'nb_communes_otex_polyculture_polyelevage');
        diagnostic.nbCommunesParOtex.nonClassees = lireCelluleInt(mapCellules, 'nb_communes_otex_non_classees');
        diagnostic.nbCommunesParOtex.sansExploitations = lireCelluleInt(mapCellules, 'nb_communes_otex_sans_exploitations');
        diagnostic.nbCommunesParOtex.nonRenseigne = lireCelluleInt(mapCellules, 'nb_communes_otex_na');
        diagnostic.codeOtexMajoritaire5Postes = lireCelluleEnum<CodeOtexMajoritaire5PostesApi>(
            mapCellules,
            'otex_majoritaire_5_postes',
            valeursCodeOtexMajoritaire5Postes
        );
    };

    private traiterCellulesCheptels: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.cheptels = [
            {
                code: 'BOVINS',
                tetes: lireCelluleInt(mapCellules, 'nb_bovins'),
                estEstime: lireCelluleBoolean(mapCellules, 'nb_bovins_est_estime'),
                ugb: lireCelluleInt(mapCellules, 'ugb_bovins')
            },
            {
                code: 'PORCINS',
                tetes: lireCelluleInt(mapCellules, 'nb_porcins'),
                estEstime: lireCelluleBoolean(mapCellules, 'nb_porcins_est_estime'),
                ugb: lireCelluleInt(mapCellules, 'ugb_porcins')
            },
            {
                code: 'OVINS',
                tetes: lireCelluleInt(mapCellules, 'nb_ovins'),
                estEstime: lireCelluleBoolean(mapCellules, 'nb_ovins_est_estime'),
                ugb: lireCelluleInt(mapCellules, 'ugb_ovins')
            },
            {
                code: 'CAPRINS',
                tetes: lireCelluleInt(mapCellules, 'nb_caprins'),
                estEstime: lireCelluleBoolean(mapCellules, 'nb_caprins_est_estime'),
                ugb: lireCelluleInt(mapCellules, 'ugb_caprins')
            },
            {
                code: 'VOLAILLES',
                tetes: lireCelluleInt(mapCellules, 'nb_volailles'),
                estEstime: lireCelluleBoolean(mapCellules, 'nb_volailles_est_estime'),
                ugb: lireCelluleInt(mapCellules, 'ugb_volailles')
            }
        ];
        diagnostic.ugbParHa = lireCelluleFloatOuNull(mapCellules, 'ugb_total_par_sau_ha');
    };

    private traiterCellulesGeographiesTerritoires: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.boundingBoxTerritoire.latitudeMin = lireCelluleFloat(mapCellules, 'latitude_min');
        diagnostic.boundingBoxTerritoire.latitudeMax = lireCelluleFloat(mapCellules, 'latitude_max');
        diagnostic.boundingBoxTerritoire.longitudeMin = lireCelluleFloat(mapCellules, 'longitude_min');
        diagnostic.boundingBoxTerritoire.longitudeMax = lireCelluleFloat(mapCellules, 'longitude_max');
    };

    private traiterCellulesPolitiqueFonciere: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.politiqueFonciere.note = lireCelluleIntOuNull(mapCellules, 'note');
        diagnostic.politiqueFonciere.artificialisation5ansHa = lireCelluleFloatOuNull(mapCellules, 'artificialisation_2013_2018_ha');
        diagnostic.politiqueFonciere.evolutionMenagesEmplois5ans = lireCelluleFloatOuNull(mapCellules, 'evolution_menages_emplois_2013_2018');
        diagnostic.politiqueFonciere.sauParHabitantM2 = lireCelluleFloatOuNull(mapCellules, 'sau_par_habitant_m2_par_hab');
        diagnostic.politiqueFonciere.rythmeArtificialisationSauPourcent = lireCelluleFloatOuNull(
            mapCellules,
            'rythme_artificialisation_sau_pourcent'
        );
        diagnostic.politiqueFonciere.evaluationPolitiqueAmenagement = lireCelluleString(mapCellules, 'evaluation_politique_amenagement');
        diagnostic.politiqueFonciere.partLogementsVacants2013Pourcent = lireCelluleFloatOuNull(mapCellules, 'part_logements_vacants_2013_pourcent');
        diagnostic.politiqueFonciere.partLogementsVacants2018Pourcent = lireCelluleFloatOuNull(mapCellules, 'part_logements_vacants_2018_pourcent');
    };

    private traiterCellulesPopulationAgricole: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.populationAgricole.note = lireCelluleIntOuNull(mapCellules, 'note');
        diagnostic.populationAgricole.populationAgricole1988 = lireCelluleIntOuNull(mapCellules, 'actifs_agricoles_permanents_1988');
        diagnostic.populationAgricole.populationAgricole2010 = lireCelluleIntOuNull(mapCellules, 'actifs_agricoles_permanents_2010');
        diagnostic.populationAgricole.partPopulationAgricole1988Pourcent = lireCelluleFloatOuNull(
            mapCellules,
            'part_population_agricole_1988_pourcent'
        );
        diagnostic.populationAgricole.partPopulationAgricole2010Pourcent = lireCelluleFloatOuNull(
            mapCellules,
            'part_population_agricole_2010_pourcent'
        );
        diagnostic.populationAgricole.nbExploitations1988 = lireCelluleIntOuNull(mapCellules, 'nb_exploitations_1988');
        diagnostic.populationAgricole.nbExploitations2010 = lireCelluleIntOuNull(mapCellules, 'nb_exploitations_2010');
        diagnostic.populationAgricole.sauHa1988 = lireCelluleIntOuNull(mapCellules, 'sau_ha_1988');
        diagnostic.populationAgricole.sauHa2010 = lireCelluleIntOuNull(mapCellules, 'sau_ha_2010');
        diagnostic.populationAgricole.sauMoyenneParExploitationHa1988 = lireCelluleIntOuNull(mapCellules, 'sau_moyenne_par_exploitation_ha_1988');
        diagnostic.populationAgricole.sauMoyenneParExploitationHa2010 = lireCelluleIntOuNull(mapCellules, 'sau_moyenne_par_exploitation_ha_2010');

        diagnostic.populationAgricole.nbExploitationsParClassesAgesChefExploitation = {
            moins_40_ans: lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_ages_chef_exploitation.moins_40_ans'),
            de_40_a_49_ans: lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_ages_chef_exploitation.40_a_49_ans'),
            de_50_a_59_ans: lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_ages_chef_exploitation.50_a_59_ans'),
            plus_60_ans: lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_ages_chef_exploitation.plus_60_ans')
        };
        diagnostic.populationAgricole.nbExploitationsParClassesSuperficies = {
            moins_20_ha: lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_superficies.moins_20_ha'),
            de_20_a_50_ha: lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_superficies.20_a_50_ha'),
            de_50_a_100_ha: lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_superficies.50_a_100_ha'),
            de_100_a_200_ha: lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_superficies.100_a_200_ha'),
            plus_200_ha: lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_superficies.plus_200_ha')
        };
        diagnostic.populationAgricole.sauHaParClassesSuperficies = {
            moins_20_ha: lireCelluleIntOuNull(mapCellules, 'sau_ha_par_classes_superficies.moins_20_ha'),
            de_20_a_50_ha: lireCelluleIntOuNull(mapCellules, 'sau_ha_par_classes_superficies.20_a_50_ha'),
            de_50_a_100_ha: lireCelluleIntOuNull(mapCellules, 'sau_ha_par_classes_superficies.50_a_100_ha'),
            de_100_a_200_ha: lireCelluleIntOuNull(mapCellules, 'sau_ha_par_classes_superficies.100_a_200_ha'),
            plus_200_ha: lireCelluleIntOuNull(mapCellules, 'sau_ha_par_classes_superficies.plus_200_ha')
        };
    };

    private traiterCellulesSyntheseIntrants: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.intrants.note = lireCelluleFloatOuNull(mapCellules, 'intrants_note');
    };

    private traiterCellulesSyntheseEau: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.eau.note = lireCelluleFloatOuNull(mapCellules, 'eau_note');
        diagnostic.eau.irrigation.note = lireCelluleFloatOuNull(mapCellules, 'irrigation_note');
        diagnostic.eau.irrigation.irrigationM3 = lireCelluleFloatOuNull(mapCellules, 'irrigation_m3');
        diagnostic.eau.irrigation.irrigationMM = lireCelluleFloatOuNull(mapCellules, 'irrigation_mm');
        diagnostic.eau.arretesSecheresse.note = lireCelluleFloatOuNull(mapCellules, 'arretes_secheresse_note');
        diagnostic.eau.arretesSecheresse.tauxImpactArretesSecheressePourcent = lireCelluleFloat(
            mapCellules,
            'taux_impact_arretes_secheresse_pourcent'
        );
    };

    private traiterCellulesPratiquesIrrigation: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.eau.irrigation.pratiquesIrrigationParCultures.push({
            categorieCulture: lireCelluleString(mapCellules, 'categorie_culture'),
            culture: lireCelluleString(mapCellules, 'culture'),
            sauHa: lireCelluleFloat(mapCellules, 'superficie_ha'),
            sauIrrigueeHa: lireCelluleFloat(mapCellules, 'superficie_irriguee_ha')
        });
    };

    private traiterCellulesIrrigationParAnnees: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        ajouterIndicateursAnneeN<IrrigationIndicateursAnneeNApi>(diagnostic.eau.irrigation.indicateursParAnnees, {
            annee: lireCelluleInt(mapCellules, 'annee'),
            irrigationM3: lireCelluleFloatOuNull(mapCellules, 'irrigation_m3'),
            irrigationMM: lireCelluleFloatOuNull(mapCellules, 'irrigation_mm')
        });
    };

    private traiterCellulesArretesSecheresseParAnneesMois: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        ajouterIndicateursAnneeMoisN<ArretesSecheresseAnneeMoisNApi>(diagnostic.eau.arretesSecheresse.indicateursParAnneesMois, {
            anneeMois: lireCelluleString(mapCellules, 'annee_mois'),
            partTerritoireEnArretePourcent: lireCelluleFloat(mapCellules, 'part_territoire_en_arrete_pourcent')
        });
    };

    private traiterCellulesSyntheseEnergie: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.energie.note = lireCelluleFloatOuNull(mapCellules, 'energie_note');
        diagnostic.energie.codePostePrincipal = lireCelluleEnum<CodePosteEnergieApi>(mapCellules, 'poste_principal_energie', valeursCodePosteEnergie);
        diagnostic.energie.energiePrimaireGJ = lireCelluleFloatOuNull(mapCellules, 'energie_EP_GJ');
        diagnostic.energie.energiePrimaireGJParHa = lireCelluleFloatOuNull(mapCellules, 'energie_EP_GJ_par_ha');
    };

    private traiterCellulesPostesEnergieSourcesTotal: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        if (lireCelluleString(mapCellules, 'source') === 'TOTAL') {
            diagnostic.energie.postes.push({
                code: lireCelluleEnum<CodePosteEnergieApi>(mapCellules, 'poste', valeursCodePosteEnergie),
                energiePrimaireGJ: lireCelluleFloatOuNull(mapCellules, 'energie_EP_GJ'),
                estEnergieDirecte: lireCelluleBooleanOuNull(mapCellules, 'est_energie_directe')
            });
        }
    };

    private traiterCellulesPostesEnergiesAutresSources: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        if (lireCelluleString(mapCellules, 'source') !== 'TOTAL') {
            const posteEnergie = diagnostic.energie.postes.find((p) => p.code === lireCelluleString(mapCellules, 'poste'));
            if (posteEnergie !== undefined) {
                if (posteEnergie.sources === undefined) {
                    posteEnergie.sources = [];
                }
                posteEnergie.sources.push({
                    code: lireCelluleEnum<CodeSourceEnergieApi>(mapCellules, 'source', valeursCodeSourceEnergie),
                    energiePrimaireGJ: lireCelluleFloatOuNull(mapCellules, 'energie_EP_GJ')
                });
            } else {
                const message = `Erreur critique : l'objet energie.poste pour le poste ${lireCelluleString(
                    mapCellules,
                    'poste'
                )} n'existe pas dans le diagnostic du territoire ${lireCelluleString(
                    mapCellules,
                    'nom_territoire'
                )}. Il aurait dû être créé au préalable par la méthode traiterCellulesPostesEnergieSourcesTotal.`;
                console.error(message);
                throw new Error(message);
            }
        }
    };

    private traiterCellulesSynthesePesticides: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.pesticides.noduNormalise = lireCelluleFloatOuNull(mapCellules, 'NODU_normalise');
        diagnostic.pesticides.note = lireCelluleFloatOuNull(mapCellules, 'pesticides_note');
    };

    private traiterCellulesPesticidesParAnnees: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        ajouterIndicateursAnneeN<PesticidesIndicateursAnneeNApi>(diagnostic.pesticides.indicateursParAnnees, {
            annee: lireCelluleInt(mapCellules, 'annee'),
            quantiteSubstanceSansDUKg: lireCelluleFloatOuNull(mapCellules, 'quantite_substance_sans_du_kg'),
            quantiteSubstanceAvecDUKg: lireCelluleFloatOuNull(mapCellules, 'quantite_substance_avec_du_kg'),
            noduHa: lireCelluleFloatOuNull(mapCellules, 'NODU_ha'),
            noduNormalise: lireCelluleFloatOuNull(mapCellules, 'NODU_normalise')
        });
    };

    private traiterCellulesPratiquesAgricoles: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.pratiquesAgricoles.indicateurHvn.indice1 = lireCelluleFloatOuNull(mapCellules, 'hvn_indice1');
        diagnostic.pratiquesAgricoles.indicateurHvn.indice2 = lireCelluleFloatOuNull(mapCellules, 'hvn_indice2');
        diagnostic.pratiquesAgricoles.indicateurHvn.indice3 = lireCelluleFloatOuNull(mapCellules, 'hvn_indice3');
        diagnostic.pratiquesAgricoles.indicateurHvn.indiceTotal = lireCelluleFloatOuNull(mapCellules, 'hvn_score');
        diagnostic.pratiquesAgricoles.partSauBioPourcent = lireCelluleFloatOuNull(mapCellules, 'part_sau_bio_pourcent');
        diagnostic.surfaceAgricoleUtile.sauBioHa = lireCelluleFloatOuNull(mapCellules, 'surface_bio_ha');
    };

    private traiterCellulesSauParCulture: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        this.ajouterSauParGroupeCulture(diagnostic, mapCellules);
    };

    private traiterCellulesConsommation: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.consommation.tauxPauvrete60Pourcent = lireCelluleFloatOuNull(mapCellules, 'taux_pauvrete_60_pourcent');
        diagnostic.consommation.codeNiveauRisquePrecariteAlimentaire = lireCelluleEnum<CodeNiveauRisquePrecariteAlimentaireApi>(
            mapCellules,
            'code_niveau_risque_precarite_alimentaire',
            valeursCodeNiveauRisquePrecariteAlimentaire
        );
    };

    private traiterCellulesProfilsTerritoires: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.profils = {
            codeProfilAgriculture: lireCelluleEnum<CodeProfilAgricultureApi>(mapCellules, 'profil_agriculture', valeursCodeProfilAgriculture),
            codeProfilCollectivite: lireCelluleEnum<CodeProfilCollectiviteApi>(mapCellules, 'profil_collectivite', valeursCodeProfilCollectivite),
            codeProfilDiagnostic: lireCelluleEnum<CodeProfilDiagnosticApi>(mapCellules, 'profil_diagnostic', valeursCodeProfilDiagnostic)
        };
    };

    private ajouterSauParGroupeCulture(diagnostic: DiagnosticApi, mapCellules: Map<string, string>) {
        const codeGroupeCulture = lireCelluleEnum<CodeGroupeCultureApi>(mapCellules, 'code_groupe_culture', valeursCodeGroupeCulture);
        let sauParGroupeCulture = diagnostic.surfaceAgricoleUtile.sauParGroupeCulture.find((s) => s.codeGroupeCulture === codeGroupeCulture);
        if (sauParGroupeCulture === undefined) {
            sauParGroupeCulture = {
                codeGroupeCulture: codeGroupeCulture,
                nomGroupeCulture: lireCelluleString(mapCellules, 'nom_groupe_culture'),
                sauHa: lireCelluleFloat(mapCellules, 'production_groupe_culture_ha'),
                sauParCulture: []
            };
            diagnostic.surfaceAgricoleUtile.sauParGroupeCulture.push(sauParGroupeCulture);
        }
        const sauParCulture: SauParCultureApi = {
            codeCulture: lireCelluleString(mapCellules, 'code_culture'),
            nomCulture: lireCelluleString(mapCellules, 'nom_culture'),
            sauHa: lireCelluleFloat(mapCellules, 'production_culture_ha')
        };
        sauParGroupeCulture.sauParCulture.push(sauParCulture);

        return sauParGroupeCulture;
    }

    private traiterCellulesProductionsBesoins: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.productionsBesoins.besoinsAssietteActuelle = {
            besoinsHa: lireCelluleFloatOuNull(mapCellules, 'besoins_assiette_actuelle_ha'),
            tauxAdequationBrutPourcent: lireCelluleFloatOuNull(mapCellules, 'taux_adequation_brut_assiette_actuelle_pourcent'),
            tauxAdequationMoyenPonderePourcent: lireCelluleFloatOuNull(mapCellules, 'taux_adequation_moyen_pondere_assiette_actuelle_pourcent'),
            besoinsParGroupeCulture: []
        };
        diagnostic.productionsBesoins.besoinsAssietteDemitarienne = {
            besoinsHa: lireCelluleFloatOuNull(mapCellules, 'besoins_assiette_demitarienne_ha'),
            tauxAdequationBrutPourcent: lireCelluleFloatOuNull(mapCellules, 'taux_adequation_brut_assiette_demitarienne_pourcent'),
            tauxAdequationMoyenPonderePourcent: lireCelluleFloatOuNull(mapCellules, 'taux_adequation_moyen_pondere_assiette_demitarienne_pourcent'),
            besoinsParGroupeCulture: []
        };
    };

    private traiterCellulesProductionsBesoinsParGroupeCulture: TraiterCellules = (mapCellules: Map<string, string>) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        const besoinsParGroupeCultureAssietteActuelle: BesoinsParGroupeCultureApi = {
            codeGroupeCulture: lireCelluleEnum<CodeGroupeCultureApi>(mapCellules, 'code_groupe_culture', valeursCodeGroupeCulture),
            nomGroupeCulture: lireCelluleString(mapCellules, 'nom_groupe_culture'),
            besoinsHa: lireCelluleFloatOuNull(mapCellules, 'besoins_assiette_actuelle_ha'),
            tauxAdequationBrutPourcent: lireCelluleFloatOuNull(mapCellules, 'taux_adequation_brut_assiette_actuelle_pourcent'),
            besoinsParCulture: []
        };
        diagnostic.productionsBesoins.besoinsAssietteActuelle.besoinsParGroupeCulture.push(besoinsParGroupeCultureAssietteActuelle);

        const besoinsParGroupeCultureAssietteDemitarienne: BesoinsParGroupeCultureApi = {
            codeGroupeCulture: lireCelluleEnum<CodeGroupeCultureApi>(mapCellules, 'code_groupe_culture', valeursCodeGroupeCulture),
            nomGroupeCulture: lireCelluleString(mapCellules, 'nom_groupe_culture'),
            besoinsHa: lireCelluleFloatOuNull(mapCellules, 'besoins_assiette_demitarienne_ha'),
            tauxAdequationBrutPourcent: lireCelluleFloatOuNull(mapCellules, 'taux_adequation_brut_assiette_demitarienne_pourcent'),
            besoinsParCulture: []
        };

        diagnostic.productionsBesoins.besoinsAssietteDemitarienne.besoinsParGroupeCulture.push(besoinsParGroupeCultureAssietteDemitarienne);
    };

    private traiterCellulesBesoinsParCulture: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        this.ajouterBesoinsParCulture(
            diagnostic.productionsBesoins.besoinsAssietteActuelle.besoinsParGroupeCulture,
            mapCellules,
            'assiette_actuelle'
        );
        this.ajouterBesoinsParCulture(
            diagnostic.productionsBesoins.besoinsAssietteDemitarienne.besoinsParGroupeCulture,
            mapCellules,
            'assiette_demitarienne'
        );
    };

    private ajouterBesoinsParCulture(
        besoinsParGroupeCulture: BesoinsParGroupeCultureApi[],
        mapCellules: Map<string, string>,
        infixeTypeAssiette: string
    ) {
        const codeGroupeCultureCourant = lireCelluleEnum<CodeGroupeCultureApi>(mapCellules, 'code_groupe_culture', valeursCodeGroupeCulture);
        const besoinsParGroupeCulturePourGroupeCultureCourant: BesoinsParGroupeCultureApi = besoinsParGroupeCulture.find(
            (s: { codeGroupeCulture: string }) => s.codeGroupeCulture === codeGroupeCultureCourant
        )!;
        if (besoinsParGroupeCulturePourGroupeCultureCourant) {
            const besoinsParCulture: BesoinsParCultureApi = {
                codeCulture: lireCelluleString(mapCellules, 'code_culture'),
                nomCulture: lireCelluleString(mapCellules, 'nom_culture'),
                alimentationAnimale: 'True' === lireCelluleString(mapCellules, 'alimentation_animale'),
                besoinsHa: lireCelluleFloatOuNull(mapCellules, `besoins_${infixeTypeAssiette}_ha`)
            };

            besoinsParGroupeCulturePourGroupeCultureCourant.besoinsParCulture.push(besoinsParCulture);
        }
    }

    private traiterCellulesProduction: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.production.note = lireCelluleIntOuNull(mapCellules, 'note');
        diagnostic.production.tauxAdequationMoyenPonderePourcent = lireCelluleFloatOuNull(mapCellules, 'taux_adequation_moyen_pondere_pourcent');
        diagnostic.production.partSauBioPourcent = lireCelluleFloatOuNull(mapCellules, 'part_sau_bio_pourcent');
        diagnostic.production.indiceHvn = lireCelluleFloatOuNull(mapCellules, 'hvn_score');
        diagnostic.production.noteTauxAdequationMoyenPondere = lireCelluleIntOuNull(mapCellules, 'taux_adequation_moyen_pondere_note');
        diagnostic.production.notePartSauBio = lireCelluleIntOuNull(mapCellules, 'part_sau_bio_note');
        diagnostic.production.noteHvn = lireCelluleIntOuNull(mapCellules, 'hvn_note');
    };

    private traiterCellulesProximiteCommercesParTypesCommerces: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.proximiteCommerces.indicateursParTypesCommerces.push({
            typeCommerce: lireCelluleString(mapCellules, 'code_type_commerce'),
            distancePlusProcheCommerceMetres: lireCelluleFloatOuNull(mapCellules, 'distance_plus_proche_commerce_m'),
            partPopulationAccesAPiedPourcent: lireCelluleFloatOuNull(mapCellules, 'part_population_acces_a_pied_pourcent'),
            partPopulationAccesAVeloPourcent: lireCelluleFloatOuNull(mapCellules, 'part_population_acces_a_velo_pourcent'),
            partPopulationDependanteVoiturePourcent: lireCelluleFloatOuNull(mapCellules, 'part_population_dependante_voiture_pourcent')
        });
    };

    private traiterCellulesProximiteCommerces: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );
        diagnostic.proximiteCommerces.partPopulationDependanteVoiturePourcent = lireCelluleFloatOuNull(
            mapCellules,
            'part_population_dependante_voiture_pourcent'
        );
        diagnostic.proximiteCommerces.partTerritoireDependantVoiturePourcent = lireCelluleFloatOuNull(
            mapCellules,
            'part_territoire_dependant_voiture_pourcent'
        );
        diagnostic.proximiteCommerces.note = lireCelluleIntOuNull(mapCellules, 'note');
    };

    private traiterCellulesAnaloguesClimatiques: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire)
        );

        const nomCommuneReference = lireCelluleStringOuNull(mapCellules, 'nom_commune_reference');
        if (nomCommuneReference === null) {
            diagnostic.climat.analoguesClimatiques = {};
        } else {
            const idCraterCommuneReference = lireCelluleString(mapCellules, 'id_commune_reference');
            const latitudeCommuneReference = lireCelluleFloatOuNull(mapCellules, 'latitude_commune_reference');
            const longitudeCommuneReference = lireCelluleFloatOuNull(mapCellules, 'longitude_commune_reference');
            let communeReference: CommuneReferenceApi | undefined = undefined;
            if (idCraterCommuneReference !== null && latitudeCommuneReference !== null && longitudeCommuneReference !== null) {
                communeReference = {
                    nom: nomCommuneReference,
                    idCrater: idCraterCommuneReference,
                    latitude: latitudeCommuneReference,
                    longitude: longitudeCommuneReference
                };
            }

            const latitudeCommuneAnalogueClimatique2000_2050 = lireCelluleFloatOuNull(mapCellules, 'latitude_commune_analogue_climatique_2000_2050');
            const longitudeCommuneAnalogueClimatique2000_2050 = lireCelluleFloatOuNull(
                mapCellules,
                'longitude_commune_analogue_climatique_2000_2050'
            );
            let communeAnalogueClimatique2000_2050: CommuneAnalogueClimatique2000_2050Api | undefined = undefined;

            if (latitudeCommuneAnalogueClimatique2000_2050 !== null && longitudeCommuneAnalogueClimatique2000_2050 !== null) {
                communeAnalogueClimatique2000_2050 = {
                    nom: lireCelluleString(mapCellules, 'nom_commune_analogue_climatique_2000_2050'),
                    pays: lireCelluleString(mapCellules, 'pays_commune_analogue_climatique_2000_2050'),
                    latitude: latitudeCommuneAnalogueClimatique2000_2050,
                    longitude: longitudeCommuneAnalogueClimatique2000_2050
                };
            }
            diagnostic.climat.analoguesClimatiques = {
                communeReference: communeReference,
                communeAnalogueClimatique2000_2050: communeAnalogueClimatique2000_2050
            };
        }
    };

    async injecterDonneesDiagnostics(dossierDonnees = '../crater-data/output-data') {
        await new InjecteurCsv().injecter(
            dossierDonnees + '/referentiel_territoires/referentiel_territoires.csv',
            this.idsTerritoires.enregistrerCorrespondanceIdsTerritoires
        );

        await Promise.all([
            new InjecteurCsv().injecter(
                dossierDonnees + '/referentiel_territoires/referentiel_territoires.csv',
                this.traiterCellulesReferentielTerritoires
            ),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/population/population.csv', this.traiterCellulesPopulation),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/occupation_sols/occupation_sols.csv', this.traiterCellulesOccupationSols),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/otex/otex_territoires.csv', this.traiterCellulesOtexTerritoires),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/cheptels/cheptels.csv', this.traiterCellulesCheptels),
            new InjecteurCsv().injecter(
                dossierDonnees + '/contours/donnees_contours/donnees_contours.csv',
                this.traiterCellulesGeographiesTerritoires
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/indicateurs/politique_fonciere/politique_fonciere.csv',
                this.traiterCellulesPolitiqueFonciere
            ),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/intrants/synthese_intrants.csv', this.traiterCellulesSyntheseIntrants),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/eau/synthese_eau.csv', this.traiterCellulesSyntheseEau),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/eau/irrigation_par_annees.csv', this.traiterCellulesIrrigationParAnnees),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/eau/pratiques_irrigation.csv', this.traiterCellulesPratiquesIrrigation),
            new InjecteurCsv().injecter(
                dossierDonnees + '/indicateurs/eau/arretes_secheresse_par_annees_mois.csv',
                this.traiterCellulesArretesSecheresseParAnneesMois
            ),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/energie/synthese_energie.csv', this.traiterCellulesSyntheseEnergie),
            this.injecterPostesEnergie(dossierDonnees + '/indicateurs/energie/postes_energie.csv'),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/pesticides/synthese_pesticides.csv', this.traiterCellulesSynthesePesticides),
            new InjecteurCsv().injecter(
                dossierDonnees + '/indicateurs/pesticides/pesticides_par_annees.csv',
                this.traiterCellulesPesticidesParAnnees
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/indicateurs/population_agricole/population_agricole.csv',
                this.traiterCellulesPopulationAgricole
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/indicateurs/pratiques_agricoles/pratiques_agricoles.csv',
                this.traiterCellulesPratiquesAgricoles
            ),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/productions_besoins/sau_par_culture.csv', this.traiterCellulesSauParCulture),
            new InjecteurCsv().injecter(
                dossierDonnees + '/indicateurs/productions_besoins/productions_besoins.csv',
                this.traiterCellulesProductionsBesoins
            ),
            this.injecterFichiersBesoinsParGroupesCultures(dossierDonnees),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/production/production.csv', this.traiterCellulesProduction),

            new InjecteurCsv().injecter(
                dossierDonnees + '/indicateurs/proximite_commerces/proximite_commerces_par_types_commerces.csv',
                this.traiterCellulesProximiteCommercesParTypesCommerces
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/indicateurs/proximite_commerces/proximite_commerces.csv',
                this.traiterCellulesProximiteCommerces
            ),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/consommation/consommation.csv', this.traiterCellulesConsommation),
            new InjecteurCsv().injecter(
                dossierDonnees + '/indicateurs/profils_territoires/profils_territoires.csv',
                this.traiterCellulesProfilsTerritoires
            ),
            new InjecteurCsv().injecter(dossierDonnees + '/indicateurs/climat/analogues_climatiques.csv', this.traiterCellulesAnaloguesClimatiques)
        ]);
    }

    private async injecterFichiersBesoinsParGroupesCultures(dossierDonnees: string) {
        // await nécessaire ici pour créer les objets ProductionsBesoinsParGroupeCulture avant d'ajouter les besoins par culture à l'intérieur
        await new InjecteurCsv().injecter(
            dossierDonnees + '/indicateurs/productions_besoins/productions_besoins_par_groupe_culture.csv',
            this.traiterCellulesProductionsBesoinsParGroupeCulture
        );
        await new InjecteurCsv().injecter(
            dossierDonnees + '/indicateurs/productions_besoins/besoins_par_culture.csv',
            this.traiterCellulesBesoinsParCulture
        );
    }

    private async injecterPostesEnergie(fichierPostesEnergie: string) {
        // await nécessaire ici pour créer les objets postes avant les objets sources (on fait 2 passes successives sur le meme fichier)
        await new InjecteurCsv().injecter(fichierPostesEnergie, this.traiterCellulesPostesEnergieSourcesTotal);
        await new InjecteurCsv().injecter(fichierPostesEnergie, this.traiterCellulesPostesEnergiesAutresSources);
    }
}
