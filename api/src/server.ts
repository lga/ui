import cors from 'cors';
import express from 'express';

import { logInfo } from './communs/logUtils';
import CraterApi from './CraterApi';

const CHEMIN_CRATER_DATA_RESULTATS = '../crater-data-resultats/data';

logInfo('CRATER-API : [' + new Date().toUTCString() + "] CRATer-API Démarrage de l'application");

const app: express.Application = express();

app.use(cors());
app.use('/crater/api/docs', express.static('api_spec/spec-openapi.yml'));

app.use(
    '/crater/api/cartes/geojson',
    express.static(CHEMIN_CRATER_DATA_RESULTATS + '/contours/geojson', {
        maxAge: '1d'
    })
);
app.use(
    '/crater/api/cartes/vectorielles',
    express.static(CHEMIN_CRATER_DATA_RESULTATS + '/contours/pmtiles', {
        maxAge: '1d'
    })
);
app.use(
    '/crater/api/sitemaps',
    express.static(CHEMIN_CRATER_DATA_RESULTATS + '/sitemaps', {
        maxAge: '1d'
    })
);

const craterApi = new CraterApi(app, CHEMIN_CRATER_DATA_RESULTATS);
craterApi.init();

const port = process.env.PORT || 3000;

app.listen(port, function () {
    logInfo(`CRATER-API : [${new Date().toUTCString()}] CRATer-API en ecoute sur le port ${port}`);
});
