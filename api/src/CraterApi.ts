import type express from 'express';
import type { NextFunction, Request, Response } from 'express';

import { Etat } from './communs/apiUtils';
import { ErreurRequeteIncorrecte, ErreurRessourceNonTrouvee, ErreurTemporairementIndisponible } from './communs/erreurs';
import { logInfo } from './communs/logUtils';
import { ApiDiagnostics } from './diagnostics/ApiDiagnostics';
import { InjecteursDiagnostics } from './diagnostics/InjecteursDiagnostics';
import { ServicesDiagnostics } from './diagnostics/ServicesDiagnostics';
import { ApiTerritoires } from './territoires/ApiTerritoires';
import { ServicesTerritoires } from './territoires/domaines/ServicesTerritoires';
import { InjecteursTerritoires } from './territoires/InjecteursTerritoires';

export default class CraterApi {
    private expressApp!: express.Application;

    private servicesTerritoires: ServicesTerritoires;
    private injecteursTerritoires: InjecteursTerritoires;
    private apiTerritoires: ApiTerritoires;

    private servicesDiagnostics: ServicesDiagnostics;
    private injecteursDiagnostics: InjecteursDiagnostics;
    private apiDiagnostics: ApiDiagnostics;
    private cheminFichiersDonnees: string;

    constructor(expressApp?: express.Application, cheminFichiersDonnees = '../crater-data-resultats') {
        this.cheminFichiersDonnees = cheminFichiersDonnees;
        if (expressApp) this.expressApp = expressApp;

        this.servicesTerritoires = new ServicesTerritoires();
        this.injecteursTerritoires = new InjecteursTerritoires(this.servicesTerritoires);
        this.apiTerritoires = new ApiTerritoires(this.expressApp, this.servicesTerritoires);
        this.servicesDiagnostics = new ServicesDiagnostics();
        this.injecteursDiagnostics = new InjecteursDiagnostics(this.servicesDiagnostics);
        this.apiDiagnostics = new ApiDiagnostics(this.expressApp, this.servicesDiagnostics);
    }

    private async injecterDonnees() {
        await this.initModuleTerritoires();
        await this.initModuleDiagnostics();
    }

    private configurerMiddlewaresRoutes() {
        this.apiTerritoires.configurerRoutes();
        this.apiDiagnostics.configurerRoutes();
        this.expressApp.use('/', this.construireReponseErreurRessourceNonTrouvee);
    }

    private construireReponseErreurRessourceNonTrouvee = (req: express.Request) => {
        throw new ErreurRessourceNonTrouvee(`Chemin inconnu : ${req.url}`);
    };

    private async initModuleTerritoires() {
        await this.injecteursTerritoires
            .injecterDonneesReferentielTerritoires(this.cheminFichiersDonnees)
            .then(() => (this.apiTerritoires.etat = Etat.PRET))
            .then(() => {
                logInfo('CRATER-API : Injection donnees territoires OK');
            })
            .catch((e) => {
                this.apiTerritoires.etat = Etat.ERREUR;
                return Promise.reject(new Error(e));
            });
    }

    private async initModuleDiagnostics() {
        await this.injecteursDiagnostics
            .injecterDonneesDiagnostics(this.cheminFichiersDonnees)
            .then(() => (this.apiDiagnostics.etat = Etat.PRET))
            .then(() => {
                logInfo('CRATER-API : Injection donnees diagnostic OK');
            })
            .catch((e) => {
                this.apiTerritoires.etat = Etat.ERREUR;
                return Promise.reject(new Error(e));
            });
    }

    private configurerMiddlewareErreurs() {
        this.expressApp.use(function (err: Error, req: Request, res: Response, next: NextFunction) {
            res.set('Cache-control', 'no-cache');
            if (err instanceof ErreurRessourceNonTrouvee) {
                res.status(404).json({
                    message: 'Ressource introuvable',
                    description: err.message
                });
            } else if (err instanceof ErreurRequeteIncorrecte) {
                res.status(400).json({
                    message: 'Requête incorrecte',
                    description: err.message
                });
            } else if (err instanceof ErreurTemporairementIndisponible) {
                res.status(503).json({
                    message: 'Service temporairement indisponible',
                    description: err.message
                });
            } else {
                // Tous les autres cas d'erreur
                res.status(500).json({
                    message: 'Erreur interne du serveur',
                    description: `Oups, une erreur technique est survenue ! Merci de réessayer dans quelques instants. Si l'erreur persiste, contactez-nous !`
                });
            }
            next();
        });
    }

    private configurerMiddlewareLogs() {
        this.expressApp.use(function (request: express.Request, response: express.Response, next) {
            const debut = +new Date();
            response.on('finish', () => {
                const duree = +new Date() - debut;
                logInfo(`${request.method} ${request.path} : HTTP ${response.statusCode} ${response.statusMessage} (${duree}ms)`);
            });
            next();
        });
    }

    private configurerMiddlewareCache() {
        this.expressApp.use(function (request: express.Request, response: express.Response, next) {
            response.set('Cache-control', 'public, max-age=3600');
            next();
        });
    }

    async init() {
        this.configurerMiddlewares();
        return this.injecterDonnees();
    }

    private configurerMiddlewares() {
        // Config des middlewares expressjs, l'ordre est important (logs en 1er, erreurs en dernier)
        this.configurerMiddlewareLogs();
        this.configurerMiddlewareCache();
        this.configurerMiddlewaresRoutes();
        this.configurerMiddlewareErreurs();
    }
}
