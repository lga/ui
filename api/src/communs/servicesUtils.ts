import type { BoundingBoxApi } from '@lga/specification-api/build/specification/api';

export class StockageMemoireIndexParId<T> {
    private entites: Map<string, T> = new Map<string, T>();

    findAll(): T[] {
        return [...this.entites.values()];
    }

    save(id: string, entite: T): void {
        this.entites.set(id, entite);
    }

    findById(id: string): T | undefined {
        return this.entites.get(id);
    }
}

export const estIntersectionVide = (bbox1: BoundingBoxApi, bbox2: BoundingBoxApi) => {
    return (
        bbox1.longitudeMax < bbox2.longitudeMin ||
        bbox1.longitudeMin > bbox2.longitudeMax ||
        bbox1.latitudeMax < bbox2.latitudeMin ||
        bbox1.latitudeMin > bbox2.latitudeMax
    );
};
