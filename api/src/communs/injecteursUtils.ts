import fs from 'fs';
import rd from 'readline';

import { logErreur, logInfo } from './logUtils';

export type TraiterCellules = (cellules: Map<string, string>) => void;

export function lireCelluleString(map: Map<string, string>, cle: string): string {
    const contenuCellule = map.get(cle);
    if (contenuCellule === undefined) {
        console.error(`Erreur critique : la colonne ${cle} ne correspond pas à un nom de colonne dans le fichier`);
        throw new Error(`Erreur critique : la colonne ${cle} ne correspond pas à un nom de colonne dans le fichier`);
    }
    return contenuCellule;
}

export function lireCelluleEnum<T>(map: Map<string, string>, cle: string, valeursAutorisees: (string | null)[]): T {
    const contenuCellule: string | null = lireCelluleStringOuNull(map, cle);
    if (!valeursAutorisees.includes(contenuCellule)) {
        const messageErreur = `Erreur critique : la valeur ${contenuCellule} lue dans la colonne ${cle} n'est pas une valeur valide pour ce champ. Liste des valeurs valides : ${valeursAutorisees.join(', ')}`;
        console.error(messageErreur);
        throw new Error(messageErreur);
    }
    return contenuCellule as T;
}

export function lireCelluleStringOuNull(map: Map<string, string>, cle: string): string | null {
    const contenuCellule = lireCelluleString(map, cle);
    return contenuCellule === '' ? null : contenuCellule;
}

export function lireCelluleInt(map: Map<string, string>, cle: string): number {
    const contenuCellule = lireCelluleString(map, cle);
    return Math.round(parseFloat(contenuCellule));
}

export function lireCelluleIntOuNull(map: Map<string, string>, cle: string): number | null {
    const contenuCellule = lireCelluleString(map, cle);
    return contenuCellule === '' ? null : Math.round(parseFloat(contenuCellule));
}

export function lireCelluleFloat(map: Map<string, string>, cle: string): number {
    const contenuCellule = lireCelluleString(map, cle);
    return Number.parseFloat(contenuCellule);
}

export function lireCelluleFloatOuNull(map: Map<string, string>, cle: string): number | null {
    const contenuCellule = lireCelluleString(map, cle);
    return contenuCellule === '' ? null : Number.parseFloat(contenuCellule);
}

export function lireCelluleBoolean(map: Map<string, string>, cle: string): boolean {
    const contenuCellule = lireCelluleString(map, cle);
    return contenuCellule === 'True';
}

export function lireCelluleBooleanOuNull(map: Map<string, string>, cle: string): boolean | null {
    const contenuCellule = lireCelluleString(map, cle);
    return contenuCellule === '' ? null : contenuCellule === 'True';
}

export class InjecteurCsv {
    public numLigneCourante = 0;
    public nomColonnes: string[] = [];
    private messageErreur?: string;

    public static readonly SEPARATEUR_POINT_VIRGULE = ';';
    public static readonly SEPARATEUR_VIRGULE = ',';

    async injecter(fichierDonnees: string, traiterCellules: TraiterCellules) {
        this.numLigneCourante = 0;

        return this.traiterFichier(fichierDonnees, traiterCellules)
            .then(() => {
                logInfo(`[${new Date().toUTCString()}'] Chargement fait pour le fichier ${fichierDonnees}, ${this.numLigneCourante} lignes lues`);
            })
            .catch((e) => {
                logErreur(`ERREUR lors du chargement du fichier ${fichierDonnees}, ${this.numLigneCourante} lignes lues\n${e}`);
                return Promise.reject(new Error(e));
            });
    }

    private traiterFichier(fichierDonnees: string, traiterCellules: TraiterCellules): Promise<void> {
        const readStream = fs.createReadStream(fichierDonnees);

        const reader = rd.createInterface(readStream);
        readStream.on('error', (erreur: Error): void => {
            this.messageErreur = `Erreur lors de la lecture dans le fichier ${fichierDonnees}.\n${erreur.message}`;
            logErreur(this.messageErreur);
            reader.emit('close');
        });

        reader.on('line', (ligne: string): void => {
            if (!ligne || ligne == '') {
                return;
            }
            this.numLigneCourante = this.numLigneCourante + 1;
            const cellules = ligne.split(InjecteurCsv.SEPARATEUR_POINT_VIRGULE);

            if (this.numLigneCourante === 1) {
                this.nomColonnes = [];
                cellules.forEach((c) => this.nomColonnes.push(c));
            } else {
                const mapCellules: Map<string, string> = new Map<string, string>();
                for (let i = 0; i < cellules.length; i++) {
                    mapCellules.set(this.nomColonnes[i], cellules[i]);
                }
                traiterCellules(mapCellules);
            }
        });

        return new Promise((resolve, reject) => {
            reader
                .on('close', (): void => {
                    if (this.messageErreur) {
                        logErreur('Erreur callbackFinFichier :' + this.messageErreur);
                        reject(new Error(this.messageErreur));
                    } else {
                        resolve();
                    }
                })
                .on('error', (e): void => {
                    reject(new Error(e));
                });
        });
    }
}
