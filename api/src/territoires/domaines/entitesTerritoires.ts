import type {
    CodeCategorieTerritoireApi,
    CodeGenreNombreTerritoireApi,
    CodePrepositionTerritoireApi,
    CodeSousCategorieTerritoireApi,
    TerritoireBaseApi
} from '@lga/specification-api/build/specification/api';

export const CATEGORIES_TERRITOIRES: CodeCategorieTerritoireApi[] = ['PAYS', 'REGION', 'DEPARTEMENT', 'EPCI', 'REGROUPEMENT_COMMUNES', 'COMMUNE'];

enum LibelleCategorieTerritoire {
    Pays = 'Pays',
    Region = 'Région',
    Departement = 'Département',
    RegroupementCommunes = 'Regroupement de communes',
    Epci = 'Intercommunalité',
    Commune = 'Commune'
}

export abstract class Territoire implements TerritoireBaseApi {
    public readonly id: string;
    public readonly idCode: string;
    public readonly idNom: string;
    public readonly nom: string;
    public readonly categorie: CodeCategorieTerritoireApi;
    public readonly idTerritoireParcel: string | null;
    public readonly boundingBox: {
        latitudeMin: number;
        latitudeMax: number;
        longitudeMin: number;
        longitudeMax: number;
    };
    public readonly genreNombre: CodeGenreNombreTerritoireApi;
    public readonly preposition: CodePrepositionTerritoireApi;

    constructor(territoireBase: TerritoireBaseApi) {
        this.id = territoireBase.id;
        this.idCode = territoireBase.idCode;
        this.idNom = territoireBase.idNom;
        this.nom = territoireBase.nom;
        this.categorie = territoireBase.categorie;
        this.idTerritoireParcel = territoireBase.idTerritoireParcel;
        this.boundingBox = territoireBase.boundingBox;
        this.genreNombre = territoireBase.genreNombre;
        this.preposition = territoireBase.preposition;
    }

    abstract get libelle(): string;
    abstract get libelleSecondaire(): string;
}

export class Pays extends Territoire {
    get libelle(): string {
        return this.nom;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.Pays;
    }
}

export class Region extends Territoire {
    constructor(
        territoireBase: TerritoireBaseApi,
        public readonly idPays: string
    ) {
        super(territoireBase);
    }

    get libelle(): string {
        return this.nom;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.Region;
    }
}

export class Departement extends Territoire {
    constructor(
        territoireBase: TerritoireBaseApi,
        public readonly idRegion: string,
        public readonly idPays: string
    ) {
        super(territoireBase);
    }

    get libelle(): string {
        return this.nom;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.Departement;
    }
}

export class RegroupementCommunes extends Territoire {
    constructor(
        territoireBase: TerritoireBaseApi,
        public readonly sousCategorie: CodeSousCategorieTerritoireApi,
        public readonly idDepartement: string,
        public readonly idRegion: string,
        public readonly idPays: string
    ) {
        super(territoireBase);
    }

    get libelle(): string {
        return this.nom;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.RegroupementCommunes;
    }
}

export class Epci extends RegroupementCommunes {
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.Epci;
    }
}

export class Commune extends Territoire {
    private _codesPostaux: string[] = [];

    constructor(
        territoireBase: TerritoireBaseApi,
        public readonly idEpci: string,
        public readonly idDepartement: string,
        public readonly idRegion: string,
        public readonly idPays: string,
        codesPostaux: string[]
    ) {
        super(territoireBase);
        if (codesPostaux) {
            this.ajouterCodePostal(...codesPostaux);
        }
    }

    ajouterCodePostal(...codePostal: string[]): void {
        codePostal.forEach((c) => this._codesPostaux.push(c));
        this._codesPostaux.sort(function (s1, s2) {
            return s1.localeCompare(s2);
        });
    }

    get codesPostaux(): string {
        return this._codesPostaux.join(', ');
    }

    contientUnCodePostalCommencantPar(debutCodePostal: string): boolean {
        const strings = this._codesPostaux.filter(function (cp: string): boolean {
            return cp.startsWith(debutCodePostal);
        });
        return strings.length > 0;
    }
    get libelle(): string {
        return `${this.nom} (${this.codesPostaux})`;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.Commune;
    }
}
