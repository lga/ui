import type {
    CommuneApi,
    DepartementApi,
    RegionApi,
    RegroupementCommunesApi,
    TerritoireApi,
    TerritoireBaseApi,
    TerritoireSyntheseApi
} from '@lga/specification-api/build/specification/api';
import type { Application, Request, Response } from 'express';

import { ajouterBodyJson, Etat, verifierEtatApi } from '../communs/apiUtils';
import { ErreurRessourceNonTrouvee, ErreurTechnique } from '../communs/erreurs';
import { Commune, Departement, type Epci, Region, RegroupementCommunes, type Territoire } from './domaines/entitesTerritoires';
import type { ServicesTerritoires } from './domaines/ServicesTerritoires';

interface PathParam {
    idTerritoire: string;
}
interface QueryParam {
    critere?: string;
    nbMaxResultats?: string;
}
type RequeteApiTerritoires = Request<PathParam, unknown, unknown, QueryParam>;
export class ApiTerritoires {
    private stockageTerritoires: ServicesTerritoires;
    private expressApp: Application;
    public etat = Etat.INIT_EN_COURS;

    constructor(expressApp: Application, stockageTerritoires: ServicesTerritoires) {
        this.expressApp = expressApp;
        this.stockageTerritoires = stockageTerritoires;
    }

    configurerRoutes(): void {
        this.expressApp.get('/crater/api/territoires', this.construireReponseGetTerritoires);
        this.expressApp.get('/crater/api/territoires/:idTerritoire', this.construireReponseGetTerritoireParId);
    }

    private construireReponseGetTerritoires = (req: RequeteApiTerritoires, res: Response) => {
        verifierEtatApi(this.etat);
        ajouterBodyJson(
            res,
            this.rechercherSynthesesTerritoiresParNomOuCodePostal(
                req.query.critere ? req.query.critere.toString() : '',
                req.query.nbMaxResultats ? req.query.nbMaxResultats.toString() : ''
            )
        );
    };

    private construireReponseGetTerritoireParId = (req: RequeteApiTerritoires, res: Response) => {
        verifierEtatApi(this.etat);
        ajouterBodyJson(res, this.rechercherTerritoireParId(req.params.idTerritoire));
    };

    private rechercherSynthesesTerritoiresParNomOuCodePostal(critere: string, nbMaxResultats: string): TerritoireSyntheseApi[] {
        const nbMaxResultatAsNumber = parseInt(nbMaxResultats) ? parseInt(nbMaxResultats) : 10;
        const territoires = this.stockageTerritoires.rechercherTousParCritere(critere, nbMaxResultatAsNumber);
        const listeTerritoiresSynthese = territoires.map(this.creerObjetTerritoireSynthese);
        return this.ajouterTerritoireSuggere(listeTerritoiresSynthese, nbMaxResultatAsNumber);
    }

    //  TODO : beaucoup de logique métier ici, il faudrait rappatrier dans le service stockageTerritoires et tester l'ensemble
    // Mais implique un peu de refacto, a faire dans un 2eme temps donc
    private ajouterTerritoireSuggere(listeTerritoiresSynthese: TerritoireSyntheseApi[], nbMaxResultats: number): TerritoireSyntheseApi[] {
        let listeAvecTerritoireSuggere = listeTerritoiresSynthese;
        if (listeTerritoiresSynthese.length > 0 && listeTerritoiresSynthese[0].categorie === 'COMMUNE') {
            const commune = this.stockageTerritoires.rechercher(listeTerritoiresSynthese[0].id) as Commune;
            const epciAppartenance = this.stockageTerritoires.rechercher(commune.idEpci) as Epci;
            if (epciAppartenance) {
                const territoireSuggere = this.creerObjetTerritoireSynthese(epciAppartenance);
                territoireSuggere.estSuggere = true;
                territoireSuggere.libelleSecondaire = `Intercommunalité de ${commune.nom}`;
                listeAvecTerritoireSuggere = listeTerritoiresSynthese.filter((t) => t.id !== territoireSuggere.id);
                listeAvecTerritoireSuggere.unshift(territoireSuggere);
                listeAvecTerritoireSuggere = listeAvecTerritoireSuggere.slice(0, nbMaxResultats);
            }
        }
        return listeAvecTerritoireSuggere;
    }

    private creerObjetTerritoireSynthese(territoire: Territoire): TerritoireSyntheseApi {
        const territoireSynthese: TerritoireSyntheseApi = {
            id: territoire.id,
            idCode: territoire.idCode,
            idNom: territoire.idNom,
            idTerritoireParcel: territoire.idTerritoireParcel,
            nom: territoire.nom,
            categorie: territoire.categorie,
            libellePrincipal: territoire.libelle,
            libelleSecondaire: territoire.libelleSecondaire
        };
        if (territoire instanceof RegroupementCommunes) {
            territoireSynthese.sousCategorie = territoire.sousCategorie;
        }
        return territoireSynthese;
    }

    private rechercherTerritoireParId(idTerritoire: string): TerritoireApi {
        const territoire = this.stockageTerritoires.rechercher(idTerritoire);
        if (!territoire) {
            throw new ErreurRessourceNonTrouvee('Territoire non trouvé (idTerritoire=' + idTerritoire + ')');
        }
        return this.creerObjetTerritoireApi(territoire);
    }

    private creerObjetTerritoireApi(territoire: Territoire): TerritoireApi {
        const territoireBaseApi: TerritoireBaseApi = {
            id: territoire.id,
            idCode: territoire.idCode,
            idNom: territoire.idNom,
            nom: territoire.nom,
            categorie: territoire.categorie,
            idTerritoireParcel: territoire.idTerritoireParcel,
            genreNombre: territoire.genreNombre,
            preposition: territoire.preposition,
            boundingBox: territoire.boundingBox
        };

        if (territoire instanceof Region) {
            return {
                ...territoireBaseApi,
                pays: this.creerTerritoireApi((territoire as Region).idPays)
            } as RegionApi;
        }
        if (territoire instanceof Departement) {
            return {
                ...territoireBaseApi,
                pays: this.creerTerritoireApi((territoire as Departement).idPays),
                region: this.creerTerritoireApi((territoire as Departement).idRegion)
            } as DepartementApi;
        }
        if (territoire instanceof RegroupementCommunes) {
            return {
                ...territoireBaseApi,
                sousCategorie: territoire.sousCategorie,
                pays: this.creerTerritoireApi((territoire as RegroupementCommunes).idPays),
                region: this.creerTerritoireApi((territoire as RegroupementCommunes).idRegion),
                departement: this.creerTerritoireApi((territoire as RegroupementCommunes).idDepartement)
            } as RegroupementCommunesApi;
        }
        if (territoire instanceof Commune) {
            return {
                ...territoireBaseApi,
                pays: this.creerTerritoireApi((territoire as Commune).idPays),
                region: this.creerTerritoireApi((territoire as Commune).idRegion),
                departement: this.creerTerritoireApi((territoire as Commune).idDepartement),
                epci: (territoire as Commune).idEpci ? this.creerTerritoireApi((territoire as Commune).idEpci) : undefined //cas des communes sans EPCI
            } as CommuneApi;
        }
        return territoireBaseApi;
    }

    private creerTerritoireApi(idTerritoire: string): TerritoireApi {
        const territoire = this.stockageTerritoires.rechercher(idTerritoire);
        if (territoire === undefined) {
            throw new ErreurTechnique(
                'Erreur lors de la création de la réponse, territoire parent non trouvé (idTerritoire parent=' + idTerritoire
                    ? idTerritoire
                    : 'UNDEFINED' + ')'
            );
        } else {
            const territoireApi: TerritoireBaseApi = {
                id: territoire.id,
                idCode: territoire.idCode,
                idNom: territoire.idNom,
                nom: territoire.nom,
                categorie: territoire.categorie,
                idTerritoireParcel: territoire.idTerritoireParcel,
                boundingBox: territoire.boundingBox,
                genreNombre: territoire.genreNombre,
                preposition: territoire.preposition
            };
            if (territoire instanceof RegroupementCommunes) {
                territoireApi.sousCategorie = territoire.sousCategorie;
            }
            return territoireApi;
        }
    }
}
