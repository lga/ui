import type {
    CodeCategorieTerritoireApi,
    CodeGenreNombreTerritoireApi,
    CodeSousCategorieTerritoireApi,
    TerritoireBaseApi
} from '@lga/specification-api/build/specification/api';
import type { CodePrepositionTerritoire } from '@lga/specification-api/build/specification/api/models/CodePrepositionTerritoire.js';
import {
    valeursCodeCategorieTerritoire,
    valeursCodeGenreNombreTerritoire,
    valeursCodePrepositionTerritoire
} from '@lga/specification-api/build/specification/valeurs-codes';
import { valeursCodeSousCategorieTerritoire } from '@lga/specification-api/build/specification/valeurs-codes';

import {
    InjecteurCsv,
    lireCelluleEnum,
    lireCelluleFloat,
    lireCelluleString,
    lireCelluleStringOuNull,
    type TraiterCellules
} from '../communs/injecteursUtils';
import { logInfo } from '../communs/logUtils';
import { MapIdsTerritoires } from '../communs/MapIdsTerritoires';
import { Commune, Departement, Epci, Pays, Region, RegroupementCommunes, type Territoire } from './domaines/entitesTerritoires';
import type { ServicesTerritoires } from './domaines/ServicesTerritoires';

function extraireCodesPostaux(string: string) {
    return string.split('|');
}

export class InjecteursTerritoires {
    private readonly idsTerritoires = new MapIdsTerritoires();

    constructor(private readonly servicesTerritoires: ServicesTerritoires) {}

    private traiterCellulesReferentielTerritoires: TraiterCellules = (mapCellules) => {
        let territoire: Territoire;
        const categorie = lireCelluleEnum<CodeCategorieTerritoireApi>(mapCellules, 'categorie_territoire', valeursCodeCategorieTerritoire);
        const territoireBaseApi: TerritoireBaseApi = {
            id: this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            idCode: lireCelluleString(mapCellules, 'id_territoire'),
            idNom: lireCelluleString(mapCellules, 'id_nom_territoire'),
            nom: lireCelluleString(mapCellules, 'nom_territoire'),
            categorie: categorie,
            idTerritoireParcel: lireCelluleStringOuNull(mapCellules, 'id_territoire_parcel'),
            boundingBox: {
                latitudeMin: 0,
                latitudeMax: 0,
                longitudeMin: 0,
                longitudeMax: 0
            },
            genreNombre: lireCelluleEnum<CodeGenreNombreTerritoireApi>(mapCellules, 'genre_nombre', valeursCodeGenreNombreTerritoire),
            preposition: lireCelluleEnum<CodePrepositionTerritoire>(mapCellules, 'preposition', valeursCodePrepositionTerritoire)
        };

        switch (categorie) {
            case 'PAYS':
                territoire = new Pays(territoireBaseApi);
                break;
            case 'REGION':
                territoire = new Region(territoireBaseApi, this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_pays') ?? '');
                break;
            case 'DEPARTEMENT':
                territoire = new Departement(
                    territoireBaseApi,
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_region') ?? '',
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_pays') ?? ''
                );
                break;
            case 'REGROUPEMENT_COMMUNES':
                territoire = new RegroupementCommunes(
                    territoireBaseApi,
                    lireCelluleEnum<CodeSousCategorieTerritoireApi>(
                        mapCellules,
                        'categorie_regroupement_communes',
                        valeursCodeSousCategorieTerritoire
                    ),
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_departement') ?? '',
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_region') ?? '',
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_pays') ?? ''
                );
                break;
            case 'EPCI':
                territoire = new Epci(
                    territoireBaseApi,
                    lireCelluleEnum<CodeSousCategorieTerritoireApi>(mapCellules, 'categorie_epci', valeursCodeSousCategorieTerritoire),
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_departement') ?? '',
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_region') ?? '',
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_pays') ?? ''
                );
                break;
            case 'COMMUNE':
                territoire = new Commune(
                    territoireBaseApi,
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_epci') ?? '',
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_departement') ?? '',
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_region') ?? '',
                    this.idsTerritoires.lireCelluleIdTerritoireSupra(mapCellules, 'id_pays') ?? '',
                    extraireCodesPostaux(mapCellules.get('codes_postaux_commune')!)
                );
                break;
            default:
                return;
        }

        this.servicesTerritoires.ajouterTerritoire(territoire);
    };

    private traiterCellulesGeographiesTerritoires: TraiterCellules = (mapCellules) => {
        const territoire = this.servicesTerritoires.rechercher(this.idsTerritoires.lireCelluleIdTerritoire(mapCellules));
        if (territoire !== undefined) {
            territoire.boundingBox.latitudeMin = lireCelluleFloat(mapCellules, 'latitude_min');
            territoire.boundingBox.latitudeMax = lireCelluleFloat(mapCellules, 'latitude_max');
            territoire.boundingBox.longitudeMin = lireCelluleFloat(mapCellules, 'longitude_min');
            territoire.boundingBox.longitudeMax = lireCelluleFloat(mapCellules, 'longitude_max');
        }
    };

    async injecterDonneesReferentielTerritoires(dossierDonnees = '../crater-data/output-data'): Promise<void> {
        logInfo("CRATER-API : Début de l'initialisation=" + dossierDonnees);
        await new InjecteurCsv().injecter(
            dossierDonnees + '/referentiel_territoires/referentiel_territoires.csv',
            this.idsTerritoires.enregistrerCorrespondanceIdsTerritoires
        );
        await new InjecteurCsv().injecter(
            dossierDonnees + '/referentiel_territoires/referentiel_territoires.csv',
            this.traiterCellulesReferentielTerritoires
        );
        await new InjecteurCsv().injecter(
            dossierDonnees + '/contours/donnees_contours/donnees_contours.csv',
            this.traiterCellulesGeographiesTerritoires
        );
    }
}
