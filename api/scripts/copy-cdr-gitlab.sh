#!/bin/sh

echo "BUILD API : copie crater-data-resultat, environnement gitlab"
echo "copy-cdr-gitlab.sh $1 $2 $3"
if [ $# -ne 3 ]; then
  echo 1>&2 "$0: Erreur, merci de renseigner les 3 paramètres obligatoires, dans l'ordre : ENV_CIBLE_BUILD, CRATER_DATA_RESULTATS_GIT_URL et CI_JOB_TOKEN"
  exit 2
fi

ENV_CIBLE_BUILD=$1
CRATER_DATA_RESULTATS_GIT_URL=$2
CI_JOB_TOKEN=$3

if [ $ENV_CIBLE_BUILD == "prod" ]; then
  CRATER_BRANCHE_CDR="master"
else
  CRATER_BRANCHE_CDR="develop"
fi

echo "  - dossier courant = $(pwd)"
echo "  - environnement cible (dev ou prod) =${ENV_CIBLE_BUILD}"
echo "  - url dépot crater-data-resultats=${CRATER_DATA_RESULTATS_GIT_URL}"
echo "  - branche crater-data-resultats utilisée pour le clone=${CRATER_BRANCHE_CDR}"
echo "  - dossier cible dans repo ui=$(pwd)/build/crater-data-resultats"

rm -rf ./build/crater-data-resultats
mkdir -p ./build
git clone --single-branch -b ${CRATER_BRANCHE_CDR} --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@${CRATER_DATA_RESULTATS_GIT_URL} ./build/crater-data-resultats
cd ./build/crater-data-resultats
ls -A1 | grep -v data | xargs rm -rf
rm -rf data/parcel/ data/surface_agricole_utile/


