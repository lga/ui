import express from 'express';
import request from 'supertest';
import { beforeAll, describe, it } from 'vitest';

import { SAUT_LIGNE } from '../../src/communs/apiUtils';
import CraterApi from '../../src/CraterApi';
import { supprimerNPremieresLignes, verifierEgaliteAvecFichierTexte } from '../test-outils';

const CHEMIN_DONNEES_TESTS_UNITAIRES = `${__dirname}/data`;

const IDENTITE_PAYS = {
    id: 'france',
    idCode: 'P-FR',
    idNom: 'france',
    nom: 'France',
    categorie: 'PAYS',
    idTerritoireParcel: '1'
};
const RESULTAT_PAYS = {
    ...IDENTITE_PAYS,
    boundingBox: {
        latitudeMax: 51.09,
        latitudeMin: 41.33,
        longitudeMax: 9.56,
        longitudeMin: -5.14
    },
    genreNombre: 'FEMININ_SINGULIER',
    preposition: 'en'
};
const RESULTAT_PAYS_RECHERCHE = {
    ...IDENTITE_PAYS,
    libellePrincipal: 'France',
    libelleSecondaire: 'Pays'
};

const RESULTAT_ILE_DE_FRANCE_RECHERCHE = {
    id: 'ile-de-france',
    nom: 'Île-de-France',
    idCode: 'R-11',
    idNom: 'ile-de-france',
    categorie: 'REGION',
    idTerritoireParcel: '11',
    libellePrincipal: 'Île-de-France',
    libelleSecondaire: 'Région'
};

const RESULTAT_NOUVELLE_AQUITAINE = {
    id: 'nouvelle-aquitaine',
    idCode: 'R-75',
    idNom: 'nouvelle-aquitaine',
    nom: 'Nouvelle-Aquitaine',
    categorie: 'REGION',
    idTerritoireParcel: '75',
    boundingBox: {
        latitudeMax: 10,
        latitudeMin: -10,
        longitudeMax: 10,
        longitudeMin: -10
    },
    genreNombre: 'FEMININ_SINGULIER',
    preposition: 'en'
};

const RESULTAT_LANDES = {
    id: 'landes',
    idCode: 'D-40',
    idNom: 'landes',
    nom: 'Landes',
    categorie: 'DEPARTEMENT',
    idTerritoireParcel: '40',
    boundingBox: {
        latitudeMax: 10,
        latitudeMin: -10,
        longitudeMax: 10,
        longitudeMin: -10
    },
    genreNombre: 'FEMININ_PLURIEL',
    preposition: 'dans'
};

const IDENTITE_EPCI_CC_ADOUR = {
    id: 'communaute-de-communes-d-aire-sur-l-adour',
    idCode: 'E-200030435',
    idNom: 'communaute-de-communes-d-aire-sur-l-adour',
    idTerritoireParcel: '200',
    nom: "CC d'Aire-sur-l'Adour",
    categorie: 'EPCI',
    sousCategorie: 'COMMUNAUTE_COMMUNES'
};
const RESULTAT_EPCI_CC_ADOUR = {
    ...IDENTITE_EPCI_CC_ADOUR,
    boundingBox: {
        latitudeMax: 10,
        latitudeMin: -10,
        longitudeMax: 10,
        longitudeMin: -10
    },
    genreNombre: 'FEMININ_SINGULIER',
    preposition: 'dans'
};

const RESULTAT_PNR = {
    id: 'pnr-fictif-pour-les-tests',
    idCode: 'PNR-1',
    idNom: 'pnr-fictif-pour-les-tests',
    idTerritoireParcel: '100',
    nom: 'PNR Fictif pour les tests',
    categorie: 'REGROUPEMENT_COMMUNES',
    sousCategorie: 'PARC_NATUREL_REGIONAL',
    boundingBox: {
        latitudeMax: 10,
        latitudeMin: -10,
        longitudeMax: 10,
        longitudeMin: -10
    },
    genreNombre: 'MASCULIN_SINGULIER',
    preposition: 'dans'
};

const IDENTITE_COMMUNE_LATRILLE = {
    id: 'latrille',
    idCode: 'C-40146',
    idNom: 'latrille',
    idTerritoireParcel: '301',
    nom: 'Latrille',
    categorie: 'COMMUNE'
};
const RESULTAT_COMMUNE_LATRILLE = {
    ...IDENTITE_COMMUNE_LATRILLE,
    boundingBox: {
        latitudeMax: 10,
        latitudeMin: -10,
        longitudeMax: 10,
        longitudeMin: -10
    },
    genreNombre: 'FEMININ_SINGULIER',
    preposition: 'à'
};
const RESULTAT_COMMUNE_LATRILLE_RECHERCHE = {
    ...IDENTITE_COMMUNE_LATRILLE,
    libellePrincipal: 'Latrille (40800)',
    libelleSecondaire: 'Commune'
};

const IDENTITE_COMMUNE_AIRE_SUR_ADOUR = {
    id: 'aire-sur-l-adour',
    idTerritoireParcel: '302',
    idCode: 'C-40001',
    idNom: 'aire-sur-l-adour',
    nom: "Aire-sur-l'Adour",
    categorie: 'COMMUNE'
};
const RESULTAT_COMMUNE_AIRE_SUR_ADOUR_RECHERCHE = {
    ...IDENTITE_COMMUNE_AIRE_SUR_ADOUR,
    libellePrincipal: "Aire-sur-l'Adour (40800)",
    libelleSecondaire: 'Commune'
};

describe('Tests unitaire API CRATer sur les données de test', () => {
    let craterApi: CraterApi;
    let expressApp: express.Application;

    beforeAll(async function () {
        expressApp = express();
        craterApi = new CraterApi(expressApp, `${CHEMIN_DONNEES_TESTS_UNITAIRES}/input`);
        await craterApi.init();
    });

    it('Test API GET /territoires recherche par nom', async () => {
        await request(expressApp)
            .get('/crater/api/territoires?critere=fra')
            .expect('Content-Type', /json/)
            .expect(200, [RESULTAT_PAYS_RECHERCHE, RESULTAT_ILE_DE_FRANCE_RECHERCHE]);
    });

    it('Test API GET /territoires recherche par nom avec nbMaxResultats', async () => {
        await request(expressApp)
            .get('/crater/api/territoires?critere=fra&nbMaxResultats=1')
            .expect('Content-Type', /json/)
            .expect(200, [RESULTAT_PAYS_RECHERCHE]);
    });

    it('Test API GET /territoires recherche par code postal', async () => {
        await request(expressApp)
            .get('/crater/api/territoires?critere=408')
            .expect('Content-Type', /json/)
            .expect(200, [
                {
                    ...IDENTITE_EPCI_CC_ADOUR,
                    estSuggere: true,
                    libellePrincipal: "CC d'Aire-sur-l'Adour",
                    libelleSecondaire: 'Intercommunalité de Latrille'
                },
                RESULTAT_COMMUNE_LATRILLE_RECHERCHE,
                RESULTAT_COMMUNE_AIRE_SUR_ADOUR_RECHERCHE
            ]);
    });

    it('Test API GET /territoires retourne un territoire suggéré avant la commune', async () => {
        await request(expressApp)
            .get('/crater/api/territoires?critere=adour')
            .expect('Content-Type', /json/)
            .expect(200, [
                {
                    ...IDENTITE_EPCI_CC_ADOUR,
                    estSuggere: true,
                    libellePrincipal: "CC d'Aire-sur-l'Adour",
                    libelleSecondaire: "Intercommunalité de Aire-sur-l'Adour"
                },
                RESULTAT_COMMUNE_AIRE_SUR_ADOUR_RECHERCHE
            ]);
    });

    it('Test API GET /territoires aucun territoire trouvé', async () => {
        await request(expressApp).get('/crater/api/territoires?critere=12').expect('Content-Type', /json/).expect(200, []);
    });

    it('Test API GET /territoires/france', async () => {
        await request(expressApp).get('/crater/api/territoires/france').expect('Content-Type', /json/).expect(200, RESULTAT_PAYS);
    });

    it('Test API GET /territoires/nouvelle-aquitaine', async () => {
        await request(expressApp)
            .get('/crater/api/territoires/nouvelle-aquitaine')
            .expect('Content-Type', /json/)
            .expect(200, {
                ...RESULTAT_NOUVELLE_AQUITAINE,
                pays: RESULTAT_PAYS
            });
    });

    it('Test API GET /territoires/landes', async () => {
        await request(expressApp)
            .get('/crater/api/territoires/landes')
            .expect('Content-Type', /json/)
            .expect(200, {
                ...RESULTAT_LANDES,
                pays: RESULTAT_PAYS,
                region: RESULTAT_NOUVELLE_AQUITAINE
            });
    });

    it('Test API GET /territoires/communaute-de-communes-d-aire-sur-l-adour', async () => {
        await request(expressApp)
            .get('/crater/api/territoires/communaute-de-communes-d-aire-sur-l-adour')
            .expect('Content-Type', /json/)
            .expect(200, {
                ...RESULTAT_EPCI_CC_ADOUR,
                pays: RESULTAT_PAYS,
                region: RESULTAT_NOUVELLE_AQUITAINE,
                departement: RESULTAT_LANDES
            });
    });

    it('Test API GET /territoires/pnr-fictif-pour-les-tests', async () => {
        await request(expressApp)
            .get('/crater/api/territoires/pnr-fictif-pour-les-tests')
            .expect('Content-Type', /json/)
            .expect(200, {
                ...RESULTAT_PNR,
                pays: RESULTAT_PAYS,
                region: RESULTAT_NOUVELLE_AQUITAINE,
                departement: RESULTAT_LANDES
            });
    });

    it('Test API GET /territoires/latrille', async () => {
        await request(expressApp)
            .get('/crater/api/territoires/latrille')
            .expect('Content-Type', /json/)
            .expect(200, {
                ...RESULTAT_COMMUNE_LATRILLE,
                pays: RESULTAT_PAYS,
                region: RESULTAT_NOUVELLE_AQUITAINE,
                departement: RESULTAT_LANDES,
                epci: RESULTAT_EPCI_CC_ADOUR
            });
    });

    it('Test API GET /territoires/unknown', async () => {
        await request(expressApp).get('/crater/api/territoires/unknown').expect('Content-Type', /json/).expect(404, {
            message: 'Ressource introuvable',
            description: 'Territoire non trouvé (idTerritoire=unknown)'
        });
    });

    it('Test API GET /unknown', async () => {
        await request(expressApp).get('/crater/api/unknown').expect('Content-Type', /json/).expect(404, {
            message: 'Ressource introuvable',
            description: 'Chemin inconnu : /crater/api/unknown'
        });
    });

    it('Test API GET /diagnostics pour France', async () => {
        const response = await request(expressApp)
            .get('/crater/api/diagnostics/france')
            .expect('Content-Type', /json/)
            // L'objet diag est gros => plus pratique de passer par une comparaison de fichiers
            .expect(200);
        verifierEgaliteAvecFichierTexte(JSON.stringify(response.body, null, 2), CHEMIN_DONNEES_TESTS_UNITAIRES, 'diagnostic_france.json');
    });

    it('Test API GET /diagnostics/csv', async () => {
        const response = await request(expressApp)
            .get('/crater/api/diagnostics/csv/france')
            .expect('Content-Type', /text\/csv/)
            .expect(200);
        // Le csv est est gros => plus pratique de passer par une comparaison de fichiers
        // On exclut les 5 premières lignes qui contiennent des infos contextuelles (difficiles à tester)
        const contenuFichierAValider = supprimerNPremieresLignes(response.text, 5, SAUT_LIGNE);
        verifierEgaliteAvecFichierTexte(contenuFichierAValider, CHEMIN_DONNEES_TESTS_UNITAIRES, 'diagnostic_france.csv');
    });

    it('Test API GET /diagnostics inconnu', async () => {
        await request(expressApp).get('/crater/api/diagnostics/unknown').expect('Content-Type', /json/).expect(404, {
            message: 'Ressource introuvable',
            description: 'Diagnostic non trouvé (idTerritoire=unknown)'
        });
    });

    it('Test API GET /error', async () => {
        await request(expressApp).get('/').expect('Content-Type', /json/).expect(404);
    });

    it('Test API GET /indicateurs pour un indicateur simple', async () => {
        await request(expressApp)
            .get('/crater/api/indicateurs/population?categorieTerritoire=PAYS')
            .expect('Content-Type', /json/)
            .expect(200, [{ idTerritoire: 'france', valeur: 64624945 }]);
    });

    it('Test API GET /indicateurs pour un indicateur objet composé avec filtrage', async () => {
        await request(expressApp)
            .get('/crater/api/indicateurs/pesticides.indicateursParAnnees.noduHa?annee=2019&categorieTerritoire=PAYS')
            .expect('Content-Type', /json/)
            .expect(200, [
                {
                    idTerritoire: 'france',
                    valeur: 19.4
                }
            ]);
    });

    it('Test API GET /indicateurs pour un indicateur avec valeur null', async () => {
        await request(expressApp)
            .get('/crater/api/indicateurs/population?categorieTerritoire=REGION')
            .expect('Content-Type', /json/)
            .expect(200, [
                { idTerritoire: 'ile-de-france', valeur: 200 },
                { idTerritoire: 'nouvelle-aquitaine', valeur: null }
            ]);
    });

    it('Test API GET /indicateurs pour la categorie COMMUNE sans idsDepartements retourne une erreur', async () => {
        await request(expressApp).get('/crater/api/indicateurs/population?categorieTerritoire=COMMUNE').expect('Content-Type', /json/).expect(400);
    });

    it('Test API GET /indicateurs pour la categorie COMMUNE et un departement', async () => {
        await request(expressApp)
            .get('/crater/api/indicateurs/occupationSols.superficieTotaleHa?categorieTerritoire=COMMUNE&idsDepartements=landes')
            .expect('Content-Type', /json/)
            .expect(200, [
                { idTerritoire: 'latrille', valeur: 9 },
                { idTerritoire: 'aire-sur-l-adour', valeur: 10 }
            ]);
    });
});
