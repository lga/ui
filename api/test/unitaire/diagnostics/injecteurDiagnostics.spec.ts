import { beforeEach, describe, expect, it } from 'vitest';

import { InjecteursDiagnostics } from '../../../src/diagnostics/InjecteursDiagnostics';
import { ServicesDiagnostics } from '../../../src/diagnostics/ServicesDiagnostics';

const CHEMIN_DONNEES_TESTS_UNITAIRES_INPUT = `${__dirname}/../data/input`;

describe('Test unitaire injecteurs diagnostic', () => {
    let servicesDiagnostics: ServicesDiagnostics;
    let injecteursDiagnostics: InjecteursDiagnostics;

    beforeEach(() => {
        servicesDiagnostics = new ServicesDiagnostics();
        injecteursDiagnostics = new InjecteursDiagnostics(servicesDiagnostics);
    });

    it('Charger tout les diagnostics et vérifier le taux de consommation', async () => {
        await injecteursDiagnostics.injecterDonneesDiagnostics(CHEMIN_DONNEES_TESTS_UNITAIRES_INPUT);
        // then
        expect(servicesDiagnostics.rechercherTous().length).toEqual(8);
        const diagnostic = servicesDiagnostics.rechercherOuCreerDiagnostic('ile-de-france', 'Île-de-France', 'REGION');
        expect(diagnostic.consommation).toEqual({
            tauxPauvrete60Pourcent: 15.5,
            codeNiveauRisquePrecariteAlimentaire: '5_TRES_ELEVE'
        });
    });

    it('Charger tout les diagnostics et vérifier le nb communes otex ', async () => {
        await injecteursDiagnostics.injecterDonneesDiagnostics(CHEMIN_DONNEES_TESTS_UNITAIRES_INPUT);
        // then
        expect(servicesDiagnostics.rechercherTous().length).toEqual(8);
        const diagnostic = servicesDiagnostics.rechercherOuCreerDiagnostic('ile-de-france', 'Île-de-France', 'REGION');
        expect(diagnostic.nbCommunesParOtex).toEqual({
            grandesCultures: 761,
            maraichageHorticulture: 58,
            viticulture: 3,
            fruits: 10,
            bovinLait: 3,
            bovinViande: 1,
            bovinMixte: 0,
            ovinsCaprinsAutresHerbivores: 12,
            porcinsVolailles: 3,
            polyculturePolyelevage: 157,
            nonClassees: 1,
            sansExploitations: 259,
            nonRenseigne: 0
        });
    });
});
