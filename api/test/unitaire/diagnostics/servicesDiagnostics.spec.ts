import { creerDiagnosticApiVide } from '@lga/specification-api/build/outils/modelesDiagnosticsUtils';
import type { BoundingBoxApi, CodeCategorieTerritoireApi } from '@lga/specification-api/build/specification/api';
import { beforeEach, describe, expect, it } from 'vitest';

import { ServicesDiagnostics } from '../../../src/diagnostics/ServicesDiagnostics';

describe('Test unitaire recherche de diagnostics par critere', () => {
    let servicesDiagnostics: ServicesDiagnostics;

    beforeEach(() => {
        servicesDiagnostics = new ServicesDiagnostics();
    });

    it('Rechercher des diagnostics de communes par département', () => {
        creerDiagnostic('C-31000', 'Commune de Haute-Garonne', 'COMMUNE', undefined, '31');
        creerDiagnostic('C-32000', 'Commune du Gers', 'COMMUNE', undefined, '32');
        creerDiagnostic('C-33000', 'Commune 1 de Gironde', 'COMMUNE', undefined, '33');
        creerDiagnostic('C-33010', 'Commune 2 de Girdone', 'COMMUNE', undefined, '33');
        creerDiagnostic('E-33', 'EPCI de Gironde', 'EPCI', undefined, '33');
        const diagnostics = servicesDiagnostics.rechercherCommunesParDepartements(['32', '33']);
        expect(diagnostics.map((d) => d.idTerritoire)).toEqual(['C-32000', 'C-33000', 'C-33010']);
    });

    it('Rechercher des diagnostics par boundingbox', () => {
        const bboxRecherche: BoundingBoxApi = { latitudeMax: 10, latitudeMin: -10, longitudeMax: 10, longitudeMin: -10 };

        creerDiagnostic('Centre', 'CommuneCentre', 'COMMUNE', {
            longitudeMin: -5,
            longitudeMax: 5,
            latitudeMin: -5,
            latitudeMax: 5
        });
        creerDiagnostic('Nord', 'CommuneNord', 'COMMUNE', {
            longitudeMin: -10,
            longitudeMax: 10,
            latitudeMin: 11,
            latitudeMax: 20
        });
        creerDiagnostic('Est', 'CommuneEst', 'COMMUNE', {
            longitudeMin: 11,
            longitudeMax: 20,
            latitudeMin: -10,
            latitudeMax: 10
        });
        creerDiagnostic('Sud', 'CommuneSud', 'COMMUNE', {
            longitudeMin: -10,
            longitudeMax: -10,
            latitudeMin: -20,
            latitudeMax: -11
        });
        creerDiagnostic('Ouest', 'CommuneOuest', 'COMMUNE', {
            longitudeMin: -20,
            longitudeMax: -11,
            latitudeMin: -10,
            latitudeMax: 10
        });
        creerDiagnostic('France', 'France', 'PAYS', {
            longitudeMin: -50,
            longitudeMax: 50,
            latitudeMin: -50,
            latitudeMax: 50
        });

        const resultat = servicesDiagnostics.rechercherTousParCategorieEtBoundingBox('COMMUNE', bboxRecherche);
        expect(resultat.map((d) => d.nomTerritoire)).toEqual(['CommuneCentre']);
    });

    const creerDiagnostic = (
        idTerritoire: string,
        nomTerritoire: string,
        categorieTerritoire: CodeCategorieTerritoireApi,
        boundingBox?: BoundingBoxApi,
        idDepartement?: string
    ) => {
        const diagnostic = creerDiagnosticApiVide(idTerritoire, nomTerritoire, categorieTerritoire);
        if (boundingBox) diagnostic.boundingBoxTerritoire = boundingBox;
        if (idDepartement) diagnostic.idDepartement = idDepartement;
        servicesDiagnostics.ajouterDiagnostic(diagnostic);
    };
});
