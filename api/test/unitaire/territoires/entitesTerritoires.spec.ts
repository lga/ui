import { describe, expect, it } from 'vitest';

import { Epci, RegroupementCommunes } from '../../../src/territoires/domaines/entitesTerritoires';

describe('Test unitaire des entités territoires', () => {
    it("Calculer le libellé d'un regroupement de communes", () => {
        const pnr = new RegroupementCommunes(
            {
                id: 'pnr10',
                idCode: 'PNR-1',
                idNom: 'pnr10',
                categorie: 'REGROUPEMENT_COMMUNES',
                nom: 'PNR Fictif',
                idTerritoireParcel: null,
                boundingBox: { latitudeMax: 10, latitudeMin: -10, longitudeMax: 10, longitudeMin: -10 },
                genreNombre: 'MASCULIN_SINGULIER',
                preposition: 'dans'
            },
            'PARC_NATUREL_REGIONAL',
            'departement1',
            'region1',
            'france'
        );
        expect(pnr.libelle).toEqual('PNR Fictif');
        expect(pnr.idTerritoireParcel).toEqual(null);

        const bassin_de_vie = new RegroupementCommunes(
            {
                id: 'bassin-de-vie-1',
                idCode: 'BV-1',
                idNom: 'bassin-de-vie-1',
                nom: 'Bassin de vie Fictif',
                categorie: 'REGROUPEMENT_COMMUNES',
                idTerritoireParcel: '11',
                boundingBox: { latitudeMax: 10, latitudeMin: -10, longitudeMax: 10, longitudeMin: -10 },
                genreNombre: 'MASCULIN_SINGULIER',
                preposition: 'dans'
            },
            'BASSIN_DE_VIE_2022',
            'departement1',
            'region1',
            'france'
        );
        expect(bassin_de_vie.libelle).toEqual('Bassin de vie Fictif');
        expect(bassin_de_vie.idTerritoireParcel).toEqual('11');
    });

    it("Calculer le libellé d'un epci", () => {
        const epci = new Epci(
            {
                id: 'epci1',
                idCode: 'E-1',
                idNom: 'epci1',
                categorie: 'EPCI',
                nom: 'EPCI Fictif',
                idTerritoireParcel: null,
                boundingBox: { latitudeMax: 10, latitudeMin: -10, longitudeMax: 10, longitudeMin: -10 },
                genreNombre: 'MASCULIN_SINGULIER',
                preposition: 'dans'
            },
            'METROPOLE',
            'departement1',
            'region1',
            'france'
        );
        expect(epci.libelle).toEqual('EPCI Fictif');
    });
});
