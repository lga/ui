import { describe, expect, it } from 'vitest';

import { ServicesTerritoires } from '../../../src/territoires/domaines/ServicesTerritoires';
import { creerCommune, creerEPCI, creerPays, creerRegroupementCommunes } from './test-territoires-outils';

describe('Test unitaire recherche de territoires par critere', () => {
    let servicesTerritoires: ServicesTerritoires;

    function initialiserServicesTerritoiresAvecCommunes(nomsCommunes: string[]) {
        const servicesTerritoires = new ServicesTerritoires();
        nomsCommunes.forEach((nom) => {
            servicesTerritoires.ajouterTerritoire(creerCommune({ nom: nom }));
        });
        return servicesTerritoires;
    }

    it('Verifier les règles de normalisation des lettres et mots de liaisons', () => {
        // supprimer caractères speciaux + basculer en majuscule
        expect(ServicesTerritoires.normaliserString('aei')).toEqual('AEI');
        expect(ServicesTerritoires.normaliserString('àéI')).toEqual('AEI');
        expect(ServicesTerritoires.normaliserString('à â ä é è ê ë ï î ô ö ù û ü ÿ ç')).toEqual('A A A E E E E I I O O U U U Y C');
        expect(ServicesTerritoires.normaliserString('œ æ')).toEqual('OE AE');
        // supprimer - et '
        expect(ServicesTerritoires.normaliserString('à-à-à')).toEqual('A A A');
        expect(ServicesTerritoires.normaliserString("a'a'a")).toEqual('A A A');
        expect(ServicesTerritoires.normaliserString('  a    a   ')).toEqual('A A');
        // supprimer prepositions
        expect(ServicesTerritoires.normaliserString('Territoire du Perche')).toEqual('TERRITOIRE PERCHE');
        expect(ServicesTerritoires.normaliserString('Territoire de Perche')).toEqual('TERRITOIRE PERCHE');
        expect(ServicesTerritoires.normaliserString('Territoire des Perche')).toEqual('TERRITOIRE PERCHE');
        expect(ServicesTerritoires.normaliserString('Territoire de la Perche')).toEqual('TERRITOIRE PERCHE');
        expect(ServicesTerritoires.normaliserString('Territoire le les Perche')).toEqual('TERRITOIRE PERCHE');
        expect(ServicesTerritoires.normaliserString("Territoire d'Perche")).toEqual('TERRITOIRE PERCHE');
        expect(ServicesTerritoires.normaliserString("Territoire l'Perche")).toEqual('TERRITOIRE PERCHE');
        // ne pas supprimer preposition si à la fin du critère car cela peut etre le début d'un nom
        expect(ServicesTerritoires.normaliserString('Territoire l')).toEqual('TERRITOIRE L');
        expect(ServicesTerritoires.normaliserString("Territoire l'")).toEqual('TERRITOIRE');
        expect(ServicesTerritoires.normaliserString('Territoire la')).toEqual('TERRITOIRE LA');
        expect(ServicesTerritoires.normaliserString('Territoire d')).toEqual('TERRITOIRE D');
        expect(ServicesTerritoires.normaliserString('Territoire du')).toEqual('TERRITOIRE DU');
    });

    it("Construire l'objet critere à partir du critere string", () => {
        let critere = ServicesTerritoires.creerCritere('machin de truc');
        expect(critere.critereNormaliseSansMotsReserves).toEqual('MACHIN TRUC');
        expect(critere.filtre).toEqual(undefined);
        expect(critere.tri).toEqual(undefined);

        critere = ServicesTerritoires.creerCritere('pat machin de truc');
        expect(critere.critereNormaliseSansMotsReserves).toEqual('MACHIN TRUC');
        expect(critere.filtre).toEqual('PROJET_ALIMENTAIRE_TERRITORIAL');
        expect(critere.tri).toEqual(undefined);

        critere = ServicesTerritoires.creerCritere('parc naturel regional de machin de truc');
        expect(critere.critereNormaliseSansMotsReserves).toEqual('MACHIN TRUC');
        expect(critere.filtre).toEqual('PARC_NATUREL_REGIONAL');
        expect(critere.tri).toEqual(undefined);

        critere = ServicesTerritoires.creerCritere('CC de machin de truc');
        expect(critere.critereNormaliseSansMotsReserves).toEqual('MACHIN TRUC');
        expect(critere.filtre).toEqual('EPCI');
        expect(critere.tri).toEqual('COMMUNAUTE_COMMUNES');
    });

    it('Trouver des territoires en normalisant (passer tout en majuscules, supprimer mots de liaison et caractères spéciaux...)', () => {
        servicesTerritoires = initialiserServicesTerritoiresAvecCommunes(['Bordeaux', 'Bläye', "l'aa-bla-ccc"]);
        let suggestions = servicesTerritoires.rechercherTousParCritere('bla', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(["l'aa-bla-ccc", 'Bläye']);

        suggestions = servicesTerritoires.rechercherTousParCritere('BLà', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(["l'aa-bla-ccc", 'Bläye']);

        suggestions = servicesTerritoires.rechercherTousParCritere('la BLà', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(["l'aa-bla-ccc", 'Bläye']);

        suggestions = servicesTerritoires.rechercherTousParCritere('L AA BLA', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(["l'aa-bla-ccc"]);
    });

    it('Trouver des territoires avec un critere contenant un ou plusieurs mots', () => {
        servicesTerritoires = initialiserServicesTerritoiresAvecCommunes([
            'Bläye',
            'Lablachère',
            'saint-blaiseret',
            'saint-blaise-du-buis-du-bois',
            'saint-blaise'
        ]);

        // Si un seul mot dans le critère, on cherche tout ce qui "commence par" + tri des résultats ou les matching par mot complet sont en 1er
        let suggestions = servicesTerritoires.rechercherTousParCritere('bla', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Bläye', 'saint-blaise', 'saint-blaiseret', 'saint-blaise-du-buis-du-bois']);
        // le matching par mot exact remonte en 1er
        suggestions = servicesTerritoires.rechercherTousParCritere('blaise', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['saint-blaise', 'saint-blaise-du-buis-du-bois', 'saint-blaiseret']);

        // un critere avec plusieurs mots fait remonter des noms de territoires contenant les mots
        // les mots ne sont pas forcément consécutifs
        suggestions = servicesTerritoires.rechercherTousParCritere('saint buis', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['saint-blaise-du-buis-du-bois']);
        // les mots ne sont pas forcément au début
        suggestions = servicesTerritoires.rechercherTousParCritere('blaise bois', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['saint-blaise-du-buis-du-bois']);
        // mais ils doivent être dans le bon ordre, sinon pas de matching
        suggestions = servicesTerritoires.rechercherTousParCritere('buis saint', 5);
        expect(suggestions.map((t) => t.nom)).toEqual([]);

        // le matching par "debut de mot" fonctionne uniquement pour le dernier mot du critère
        suggestions = servicesTerritoires.rechercherTousParCritere('saint bui', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['saint-blaise-du-buis-du-bois']);
        suggestions = servicesTerritoires.rechercherTousParCritere('sain bui', 5);
        expect(suggestions.map((t) => t.nom)).toEqual([]);
    });

    it('Trouver des territoires par le code postal', () => {
        servicesTerritoires = new ServicesTerritoires();
        servicesTerritoires.ajouterTerritoire(creerCommune({ nom: 'Commune1', codesPostaux: ['01000', '01001'] }));
        servicesTerritoires.ajouterTerritoire(creerCommune({ nom: 'CommuneC', codesPostaux: ['02000'] }));
        servicesTerritoires.ajouterTerritoire(creerCommune({ nom: 'CommuneA', codesPostaux: ['02000', '02001'] }));
        servicesTerritoires.ajouterTerritoire(creerPays());
        let suggestions = servicesTerritoires.rechercherTousParCritere('02001', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['CommuneA']);

        suggestions = servicesTerritoires.rechercherTousParCritere('020', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['CommuneA', 'CommuneC']);
    });

    it('Trier les résultats par groupe: correspondance mot complet en premier, puis mot partiel, et dans chaque groupe tri par taille de nom et ordre alphabétique quand meme taille', () => {
        servicesTerritoires = initialiserServicesTerritoiresAvecCommunes(['eloic', 'eloib', 'eloiaa', "CU d'Eloi", "CC d'Eloi", 'eloi']);
        const suggestions = servicesTerritoires.rechercherTousParCritere('éloi', 10);
        expect(suggestions.map((t) => t.nom)).toEqual(['eloi', "CC d'Eloi", "CU d'Eloi", 'eloib', 'eloic', 'eloiaa']);
    });

    it('Pour un critère de moins de 3 lettres, ne retourner que les territoires avec correspondance exacte du nom', () => {
        servicesTerritoires = initialiserServicesTerritoiresAvecCommunes(['y', 'ar', 'a', 'arras']);
        let suggestions = servicesTerritoires.rechercherTousParCritere('a', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['a']);

        suggestions = servicesTerritoires.rechercherTousParCritere('ar', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['ar']);
    });

    it('Faire une recherche en utilisant des mots réservés dans le critère', () => {
        servicesTerritoires = new ServicesTerritoires();
        servicesTerritoires.ajouterTerritoire(
            creerEPCI({
                nom: 'Communauté de communes de Machin de Truc de Bidule',
                sousCategorie: 'COMMUNAUTE_COMMUNES'
            })
        );
        servicesTerritoires.ajouterTerritoire(
            creerEPCI({
                nom: "Communauté d'agglomération de Machin de Truc de Bidule",
                sousCategorie: 'COMMUNAUTE_AGGLOMERATION'
            })
        );
        servicesTerritoires.ajouterTerritoire(
            creerRegroupementCommunes({
                nom: 'Machin de Truc de Bidule_PAT',
                sousCategorie: 'PROJET_ALIMENTAIRE_TERRITORIAL'
            })
        );
        servicesTerritoires.ajouterTerritoire(
            creerRegroupementCommunes({
                nom: 'Machin de Truc de Bidule_PN',
                sousCategorie: 'PARC_NATIONAL'
            })
        );
        servicesTerritoires.ajouterTerritoire(
            creerRegroupementCommunes({
                nom: 'Machin de Truc de Bidule_PNR',
                sousCategorie: 'PARC_NATUREL_REGIONAL'
            })
        );
        servicesTerritoires.ajouterTerritoire(
            creerRegroupementCommunes({
                nom: 'Pays de Machin',
                sousCategorie: 'PAYS_PETR'
            })
        );
        servicesTerritoires.ajouterTerritoire(
            creerRegroupementCommunes({
                nom: 'SCoT de la Region de Machin',
                sousCategorie: 'SCHEMA_COHERENCE_TERRITORIAL'
            })
        );

        // Règles pour les EPCIs - priorisation selon mot reservé
        let suggestions = servicesTerritoires.rechercherTousParCritere('cc truc', 5);
        expect(suggestions.map((t) => t.nom)).toEqual([
            'Communauté de communes de Machin de Truc de Bidule',
            "Communauté d'agglomération de Machin de Truc de Bidule"
        ]);
        // Marche aussi avec le début du mot
        suggestions = servicesTerritoires.rechercherTousParCritere('cc tru', 5);
        expect(suggestions.map((t) => t.nom)).toEqual([
            'Communauté de communes de Machin de Truc de Bidule',
            "Communauté d'agglomération de Machin de Truc de Bidule"
        ]);

        suggestions = servicesTerritoires.rechercherTousParCritere('ca truc', 5);
        expect(suggestions.map((t) => t.nom)).toEqual([
            "Communauté d'agglomération de Machin de Truc de Bidule",
            'Communauté de communes de Machin de Truc de Bidule'
        ]);

        suggestions = servicesTerritoires.rechercherTousParCritere('communaute de communes truc', 5);
        expect(suggestions.map((t) => t.nom)).toEqual([
            'Communauté de communes de Machin de Truc de Bidule',
            "Communauté d'agglomération de Machin de Truc de Bidule"
        ]);

        // Règles pour les PATs
        suggestions = servicesTerritoires.rechercherTousParCritere('pat machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Machin de Truc de Bidule_PAT']);
        suggestions = servicesTerritoires.rechercherTousParCritere('pait machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Machin de Truc de Bidule_PAT']);
        suggestions = servicesTerritoires.rechercherTousParCritere('paat machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Machin de Truc de Bidule_PAT']);
        suggestions = servicesTerritoires.rechercherTousParCritere('projet alimentaire territorial machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Machin de Truc de Bidule_PAT']);
        // Règles pour les PN&PNRs
        suggestions = servicesTerritoires.rechercherTousParCritere('pn machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Machin de Truc de Bidule_PN']);
        suggestions = servicesTerritoires.rechercherTousParCritere('parc national machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Machin de Truc de Bidule_PN']);
        suggestions = servicesTerritoires.rechercherTousParCritere('pnr machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Machin de Truc de Bidule_PNR']);
        suggestions = servicesTerritoires.rechercherTousParCritere('parc naturel regional machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Machin de Truc de Bidule_PNR']);
        // Règles pour les PETR&PAYS
        suggestions = servicesTerritoires.rechercherTousParCritere('pays machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Pays de Machin']);
        suggestions = servicesTerritoires.rechercherTousParCritere('petr machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Pays de Machin']);
        suggestions = servicesTerritoires.rechercherTousParCritere('petr pays machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Pays de Machin']);
        suggestions = servicesTerritoires.rechercherTousParCritere('pays petr machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Pays de Machin']);
        // Règles pour les SCOT - avec le mot "Region" dans le nom
        // Si un regroupement de commune s'appelle "scot de la region machin" on ne le trouvera pas si on met region en premiere place dans le critere car le mot region est réservé
        suggestions = servicesTerritoires.rechercherTousParCritere('region m', 5);
        expect(suggestions.map((t) => t.nom)).toEqual([]);
        // mais on le trouve bien avec scot region machin
        suggestions = servicesTerritoires.rechercherTousParCritere('scot region machin', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['SCoT de la Region de Machin']);
    });
});
