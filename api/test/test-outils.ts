import * as fs from 'fs';
import * as path from 'path';
import { expect } from 'vitest';

// TODO : duplication avec fonction dans apps/crater. A factoriser dans un module de /modules
export function verifierEgaliteAvecFichierTexte(contenuFichierAValider: string, cheminDossierDonneesTest: string, nomFichierResultatAttendu: string) {
    const cheminFichierAttendu = path.join(cheminDossierDonneesTest, 'expected', nomFichierResultatAttendu);
    let contenuFichierAttendu = '';
    if (fs.existsSync(cheminFichierAttendu)) {
        contenuFichierAttendu = fs.readFileSync(cheminFichierAttendu).toString();
    } else {
        throw new Error(`Erreur : le fichier attendu n'existe pas (pas de fichier ${cheminFichierAttendu}`);
    }
    const dossierOutput = path.join(cheminDossierDonneesTest, 'output');
    if (!fs.existsSync(dossierOutput)) {
        fs.mkdirSync(dossierOutput);
    }
    // écriture d'un fichier avec le contenu pour faciliter les diff avec le resultat attendu en cas d'erreur
    const cheminFichierResultat = path.join(dossierOutput, nomFichierResultatAttendu);
    fs.writeFileSync(cheminFichierResultat, contenuFichierAValider);

    if (contenuFichierAValider !== contenuFichierAttendu) {
        expect(
            contenuFichierAValider,
            `Erreur : les données ne sont pas identiques entre le fichier resultat (${cheminFichierResultat}) et le fichier attendu ${cheminFichierAttendu}`
        ).toEqual(contenuFichierAttendu);
    }
}

export function supprimerNPremieresLignes(contenu: string, nbLigne: number | undefined, separateurFinLigne: string) {
    if (nbLigne === undefined) {
        return contenu;
    } else {
        return contenu.split(separateurFinLigne).slice(nbLigne).join(separateurFinLigne);
    }
}
