import express from 'express';
import request from 'supertest';
import { beforeAll, describe, it } from 'vitest';

import CraterApi from '../../src/CraterApi';

const RESULTAT_PAYS = {
    id: 'france',
    idCode: 'P-FR',
    idNom: 'france',
    nom: 'France',
    categorie: 'PAYS',
    idTerritoireParcel: '1',
    boundingBox: {
        latitudeMax: 51.089,
        latitudeMin: 41.334,
        longitudeMax: 9.56,
        longitudeMin: -5.141
    },
    genreNombre: 'FEMININ_SINGULIER',
    preposition: 'en'
};

const RESULTAT_REGION = {
    id: 'auvergne-rhone-alpes',
    idCode: 'R-84',
    idNom: 'auvergne-rhone-alpes',
    nom: 'Auvergne-Rhône-Alpes',
    categorie: 'REGION',
    idTerritoireParcel: '12',
    boundingBox: {
        latitudeMax: 46.804,
        latitudeMin: 44.115,
        longitudeMax: 7.186,
        longitudeMin: 2.063
    },
    genreNombre: 'FEMININ_SINGULIER',
    preposition: 'en'
};

const RESULTAT_DEPARTEMENT = {
    id: 'isere',
    idCode: 'D-38',
    idNom: 'isere',
    nom: 'Isère',
    categorie: 'DEPARTEMENT',
    idTerritoireParcel: '115',
    boundingBox: {
        latitudeMax: 45.883,
        latitudeMin: 44.696,
        longitudeMax: 6.359,
        longitudeMin: 4.742
    },
    genreNombre: 'FEMININ_SINGULIER',
    preposition: 'en'
};

const RESULTAT_EPCI = {
    id: 'grenoble-alpes-metropole',
    idCode: 'E-200040715',
    idNom: 'grenoble-alpes-metropole',
    nom: 'Grenoble-Alpes-Métropole',
    categorie: 'EPCI',
    idTerritoireParcel: '45891',
    sousCategorie: 'METROPOLE',
    boundingBox: {
        latitudeMax: 45.322,
        latitudeMin: 44.956,
        longitudeMax: 5.881,
        longitudeMin: 5.57
    },
    genreNombre: 'FEMININ_SINGULIER',
    preposition: 'à'
};

const RESULTAT_PNR = {
    id: 'pnr-du-vercors',
    idCode: 'PNR-FR8000001',
    idNom: 'pnr-du-vercors',
    nom: 'PNR du Vercors',
    categorie: 'REGROUPEMENT_COMMUNES',
    idTerritoireParcel: null,
    sousCategorie: 'PARC_NATUREL_REGIONAL',
    boundingBox: {
        latitudeMax: 45.303,
        latitudeMin: 44.639,
        longitudeMax: 5.83,
        longitudeMin: 5.066
    },
    genreNombre: 'MASCULIN_SINGULIER',
    preposition: 'dans'
};

const RESULTAT_COMMUNE = {
    id: 'grenoble',
    idCode: 'C-38185',
    idNom: 'grenoble',
    nom: 'Grenoble',
    categorie: 'COMMUNE',
    idTerritoireParcel: '38555',
    boundingBox: {
        latitudeMax: 45.214,
        latitudeMin: 45.154,
        longitudeMax: 5.753,
        longitudeMin: 5.678
    },
    genreNombre: 'FEMININ_SINGULIER',
    preposition: 'à'
};

describe('Tests integration API CRATer pour valider le fonctionnement avec les données réelles', () => {
    let craterApi: CraterApi;
    let expressApp: express.Application;

    beforeAll(async function () {
        expressApp = express();
        craterApi = new CraterApi(expressApp, 'build/crater-data-resultats/data');
        await craterApi.init();
    });

    it('Test API GET /territoires?critere=toulouse', async () => {
        await request(expressApp).get('/crater/api/territoires?critere=toulouse').expect('Content-Type', /json/).expect(200);
    });

    it('Test API GET /territoires?critere=saint', async () => {
        await request(expressApp)
            .get('/crater/api/territoires?critere=saint')
            .expect('Content-Type', /json/)
            .expect((res: request.Response) => {
                if (res.body.length != 10) {
                    throw new Error('Erreur : la réponse ne contient pas 10 territoires');
                }
            })
            .expect(200);
    });

    it('Test API GET /territoires?paraminconnu=12', async () => {
        await request(expressApp).get('/crater/api/territoires?paraminconnu=12').expect('Content-Type', /json/).expect(200, []);
    });

    it('Test API GET /territoires/P-FR', async () => {
        await request(expressApp).get('/crater/api/territoires/france').expect('Content-Type', /json/).expect(200, RESULTAT_PAYS);
    });

    it('Test API GET /territoires/auvergne-rhone-alpes', async () => {
        await request(expressApp)
            .get('/crater/api/territoires/auvergne-rhone-alpes')
            .expect('Content-Type', /json/)
            .expect(200, {
                ...RESULTAT_REGION,
                pays: RESULTAT_PAYS
            });
    });

    it('Test API GET /territoires/isere', async () => {
        await request(expressApp)
            .get('/crater/api/territoires/isere')
            .expect('Content-Type', /json/)
            .expect(200, {
                ...RESULTAT_DEPARTEMENT,
                pays: RESULTAT_PAYS,
                region: RESULTAT_REGION
            });
    });

    it('Test API GET /territoires/grenoble-alpes-metropole', async () => {
        await request(expressApp)
            .get('/crater/api/territoires/grenoble-alpes-metropole')
            .expect('Content-Type', /json/)
            .expect(200, {
                ...RESULTAT_EPCI,
                pays: RESULTAT_PAYS,
                region: RESULTAT_REGION,
                departement: RESULTAT_DEPARTEMENT
            });
    });

    it('Test API GET /territoires/pnr-du-vercors', async () => {
        await request(expressApp)
            .get('/crater/api/territoires/pnr-du-vercors')
            .expect('Content-Type', /json/)
            .expect(200, {
                ...RESULTAT_PNR,
                pays: RESULTAT_PAYS,
                region: RESULTAT_REGION,
                departement: RESULTAT_DEPARTEMENT
            });
    });

    it('Test API GET /territoires/grenoble', async () => {
        await request(expressApp)
            .get('/crater/api/territoires/grenoble')
            .expect('Content-Type', /json/)
            .expect(200, {
                ...RESULTAT_COMMUNE,
                pays: RESULTAT_PAYS,
                region: RESULTAT_REGION,
                departement: RESULTAT_DEPARTEMENT,
                epci: RESULTAT_EPCI
            });
    });

    it('Test API GET /diagnostics par codeInsee pour France', async () => {
        await request(expressApp)
            .get('/crater/api/diagnostics/france')
            .expect('Content-Type', /json/)
            .expect((res: request.Response) => {
                if (!('idTerritoire' in res.body)) {
                    throw new Error('Erreur : la réponse ne contient pas de champ idTerritoire');
                }
                if (res.body.idTerritoire != 'france') {
                    throw new Error("Erreur : l'ID Territoire dans la réponse du diagnostic n'a pas la bonne valeur");
                }
            })
            .expect(200);
    });

    it('Test API GET /diagnostics par codeInsee pour une région', async () => {
        await request(expressApp)
            .get('/crater/api/diagnostics/nouvelle-aquitaine')
            .expect('Content-Type', /json/)
            .expect((res: request.Response) => {
                if (!('idTerritoire' in res.body)) {
                    throw new Error('Erreur : la réponse ne contient pas de champ idTerritoire');
                }
                if (res.body.idTerritoire != 'nouvelle-aquitaine') {
                    throw new Error("Erreur : l'ID Territoire dans la réponse du diagnostic n'a pas la bonne valeur");
                }
            })
            .expect(200);
    });

    it('Test API GET /indicateurs/occupationSols.superficieTotaleHa pour les EPCIs', async () => {
        await request(expressApp)
            .get('/crater/api/indicateurs/occupationSols.superficieTotaleHa?categorieTerritoire=EPCI')
            .expect('Content-Type', /json/)
            .expect(200);
    });
});
