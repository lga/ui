import express from 'express';
import request from 'supertest';
import { beforeEach, describe, expect, it } from 'vitest';

import CraterApi from '../../src/CraterApi';

const attendre = (ms: number) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
};

async function fetchRetry(expressApp: express.Application, url: string, statusAttendu: number, nbMaxEssais: number) {
    let statusReponse = -1;
    while (statusReponse != statusAttendu && nbMaxEssais >= 1) {
        request(expressApp)
            .get(url)
            .end(function (err, res) {
                statusReponse = res.status;
            });
        await attendre(1000);
        nbMaxEssais--;
    }
    return statusReponse;
}

describe('Tests integration API CRATer pour valider les états au démarrage', () => {
    let craterApi: CraterApi;
    let expressApp: express.Application;

    beforeEach(function () {
        expressApp = express();
        craterApi = new CraterApi(expressApp, 'build/crater-data-resultats/data');
        craterApi.init(); // pas de await pour tester les appels à l'api avant que l'init soit terminée
    });

    it('Test démarrage API GET /diagnostics/{idDiagnostic}  : retourne 503, puis 200', async () => {
        expect(await fetchRetry(expressApp, '/crater/api/diagnostics/blagnac', 503, 3)).toEqual(503);
        expect(await fetchRetry(expressApp, '/crater/api/diagnostics/blagnac', 200, 40)).toEqual(200);
    });

    it('Test démarrage API GET /territoires : retourne 503, puis 200', async () => {
        expect(await fetchRetry(expressApp, '/crater/api/territoires?critere=saint', 503, 3)).toEqual(503);
        expect(await fetchRetry(expressApp, '/crater/api/territoires?critere=saint', 200, 40)).toEqual(200);
    });

    it('Test démarrage API GET /territoires/{idTerritoire} : retourne 503, puis 200', async () => {
        expect(await fetchRetry(expressApp, '/crater/api/territoires/blagnac', 503, 3)).toEqual(503);
        expect(await fetchRetry(expressApp, '/crater/api/territoires/blagnac', 200, 40)).toEqual(200);
    });
});
