import type { ImageDefinitionAvecLien } from './ImageDefinition';

export const LOGO_TEF: ImageDefinitionAvecLien = {
    id: 'logo-tef',
    url: '/images/logos/territoires-fertiles-573x65.png',
    href: 'https://territoiresfertiles.fr/',
    resolutionX: 573,
    resolutionY: 65,
    descriptionAlt: 'Logo Territoires Fertiles',
    credit: 'Territoires Fertiles',
    source: 'https://territoiresfertiles.fr/'
};

export const LOGO_LGA: ImageDefinitionAvecLien = {
    id: 'logo-lga',
    url: '/images/logos/les-greniers-d-abondance-256x136.png',
    href: 'https://resiliencealimentaire.org/',
    resolutionX: 256,
    resolutionY: 136,
    descriptionAlt: 'Logo Les Greniers d’Abondance',
    credit: "Les Greniers d'Abondance",
    source: 'https://resiliencealimentaire.org/'
};

export const LOGO_FNAB: ImageDefinitionAvecLien = {
    id: 'logo-fnab',
    href: 'https://www.fnab.org/',
    url: '/images/logos/fnab-219x270.png',
    resolutionX: 219,
    resolutionY: 270,
    descriptionAlt: 'Logo Fédération Nationale d’Agriculture Biologique',
    credit: 'Fédération Nationale d’Agriculture Biologique',
    source: 'https://www.fnab.org/'
};
export const LOGO_LE_BASIC: ImageDefinitionAvecLien = {
    id: 'logo-le-basic',
    href: 'https://lebasic.com/',
    url: '/images/logos/le-basic-212x136.png',
    resolutionX: 212,
    resolutionY: 136,
    descriptionAlt: 'Logo Le Basic',
    credit: 'Le Basic',
    source: 'https://lebasic.com/'
};
export const LOGO_TERRE_DE_LIENS: ImageDefinitionAvecLien = {
    id: 'logo-terre-de-liens',
    href: 'https://terredeliens.org/',
    url: '/images/logos/terre-de-liens-238x248.png',
    resolutionX: 238,
    resolutionY: 248,
    descriptionAlt: 'Logo Terre de Liens',
    credit: 'Terre de Liens',
    source: 'https://terredeliens.org/'
};

export const LOGO_BANQUE_DES_TERRITOIRES: ImageDefinitionAvecLien = {
    id: 'logo-bande-des-territoires',
    href: 'https://www.banquedesterritoires.fr/',
    url: '/images/logos/la-banque-des-territoires-600x133.png',
    resolutionX: 600,
    resolutionY: 133,
    descriptionAlt: 'Logo La Banque des Territoires',
    credit: 'Banque des Territoires',
    source: 'https://www.banquedesterritoires.fr/'
};

export const LOGO_ADEME: ImageDefinitionAvecLien = {
    id: 'logo-ademe',
    href: 'https://www.ademe.fr/',
    url: '/images/logos/ademe-388x192.jpg',
    resolutionX: 388,
    resolutionY: 192,
    descriptionAlt: 'Logo ADEME',
    credit: 'ADEME',
    source: 'https://www.ademe.fr/'
};

export const LOGO_PNA: ImageDefinitionAvecLien = {
    id: 'logo-pna',
    href: 'https://agriculture.gouv.fr/programme-national-pour-lalimentation-les-64-laureats-de-lappel-projets-2023-2024',
    url: '/images/logos/pna-267x135.png',
    resolutionX: 267,
    resolutionY: 135,
    descriptionAlt: 'Logo PNA',
    credit: "Programme National pour l'Alimentation",
    source: 'https://agriculture.gouv.fr/programme-national-pour-lalimentation-les-64-laureats-de-lappel-projets-2023-2024'
};

export const LOGO_CARASSO: ImageDefinitionAvecLien = {
    id: 'logo-carasso',
    href: 'https://www.fondationcarasso.org/',
    url: '/images/logos/fondation-carasso-304x136.png',
    resolutionX: 304,
    resolutionY: 136,
    descriptionAlt: 'Logo Fondation Carasso',
    credit: 'Fondation Carasso',
    source: 'https://www.fondationcarasso.org/'
};

export const LOGO_FONDATION_DE_FRANCE: ImageDefinitionAvecLien = {
    id: 'logo-fondation-de-france',
    href: 'http://www.fondationdefrance.org/',
    url: '/images/logos/fondation-de-france-180x230.jpg',
    resolutionX: 180,
    resolutionY: 230,
    descriptionAlt: 'Logo Fondation de France',
    credit: 'Fondation de France',
    source: 'https://www.fondationdefrance.org/'
};

export const LOGOS_PARTENAIRES: ImageDefinitionAvecLien[] = [LOGO_LGA, LOGO_LE_BASIC, LOGO_FNAB, LOGO_TERRE_DE_LIENS];

export const LOGOS_PARTENAIRES_ET_BDT: ImageDefinitionAvecLien[] = [...LOGOS_PARTENAIRES, LOGO_BANQUE_DES_TERRITOIRES];

export const LOGOS_FINANCEURS: ImageDefinitionAvecLien[] = [
    LOGO_BANQUE_DES_TERRITOIRES,
    LOGO_ADEME,
    LOGO_PNA,
    LOGO_CARASSO,
    LOGO_FONDATION_DE_FRANCE
];
