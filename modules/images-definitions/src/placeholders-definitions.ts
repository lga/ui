import type { ImageDefinition } from './ImageDefinition';

export const PLACEHOLDER001_400x600: ImageDefinition = {
    id: 'placeholder001',
    url: '/images/placeholders/placeholder001_400x600.svg',
    resolutionX: 400,
    resolutionY: 600,
    descriptionAlt: 'Image placeholder 400x600',
    credit: '-',
    source: '-'
};

export const PLACEHOLDER002_600x400: ImageDefinition = {
    id: 'placeholder002',
    url: '/images/placeholders/placeholder002_600x400.svg',
    resolutionX: 600,
    resolutionY: 400,
    descriptionAlt: 'Image placeholder 600x400',
    credit: '-',
    source: '-'
};
