export interface ImageDefinition {
    id: string;
    url: string;
    descriptionAlt: string;
    resolutionX: number;
    resolutionY: number;
    credit: string;
    source: string;
}
export interface ImageDefinitionAvecLien extends ImageDefinition {
    href: string;
}
