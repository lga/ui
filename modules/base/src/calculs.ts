export function arrondirANDecimales(nombre: number, nbDecimales = 1): number {
    const facteur = Math.pow(10, nbDecimales);
    return Math.round(nombre * facteur) / facteur;
}

export function calculerValeurEntiereNonNulle(nombre: number | null): number {
    if (nombre === null) {
        return 0;
    }
    return Math.round(nombre);
}

export function clip(valeur: number, borneInf: number, borneSup: number) {
    return Math.max(borneInf, Math.min(valeur, borneSup));
}

export function sommeArray(array: (number | null)[]): number | null {
    if (array.length === 0) return null;

    return array.reduce((a, b) => (a ?? 0) + (b ?? 0));
}

export function calculerPartsArray(valeurs: number[], nbDecimales = 0): number[] {
    let parts: number[] = [];
    const totalValeurs = sommeArray(valeurs)!;
    if (totalValeurs > 0) {
        parts = valeurs.map((i) => arrondirANDecimales((i / totalValeurs) * 100, nbDecimales));
        //pour rendre le total des parts = 100 en ajustant la valeur de la plus grande part
        const ecartTotalPartsAvec100 = arrondirANDecimales(sommeArray(parts)! - 100);
        const indexDeLaPlusGrandeValeur = parts.indexOf(Math.max(...parts));
        parts[indexDeLaPlusGrandeValeur] = arrondirANDecimales(parts[indexDeLaPlusGrandeValeur] - ecartTotalPartsAvec100);
    }
    return parts;
}

export function calculerValeurAbsolue(nombre: number | null | undefined): number | null | undefined {
    if (nombre === null || nombre === undefined || isNaN(nombre)) {
        return nombre;
    }
    return Math.abs(nombre);
}

export function calculerPuissance(nombre: number): number {
    if (nombre === 0) return 0;
    return Math.floor(Math.log10(Math.abs(nombre)));
}

export enum Signe {
    NULL = 'NULL',
    ZERO = 'ZERO',
    POSITIF = 'POSITIF',
    NEGATIF = 'NEGATIF'
}

export function calculerSigneNombre(nombre: number | null): Signe {
    if (nombre === null) return Signe.NULL;
    else if (nombre === 0) return Signe.ZERO;
    else if (nombre > 0) return Signe.POSITIF;
    else return Signe.NEGATIF;
}

export enum QualificatifEvolution {
    progres = 'progres',
    declin = 'declin',
    neutre = 'neutre',
    valide = 'validé',
    rate = 'rate'
}

export function calculerQualificatifEvolution(signe: Signe, correlation: 'positive' | 'negative' = 'positive'): QualificatifEvolution {
    if (signe === Signe.POSITIF) {
        if (correlation === 'positive') return QualificatifEvolution.progres;
        else return QualificatifEvolution.declin;
    } else if (signe === Signe.NEGATIF) {
        if (correlation === 'positive') return QualificatifEvolution.declin;
        else return QualificatifEvolution.progres;
    }
    return QualificatifEvolution.neutre;
}

export function calculerEvolutionParPasDeTemps(evolutionAnnuelle: number | null): { nombre: number | null; periode: string | null } {
    if (evolutionAnnuelle === null) {
        return {
            nombre: null,
            periode: null
        };
    }
    let pasDetemps = 'tous les dix ans';
    let evolution = Math.abs(evolutionAnnuelle * 10);
    if (evolution / 2 > 5) {
        evolution = evolution / 2;
        pasDetemps = 'tous les cinq ans';
        if (evolution / 5 > 5) {
            evolution = evolution / 5;
            pasDetemps = 'par an';
            if (evolution / 12 > 5) {
                evolution = evolution / 12;
                pasDetemps = 'par mois';
                if ((evolution * 12) / 52 > 5) {
                    evolution = (evolution * 12) / 52;
                    pasDetemps = 'par semaine';
                    if (evolution / 7 > 5) {
                        evolution = evolution / 7;
                        pasDetemps = 'par jour';
                        if (evolution / 24 > 5) {
                            evolution = evolution / 24;
                            pasDetemps = 'par heure';
                            if (evolution / 60 > 5) {
                                evolution = evolution / 60;
                                pasDetemps = 'par minute';
                                if (evolution / 60 > 5) {
                                    evolution = evolution / 60;
                                    pasDetemps = 'par seconde';
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return {
        nombre: evolutionAnnuelle < 0 ? arrondirANDecimales(evolution, 0) * -1 : arrondirANDecimales(evolution, 0),
        periode: pasDetemps
    };
}

export function regressionLineaire(
    x: (number | null)[],
    y: (number | null)[]
): { a: number | null; b: number | null; r2: number | null; fn: (x: number) => number | null } {
    let n = 0;
    let somme_x = 0;
    let somme_y = 0;
    let somme_xy = 0;
    let somme_xx = 0;
    let somme_yy = 0;

    for (let i = 0; i < x.length; i++) {
        if (x[i] !== null && y[i] !== null) {
            n += 1;
            somme_x += x[i]!;
            somme_y += y[i]!;
            somme_xy += x[i]! * y[i]!;
            somme_xx += x[i]! * x[i]!;
            somme_yy += y[i]! * y[i]!;
        }
    }
    if (n > 1) {
        const a = (n * somme_xy - somme_x * somme_y) / (n * somme_xx - somme_x * somme_x); //pente
        const b = (somme_y - a * somme_x) / n; //ordonnée à l'origine
        const r2 = Math.pow(
            (n * somme_xy - somme_x * somme_y) / Math.sqrt((n * somme_xx - somme_x * somme_x) * (n * somme_yy - somme_y * somme_y)),
            2
        );
        return { a: arrondirANDecimales(a, 3), b: arrondirANDecimales(b, 3), r2: arrondirANDecimales(r2, 3), fn: (x: number): number => a * x + b };
    } else {
        return { a: null, b: null, r2: null, fn: () => null };
    }
}

export function coordonneesPolairesVersCartesiennes(rayon: number, angleEnRadians: number) {
    const x = arrondirANDecimales(rayon * Math.sin(angleEnRadians), 3);
    const y = arrondirANDecimales(rayon * Math.cos(angleEnRadians), 3);
    return { x: x, y: y };
}

export const calculerIndiceClasse = (valeur: number | null, valeurMax: number, nbClasses: number): number | null => {
    return valeur === null ? null : valeur <= 0 ? 0 : valeur >= valeurMax ? nbClasses - 1 : Math.floor(valeur / (valeurMax / nbClasses));
};
