import stringify from 'json-stable-stringify';

// Fusionne les champs de objet2 dans objet1. Fonctionne sur tous les niveaux (deep merge).
// Si le même champ est présent dans objet1 et objet2, la valeur de objet2 vient écraser celle de objet1
export function fusionner(objet1: object, objet2: object): object {
    const resultat = {};
    mergeDeep(resultat, objet1, objet2);
    return resultat;
}

function isObject(item: undefined | object): boolean | undefined {
    return item && typeof item === 'object' && !Array.isArray(item);
}

// TODO ajouter TU, virer les any
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mergeDeep(target: any, ...sources: any[]): any {
    if (!sources.length) return target;
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const source = sources.shift();

    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    if (isObject(target) && isObject(source)) {
        for (const key in source) {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-member-access
            if (isObject(source[key])) {
                // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
                if (!target[key]) Object.assign(target, { [key]: {} });
                // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
                mergeDeep(target[key], source[key]);
            } else {
                // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/no-unsafe-assignment
                Object.assign(target, { [key]: source[key] });
            }
        }
    }

    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    return mergeDeep(target, ...sources);
}

export function stringifyAvecTri(json: object): string {
    function comparateurProprietes(a: stringify.Element, b: stringify.Element): 1 | -1 {
        const estObjet = (a: stringify.Element) => typeof a.value === 'object' && !Array.isArray(a.value);
        const estTableau = (a: stringify.Element) => Array.isArray(a.value);
        const estTypeSimple = (a: stringify.Element) => !estObjet(a) && !estTableau(a);

        const memeType = (estTypeSimple(a) && estTypeSimple(b)) || (estTableau(a) && estTableau(b)) || (estObjet(a) && estObjet(b));
        if (memeType) return a.key.toUpperCase() < b.key.toUpperCase() ? -1 : 1;
        if (estTypeSimple(a)) return -1;
        if (estTypeSimple(b)) return 1;
        if (estTableau(a)) return -1;
        if (estTableau(b)) return 1;
        return 1;
    }

    return stringify(json, { cmp: comparateurProprietes, space: 2 });
}
