export type KebabVersCamelCase<S extends string> = S extends `${infer T}-${infer U}` ? `${T}${Capitalize<KebabVersCamelCase<U>>}` : S;
