import { describe, expect, it } from 'vitest';

import { htmlstring } from './HtmlString';

describe('Test du tag template htmlstring', () => {
    it('Test htmlstring', () => {
        const unNombre = 1;
        const uneString = 'contenu_string';
        const unHtmlString = htmlstring`${unNombre} contenu_htmlstring`;

        expect(htmlstring`<h1>Contenu literal</h1>`).toEqual('<h1>Contenu literal</h1>');
        expect(htmlstring`<h1>${unNombre} ${uneString} ${unHtmlString}</h1>`).toEqual('<h1>1 contenu_string 1 contenu_htmlstring</h1>');
    });
});
