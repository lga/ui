export interface ImageModele {
    url: string;
    descriptionAlt: string;
    resolutionX: number;
    resolutionY: number;
}
