import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-barre-onglets-item': BarreOngletsItem;
    }
}

@customElement('c-barre-onglets-item')
export class BarreOngletsItem extends LitElement {
    static readonly THEME = {
        classique: 'classique',
        simple: 'simple'
    };
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            .theme-classique {
                --couleur-selection: var(--couleur-primaire);
                --hauteur-selection: 3px;
            }
            .theme-simple {
                --couleur-selection: var(--couleur-neutre);
                --hauteur-selection: 2px;
            }

            #div-conteneur {
                max-width: 210px;
                width: fit-content;
                height: 100%;
                position: relative;
            }

            #div-conteneur.theme-classique {
                padding: 0 calc(2 * var(--dsem));
            }

            #div-conteneur.theme-simple {
                margin: 0 calc(2 * var(--dsem));
            }

            #div-selection:hover::after,
            #div-selection.selectionne::after {
                display: block;
                position: absolute;
                left: 0px;
                bottom: 0px;
                content: '';
                height: var(--hauteur-selection);
                width: 100%;
                padding: 0;
                border-radius: var(--hauteur-selection);
                background-color: var(--couleur-accent);
            }
            #div-selection.selectionne::after {
                background-color: var(--couleur-selection);
            }
            #div-selection.selectionne:hover::after {
                background-color: var(--couleur-accent);
            }

            a {
                display: flex;
                flex-direction: column;
                align-items: start;
                justify-content: center;
                padding: calc(2 * var(--dsem)) 0;
                text-decoration: none;
            }

            .theme-simple a {
                padding: 0;
            }

            a:hover,
            a:active {
                cursor: pointer;
                text-decoration: none;
                color: var(--couleur-accent);
            }

            .libelle {
                margin: 0;
                padding: 0;
                max-width: 100%;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .libelle {
                color: var(--couleur-primaire);
                font-weight: 500;
            }

            a:hover .libelle {
                color: var(--couleur-accent);
            }

            .selectionne .libelle {
                font-weight: bold;
            }
        `
    ];

    @property()
    libelle = '';

    @property()
    sousLibelle?: string;

    @property()
    href?: string;

    @property()
    theme: string = BarreOngletsItem.THEME.classique;

    @property({ type: Boolean })
    selectionne = false;

    render() {
        return html`
            <div id="div-conteneur" class="theme-${this.theme}">
                <div id="div-selection" class="${this.selectionne ? `selectionne` : ``}">
                    <a href="${ifDefined(this.href)}">
                        <div class="libelle texte-moyen">${unsafeHTML(this.libelle)}</div>
                        <div class="libelle texte-petit-majuscule">${unsafeHTML(this.sousLibelle)}</div>
                    </a>
                </div>
            </div>
        `;
    }
}
