import { css, html, LitElement, type TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { styleMap } from 'lit/directives/style-map.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

export interface OptionsTableau {
    entetes: TemplateResult[];
    contenuHtml: TemplateResult[][];
    ligneFin?: TemplateResult[];
    jaugesDansColonnes?: boolean[];
}

declare global {
    interface HTMLElementTagNameMap {
        'c-tableau': Tableau;
    }
}

@customElement('c-tableau')
export class Tableau extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                overflow-x: auto;
                display: block;
            }

            table {
                border-collapse: collapse;
                margin-left: auto;
                margin-right: auto;
            }

            td,
            th {
                padding: calc(0.5 * var(--dsem));
                min-width: 100px;
                text-align: right;
            }

            tbody td:first-child {
                text-align: left;
            }
            tbody tr.activer-alternance-fond:nth-child(even) {
                background-color: var(--couleur-neutre-20);
            }

            th {
                padding: var(--dsem);
                text-align: center;
                border-bottom: 3px solid var(--couleur-accent);
            }

            tfoot {
                border-top: var(--dsem) solid var(--couleur-neutre-40);
            }

            dd {
                padding-left: var(--dsem);
            }
        `
    ];

    @property({ attribute: false })
    options?: OptionsTableau;

    render() {
        const contientColonneAvecJauges = this.options?.jaugesDansColonnes?.some((b) => b);
        return html`
            <table>
                <thead>
                    <tr class="texte-moyen">
                        ${this.options?.entetes.map((e) => html`<th>${e}</th>`)}
                    </tr>
                </thead>
                <tbody>
                    ${this.options?.contenuHtml.map(
                        (ligne) =>
                            html`<tr class="texte-moyen${!contientColonneAvecJauges ? ' activer-alternance-fond' : ''}">
                                ${ligne.map((contenuCellule, index) => {
                                    let part = 0;
                                    let contenu = contenuCellule;
                                    if (this.options?.jaugesDansColonnes?.at(index)) {
                                        // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
                                        part = parseFloat(contenuCellule.strings[0] + contenuCellule.values[0]);
                                        contenu = isNaN(part) ? html`/` : html`${part.toString()} %`;
                                    }
                                    return html`<td
                                        style=${styleMap(
                                            contientColonneAvecJauges
                                                ? {
                                                      background: `linear-gradient(.25turn, var(--couleur-neutre-transparent) ${part}%, transparent 0%);`,
                                                      width: this.options?.jaugesDansColonnes?.at(index) ? '50px' : null
                                                  }
                                                : {}
                                        )}
                                    >
                                        ${contenu}
                                    </td>`;
                                })}
                            </tr>`
                    )}
                </tbody>
                <tfoot>
                    ${this.options?.ligneFin
                        ? html`<tr class="texte-moyen">
                              ${this.options.ligneFin.map((e) => html`<td>${e}</td>`)}
                          </tr>`
                        : ''}
                </tfoot>
            </table>
        `;
    }
}
