import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '../styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-bouton': Bouton;
    }
}

@customElement('c-bouton')
export class Bouton extends LitElement {
    static readonly TYPE = {
        relief: 'relief',
        encadre: 'encadre',
        plat: 'plat'
    };
    static readonly THEME_COULEUR = {
        primaire: 'primaire',
        secondaire: 'secondaire',
        neutre: 'neutre',
        accent: 'accent'
    };
    static readonly POSITION_ICONE = {
        droite: 'droite',
        gauche: 'gauche'
    };
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                --taille-icone: 1.2em;
                --coefficient-padding: 1.5;
                --coefficient-border-radius: 4;
            }

            .theme-relief-primaire {
                --couleur-fond-base: var(--couleur-primaire);
                --couleur-texte-base: var(--couleur-blanc);
                --couleur-fond-active: var(--couleur-primaire-sombre);
                --couleur-texte-active: var(--couleur-blanc);
            }

            .theme-relief-secondaire {
                --couleur-fond-base: var(--couleur-secondaire);
                --couleur-texte-base: var(--couleur-blanc);
                --couleur-fond-active: var(--couleur-secondaire-sombre);
                --couleur-texte-active: var(--couleur-blanc);
            }

            .theme-relief-neutre {
                --couleur-fond-base: var(--couleur-neutre-80);
                --couleur-texte-base: var(--couleur-blanc);
                --couleur-fond-active: var(--couleur-neutre-80);
                --couleur-texte-active: var(--couleur-blanc);
            }

            .theme-relief-accent {
                --couleur-fond-base: var(--couleur-accent);
                --couleur-texte-base: var(--couleur-blanc);
                --couleur-fond-active: var(--couleur-accent-sombre);
                --couleur-texte-active: var(--couleur-blanc);
            }

            .theme-relief-desactive {
                --couleur-fond-base: var(--couleur-neutre-clair);
                --couleur-texte-base: var(--couleur-neutre);
                --couleur-fond-active: var(--couleur-neutre-clair);
                --couleur-texte-active: var(--couleur-neutre);
            }

            .theme-encadre-primaire {
                --couleur-fond-base: var(--couleur-fond);
                --couleur-texte-base: var(--couleur-primaire);
                --couleur-fond-active: var(--couleur-primaire);
                --couleur-texte-active: var(--couleur-blanc);
            }
            .theme-encadre-secondaire {
                --couleur-fond-base: var(--couleur-fond);
                --couleur-texte-base: var(--couleur-secondaire);
                --couleur-fond-active: var(--couleur-secondaire);
                --couleur-texte-active: var(--couleur-blanc);
            }
            .theme-encadre-neutre {
                --couleur-fond-base: var(--couleur-fond);
                --couleur-texte-base: var(--couleur-neutre-80);
                --couleur-fond-active: var(--couleur-neutre-80);
                --couleur-texte-active: var(--couleur-blanc);
            }

            .theme-encadre-accent {
                --couleur-fond-base: var(--couleur-fond);
                --couleur-texte-base: var(--couleur-accent);
                --couleur-fond-active: var(--couleur-accent);
                --couleur-texte-active: var(--couleur-blanc);
            }

            .theme-encadre-desactive {
                --couleur-fond-base: var(--couleur-fond);
                --couleur-texte-base: var(--couleur-neutre);
                --couleur-fond-active: var(--couleur-blanc);
                --couleur-texte-active: var(--couleur-neutre);
            }

            .theme-plat-primaire {
                --couleur-fond-base: transparent;
                --couleur-texte-base: var(--couleur-primaire);
                --couleur-fond-active: transparent;
                --couleur-texte-active: var(--couleur-accent);
            }
            .theme-plat-secondaire {
                --couleur-fond-base: transparent;
                --couleur-texte-base: var(--couleur-secondaire);
                --couleur-fond-active: transparent;
                --couleur-texte-active: var(--couleur-accent);
            }
            .theme-plat-neutre {
                --couleur-fond-base: transparent;
                --couleur-texte-base: var(--couleur-neutre-80);
                --couleur-fond-active: transparent;
                --couleur-texte-active: var(--couleur-accent);
            }

            .theme-plat-accent {
                --couleur-fond-base: transparent;
                --couleur-texte-base: var(--couleur-accent);
                --couleur-fond-active: transparent;
                --couleur-texte-active: var(--couleur-accent-sombre);
            }

            .theme-plat-desactive {
                --couleur-fond-base: transparent;
                --couleur-texte-base: var(--couleur-neutre);
                --couleur-fond-active: transparent;
                --couleur-texte-active: var(--couleur-neutre);
            }

            :host {
                display: block;
                width: fit-content;
            }

            a {
                cursor: pointer;
                display: flex;
                align-items: center;
                justify-content: center;
                gap: var(--dsem);
                padding: calc(0.5 * var(--coefficient-padding) * var(--dsem)) calc(2 * var(--coefficient-padding) * var(--dsem));
                border-radius: calc(var(--coefficient-border-radius) * var(--dsem));
                background-color: var(--couleur-fond-base);
                text-decoration: none;
                border: 1px solid var(--couleur-fond-base);
            }

            p {
                white-space: nowrap;
                margin: 0;
                padding: 0;
                color: var(--couleur-texte-base);
            }

            ::slotted(svg) {
                width: var(--taille-icone);
                height: var(--taille-icone);
                fill: var(--couleur-fond-base);
                stroke: var(--couleur-texte-base);
            }

            a:active {
                background-color: var(--couleur-fond-active);
                border-color: var(--couleur-fond-active);
                text-decoration: none;
            }

            a:active p {
                color: var(--couleur-texte-active);
            }

            a:active ::slotted(svg) {
                fill: var(--couleur-fond-active);
                stroke: var(--couleur-texte-active);
            }

            .desactive,
            .desactive:hover,
            .desactive:active {
                box-shadow: none;
                cursor: default;
            }

            .encadre {
                border-color: var(--couleur-texte-base);
            }

            .relief:hover,
            .encadre:hover {
                box-shadow: 0 2px 8px 0 var(--couleur-neutre-clair);
                background-color: var(--couleur-texte-base);
                border-color: var(--couleur-texte-base);
            }

            .relief:hover p,
            .encadre:hover p {
                color: var(--couleur-fond-base);
            }

            .relief:hover ::slotted(svg),
            .encadre:hover ::slotted(svg) {
                stroke: var(--couleur-fond-base);
                fill: var(--couleur-texte-base);
            }

            .plat:hover p {
                color: var(--couleur-texte-active);
            }

            .plat:hover ::slotted(svg) {
                stroke: var(--couleur-texte-active);
            }

            .icone-uniquement,
            .taille-reduite {
                --coefficient-padding: 0;
                --taille-icone: 2em;
            }

            a.icone-uniquement {
                border: none;
            }

            #libelle-court {
                display: none;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                :host {
                    --coefficient-padding: 0.8;
                    --taille-icone: 2em;
                }

                #libelle {
                    display: none;
                }

                #libelle-court {
                    display: block;
                }
            }
        `
    ];

    @property()
    libelle?: string;

    @property()
    libelleCourt?: string;

    @property()
    libelleTooltip: string | undefined;

    @property()
    positionIcone: string = Bouton.POSITION_ICONE.droite;

    @property()
    type: string = Bouton.TYPE.relief;

    @property()
    themeCouleur: string = Bouton.THEME_COULEUR.primaire;

    @property()
    href?: string;

    @property()
    target?: string;

    @property({ type: Boolean })
    desactive = false;

    @property({ type: Boolean })
    tailleReduite = false;

    render() {
        const nomClasseThemeCouleur = this.desactive ? `theme-${this.type}-desactive` : `theme-${this.type}-${this.themeCouleur}`;
        const iconeUniquement = !this.libelle || this.libelle === '';
        return html`
            <a
                class="${this.type} ${this.desactive ? 'desactive' : ''} ${this.tailleReduite ? 'taille-reduite' : ''}
                    ${!iconeUniquement ? nomClasseThemeCouleur : 'icone-uniquement'}"
                title="${ifDefined(this.libelleTooltip)}"
                href="${ifDefined(this.href)}"
                target="${ifDefined(this.target)}"
            >
                ${this.positionIcone === Bouton.POSITION_ICONE.gauche
                    ? html` <slot class="${iconeUniquement ? nomClasseThemeCouleur : ''}" name="icone"></slot>`
                    : ''}
                ${this.libelle ? html`<p id="libelle" class="texte-moyen">${this.libelle}</p>` : ''}
                ${iconeUniquement
                    ? ''
                    : this.libelleCourt
                      ? html`<p id="libelle-court" class="texte-moyen">${this.libelleCourt}</p>`
                      : html`<p id="libelle-court" class="texte-moyen">${this.libelle}</p>`}
                ${this.positionIcone === Bouton.POSITION_ICONE.droite
                    ? html` <slot class="${iconeUniquement ? nomClasseThemeCouleur : ''}" name="icone"></slot>`
                    : ''}
            </a>
        `;
    }
}
