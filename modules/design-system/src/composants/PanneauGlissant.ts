import { css, html, LitElement } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { styleMap } from 'lit/directives/style-map.js';

import { EvenementFermer } from '../evenements/EvenementFermer';

declare global {
    interface HTMLElementTagNameMap {
        'c-panneau-glissant': PanneauGlissant;
    }
}

@customElement('c-panneau-glissant')
export class PanneauGlissant extends LitElement {
    static readonly COTE_GLISSEMENT = {
        droite: 'droite',
        gauche: 'gauche'
    };
    static styles = [
        css`
            div {
                display: block;
                position: relative;
                z-index: 100;
            }

            main {
                position: fixed;
                right: 0;
                top: 0;
                height: 100vh;
                overflow-y: auto;
                padding: 0;
            }

            .visible.droite {
                visibility: visible;
                right: 0;
                transition:
                    visibility 0s,
                    ease-in 300ms;
            }

            .invisible.droite {
                visibility: hidden;
                right: -100%;
                transition:
                    ease-in 300ms,
                    visibility 500ms;
            }

            .visible.gauche {
                visibility: visible;
                left: 0;
                transition:
                    visibility 0s,
                    ease-in 300ms;
            }

            .invisible.gauche {
                visibility: hidden;
                left: -100%;
                transition:
                    ease-in 300ms,
                    visibility 500ms;
            }
        `
    ];

    @property()
    coteGlissement = PanneauGlissant.COTE_GLISSEMENT.droite;

    @property()
    largeur = '100vw';

    @state()
    classVisibilite = `invisible ${this.coteGlissement}`;

    render() {
        const styles = {
            width: this.largeur
        };
        return html`
            <div>
                <slot name="bouton-ouvrir" @click=${this.deplier}></slot>
                <main class="${this.classVisibilite}" style=${styleMap(styles)}>
                    <slot name="panneau"></slot>
                </main>
            </div>
        `;
    }

    private deplier() {
        this.inscrireListenerPlier();
        this.classVisibilite = `visible ${this.coteGlissement}`;
    }

    // arrow function car utilisée comme callback dans un listener
    private plier = (event: Event) => {
        const estTypeFermer: boolean = event.type === EvenementFermer.ID;
        if (estTypeFermer) {
            this.supprimerListenerPlier();
            this.classVisibilite = `invisible ${this.coteGlissement}`;
            if (!(event as EvenementFermer).detail.fermerParents) {
                event.stopPropagation();
            }
        }
    };

    private inscrireListenerPlier() {
        this.addEventListener(EvenementFermer.ID, this.plier);
    }

    private supprimerListenerPlier() {
        this.removeEventListener(EvenementFermer.ID, this.plier);
    }
}
