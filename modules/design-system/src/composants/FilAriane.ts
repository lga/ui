import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-fil-ariane': FilAriane;
    }
}

export interface LienFilAriane {
    libelle: string;
    href: string;
}

@customElement('c-fil-ariane')
export class FilAriane extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100%;
                height: fit-content;
            }

            div {
                display: flex;
                flex-wrap: wrap;
                flex-direction: row;
                align-items: center;
                column-gap: calc(var(--dsem) / 2);
            }

            a {
                display: grid;
                cursor: pointer;
                padding: calc(var(--dsem) / 2);
                color: var(--couleur-neutre);
                text-decoration: none;
                max-width: 90%;
            }

            a:hover {
                text-decoration: underline;
            }

            svg {
                width: 1.5em;
                height: 1.5em;
                fill: var(--couleur-neutre);
                margin: auto;
            }

            svg:hover {
                fill: var(--couleur-primaire);
            }

            .separateur-liens {
                color: var(--couleur-neutre);
            }
        `
    ];

    @property({ attribute: false })
    liens: LienFilAriane[] = [];

    @property()
    hrefAccueil = '/';

    render() {
        return html`
            <div>
                ${this.liens.map(
                    (l, index) =>
                        html` ${index !== 0 ? html`<div class="texte-petit-majuscule separateur-liens">></div>` : ``}
                            <a class="texte-petit-majuscule" href="${l.href}">${l.libelle}</a>`
                )}
            </div>
        `;
    }
}
