import './MenuItem.js';
import './MenuSousItem.js';

import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { EvenementSelectionner } from '../evenements/EvenementSelectionner.js';
import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-accordeon': MenuAccordeon;
    }
}

export interface MenuAccordeonItem {
    id: string;
    libelle: string;
    sousLibelle?: string;
    href?: string;
    icone?: string;
    sousItems?: MenuAccordeonSousItem[];
}

export interface MenuAccordeonSousItem {
    id: string;
    libelle: string;
    href?: string;
}

@customElement('c-menu-accordeon')
export class MenuAccordeon extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: flex;
                flex-direction: column;
                justify-content: space-around;
                min-width: fit-content;
            }

            c-menu-sous-item {
                padding-left: calc(4 * var(--dsem));
            }
        `
    ];

    @property({ attribute: false })
    items: MenuAccordeonItem[] = [];

    @property()
    idItemSelectionne?: string;

    render() {
        return html`
            ${this.items.map(
                (i) => html`
                    <c-menu-item
                        libelle=${i.libelle}
                        sousLibelle=${ifDefined(i.sousLibelle)}
                        ?selectionne=${i.id === this.idItemSelectionne}
                        ?chevronVersBas=${this.itemEstOuvert(i)}
                        href="${ifDefined(i.href)}"
                        @click=${this.selectionner}
                    >
                        <div slot="icone">${unsafeSVG(i.icone)}</div>
                    </c-menu-item>
                    ${this.sousItemsAAfficher(i)
                        ? i.sousItems?.map(
                              (si) => html`
                                  <c-menu-sous-item
                                      libelle=${si.libelle}
                                      ?selectionne=${si.id === this.idItemSelectionne}
                                      href="${ifDefined(si.href)}"
                                      @click=${this.selectionner}
                                      >${si.libelle}
                                  </c-menu-sous-item>
                              `
                          )
                        : ``}
                `
            )}
        `;
    }

    private itemEstOuvert(item: MenuAccordeonItem) {
        return (
            (item.id === this.idItemSelectionne && item.sousItems !== undefined) ||
            item.sousItems?.find((si) => si.id == this.idItemSelectionne) !== undefined
        );
    }

    private sousItemsAAfficher(item: MenuAccordeonItem) {
        return item.id === this.idItemSelectionne || item.sousItems?.find((si) => si.id == this.idItemSelectionne);
    }

    private selectionner(e: Event) {
        this.dispatchEvent(new EvenementSelectionner((e.target as HTMLElement).id));
    }
}
