import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { EvenementSelectionner } from '../evenements/EvenementSelectionner';
import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

export interface OptionsRadioBouton {
    boutons: {
        id: string;
        libelle: string;
        sousLibelle?: string;
    }[];
    idBoutonActif: string;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-radio-bouton': RadioBouton;
    }
}

@customElement('c-radio-bouton')
export class RadioBouton extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            form {
                display: flex;
                flex-wrap: wrap;
                flex-direction: column;
            }

            label {
                display: flex;
                cursor: pointer;
                position: relative;
                overflow: hidden;
                margin-bottom: 0.375em;
            }

            input {
                position: absolute;
                left: -9999px;
            }
            input:checked + span:before {
                box-shadow: inset 0 0 0 0.4375em var(--couleur-primaire);
            }

            span {
                display: flex;
                align-items: center;
                padding: 0.375em 0.75em 0.375em 0.375em;
                border-radius: 2em;
                transition: 0.25s ease;
            }
            span:hover {
                background-color: var(--couleur-neutre-clair);
            }
            span:before {
                display: flex;
                flex-shrink: 0;
                content: '';
                background-color: #fff;
                width: 1.5em;
                height: 1.5em;
                border-radius: 50%;
                margin-right: 0.75em;
                transition: 0.25s ease;
                box-shadow: inset 0 0 0 0.125em var(--couleur-primaire);
            }
            h2,
            h3 {
                margin: 0;
                color: var(--couleur-neutre);
            }
        `
    ];

    @property({ attribute: false })
    options?: OptionsRadioBouton;

    render() {
        return html`
            <form @change=${this.changerBoutonActif}>
                ${this.options?.boutons?.map(
                    (b) =>
                        html`<label>
                            <input
                                type="radio"
                                name="groupe-boutons"
                                id="${b.id}"
                                value="${b.id}"
                                .checked=${b.id === this.options?.idBoutonActif || false}
                            />
                                <span>
                                    <div>
                                    <h2 class="texte-titre">${unsafeHTML(b.libelle)}</h2>
                                ${b.sousLibelle ? html`<h3 class="texte-moyen">${unsafeHTML(b.sousLibelle)}</h3>` : ''}
                                </div>
                                </span>
                            </label>
                        </label>`
                )}
            </form>
        `;
    }
    private changerBoutonActif(e: Event) {
        this.dispatchEvent(new EvenementSelectionner((e.target as HTMLElement).id));
    }
}
