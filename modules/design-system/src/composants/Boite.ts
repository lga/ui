import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-boite': Boite;
    }
}

@customElement('c-boite')
export class Boite extends LitElement {
    static readonly ARRONDI = {
        petit: 'petit',
        moyen: 'moyen',
        large: 'large'
    };
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100%;
                height: 100%;
            }
            .petit {
                border-radius: 4px;
            }
            .moyen {
                border-radius: 8px;
            }
            .large {
                border-radius: 12px;
            }
            .cadre {
                border: solid 1px var(--couleur-neutre-20);
            }
            .ombre {
                box-shadow: 0px 0px 8px var(--couleur-neutre-20);
            }
            div {
                background-color: var(--couleur-fond-boite, var(--couleur-blanc));
                width: 100%;
                height: 100%;
            }
        `
    ];
    @property({ type: Boolean })
    ombre = false;

    @property({ type: Boolean })
    encadrement = false;

    @property()
    arrondi: string = Boite.ARRONDI.petit;

    render() {
        return html`
            <div class="${this.encadrement ? 'cadre ' : ''} ${this.ombre ? 'ombre' : ''} ${this.arrondi}">
                <slot> </slot>
            </div>
        `;
    }
}
