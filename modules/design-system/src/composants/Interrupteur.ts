import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EvenementActionnerInterrupteur } from '../evenements/EvenementActionnerInterrupteur.js';
import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-interrupteur': Interrupteur;
    }
}

@customElement('c-interrupteur')
export class Interrupteur extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: fit-content;
                height: fit-content;
            }

            main {
                cursor: pointer;
                display: grid;
                grid-template-columns: auto auto;
                width: fit-content;
                place-items: center;
            }

            #cadre {
                width: var(--dsem4);
                height: var(--dsem2);
                padding: var(--dsem0_5);
                box-sizing: content-box;
                background-color: var(--couleur-secondaire);
                border-radius: var(--dsem2);
                display: grid;
            }

            #cercle {
                place-self: end;
                width: var(--dsem2);
                height: var(--dsem2);
                background-color: white;
                border-radius: 100%;
            }

            #libelle {
                padding-left: var(--dsem);
                color: var(--couleur-secondaire);
                cursor: pointer;
            }

            p {
                margin: 0;
            }

            main:hover > #libelle {
                color: var(--couleur-accent);
            }

            .desactive > #cadre,
            .desactive:not(:hover) > #libelle {
                filter: grayscale(1) opacity(0.6);
            }

            .desactive > * #cercle {
                place-self: start;
            }
        `
    ];

    @property()
    libelle = '';

    @property({ type: Boolean })
    estActive = false;

    render() {
        return html`
            <main class=${this.estActive ? 'active' : 'desactive'} @click=${this.actionnerInterrupteur}>
                <div id="cadre">
                    <div id="cercle"></div>
                </div>
                <p id="libelle">${this.libelle}</p>
            </main>
        `;
    }

    private actionnerInterrupteur() {
        this.estActive = !this.estActive;
        this.dispatchEvent(new EvenementActionnerInterrupteur(this.estActive));
    }
}
