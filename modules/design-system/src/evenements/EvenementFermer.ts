import { EvenementPersonnalisable } from './EvenementPersonnalisable';

export class EvenementFermer extends EvenementPersonnalisable<{ fermerParents: boolean }> {
    static ID = 'fermer';
    constructor(fermerParents = false) {
        super(EvenementFermer.ID, { fermerParents: fermerParents });
    }
}
