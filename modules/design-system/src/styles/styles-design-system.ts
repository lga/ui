import { css, unsafeCSS } from 'lit';

import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from './styles-breakpoints';

export const STYLES_DESIGN_SYSTEM = css`
    *,
    *:before,
    *:after {
        box-sizing: border-box;
    }

    .italique {
        font-style: italic;
    }

    .gras {
        font-weight: bold;
    }

    em {
        color: var(--couleur-primaire);
        font-weight: bold;
        font-style: normal;
    }

    abbr {
        text-decoration-thickness: 2px;
    }

    .texte-petit {
        font-family: var(--font-family-texte-ds);
        font-size: 14px;
        line-height: 20px;
        letter-spacing: 0.02em;
    }

    .texte-petit-majuscule {
        font-family: var(--font-family-texte-ds);
        font-weight: bold;
        font-size: 14px;
        line-height: 20px;
        letter-spacing: 0.02em;
        text-transform: uppercase;
    }

    .texte-moyen {
        font-family: var(--font-family-texte-ds);
        font-size: 16px;
        line-height: 26px;
        letter-spacing: 0.02em;
    }

    h6,
    p,
    li {
        font-family: var(--font-family-texte-ds);
        color: var(--couleur-primaire);
        font-size: 16px;
        line-height: 26px;
        letter-spacing: 0.02em;
    }

    .texte-grand {
        font-family: var(--font-family-texte-ds);
        font-size: 18px;
        line-height: 30px;
        letter-spacing: 0.02em;
    }

    .texte-titre {
        font-family: var(--font-family-texte-ds);
        font-weight: bold;
        font-size: 20px;
        line-height: 32px;
        letter-spacing: 0.02em;
    }

    .texte-alternatif {
        font-family: var(--font-family-texte-ds);
        font-weight: bold;
        text-transform: uppercase;
        font-size: 16px;
        line-height: 26px;
        letter-spacing: 0.02em;
    }

    .titre-petit,
    h4 {
        font-family: var(--font-family-texte-ds);
        font-weight: bold;
        font-size: 18px;
        line-height: 30px;
        letter-spacing: 0.02em;
    }

    h3 {
        font-family: var(--font-family-texte-ds);
        font-weight: bold;
        font-size: 20px;
        line-height: 32px;
        letter-spacing: 0.02em;
    }

    .titre-moyen,
    h2 {
        font-family: var(--font-family-titre-ds);
        font-weight: normal;
        font-size: 32px;
        line-height: 44px;
        letter-spacing: 0.02em;
    }

    .titre-large,
    h1 {
        font-family: var(--font-family-titre-ds);
        font-weight: normal;
        font-size: 48px;
        line-height: 64px;
        letter-spacing: 0.02em;
    }

    .chiffre-moyen {
        font-family: var(--font-family-texte-ds);
        font-weight: bold;
        font-size: 20px;
        line-height: 20px;
        letter-spacing: 0.02em;
    }

    .chiffre-grand {
        font-family: var(--font-family-texte-ds);
        font-weight: bold;
        font-size: 32px;
        line-height: 32px;
        letter-spacing: 0.02em;
    }

    .chiffre-tres-grand {
        font-family: var(--font-family-texte-ds);
        font-weight: bold;
        font-size: 40px;
        line-height: 40px;
        letter-spacing: 0.02em;
    }

    .unite-tres-grand {
        font-family: var(--font-family-texte-ds);
        font-size: 20px;
        line-height: 24px;
        letter-spacing: 0.02em;
    }

    @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
        .titre-moyen,
        h2 {
            font-size: 24px;
            line-height: 36px;
        }

        .titre-large,
        h1 {
            font-size: 32px;
            line-height: 44px;
        }
    }
`;
