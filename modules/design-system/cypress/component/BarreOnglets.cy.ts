import '../../src/composants/BarreOnglets.js';
import '../../public/theme-defaut/theme-defaut.css';

import { html } from 'lit';

import type { OptionsBarreOnglets } from '../../src/composants/BarreOnglets';

describe('Test composant BarreOnglets', () => {
    it('Sous-item non selectionné', () => {
        // given
        cy.viewport(800, 500);
        const options: OptionsBarreOnglets = {
            onglets: [
                { id: 'onglet-1', libelle: 'Onglet avec sous libelle', sousLibelle: 'Sous-libellé' },
                { id: 'onglet-2', libelle: 'Onglet avec href', href: '/lien' }
            ],
            idOngletSelectionne: 'onglet-2'
        };
        cy.document()
            .then((doc) => {
                doc.addEventListener('selectionner', cy.stub().as('selectionner'));
            })
            .mount<'c-barre-onglets'>(html`<c-barre-onglets .options="${options}"></c-barre-onglets>`);
        //then
        cy.get('c-barre-onglets').find('#onglet-1').click();
        cy.get('@selectionner').should('have.been.calledOnce');
    });
});
