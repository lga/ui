import '../../src/composants/BoutonCarre.js';
import '../../public/theme-defaut/theme-defaut.css';

import chaiColors from 'chai-colors';
import { html } from 'lit';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { VALEURS_COULEURS } from './outils_tests';

// permet de comparer des couleurs hex rgb, etc...
chai.use(chaiColors as Chai.ChaiPlugin);

const ICONE = `
<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
<path d="M20.0032 12.0003V7.82659C20.0032 7.29598 19.7923 6.78715 19.417 6.412L16.5878 3.58283C16.2127 3.20757 15.7038 2.99658 15.1732 2.99658H5.99742C4.89239 2.99658 3.99658 3.89239 3.99658 4.99742V19.0032C3.99658 20.1083 4.89239 21.0041 5.99742 21.0041H7.99825" stroke-linecap="round" stroke-linejoin="round"/>
<rect x="10.9995" y="17.2524" width="3.00125" height="3.75156" stroke-linecap="round" stroke-linejoin="round"/>
<rect x="14.001" y="13.0005" width="3.00125" height="8.00333" stroke-linecap="round" stroke-linejoin="round"/>
<rect x="17.0024" y="15.2515" width="3.00125" height="5.7524" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M7.99854 7.99826H15.0015" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M7.99854 11.4997H10.4996" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
`;

describe('Test composant Bouton Carre', () => {
    it('Boutons simple', () => {
        cy.viewport(320, 500);

        // given
        cy.mount<'c-bouton-carre'>(html`<c-bouton-carre libelle="Diagnostic"> ${unsafeSVG(ICONE)} </c-bouton-carre>`);
        // then
        cy.get('c-bouton-carre').find('p').should('exist');
        cy.get('c-bouton-carre').find('svg').should('exist');
        cy.get('c-bouton-carre').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_neutre);
        cy.get('c-bouton-carre').find('div').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_blanc);
    });

    it('Boutons selectionné', () => {
        cy.viewport(320, 500);

        // given
        cy.mount<'c-bouton-carre'>(html`<c-bouton-carre libelle="Diagnostic" selectionne> ${unsafeSVG(ICONE)} </c-bouton-carre>`);

        // then
        cy.get('c-bouton-carre').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_primaire);
    });

    it('Bouton désactivé', () => {
        cy.viewport(320, 500);

        // given
        cy.mount<'c-bouton-carre'>(html`<c-bouton-carre libelle="Diagnostic" desactive> ${unsafeSVG(ICONE)} </c-bouton-carre>`);

        // then
        cy.get('c-bouton-carre').find('p').should('exist');
        cy.get('c-bouton-carre').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_neutre_20);
    });
});
