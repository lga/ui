import '../../public/theme-defaut/theme-defaut.css';
import '../../src/composants/FilAriane.js';

import chaiColors from 'chai-colors';
import { html } from 'lit';

import { VALEURS_COULEURS } from './outils_tests';

// permet de comparer des couleurs hex rgb, etc...
chai.use(chaiColors as Chai.ChaiPlugin);

describe('Test composant Fil Ariane', () => {
    beforeEach(() => {
        cy.viewport(1200, 500);
    });

    it('Fil à n niveaux', () => {
        // given
        const liens = [
            { libelle: 'Niveau 1', href: '/lien1' },
            { libelle: 'Niveau 2', href: '/lien2' },
            { libelle: 'Niveau 3', href: '/lien3' },
            { libelle: 'Niveau 4', href: '/lien4' }
        ];
        cy.mount<'c-fil-ariane'>(html`<c-fil-ariane .liens=${liens}></c-fil-ariane>`);
        // then
        cy.get('c-fil-ariane').find('a').should('have.length', 4);
        cy.get('c-fil-ariane').find('.separateur-liens').should('have.length', 3);
        cy.get('c-fil-ariane').find('a').last().should('have.attr', 'href').and('eq', '/lien4');
        cy.get('c-fil-ariane').find('a').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_neutre);
    });

    it('Mobile', () => {
        // given
        cy.viewport(320, 500);
        const liens = [
            { libelle: 'Niveau 1', href: '/lien1' },
            { libelle: 'Niveau 2', href: '/lien2' },
            { libelle: 'Niveau 3', href: '/lien3' },
            { libelle: 'Niveau 4', href: '/lien4' }
        ];
        cy.mount<'c-fil-ariane'>(html`<c-fil-ariane .liens=${liens}></c-fil-ariane>`);
    });
});
