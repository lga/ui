import '../../public/theme-defaut/theme-defaut.css';
import '../../src/composants/MenuAccordeon.js';

import chaiColors from 'chai-colors';
import { html } from 'lit';

import type { MenuAccordeonItem } from '../../src/composants/MenuAccordeon';
import { VALEURS_COULEURS } from './outils_tests';

// permet de comparer des couleurs hex rgb, etc...
chai.use(chaiColors as Chai.ChaiPlugin);

const pastille = `
<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
    <circle cx="16" cy="16" r="14" fill="#B1CC7E"/>
</svg>
`;

const items: MenuAccordeonItem[] = [
    { id: '1', libelle: 'Item 1 sans sous-items', href: '/lien1' },
    {
        id: '2',
        libelle: 'Item 2 avec 2 sous-items',
        icone: pastille,
        sousItems: [
            { id: '21', libelle: 'Sous-item 21', href: '/lien21' },
            { id: '22', libelle: 'Sous-item 22', href: '/lien22' }
        ]
    },
    {
        id: '3',
        libelle: 'Item 3 avec 3 sous-items dont un avec un libellé plus long',
        sousItems: [
            { id: '31', libelle: 'Sous-item 31', href: '/lien31' },
            { id: '32', libelle: 'Sous-item plus long 32', href: '/lien32' },
            { id: '33', libelle: 'Sous-item 33', href: '/lien33' }
        ]
    }
];

describe('Test composant Menu', () => {
    it('Selection item sans sous-item', () => {
        // given
        cy.mount(html`<div>Selection item 1</div>`);
        cy.mount<'c-menu-accordeon'>(html`<c-menu-accordeon .items=${items} idItemSelectionne="1"></c-menu-accordeon>`);
        // then
        cy.get('c-menu-accordeon').find('c-menu-item').should('have.length', 3);
        cy.get('c-menu-accordeon').find('c-menu-sous-item').should('have.length', 0);
        cy.get('c-menu-accordeon').find('.selectionne').should('have.length', 1);
        cy.get('c-menu-accordeon').find('c-menu-item').eq(0).find('a').should('have.class', 'selectionne');
        cy.get('c-menu-accordeon').find('.selectionne').find('.chevron').should('not.have.class', 'tourne');
        cy.get('c-menu-accordeon')
            .find('.selectionne')
            .should('have.css', 'background-color')
            .and('be.colored', VALEURS_COULEURS.couleur_neutre_clair);
    });

    it('Selection item avec sous-items', () => {
        // given
        cy.mount(html`<div>Selection item 2</div>`);
        cy.mount<'c-menu-accordeon'>(html`<c-menu-accordeon .items=${items} idItemSelectionne="2"></c-menu-accordeon>`);
        // then
        cy.get('c-menu-accordeon').find('c-menu-item').should('have.length', 3);
        cy.get('c-menu-accordeon').find('c-menu-sous-item').should('have.length', 2);
        cy.get('c-menu-accordeon').find('.selectionne').should('have.length', 1);
        cy.get('c-menu-accordeon').find('c-menu-item').eq(1).find('a').should('have.class', 'selectionne');
        cy.get('c-menu-accordeon').find('c-menu-item').eq(0).find('.chevron').should('not.have.class', 'tourne');
        cy.get('c-menu-accordeon').find('c-menu-item').eq(1).find('.chevron').should('have.class', 'tourne');
        cy.get('c-menu-accordeon').find('c-menu-item').eq(2).find('.chevron').should('not.have.class', 'tourne');
        cy.get('c-menu-accordeon')
            .find('.selectionne')
            .should('have.css', 'background-color')
            .and('be.colored', VALEURS_COULEURS.couleur_neutre_clair);
    });

    it('Selection sous-item', () => {
        // given
        cy.mount(html`<div>Selection item 32</div>`);
        cy.mount<'c-menu-accordeon'>(html`<c-menu-accordeon .items=${items} idItemSelectionne="32"></c-menu-accordeon>`);
        // then
        cy.get('c-menu-accordeon').find('.selectionne').should('have.length', 1);
        cy.get('c-menu-accordeon').find('c-menu-sous-item').eq(1).find('a').should('have.class', 'selectionne');
        cy.get('c-menu-accordeon').find('c-menu-item').eq(0).find('.chevron').should('not.have.class', 'tourne');
        cy.get('c-menu-accordeon').find('c-menu-item').eq(1).find('.chevron').should('not.have.class', 'tourne');
        cy.get('c-menu-accordeon').find('c-menu-item').eq(2).find('.chevron').should('have.class', 'tourne');
        cy.get('c-menu-accordeon')
            .find('.selectionne')
            .should('have.css', 'background-color')
            .and('be.colored', VALEURS_COULEURS.couleur_neutre_clair);
    });
});
