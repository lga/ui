import '../../public/theme-defaut/theme-defaut.css';
import '../../src/composants/MenuItem.js';

import chaiColors from 'chai-colors';
import { html } from 'lit';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { VALEURS_COULEURS } from './outils_tests';

// permet de comparer des couleurs hex rgb, etc...
chai.use(chaiColors as Chai.ChaiPlugin);

const pastille = `
<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
    <circle cx="16" cy="16" r="14" fill="#B1CC7E"/>
</svg>
`;

describe('Test composant MenuItem', () => {
    it('Item', () => {
        // given
        cy.mount<'c-menu-item'>(html` <c-menu-item libelle="item avec libellé seul"></c-menu-item>`);

        cy.mount<'c-menu-item'>(html` <c-menu-item libelle="item avec sous-libellé" sousLibelle="sous-libellé"></c-menu-item>`);

        cy.mount<'c-menu-item'>(
            html` <c-menu-item libelle="item avec avec icone">
                <div slot="icone">${unsafeSVG(pastille)}</div>
            </c-menu-item>`
        );

        cy.mount<'c-menu-item'>(
            html` <c-menu-item id="item" href="/lien" libelle="item avec sous-libellé et icone" sousLibelle="sous-libellé">
                <div slot="icone">${unsafeSVG(pastille)}</div>
            </c-menu-item>`
        );
        //then
        cy.get('#item').find('.libelle').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_primaire_sombre);
        cy.get('#item').find('.sous-libelle').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_neutre_40);
        cy.get('#item').find('.chevron').should('exist');
    });

    it('Item selectionné', () => {
        // given
        cy.mount<'c-menu-item'>(html`<c-menu-item libelle="item sélectionné" selectionne></c-menu-item>`);
        //then
        cy.get('c-menu-item').find('a').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_neutre_clair);
    });

    it('Item selectionné et chevron tourné', () => {
        // given
        cy.mount<'c-menu-item'>(html`<c-menu-item libelle="item sélectionné et chevron vers le bas" selectionne chevronVersBas></c-menu-item>`);
        //then
        cy.get('c-menu-item').find('a').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_neutre_clair);
    });
});
