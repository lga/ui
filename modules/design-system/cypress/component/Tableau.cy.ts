import '../../public/theme-defaut/theme-defaut.css';
import '../../src/composants/Tableau.js';

import { html } from 'lit';

import type { OptionsTableau } from '../../src/composants/Tableau';

describe('Test Tableau', () => {
    const options: OptionsTableau = {
        entetes: [html`Entête 1`, html`Entête 2`, html`Entête 3`],
        contenuHtml: [
            [html`Contenu 1`, html`Contenu 2`, html`10`],
            [html`Contenu 3`, html`<a href="lien">Contenu 4 sous forme de lien</a>`, html`50`],
            [html`Contenu 5`, html`Contenu 6`, html`10`]
        ],
        ligneFin: [html`Fin 1`, html`Fin 2`, html`Fin 3`]
    };

    it('Test affichage nominal', () => {
        // given
        cy.viewport(500, 500);

        cy.mount<'c-tableau'>(html` <c-tableau .options="${options}"> </c-tableau> `);
        // then
        cy.get('c-tableau').find('thead tr').should('have.length', 1);
        cy.get('c-tableau').find('tbody tr').should('have.length', 3);
        cy.get('c-tableau').find('tfoot tr').should('have.length', 1);
        cy.get('c-tableau').find('tbody tr').eq(1).should('have.css', 'background', 'rgb(209, 213, 219)');
        cy.get('c-tableau').find('tbody tr').eq(1).find('td').eq(1).invoke('text').should('not.contain', '<a');
    });

    it('Test affichage avec jauges', () => {
        // given
        cy.viewport(500, 500);

        options.jaugesDansColonnes = [false, false, true];

        cy.mount<'c-tableau'>(html` <c-tableau .options="${options}"> </c-tableau> `);
        // then
        cy.get('c-tableau')
            .find('tbody tr td')
            .eq(2)
            .should('have.css', 'background', 'linear-gradient(90deg, rgba(55, 65, 81, 0.125) 10%, rgba(0, 0, 0, 0) 0%)');
    });
});
