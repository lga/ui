import '../../src/composants/Legende.js';
import '../../public/theme-defaut/theme-defaut.css';

import chaiColors from 'chai-colors';
import { html } from 'lit';

import { type ItemLegende, Legende } from '../../src/composants/Legende';
import { VALEURS_COULEURS } from './outils_tests';

// permet de comparer des couleurs hex rgb, etc...
chai.use(chaiColors as Chai.ChaiPlugin);

describe('Test composant Legende', () => {
    it('Test affichage Legende type DISCRETE ', () => {
        // given
        cy.viewport(500, 500);

        const MOTIF = 'repeating-linear-gradient(90deg, rgba(255, 255, 255, 1) 0 2px, rgba(0, 0, 0, 0) 1px 5px)';
        const items: ItemLegende[] = [
            { couleur: VALEURS_COULEURS.couleur_succes, libelle: 'Succes' },
            { couleur: VALEURS_COULEURS.couleur_danger, libelle: 'Danger', motif: MOTIF }
        ];

        cy.mount<'c-legende'>(html`
            <c-legende .items=${items} type=${Legende.TYPE_LEGENDE.discrete} style="background-color: #E1E1E1"></c-legende>
        `);

        cy.get('c-legende').find('.dot').eq(0).should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_succes);
        cy.get('c-legende').find('.dot').eq(1).should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_danger);
        cy.get('c-legende')
            .find('.dot')
            .eq(1)
            .should('have.css', 'background-image')
            .and('match', /repeating-linear-gradient/);
    });

    it('Test affichage Legende type CONTINUE ', () => {
        // given
        cy.viewport(500, 500);

        const items: ItemLegende[] = [
            { couleur: VALEURS_COULEURS.couleur_succes, libelle: '0' },
            { couleur: VALEURS_COULEURS.couleur_neutre_20, libelle: '50' },
            { couleur: VALEURS_COULEURS.couleur_danger, libelle: '100' }
        ];

        cy.mount<'c-legende'>(html`
            <c-legende .items=${items} type=${Legende.TYPE_LEGENDE.continue} style="background-color: #E1E1E1"></c-legende>
        `);

        cy.get('c-legende').find('.rond').eq(0).should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_succes);
        cy.get('c-legende').find('.rond').eq(1).should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_danger);
    });
});
