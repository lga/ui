import '../../src/composants/PanneauGlissant.js';

import { html, LitElement } from 'lit';

import { EvenementFermer } from '../../src/evenements/EvenementFermer';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
class ContenuPanneau extends LitElement {
    render() {
        return html`
            <div style="background-color: #B1B1B1;height: 100%">
                <br />
                <p>Contenu panneau enfant</p>
                <br />
                <a id="bouton-fermer" @click="${() => this.dispatchEvent(new EvenementFermer())}">Fermer panneau actuel</a>
                <br />
                <a id="bouton-fermer-tous" @click="${() => this.dispatchEvent(new EvenementFermer(true))}">Fermer tous</a>
                <br />
            </div>
        `;
    }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
class ContenuPanneauParent extends LitElement {
    render() {
        return html`
            <div style="background-color: #90d8ff;height: 100%">
                <br />
                <p>Contenu panneau parent</p>
                <br />
                <c-panneau-glissant>
                    <a id="bouton-ouvrir-enfant" slot="bouton-ouvrir">Ouvrir panneau enfant</a>
                    <t-panneau slot="panneau"></t-panneau>
                </c-panneau-glissant>
                <br />
            </div>
        `;
    }
}
customElements.define('t-panneau', ContenuPanneau);
customElements.define('t-panneau-parent', ContenuPanneauParent);

describe('Test composant PanneauGlissant', () => {
    it('Deplier et replier un panneau simple', () => {
        // given
        cy.viewport(320, 600);
        cy.mount<'c-panneau-glissant'>(html`
            <c-panneau-glissant>
                <a slot="bouton-ouvrir">Ouvrir panneau</a>
                <t-panneau slot="panneau"></t-panneau>
            </c-panneau-glissant>
        `);
        // when-then
        cy.get('a[slot=bouton-ouvrir]').click();
        cy.get('t-panneau').should('be.visible');

        cy.get('#bouton-fermer').click();
        cy.get('t-panneau').should('not.be.visible');
    });

    it('Deplier et replier deux niveaux de panneau', () => {
        // given
        cy.viewport(320, 600);
        cy.mount<'c-panneau-glissant'>(html`
            <c-panneau-glissant>
                <a id="bouton-ouvrir-parent" slot="bouton-ouvrir">Ouvrir panneau parent</a>
                <t-panneau-parent slot="panneau"></t-panneau-parent>
            </c-panneau-glissant>
        `);
        // when-then
        cy.get('#bouton-ouvrir-parent').click();
        cy.get('t-panneau-parent').should('be.visible');
        cy.get('t-panneau').should('not.be.visible');

        cy.get('#bouton-ouvrir-enfant').click();
        cy.get('t-panneau-parent').should('be.visible');
        cy.get('t-panneau').should('be.visible');

        // Fermer uniquement le panneau courant
        cy.get('#bouton-fermer').click();
        cy.get('t-panneau-parent').should('be.visible');
        cy.get('t-panneau').should('not.be.visible');

        cy.get('#bouton-ouvrir-enfant').click();
        cy.get('t-panneau-parent').should('be.visible');
        cy.get('t-panneau').should('be.visible');

        //  Fermer tous les panneaux
        cy.get('#bouton-fermer-tous').click();
        cy.get('t-panneau-parent').should('not.be.visible');
        cy.get('t-panneau').should('not.be.visible');
    });
});
