import '../../public/theme-defaut/theme-defaut.css';
import '../../src/composants/Interrupteur.js';

import { html } from 'lit';

import type { EvenementActionnerInterrupteur } from '../../src/evenements/EvenementActionnerInterrupteur.js';

describe('Test Interrupteur', () => {
    it("Test affichage bouton qu'on active", () => {
        // given
        cy.viewport(500, 500);
        cy.document()
            .then((doc) => {
                doc.addEventListener('actionnerInterrupteur', cy.stub().as('actionnerInterrupteur'));
            })
            .mount<'c-interrupteur'>(html` <c-interrupteur libelle="Activer interrupteur"></c-interrupteur> `);
        // then
        cy.get('c-interrupteur').get('#cadre').should('have.css', 'filter', 'grayscale(1) opacity(0.6)');
        //given
        cy.get('c-interrupteur').click();
        // then
        cy.get('c-interrupteur').get('#cadre').should('have.css', 'filter', 'none');
        cy.get('@actionnerInterrupteur').should('have.been.calledOnce');
        cy.get('@actionnerInterrupteur').should('be.calledWithMatch', (event: EvenementActionnerInterrupteur) => {
            return event.detail.estActive;
        });
    });
    it("Test affichage bouton qu'on désactive", () => {
        // given
        cy.viewport(500, 500);
        cy.document()
            .then((doc) => {
                doc.addEventListener('actionnerInterrupteur', cy.stub().as('actionnerInterrupteur'));
            })
            .mount<'c-interrupteur'>(html` <c-interrupteur ?estActive=${true} libelle="Activer interrupteur"></c-interrupteur> `);
        // then
        cy.get('c-interrupteur').get('#cadre').should('have.css', 'filter', 'none');
        //given
        cy.get('c-interrupteur').click();
        // then
        cy.get('c-interrupteur').get('#cadre').should('have.css', 'filter', 'grayscale(1) opacity(0.6)');
        cy.get('@actionnerInterrupteur').should('have.been.calledOnce');
        cy.get('@actionnerInterrupteur').should('be.calledWithMatch', (event: EvenementActionnerInterrupteur) => {
            return !event.detail.estActive;
        });
    });
});
