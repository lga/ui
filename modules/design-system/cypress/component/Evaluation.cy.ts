import '../../public/theme-defaut/theme-defaut.css';
import '../../src/composants/Evaluation.js';

import chaiColors from 'chai-colors';
import { html } from 'lit';

import { VALEURS_COULEURS } from './outils_tests';

// permet de comparer des couleurs hex rgb, etc...
chai.use(chaiColors as Chai.ChaiPlugin);

describe('Test composant Evaluation', () => {
    it('3 niveaux de note', () => {
        // given
        cy.viewport(320, 400);
        cy.mount<'c-evaluation'>(html`<c-evaluation note="1"></c-evaluation>`);
        cy.mount('<br>');
        cy.mount<'c-evaluation'>(html`<c-evaluation note="2"></c-evaluation>`);
        cy.mount('<br>');
        cy.mount<'c-evaluation'>(html`<c-evaluation note="3"></c-evaluation>`);
        cy.mount('<br>');
        //then
        cy.get('c-evaluation')
            .eq(1)
            .find('.rond')
            .eq(0)
            .should('have.css', 'background-color')
            .and('be.colored', VALEURS_COULEURS.couleur_secondaire);
        cy.get('c-evaluation')
            .eq(1)
            .find('.rond')
            .eq(1)
            .should('have.css', 'background-color')
            .and('be.colored', VALEURS_COULEURS.couleur_secondaire);
        cy.get('c-evaluation').eq(1).find('.rond').eq(2).should('have.css', 'background-color').and('be.colored', 'transparent');
    });
});
