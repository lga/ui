import '../../src/composants/BoiteAvecLien.js';
import '../../public/theme-defaut/theme-defaut.css';

import { html } from 'lit';

describe('Test BoiteAvecLien', () => {
    it('Test affichage desktop', () => {
        // given
        cy.viewport(800, 500);
        cy.mount<'c-boite-avec-lien'>(
            html`<c-boite-avec-lien titre="Titre" libelleLien="En savoir plus sur ça" href="/test"><p slot="contenu">Contenu</p></c-boite-avec-lien>`
        );
        // then
        cy.get('c-boite-avec-lien').contains('Contenu');
    });

    it('Test affichage mobile', () => {
        // given
        cy.viewport(300, 500);
        cy.mount<'c-boite-avec-lien'>(
            html`<c-boite-avec-lien titre="Titre" libelleLien="En savoir plus sur ça" href="/test"><p slot="contenu">Contenu</p></c-boite-avec-lien>`
        );
        // then
        cy.get('c-boite-avec-lien').contains('Contenu');
    });
});
