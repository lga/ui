import '../../src/composants/Tooltip.js';
import '../../public/theme-defaut/theme-defaut.css';

import chaiColors from 'chai-colors';
import { html } from 'lit';

import { VALEURS_COULEURS } from './outils_tests';

// permet de comparer des couleurs hex rgb, etc...
chai.use(chaiColors as Chai.ChaiPlugin);

describe('Test composant Boite', () => {
    it('Test affichage mobile ', () => {
        // given
        cy.viewport(500, 500);
        cy.mount<'c-tooltip'>(html`
            <c-tooltip
                style="visibility: visible"
                titre="Titre"
                .contenu=${html`<div>
                    Contenu
                    <div>Détail</div>
                </div>`}
            ></c-tooltip>
        `);

        cy.get('c-tooltip').find('header').invoke('text').should('contain', 'Titre');
        cy.get('c-tooltip').find('div').eq(0).invoke('text').should('contain', 'Contenu');
        cy.get('c-tooltip').find('div').eq(1).invoke('text').should('contain', 'Détail');
        cy.get('c-tooltip').find('main').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_neutre);
    });
});
