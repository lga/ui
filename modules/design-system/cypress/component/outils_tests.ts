export const VALEURS_COULEURS = {
    couleur_primaire_clair: '#B4C5BE',
    couleur_primaire: '#124F4B',
    couleur_primaire_sombre: '#042F2E',

    couleur_secondaire_clair: '#DBEAFE',
    couleur_secondaire: '#075985',
    couleur_secondaire_sombre: '#082F49',

    couleur_accent_clair: '#FFA185',
    couleur_accent: '#FF6433',
    couleur_accent_sombre: '#B82B00',

    couleur_neutre_clair: '#f3f4f6',
    couleur_neutre: '#374151',
    couleur_neutre_sombre: '#111827',
    couleur_neutre_80: '#4b5563',
    couleur_neutre_60: '#6b7280',
    couleur_neutre_40: '#9ca3af',
    couleur_neutre_20: '#d1d5db',

    couleur_blanc: '#FFFFFF',
    couleur_fond: '#F8F5F0',

    couleur_succes: '#14b8a6',
    couleur_danger: '#B82B00'
};
