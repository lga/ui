import fs from 'fs';
import http from 'http';
import path from 'path';

const PORT = 5555;

interface MappingUrlFichierFixture {
    debutUrl: string;
    fichierFixture: string;
}

interface MappingUrlReponseJson {
    debutUrl: string;
    reponseJson: JSON;
}

const urlsEtFichiers: MappingUrlFichierFixture[] = [
    { debutUrl: '/crater/api/territoires?critere=f', fichierFixture: 'territoire-liste-suggestions-france.json' },
    { debutUrl: '/crater/api/territoires?critere=o', fichierFixture: 'territoire-liste-suggestions-occitanie.json' },
    { debutUrl: '/crater/api/territoires/f', fichierFixture: 'territoire-france.json' },
    { debutUrl: '/crater/api/territoires/o', fichierFixture: 'territoire-occitanie.json' },
    { debutUrl: '/crater/api/diagnostics/f', fichierFixture: 'diagnostic-france.json' },
    { debutUrl: '/crater/api/diagnostics/o', fichierFixture: 'diagnostic-occitanie.json' },
    { debutUrl: '/crater/api/indicateurs', fichierFixture: 'indicateurs-communes.json' }
];

const urlsEtReponsesJson: MappingUrlReponseJson[] = urlsEtFichiers.map((urlEtFichier) => {
    return {
        debutUrl: urlEtFichier.debutUrl,
        reponseJson: JSON.parse(fs.readFileSync(path.join('fixtures', urlEtFichier.fichierFixture), 'utf8'))
    };
});

const serveur = http.createServer((req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');

    if (req.method === 'HEAD' || (req.method === 'GET' && req.url === '/')) {
        res.writeHead(200);
        res.end(JSON.stringify({ message: 'Le serveur de fixtures est UP' }));
    } else if (req.method === 'GET') {
        let reponseJson: string | null = null;

        for (const urlEtReponseJson of urlsEtReponsesJson) {
            if (req.url.startsWith(urlEtReponseJson.debutUrl)) {
                reponseJson = JSON.stringify(urlEtReponseJson.reponseJson);
                break;
            }
        }

        if (reponseJson !== null) {
            res.writeHead(200);
            res.end(reponseJson);
        } else {
            res.writeHead(404);
            res.end(JSON.stringify({ error: `Not Found : l'url ${req.url} ne correspond à aucun couple url/fixture défini` }));
        }
    } else {
        res.writeHead(405);
        res.end(JSON.stringify({ error: 'Le serveur de fixutres implémente uniquement des méthodes de type GET' }));
    }
});

serveur.listen(PORT, () => {
    console.log('Le serveur de fixtures est lancé sur http://127.0.0.1:${PORT}');
});
