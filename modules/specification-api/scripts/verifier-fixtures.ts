import { promises as fs } from 'fs';
import { z } from 'zod';

import { stringifyAvecTri } from '../../base/src/objets';
import { schemas } from '../src';

async function validerFichierSelonSchema(cheminJson: string, schema: z.ZodSchema) {
    try {
        const contenuFichierJson = await fs.readFile(cheminJson, 'utf-8');
        const donneesJson = JSON.parse(contenuFichierJson);
        const resultat = schema.safeParse(donneesJson);

        if (!resultat.success) {
            console.error(
                `Erreur : le fichier ${cheminJson} n'est pas conforme à la spécification de l'api : ${JSON.stringify(resultat.error, null, 2)}`
            );
            process.exit(1);
        } else {
            console.log(`Le fichier ${cheminJson} est conforme à la spécification de l'api.`);
        }
    } catch (error) {
        console.error(`Erreur lors de la lecture du fichier ${cheminJson} : `, error);
        process.exit(1);
    }
}

async function lintFichierJson(cheminJson: string) {
    try {
        const contenuFichierJson = await fs.readFile(cheminJson, 'utf-8');
        const donneesJson = JSON.parse(contenuFichierJson);
        const jsonTrie = stringifyAvecTri(donneesJson);
        await fs.writeFile(cheminJson, jsonTrie);
    } catch (error) {
        console.error(`Erreur lors du lint du fichier ${cheminJson} : `, error);
        process.exit(1);
    }
}

lintFichierJson('./fixtures/territoire-liste-suggestions-france.json');
lintFichierJson('./fixtures/territoire-liste-suggestions-occitanie.json');
lintFichierJson('./fixtures/territoire-france.json');
lintFichierJson('./fixtures/territoire-occitanie.json');
lintFichierJson('./fixtures/diagnostic-france.json');
lintFichierJson('./fixtures/diagnostic-occitanie.json');

validerFichierSelonSchema('./fixtures/territoire-liste-suggestions-france.json', z.array(schemas.TerritoireSynthese));
validerFichierSelonSchema('./fixtures/territoire-liste-suggestions-occitanie.json', z.array(schemas.TerritoireSynthese));

validerFichierSelonSchema('./fixtures/territoire-france.json', schemas.Territoire);
validerFichierSelonSchema('./fixtures/territoire-occitanie.json', schemas.Territoire);

validerFichierSelonSchema('./fixtures/diagnostic-france.json', schemas.Diagnostic);
validerFichierSelonSchema('./fixtures/diagnostic-occitanie.json', schemas.Diagnostic);
