import type { BesoinsAssietteApi, ClimatApi, OccupationSolsApi } from '../specification/api';
import type { CheptelApi } from '../specification/api';
import type { ConsommationApi } from '../specification/api';
import type { DiagnosticApi } from '../specification/api';
import type { EauApi } from '../specification/api';
import type { EnergieApi } from '../specification/api';
import type { IntrantsApi } from '../specification/api';
import type { NbCommunesParOtexApi } from '../specification/api';
import type { PesticidesApi } from '../specification/api';
import type { PolitiqueFonciereApi } from '../specification/api';
import type { PopulationAgricoleApi } from '../specification/api';
import type { PratiquesAgricolesApi } from '../specification/api';
import type { ProductionApi } from '../specification/api';
import type { ProductionsBesoinsApi } from '../specification/api';
import type { ProximiteCommercesApi } from '../specification/api';
import type { SurfaceAgricoleUtileApi } from '../specification/api';
import type { ValeursParClassesAgesApi } from '../specification/api';
import type { ValeursParClassesSuperficiesApi } from '../specification/api';
import type { CodeCategorieTerritoireApi } from '../specification/api';
import type { AnaloguesClimatiquesApi } from '../specification/api';

interface IndicateursParAnnee {
    annee: number;
}

export function ajouterIndicateursAnneeN<I extends IndicateursParAnnee>(indicateursParAnnees: I[], indicateursAnneeN: I) {
    indicateursParAnnees.push(indicateursAnneeN);
    indicateursParAnnees.sort((ia1, ia2) => {
        return ia1.annee - ia2.annee;
    });
}

interface IndicateursParAnneeMois {
    anneeMois: string;
}
export function ajouterIndicateursAnneeMoisN<I extends IndicateursParAnneeMois>(indicateursParAnneesMois: I[], indicateursAnneeMoisN: I) {
    indicateursParAnneesMois.push(indicateursAnneeMoisN);
    indicateursParAnneesMois.sort((ia1, ia2) => {
        return ia1.anneeMois.localeCompare(ia2.anneeMois);
    });
}

export function creerDiagnosticApiVide(
    idTerritoire?: string,
    nomTerritoire?: string,
    categorieTerritoire?: CodeCategorieTerritoireApi
): DiagnosticApi {
    const nbCommunesParOtex: NbCommunesParOtexApi = {
        grandesCultures: 0,
        maraichageHorticulture: 0,
        viticulture: 0,
        fruits: 0,
        bovinLait: 0,
        bovinViande: 0,
        bovinMixte: 0,
        ovinsCaprinsAutresHerbivores: 0,
        porcinsVolailles: 0,
        polyculturePolyelevage: 0,
        nonClassees: 0,
        sansExploitations: 0,
        nonRenseigne: 0
    };
    const occupationSols: OccupationSolsApi = {
        superficieTotaleHa: 0,
        superficieArtificialiseeClcHa: 0,
        superficieAgricoleClcHa: 0,
        superficieNaturelleOuForestiereClcHa: 0,
        superficieAgricoleRA2020Ha: 0
    };
    const surfaceAgricoleUtile: SurfaceAgricoleUtileApi = {
        sauTotaleHa: 0,
        sauProductiveHa: 0,
        sauPeuProductiveHa: 0,
        sauBioHa: null,
        sauParGroupeCulture: []
    };
    const cheptels: CheptelApi[] = [
        {
            code: 'BOVINS',
            tetes: 0,
            estEstime: false,
            ugb: 0
        },
        {
            code: 'PORCINS',
            tetes: 0,
            estEstime: false,
            ugb: 0
        },
        {
            code: 'OVINS',
            tetes: 0,
            estEstime: false,
            ugb: 0
        },
        {
            code: 'CAPRINS',
            tetes: 0,
            estEstime: false,
            ugb: 0
        },
        {
            code: 'VOLAILLES',
            tetes: 0,
            estEstime: false,
            ugb: 0
        }
    ];
    const politiqueFonciere: PolitiqueFonciereApi = {
        note: null,
        artificialisation5ansHa: null,
        evolutionMenagesEmplois5ans: null,
        sauParHabitantM2: null,
        rythmeArtificialisationSauPourcent: null,
        evaluationPolitiqueAmenagement: '',
        partLogementsVacants2013Pourcent: null,
        partLogementsVacants2018Pourcent: null
    };
    const consommation: ConsommationApi = {
        tauxPauvrete60Pourcent: null,
        codeNiveauRisquePrecariteAlimentaire: null
    };
    const valeursParClassesAges: ValeursParClassesAgesApi = {
        moins_40_ans: null,
        de_40_a_49_ans: null,
        de_50_a_59_ans: null,
        plus_60_ans: null
    };
    const valeursParClassesSuperficies: ValeursParClassesSuperficiesApi = {
        moins_20_ha: null,
        de_20_a_50_ha: null,
        de_50_a_100_ha: null,
        de_100_a_200_ha: null,
        plus_200_ha: null
    };
    const populationAgricole: PopulationAgricoleApi = {
        note: null,
        populationAgricole1988: null,
        populationAgricole2010: null,
        partPopulationAgricole2010Pourcent: null,
        partPopulationAgricole1988Pourcent: null,
        nbExploitations1988: null,
        nbExploitations2010: null,
        sauHa1988: null,
        sauHa2010: null,
        sauMoyenneParExploitationHa1988: null,
        sauMoyenneParExploitationHa2010: null,
        nbExploitationsParClassesAgesChefExploitation: valeursParClassesAges,
        nbExploitationsParClassesSuperficies: valeursParClassesSuperficies,
        sauHaParClassesSuperficies: valeursParClassesSuperficies
    };
    const intrants: IntrantsApi = {
        note: null
    };
    const eau: EauApi = {
        note: null,
        irrigation: {
            note: null,
            irrigationM3: null,
            irrigationMM: null,
            indicateursParAnnees: [],
            pratiquesIrrigationParCultures: []
        },
        arretesSecheresse: {
            note: null,
            tauxImpactArretesSecheressePourcent: 0,
            indicateursParAnneesMois: []
        }
    };
    const energie: EnergieApi = {
        note: null,
        codePostePrincipal: 'ALIMENTATION_ANIMALE',
        energiePrimaireGJ: null,
        energiePrimaireGJParHa: null,
        postes: []
    };

    const pesticides: PesticidesApi = {
        note: null,
        noduNormalise: null,
        indicateursParAnnees: []
    };
    const pratiquesAgricoles: PratiquesAgricolesApi = {
        partSauBioPourcent: null,
        indicateurHvn: {
            indice1: null,
            indice2: null,
            indice3: null,
            indiceTotal: null
        }
    };
    const besoinsAssiette: BesoinsAssietteApi = {
        besoinsHa: null,
        tauxAdequationBrutPourcent: null,
        tauxAdequationMoyenPonderePourcent: null,
        besoinsParGroupeCulture: []
    };
    const productionsBesoins: ProductionsBesoinsApi = {
        besoinsAssietteActuelle: besoinsAssiette,
        besoinsAssietteDemitarienne: Object.assign({}, besoinsAssiette)
    };
    const production: ProductionApi = {
        note: null,
        tauxAdequationMoyenPonderePourcent: null,
        partSauBioPourcent: null,
        indiceHvn: null,
        noteTauxAdequationMoyenPondere: null,
        notePartSauBio: null,
        noteHvn: null
    };
    const proximiteCommerces: ProximiteCommercesApi = {
        note: null,
        partPopulationDependanteVoiturePourcent: null,
        partTerritoireDependantVoiturePourcent: null,
        indicateursParTypesCommerces: []
    };

    const analoguesClimatiques: AnaloguesClimatiquesApi = {};

    const climat: ClimatApi = {
        analoguesClimatiques: analoguesClimatiques
    };

    return {
        idTerritoire: idTerritoire ?? '',
        nomTerritoire: nomTerritoire ?? '',
        categorieTerritoire: categorieTerritoire ?? 'COMMUNE',
        boundingBoxTerritoire: {
            latitudeMin: -10,
            latitudeMax: 10,
            longitudeMin: -10,
            longitudeMax: 10
        },
        idDepartement: '',
        population: null,
        densitePopulationHabParKm2: null,
        idTerritoireParcel: null,
        nbCommunes: 0,
        profils: {
            codeProfilAgriculture: 'GRANDES_CULTURES',
            codeProfilCollectivite: 'BOURG_RURAL',
            codeProfilDiagnostic: 'DEFAUT'
        },
        ugbParHa: null,
        codeOtexMajoritaire5Postes: 'SANS_OTEX_MAJORITAIRE',
        nbCommunesParOtex: nbCommunesParOtex,
        occupationSols: occupationSols,
        surfaceAgricoleUtile: surfaceAgricoleUtile,
        cheptels: cheptels,
        politiqueFonciere: politiqueFonciere,
        consommation: consommation,
        populationAgricole: populationAgricole,
        intrants: intrants,
        eau: eau,
        energie: energie,
        pesticides: pesticides,
        pratiquesAgricoles: pratiquesAgricoles,
        productionsBesoins: productionsBesoins,
        production: production,
        proximiteCommerces: proximiteCommerces,
        climat: climat
    };
}
