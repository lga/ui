import { describe, expect, it } from 'vitest';

import { GroupeCulture } from './GroupeCulture';

describe("Test de l'énumération GroupeCulture", () => {
    it('Récupérer GroupeCulture depuis code', () => {
        expect(GroupeCulture.fromString('FOU').code).toEqual('FOU');
    });
    it('Calculer nom court', () => {
        expect(GroupeCulture.fromString('FOU').nomCourt).toEqual('FO');
    });
    it('Comparer des groupes cultures', () => {
        expect(GroupeCulture.Cereales.comparer(GroupeCulture.AutresCultures)).toBeLessThan(0);
        expect(GroupeCulture.Cereales.comparer(GroupeCulture.Cereales)).toEqual(0);
    });
    it('Récuperer tous les groupes cultures, dans le bon ordre', () => {
        expect(GroupeCulture.tous.map((g) => g.code)).toStrictEqual(['FOU', 'CER', 'OLP', 'FLC', 'DVC', 'SNC']);
        expect(GroupeCulture.codes).toStrictEqual(['FOU', 'CER', 'OLP', 'FLC', 'DVC', 'SNC']);
    });
    it("Récuperer les groupes cultures pour l'alimentation animale", () => {
        expect(GroupeCulture.groupesAlimentationAnimale.map((g) => g.code)).toStrictEqual(['FOU', 'CER', 'OLP']);
    });
    it('Récuperer les groupes cultures hors SNC', () => {
        expect(GroupeCulture.groupesHorsSNC.map((g) => g.code)).toStrictEqual(['FOU', 'CER', 'OLP', 'FLC', 'DVC']);
    });
});
