import type { VoieResilience } from './VoieResilience.js';

export const IDS_VOIES_RESILIENCE = {
    VRL_1: 'VRL-1',
    VRL_2: 'VRL-2',
    VRL_3: 'VRL-3',
    VRL_4: 'VRL-4',
    VRL_5: 'VRL-5',
    VRL_6: 'VRL-6',
    VRL_7: 'VRL-7',
    VRL_8: 'VRL-8',
    VRL_9: 'VRL-9',
    VRL_10: 'VRL-10',
    VRL_11: 'VRL-11'
};

export const DONNEES_VOIES_DE_RESILIENCE: VoieResilience[] = [
    {
        id: IDS_VOIES_RESILIENCE.VRL_1,
        description: `Augmenter la population agricole`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR1_Augmenter_population_agricole.pdf'
    },
    {
        id: IDS_VOIES_RESILIENCE.VRL_2,
        description: `Préserver les terres agricoles`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR2_Preserver_terres_agricoles.pdf'
    },
    {
        id: IDS_VOIES_RESILIENCE.VRL_3,
        description: `Favoriser l'autonomie technique et énergétique des fermes`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR3_Favoriser_autonomie_technique_energetique_des_fermes.pdf'
    },
    {
        id: IDS_VOIES_RESILIENCE.VRL_4,
        description: `Diversifier les variétés cultivées et développer l'autonomie en semences`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR4_Diversifier_varietes_cultivees_et_developper_autonomie_en_semences.pdf'
    },
    {
        id: IDS_VOIES_RESILIENCE.VRL_5,
        description: `Adopter une gestion intégrée de la ressource en eau`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR5_Adopter_gestion_integree_ressource_eau.pdf'
    },
    {
        id: IDS_VOIES_RESILIENCE.VRL_6,
        description: `Évoluer vers une agriculture nourricière`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR6_Evoluer_vers_une_agriculture_nourriciere.pdf'
    },
    {
        id: IDS_VOIES_RESILIENCE.VRL_7,
        description: `Généraliser l'agroécologie`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR7_Generaliser_agroecologie.pdf'
    },
    {
        id: IDS_VOIES_RESILIENCE.VRL_8,
        description: `Développer des outils locaux de stockage et transformation`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR8_developper_outils_locaux_stockage_transformation.pdf'
    },
    {
        id: IDS_VOIES_RESILIENCE.VRL_9,
        description: `Simplifier et raccourcir la logistique et l'achat alimentaire`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR9_simplifier_et_raccourcir_la_logistique_et_achat_alimentaire.pdf'
    },
    {
        id: IDS_VOIES_RESILIENCE.VRL_10,
        description: `Manger plus végétal`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR10_Manger_plus_vegetal.pdf'
    },
    {
        id: IDS_VOIES_RESILIENCE.VRL_11,
        description: `Recycler massivement les nutriments`,
        question: ``,
        lien: '/pdf/Vers_la_resilience_alimentaire_VR11_Recycler_massivement_nutriments.pdf'
    }
];
