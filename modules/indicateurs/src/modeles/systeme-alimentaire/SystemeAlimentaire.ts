import { Domaine, type DonneesDomaine } from '../domaines/Domaine.js';
import type { IndicateurCarte, IndicateurDetaille } from '../indicateurs/indicateurs.js';
import type { DonneesMaillon } from './index.js';
import { Maillon } from './Maillon';
import { OutilExterne } from './OutilExterne';
import { VoieResilience } from './VoieResilience';

export class SystemeAlimentaire {
    public voiesResilience: VoieResilience[] = [];
    public outilsExternes: OutilExterne[] = [];
    public maillons: Maillon[] = [];
    public domaines: Domaine[] = [];

    constructor(
        donneesVoiesResilience: VoieResilience[],
        donneesOutilsExternes: OutilExterne[],
        donneesMaillons: DonneesMaillon[],
        donneesDomaines: DonneesDomaine[]
    ) {
        this.voiesResilience = donneesVoiesResilience.map((i: VoieResilience) => new VoieResilience(i.id, i.description, i.question, i.lien));
        this.outilsExternes = donneesOutilsExternes.map((i: OutilExterne) => new OutilExterne(i.id, i.titre, i.description, i.question, i.lien));
        this.maillons = donneesMaillons.map(
            (m) =>
                new Maillon(
                    m.id,
                    m.nom,
                    m.icone,
                    m.description,
                    m.citations,
                    m.idsVoiesResilience.map((l) => this.getVoieResilience(l)!),
                    m.idsOutilsExternes.map((l) => this.getOutilExterne(l)!),
                    m.informationComplementaire
                )
        );
        this.domaines = donneesDomaines.map((i) => new Domaine(i));
    }

    public getVoieResilience(id: string): VoieResilience | undefined {
        return this.voiesResilience.find((l) => l.id === id);
    }

    public getOutilExterne(id: string): OutilExterne | undefined {
        return this.outilsExternes.find((l) => l.id === id);
    }

    public getMaillon(id: string): Maillon | undefined {
        return this.maillons.find((m) => m.id === id);
    }

    public getDomaine(id: string): Domaine | undefined {
        return this.domaines.find((i) => i.id === id);
    }

    get indicateursDetailles(): IndicateurDetaille[] {
        return this.domaines.flatMap((i) => i.indicateursDetailles);
    }

    public getIndicateurDetaille(id: string): IndicateurDetaille | undefined {
        return this.indicateursDetailles.find((i) => i.id === id);
    }

    public getDomaineParIdDomaineOuIdIndicateurDetaille(idDomaineOuIndicateur: string): Domaine | undefined {
        const domaine = this.getDomaine(idDomaineOuIndicateur);
        const indicateur = this.getIndicateurDetaille(idDomaineOuIndicateur);

        if (domaine) return domaine;
        if (indicateur) return this.getDomaine(indicateur.idDomaine);
        return undefined;
    }

    get indicateursCarte(): IndicateurCarte[] {
        return this.indicateursDetailles.filter((i): i is IndicateurCarte => i.styleCarte !== undefined && i.idMaillon !== undefined);
    }

    public getIndicateurCarte(id: string): IndicateurCarte | undefined {
        return this.indicateursCarte.find((i) => i.id === id);
    }
}
