import { type HtmlString, htmlstring } from '@lga/base';

export interface Glose {
    libelle: string;
    definition: HtmlString;
}

export type CleGlose =
    | 'adventices'
    | 'agricultureBiologique'
    | 'agroecologie'
    | 'agroforesterie'
    | 'assolement'
    | 'autonomieAlimentaire'
    | 'conditionsPedoClimatiques'
    | 'durabilite'
    | 'engraisMinerauxSynthese'
    | 'epci'
    | 'erosion'
    | 'insecuriteAlimentaire'
    | 'pat'
    | 'pac'
    | 'pean'
    | 'plu'
    | 'potentielNourricier'
    | 'menace'
    | 'mesuresMiroirs'
    | 'otex'
    | 'perturbation'
    | 'precariteAlimentaire'
    | 'resilience'
    | 'resilienceAlimentaire'
    | 'safer'
    | 'sau'
    | 'sauProductiveVsPeuProductive'
    | 'sauPeuProductiveHorsPraires'
    | 'scot'
    | 'securiteAlimentaire'
    | 'souveraineteAlimentaire'
    | 'systemeAgroIndustriel'
    | 'systemeAlimentaire'
    | 'terresArables'
    | 'ugb'
    | 'zeroArtificialisation'
    | 'zap';

export const GLOSSAIRE: Record<CleGlose, Glose> = {
    adventices: {
        libelle: 'adventices',
        definition: htmlstring`Plantes non désirées, communément appelées &quot;mauvaises herbes&quot;.`
    },
    agricultureBiologique: {
        libelle: 'agriculture biologique',
        definition: htmlstring`Agriculture promouvant des pratiques préservant l’environnement et les ressources naturelles, et favorables au bien-être et à la santé des animaux, des agriculteurs et des consommateurs. Certifiée par un label européen, elle répond à un cahier des charges précis. En particulier, l’utilisation de produits fertilisants et phytosanitaires de synthèse est interdite.`
    },
    agroecologie: {
        libelle: 'agroécologie',
        definition: htmlstring`Ensemble d’idées et de pratiques consistant à appliquer à l’agronomie des savoirs issus de l’écologie scientifique, dans le but de concevoir des systèmes agraires durables. L’agroécologie ne répond pas à un cahier des charges ou un itinéraire technique précis mais constitue un cadre d’analyse général mettant l’accent sur certains grands principes agronomiques.`
    },
    agroforesterie: {
        libelle: 'agroforesterie',
        definition: htmlstring`Ensemble des pratiques visant à intégrer les arbres dans les espaces agricoles, que ce soit dans une démarche de production ou d’aménagement du paysage.`
    },
    autonomieAlimentaire: {
        libelle: 'autonomie alimentaire',
        definition: htmlstring`Possibilité pour les habitants d’un territoire de subvenir à leurs besoins alimentaires avec les seules ressources de ce territoire.`
    },
    conditionsPedoClimatiques: {
        libelle: 'conditions pédo-climatiques',
        definition: htmlstring`Caractéristiques du sol (profondeur, texture, acidité…) et du climat (évolution des températures, pluviométrie…) qui déterminent en partie les rendements attendus pour différentes cultures.`
    },
    assolement: {
        libelle: 'assolement',
        definition: htmlstring`L’assolement correspond à la répartition des cultures dans l’espace, à l’échelle d’une exploitation ou d’un territoire.`
    },
    durabilite: {
        libelle: 'durabilité',
        definition: htmlstring`Caractéristique d'un système dont le fonctionnement répond aux besoins du présent sans compromettre les capacités des générations futures à répondre aux leurs.`
    },
    engraisMinerauxSynthese: {
        libelle: 'engrais minéraux de synthèse',
        definition: htmlstring`Produits fertilisants, riches en nutriments comme l'azote, le phosphore et le potassium, dont la fabrication repose sur des procédés de chimie industrielle et sur l’exploitation de gisements miniers (phosphore et potassium).`
    },
    epci: {
        libelle: 'EPCI',
        definition: htmlstring`Etablissement public de coopération intercommunale.`
    },
    erosion: {
        libelle: 'érosion',
        definition: htmlstring`Perte progressive de petites particules, emportées par le vent ou les pluies.`
    },
    insecuriteAlimentaire: {
        libelle: 'insécurité alimentaire',
        definition: htmlstring`Une personne est en situation d’insécurité alimentaire lorsqu’elle n’a pas accès à une nourriture suffisante, saine et nutritive lui permettant de satisfaire ses besoins et préférences alimentaires et de mener une vie saine et active.`
    },
    menace: {
        libelle: 'menace',
        definition: htmlstring`contexte susceptible de produire des perturbations touchant un ou plusieurs maillons des systèmes alimentaires. Plus une menace s’aggrave, plus ces perturbations sont fréquentes et intenses. Exemples : dérèglement climatique, épuisement des ressources pétrolières.`
    },
    mesuresMiroirs: {
        libelle: 'mesures-miroirs',
        definition: htmlstring`Ces mesures conditionnent l’accès au marché européen au respect des règles fixées par l’UE à ses propres agriculteurs en matière de protection de l’environnement, de la santé, et du bien-être animal.`
    },
    otex: {
        libelle: 'OTEX',
        definition: htmlstring`Les exploitations sont classées selon leur spécialisation : l'orientation technico-économique (OTEX). Ce classement se fait à  partir des coefficients de production brute standard (PBS). Une exploitation est spécialisée dans un domaine si la PBS de la ou des productions concernées dépasse deux tiers du total. <TexteLien url=https://agreste.agriculture.gouv.fr/agreste-web/methodon/N-Otex/methodon/>Voir Agreste</TexteLien>.`
    },
    pac: {
        libelle: 'PAC',
        definition: htmlstring`Politique Agricole Commune.`
    },
    pat: {
        libelle: 'PAT',
        definition: htmlstring`Projet alimentaire territorial. Les PAT ont pour objectif de relocaliser l'agriculture et l'alimentation dans les territoires en soutenant l'installation d'agriculteurs, les circuits courts ou les produits locaux dans les cantines. Issus de la Loi d'avenir pour l'agriculture qui encourage leur développement depuis 2014, ils sont élaborés de manière collective à l’initiative des acteurs d'un territoire (collectivités, entreprises agricoles et agroalimentaires, artisans, citoyens etc.).`
    },
    pean: {
        libelle: 'PEAN',
        definition: htmlstring`Périmètre de protection des Espaces Agricoles et Naturels`
    },
    perturbation: {
        libelle: 'perturbation',
        definition: htmlstring`Tendance ou événement affectant les systèmes alimentaires. Les perturbations graduelles (ou stress) ont des conséquences diffuses et progressives. Elles sont relativement prévisibles, bien que leur dynamique soit variable dans l’espace et le temps. Elles conduisent à des dégradations de fond des maillons constitutifs des systèmes alimentaires et peuvent accroître la vulnérabilité de ces derniers face aux perturbations brutales. Exemple : diminution du niveau moyen d’humidité des sols agricoles, déclin des insectes auxiliaires de cultures. Les perturbations brutales (ou chocs) correspondent quant à elles à des événements soudains, peu prévisibles, provoquant des situations de crise aux conséquences plus ou moins graves selon leur durée, leur intensité, et la vulnérabilité des éléments affectés. Exemples : vague de chaleur, choc pétrolier.`
    },
    plu: {
        libelle: 'PLU',
        definition: htmlstring`Plan local d'urbanisme`
    },
    potentielNourricier: {
        libelle: 'potentiel nourricier',
        definition: htmlstring`Part de la population qui pourrait en théorie être nourrie avec les productions agricoles du territoire.`
    },
    precariteAlimentaire: {
        libelle: 'précarité alimentaire',
        definition: htmlstring`Forme d’insécurité alimentaire résultant de la précarité économique. Le terme est notamment employé dans les pays du Nord pour souligner les conséquences de la pauvreté sur l’accès à l’alimentation.`
    },
    resilience: {
        libelle: 'résilience',
        definition: htmlstring`Capacité d’un système à maintenir ou à retrouver ses fonctions essentielles lorsqu’il est soumis à une perturbation.`
    },
    resilienceAlimentaire: {
        libelle: 'résilience alimentaire',
        definition: htmlstring`Capacité d’un système alimentaire et de ses éléments constitutifs à garantir la sécurité alimentaire au cours du temps, malgré des perturbations variées et non prévues.`
    },
    safer: {
        libelle: 'SAFER',
        definition: htmlstring`Sociétés d’Aménagement Foncier et d’Établissement Rural, organismes sous tutelle de l’État qui, entre autres missions, encadrent les transactions foncières.`
    },
    sau: {
        libelle: 'surface agricole utile (SAU)',
        definition: htmlstring`La surface agricole utile (SAU) est un indicateur statistique destiné à évaluer la surface consacrée à la production agricole. La SAU est composée de terres arables (grandes cultures, cultures maraîchères, prairies artificielles...), surfaces toujours en herbe (prairies permanentes, alpages) et cultures pérennes (vignes, vergers...). Elle n'inclut pas les bois et forêts. Elle comprend en revanche les surfaces en jachère (comprises dans les terres arables). En France en 2020, la SAU représente environ 29 millions d'hectares, soit 54 % du territoire national. Elle se répartit en terres arables pour 62 %, en surfaces toujours en herbe pour 34 % et en cultures pérennes pour 4 % .`
    },
    sauProductiveVsPeuProductive: {
        libelle: 'surface agricole utile (SAU) productive/peu productive',
        definition: htmlstring`Dans CRATer, une nomenclature des surfaces agricoles est employée pour faire la distinction entre surfaces dites peu productives et celles dites productives afin d'éviter de surestimer les capacités nourricières d'un territoire – par exemple de moyenne ou haute altitude. Elle est réalisée à partir des catégories de culture du Recensement Parcellaire Graphique (RPG) : les surfaces agricoles peu productives (dites aussi non cultivées) rassemblent les groupes de cultures “17 Estives et landes” (contenant notamment les pâturages d’altitudes), “11 Gel (surfaces gelées sans production)” (contenant les jachères) et une partie de “28 Divers” ; les surfaces agricoles productives rassemblent tous les autres groupes de cultures du RPG. `
    },
    sauPeuProductiveHorsPraires: {
        libelle: 'surface agricole utile (SAU) hors prairies',
        definition: htmlstring`La "SAU productive hors prairies" est la <TexteLien url="#sau-productive-peu-productive">surface agricole utile productive</TexteLien> à laquelle ont été retranchées les prairies. Cette valeur est utilisée notamment dans les indicateurs sur l'eau car elle permet d'exclure les surfaces qui ne sont généralement pas irriguées.`
    },
    scot: {
        libelle: 'SCoT',
        definition: htmlstring`Schéma de Cohérence Territoriale`
    },
    securiteAlimentaire: {
        libelle: 'sécurité alimentaire',
        definition: htmlstring`Assurée sur un territoire lorsque tous ses habitants ont à tout moment la possibilité physique, sociale et économique de se procurer une nourriture suffisante, saine et nutritive leur permettant de satisfaire leurs besoins et préférences alimentaires pour mener une vie saine et active.`
    },
    souveraineteAlimentaire: {
        libelle: 'souveraineté alimentaire',
        definition: htmlstring`Possibilité d’organiser le système alimentaire d’un territoire selon les choix politiques de ses habitants, en particulier pour ce qui relève des conditions sociales et environnementales de production.`
    },
    systemeAgroIndustriel: {
        libelle: 'système agro-industriel',
        definition: htmlstring`Système alimentaire dominant caractérisé par l’importance de l’industrie et des grandes firmes multinationales dans son fonctionnement. Ses attributs sont ceux d’autres secteurs industriels : productivisme, standardisation, concentration, financiarisation, mondialisation.`
    },
    systemeAlimentaire: {
        libelle: 'système alimentaire',
        definition: htmlstring`Ensemble des activités qui permettent de produire, transformer, transporter, consommer les aliments qui nourrissent quotidiennement la population.`
    },
    terresArables: {
        libelle: 'terres arables',
        definition: htmlstring`On entend par terres arables les surfaces agricoles ayant un potentiel cultivable ; les surfaces peu productives comme les estives et les parcours en sont donc exclues, de même que les prairies permanentes faute d’informations détaillées sur leur potentiel cultivable.`
    },
    ugb: {
        libelle: 'Unité Gros Bétail (UGB)',
        definition: htmlstring`Il s'agit de l'unité de référence permettant de calculer les besoins nutritionnels ou alimentaires de chaque type d’animal d'élevage et donc de les comparer entre eux. Ici, nous utilisons l'UGB Tous Aliments (et non l'UGB Alimentation Grossière) pour comparer tous les animaux entre eux et pas seulement les herbivores. Un Ovin correspond par exemple à un UGB de 0.15 alors qu'un Bovin adulte a un UGB de 1.`
    },
    zap: {
        libelle: 'ZAP',
        definition: htmlstring`Zone Agricole Protégée`
    },
    zeroArtificialisation: {
        libelle: 'Zéro Artificialisation',
        definition: htmlstring`L'artificialisation est la transformation d'un sol naturel, agricole ou forestier pour l'affecter à des fonctions urbaines ou de transport (habitat, activités, commerces, infrastructures, équipements publics…). Cette transformation entraîne une imperméabilisation partielle ou totale et a des répercussions directes sur l’environnement. L'objectif Zéro Artificialisation consiste à stopper la consommation de nouveaux espaces.`
    }
};
