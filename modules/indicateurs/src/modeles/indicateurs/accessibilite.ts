import type { CodeNiveauRisquePrecariteAlimentaireApi } from '@lga/specification-api';
import { type FonctionCalculerLibelleTooltip, StyleCarteChorochromatique } from '@lga/styles-carte';

interface ClasseNiveauRisquePrecariteAlimentaire {
    code: CodeNiveauRisquePrecariteAlimentaireApi;
    couleur: string;
    libelle: string;
}

export const CLASSES_NIVEAU_RISQUE_PRECARITE_ALIMENTAIRE: ClasseNiveauRisquePrecariteAlimentaire[] = [
    {
        code: '1_TRES_BAS',
        libelle: 'Très bas',
        couleur: '#FFEEF1'
    },
    {
        code: '2_BAS',
        libelle: 'Bas',
        couleur: '#FFAAB9'
    },
    {
        code: '3_MOYEN',
        libelle: 'Moyen',
        couleur: '#FF6A85'
    },
    {
        code: '4_ELEVE',
        libelle: 'Elevé',
        couleur: '#CB1843'
    },
    {
        code: '5_TRES_ELEVE',
        libelle: 'Très élevé',
        couleur: '#7A112A'
    }
];

const calculerLibelle: FonctionCalculerLibelleTooltip = (valeur: number | string | null) => {
    return (
        'Risque de précarité alimentaire ' +
        (CLASSES_NIVEAU_RISQUE_PRECARITE_ALIMENTAIRE.find((classe) => classe.code === valeur)?.libelle ?? 'Non disponible').toLocaleLowerCase()
    );
};

export const styleCartePrecariteAlimentaire = new StyleCarteChorochromatique({
    couleurContours: 'grey',
    classes: CLASSES_NIVEAU_RISQUE_PRECARITE_ALIMENTAIRE,
    calculerLibelleTooltipPersonnalise: calculerLibelle
});
