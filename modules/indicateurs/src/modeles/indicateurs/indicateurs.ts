import type { HtmlString } from '@lga/base';
import type { StyleCarte } from '@lga/styles-carte';

import type { DefinitionRapportEtude, DefinitionSourceDonnee, DefinitionSourceDonneeRetraitee } from '../sources-donnees/sources-donnees.js';

export interface SourceIndicateur {
    source: DefinitionSourceDonnee | DefinitionSourceDonneeRetraitee | DefinitionIndicateur | DefinitionRapportEtude;
    anneesMobilisees?: string;
}

export interface DefinitionIndicateur {
    id: string;
    libelle: string;
    description: HtmlString;
    descriptionDetaillee?: HtmlString;
    valeur?: number;
    unite: string;
    sources: SourceIndicateur[];
    estIndicateurNational?: boolean;
    nomIndicateurRequeteApi?: string;
    nomIndicateurComplementaireRequeteApi?: string;
    styleCarte?: StyleCarte;
}

export interface IndicateurDetaille extends DefinitionIndicateur {
    idDomaine: string;
    idMaillon: string;
    estScore: boolean;
    icone: string;
}

export interface IndicateurCarte extends IndicateurDetaille {
    nomIndicateurRequeteApi: string;
    styleCarte: StyleCarte;
}

export interface ValeurIndicateur {
    indicateur: DefinitionIndicateur;
    chiffre: string | null;
    unite: string;
    chiffreComplement?: string;
}
