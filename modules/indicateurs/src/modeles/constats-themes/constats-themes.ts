import { type IdConstat, IDS_CONSTATS } from './IdConstat';
import type { IdTheme } from './IdTheme';

export type IdConstatIdTheme = {
    [key in IdConstat]: IdTheme;
};
export const MAP_ID_CONSTAT_ID_THEME: IdConstatIdTheme = {
    'terres-insuffisantes': 'terres-agricoles',
    'terres-partiellement-suffisantes': 'terres-agricoles',
    'terres-suffisantes': 'terres-agricoles',
    'precarite-alimentaire': 'accessibilite',
    'commerces-alimentaires': 'accessibilite',
    'evolution-population-agricole': 'agriculteurs',
    revenus: 'agriculteurs',
    distances: 'autonomie-alimentaire',
    'paysages-agricoles': 'biodiversite',
    haies: 'biodiversite',
    'analogue-climatique': 'climat',
    'regime-alimentaire-trop-carne': 'consommation',
    'dependances-ressources': 'ressources',
    'alimentation-animale-importee': 'ressources',
    'prelevements-eau': 'ressources'
};

export function getIdsConstatsDepuisIdsTheme(idTheme: IdTheme): IdConstat[] {
    return Object.values(IDS_CONSTATS).filter((idConstat) => MAP_ID_CONSTAT_ID_THEME[idConstat] === idTheme);
}

export type IdThemeIdsConstats = {
    [key in IdTheme]: IdConstat[];
};

export const MAP_ID_THEME_IDS_CONSTATS: IdThemeIdsConstats = {
    'risques-securite-alimentaire': [],
    'terres-agricoles': getIdsConstatsDepuisIdsTheme('terres-agricoles'),
    'autonomie-alimentaire': getIdsConstatsDepuisIdsTheme('autonomie-alimentaire'),
    consommation: getIdsConstatsDepuisIdsTheme('consommation'),
    accessibilite: getIdsConstatsDepuisIdsTheme('accessibilite'),
    agriculteurs: getIdsConstatsDepuisIdsTheme('agriculteurs'),
    ressources: getIdsConstatsDepuisIdsTheme('ressources'),
    biodiversite: getIdsConstatsDepuisIdsTheme('biodiversite'),
    climat: getIdsConstatsDepuisIdsTheme('climat'),
    'transformation-distribution': []
};

export type IdConstatOuTheme = IdConstat | IdTheme;
