export function getApiBaseUrl() {
    const apiBaseUrl: string | undefined = import.meta.env.VITE_API_BASE_URL;
    return supprimerSlashFinal(apiBaseUrl);
}

export function getTefBaseUrl() {
    const tefBaseUrl: string | undefined = import.meta.env.VITE_TEF_BASE_URL;
    return supprimerSlashFinal(tefBaseUrl);
}

function supprimerSlashFinal(url: string | undefined): string {
    if (url) return url.endsWith('/') ? url.slice(0, -1) : url;
    else return ''; // dans certains tests (par ex BarreMenuTerritoires, la variable VITE_API_BASE_URL n'est pas positionnée)
}
