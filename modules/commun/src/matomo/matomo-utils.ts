declare global {
    interface Window {
        _paq: unknown[];
    }
}

let urlPrecedante = '';

const ID_SITE_MATOMO_CRATER_PROD = '7';
const ID_SITE_MATOMO_CRATER_DEV = '4';
const ID_SITE_MATOMO_TAT_OLD_DEV = '8';
const ID_SITE_MATOMO_TAT_OLD_PROD = '9';

window._paq = window._paq || [];

initaliserSuiviAudience();

function initaliserSuiviAudience() {
    if (
        import.meta.env.VITE_MATOMO_SITE_ID === ID_SITE_MATOMO_CRATER_DEV ||
        import.meta.env.VITE_MATOMO_SITE_ID === ID_SITE_MATOMO_CRATER_PROD ||
        import.meta.env.VITE_MATOMO_SITE_ID === ID_SITE_MATOMO_TAT_OLD_DEV ||
        import.meta.env.VITE_MATOMO_SITE_ID === ID_SITE_MATOMO_TAT_OLD_PROD
    ) {
        (function () {
            const baseUrlMatomo = 'https://matomo.resiliencealimentaire.org/';
            window._paq.push(['setTrackerUrl', baseUrlMatomo + 'matomo.php']);
            window._paq.push(['setSiteId', import.meta.env.VITE_MATOMO_SITE_ID]);
            // const scriptMatomo = document.getElementById('script-matomo');
            const scriptMatomo = document.createElement('script');
            scriptMatomo.async = true;
            scriptMatomo.src = baseUrlMatomo + 'matomo.js';
            document.querySelector('head')?.appendChild(scriptMatomo);
        })();
    }
}

export function enregistrerSuiviPage(url: string, titre: string) {
    window._paq.push(['setCustomUrl', url]);
    window._paq.push(['setDocumentTitle', titre]);
    if (urlPrecedante) {
        window._paq.push(['setReferrerUrl', urlPrecedante]);
    }
    urlPrecedante = url;
    window._paq.push(['trackPageView']);
    window._paq.push(['enableLinkTracking']);
    window._paq.push(['enableHeartBeatTimer']);
}

// Voir https://developer.matomo.org/guides/tracking-javascript-guide#optional-creating-a-custom-opt-out-form
export function recupererStatutConsentement(callbackSetConsentement: (consentement: boolean) => void): void {
    window._paq.push([
        function () {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            if (this.isUserOptedOut()) {
                callbackSetConsentement(false);
            } else {
                callbackSetConsentement(true);
            }
        }
    ]);
}

export function enregistrerRefusConsentement() {
    window._paq.push(['optUserOut']);
}

export function enregistrerAccordConsentement() {
    window._paq.push(['forgetUserOptOut']);
}
