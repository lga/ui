import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, state } from 'lit/decorators.js';

import { enregistrerAccordConsentement, enregistrerRefusConsentement, recupererStatutConsentement } from './matomo-utils';

declare global {
    interface HTMLElementTagNameMap {
        'c-gestion-consentement-matomo': GestionConsentementMatomo;
    }
}

@customElement('c-gestion-consentement-matomo')
export class GestionConsentementMatomo extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            main {
                padding: 1rem;
                border: 1px solid var(--couleur-neutre);
                background-color: var(--couleur-neutre-20);
            }
        `
    ];

    private cookiesDesactives = !navigator || !navigator.cookieEnabled;

    @state()
    estOKConsentement?: boolean;

    render() {
        const contenuErreurCookiesDesactives = html` <p class="texte-moyen">
            La fonctionnalité de désactivation du suivi nécessite que les cookies soient autorisés.
        </p>`;

        const contenuAccordConsentement = html` <p>
                Vous pouvez vous opposer au suivi de votre navigation sur ce site web. Cela protégera votre vie privée, mais empêchera également le
                propriétaire d'apprendre de vos actions et de créer une meilleure expérience pour vous et les autres utilisateurs.
            </p>
            <input @click=${this.desactiverSuivi} id="trackVisits" type="checkbox" checked="checked" />
            <label for="trackVisits"
                ><strong><span class="texte-moyen"> Vous n'êtes pas exclu(e). Décochez cette case pour vous exclure. </span></strong></label
            >`;
        const contenuRefusConsentement = html`<p class="texte-moyen">
                Cookie d'exclusion installé.<br />Vos visites sur ce site web ne seront PAS enregistrées par notre outil d'analyse web.<br />
                Note : si vous nettoyez vos cookies et supprimez le cookie d'exclusion, ou bien si vous changez d'ordinateur et/ou de navigateur, il
                vous faudra de nouveau effectuer la procédure d'exclusion.
            </p>
            <input @click=${this.activerSuivi} id="trackVisits" type="checkbox" />
            <label for="trackVisits"
                ><strong
                    ><span class="texte-moyen"> Vous n'êtes actuellement pas suivi(e). Cochez cette case pour ne plus être exclu(e). </span></strong
                ></label
            >`;
        const contenuStatutConsentementInconnu = html`<p class="texte-moyen">Statut du consentement inconnu ou en cours de récupération.</p> `;

        return html`
            <main>
                ${this.cookiesDesactives
                    ? contenuErreurCookiesDesactives
                    : this.estOKConsentement === undefined
                      ? contenuStatutConsentementInconnu
                      : this.estOKConsentement
                        ? contenuAccordConsentement
                        : contenuRefusConsentement}
            </main>
        `;
    }

    firstUpdated() {
        this.majStatutConsentement();
    }

    private activerSuivi() {
        enregistrerAccordConsentement();
        this.majStatutConsentement();
    }

    private desactiverSuivi() {
        enregistrerRefusConsentement();
        this.majStatutConsentement();
    }

    private majStatutConsentement() {
        recupererStatutConsentement((statutConsentement) => {
            this.estOKConsentement = statutConsentement;
        });
    }
}
