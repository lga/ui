import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable';

export class EvenementErreur extends EvenementPersonnalisable<{ codeErreur: string; messageErreur: string }> {
    static ID = 'erreur';
    constructor(codeErreur: string, messageErreur: string) {
        super(EvenementErreur.ID, { codeErreur: codeErreur, messageErreur: messageErreur });
    }
}
