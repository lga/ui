import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable';

import type { DonneesPageSimple } from './donnees-pages';

export class EvenementMajDonneesPage<D extends DonneesPageSimple> extends EvenementPersonnalisable<{ donneesPage: D }> {
    static ID = 'majDonneesPage';
    constructor(donneesPage: D) {
        super(EvenementMajDonneesPage.ID, { donneesPage: donneesPage });
    }
}
