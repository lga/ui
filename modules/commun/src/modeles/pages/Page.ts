import { type DonneesPageSimple, majDonneesPage } from './donnees-pages';
import type { ModelePage } from './ModelePage';

export class Page<D extends DonneesPageSimple> {
    constructor(
        public readonly modelePage: ModelePage<D>,
        public donnees: D = {} as D
    ) {}

    getId(): string {
        return this.modelePage.getId(this.donnees);
    }

    getUrl(): string {
        return this.modelePage.getUrl(this.donnees);
    }

    getUrlCanonique(): string {
        return this.modelePage.getUrlCanonique(this.donnees);
    }

    getTitreLong(): string {
        return this.modelePage.getTitreLong(this.donnees);
    }

    getMetaDescription(): string | undefined {
        return this.modelePage.getMetaDescription(this.donnees);
    }

    majDonnees(donneesMaj: Partial<D>) {
        this.donnees = majDonneesPage(this.donnees, donneesMaj);
    }
}
