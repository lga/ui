import type { DonneesPageSimple } from './donnees-pages';
import { nettoyerParamSansValeur } from './url-utils';

export interface OptionsModelePage<D extends DonneesPageSimple> {
    url: (donneesPage: D) => string;
    urlCanonique?: (donneesPage: D) => string;
    titreCourt: string;
    titreLong?: (donneesPage: D) => string;
    metaDescription?: (donneesPage: D) => string;
}

export class ModelePage<D extends DonneesPageSimple> {
    constructor(private readonly options: OptionsModelePage<D>) {}

    private getUrlNettoyee(donneesPage: D) {
        return nettoyerParamSansValeur(this.options.url(donneesPage)).toLowerCase();
    }
    getUrl(donneesPage: D): string {
        return donneesPage.idElementCibleScroll
            ? `${this.getUrlNettoyee(donneesPage)}#${donneesPage.idElementCibleScroll}`
            : this.getUrlNettoyee(donneesPage);
    }
    private getUrlPath(donneesPage: D): string {
        return this.getUrlNettoyee(donneesPage).split(/\?|#/g)[0];
    }
    getId(donneesPage: D): string {
        return this.getUrlPath(donneesPage);
    }
    getTitreCourt(): string {
        return this.options.titreCourt;
    }
    getTitreLong(donneesPage: D): string {
        return this.options.titreLong ? this.options.titreLong(donneesPage) : this.getTitreCourt();
    }
    getUrlCanonique(donneesPage: D): string {
        return this.options.urlCanonique ? this.options.urlCanonique(donneesPage).toLowerCase() : this.getUrlNettoyee(donneesPage);
    }
    getMetaDescription(donneesPage: D): string | undefined {
        return this.options.metaDescription ? this.options.metaDescription(donneesPage) : undefined;
    }
}
