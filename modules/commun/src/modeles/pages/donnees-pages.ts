export interface DonneesPageSimple {
    idElementCibleScroll?: string;
}

export function majDonneesPage<D extends DonneesPageSimple>(donneesInitiales: D, donneesMaj: Partial<D>) {
    return { ...donneesInitiales, ...donneesMaj };
}
