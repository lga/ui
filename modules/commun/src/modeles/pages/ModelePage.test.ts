import { describe, expect, it } from 'vitest';

import type { DonneesPageSimple } from './donnees-pages';
import { ModelePage } from './ModelePage';

describe('Test de ModelePage', () => {
    it('ModelePage avec définition minimaliste et valeurs par défaut', () => {
        const modelePage = new ModelePage<DonneesPageSimple>({
            url: () => `/accueil`,
            titreCourt: 'Titre court'
        });
        expect(modelePage.getUrl({})).toEqual('/accueil');
        expect(modelePage.getId({})).toEqual('/accueil');
        expect(modelePage.getTitreCourt()).toEqual('Titre court');
        expect(modelePage.getTitreLong({})).toEqual('Titre court');
        expect(modelePage.getUrlCanonique({})).toEqual('/accueil');
        expect(modelePage.getMetaDescription({})).toBeUndefined();
    });

    it('ModelePage avec définition exhaustive', () => {
        const modelePage = new ModelePage<DonneesPageSimple>({
            url: () => `/accueil`,
            titreCourt: 'Titre court',
            titreLong: () => 'Titre long',
            urlCanonique: () => `/urlcanonique`,
            metaDescription: () => `Meta description`
        });
        expect(modelePage.getUrl({})).toEqual('/accueil');
        expect(modelePage.getId({})).toEqual('/accueil');
        expect(modelePage.getTitreCourt()).toEqual('Titre court');
        expect(modelePage.getTitreLong({})).toEqual('Titre long');
        expect(modelePage.getUrlCanonique({})).toEqual('/urlcanonique');
        expect(modelePage.getMetaDescription({})).toEqual('Meta description');
    });

    it('ModelePage avec query param dans url', () => {
        const modelePage = new ModelePage<DonneesPageSimple>({
            url: () => `/accueil?param1=1`,
            titreCourt: 'Titre court'
        });
        expect(modelePage.getId({})).toEqual('/accueil');
        expect(modelePage.getUrl({})).toEqual('/accueil?param1=1');
    });

    it('ModelePage avec hash dans url', () => {
        const modelePage = new ModelePage<DonneesPageSimple>({
            url: () => `/accueil`,
            titreCourt: 'Titre court'
        });
        const donneesPage = { idElementCibleScroll: 'hashid' };

        expect(modelePage.getId(donneesPage)).toEqual('/accueil');
        expect(modelePage.getUrl(donneesPage)).toEqual('/accueil#hashid');
        expect(modelePage.getUrlCanonique(donneesPage)).toEqual('/accueil');
    });

    it('ModelePage avec plusieurs paramètres et hash', () => {
        type DonneesPageTest = DonneesPageSimple & {
            idPage: string;
            nomPage?: string;
            idPathParam?: string;
            idRequestParam?: string;
        };
        const modelePageTest = new ModelePage<DonneesPageTest>({
            url: (c) => `/${c.idPage}/${c.idPathParam}?idRequestParam=${c.idRequestParam}`,
            titreCourt: 'Page',
            titreLong: (c) => `Page, ${c.nomPage}`,
            urlCanonique: (c) => `/${c.idPage}`,
            metaDescription: (c) => `Page ${c.idPage}`
        });
        const donneesPage: DonneesPageTest = {
            idPage: 'idPage',
            nomPage: 'NomPage',
            idPathParam: 'idPathParam',
            idElementCibleScroll: 'idhash'
        };
        expect(modelePageTest.getUrl(donneesPage)).toEqual('/idpage/idpathparam#idhash');
        expect(modelePageTest.getTitreLong(donneesPage)).toEqual('Page, NomPage');
        expect(modelePageTest.getId(donneesPage)).toEqual('/idpage/idpathparam');
        expect(modelePageTest.getUrlCanonique(donneesPage)).toEqual('/idpage');
        expect(modelePageTest.getMetaDescription(donneesPage)).toEqual('Page idPage');
    });
});
