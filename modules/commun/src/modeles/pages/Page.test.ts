import { describe, expect, it } from 'vitest';

import type { DonneesPageSimple } from './donnees-pages';
import { ModelePage } from './ModelePage';
import { Page } from './Page';

describe('Test de Page', () => {
    type DonneesPageTest = DonneesPageSimple & {
        idTerritoirePrincipal: string;
        idEchelleTerritoriale?: string;
        idTerritoireActif?: string;
    };

    const modelePage = new ModelePage<DonneesPageTest>({
        url: (d) => `diagnostics/${d.idTerritoirePrincipal}?echelleterritoriale=${d.idEchelleTerritoriale}`,
        titreCourt: 'Titre Court',
        urlCanonique: (d) => `diagnostics/${d.idTerritoirePrincipal}`,
        titreLong: (d) => `Titre long ${d.idTerritoirePrincipal}`,
        metaDescription: (d) => `Meta description ${d.idTerritoirePrincipal}`
    });

    it('Construction et vérification des attributs', () => {
        const page = new Page(modelePage, { idTerritoirePrincipal: 'france', idEchelleTerritoriale: 'PAYS' });
        expect(page.getId()).toEqual('diagnostics/france');
        expect(page.getUrl()).toEqual('diagnostics/france?echelleterritoriale=pays');
        expect(page.getUrlCanonique()).toEqual('diagnostics/france');
        expect(page.getTitreLong()).toEqual('Titre long france');
        expect(page.getMetaDescription()).toEqual('Meta description france');
    });

    it("Maj d'une partie des données", () => {
        const page = new Page(modelePage, { idTerritoirePrincipal: 'france', idEchelleTerritoriale: 'PAYS' });
        page.majDonnees({ idEchelleTerritoriale: 'REGION', idTerritoireActif: 'occitanie' });
        expect(page.donnees).toEqual({ idTerritoirePrincipal: 'france', idEchelleTerritoriale: 'REGION', idTerritoireActif: 'occitanie' });
    });
});
