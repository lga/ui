import { describe, expect, it } from 'vitest';

import { ModelePageSimple } from './ModelePageSimple';

describe('Test de ModelePageStatique', () => {
    it('Création / consultation page', () => {
        const modelePage = new ModelePageSimple({
            url: `/accueil`,
            titreCourt: 'Titre court',
            titreLong: 'Titre long',
            urlCanonique: `/urlcanonique`,
            metaDescription: `Meta description`
        });
        expect(modelePage.getId()).toEqual('/accueil');
        expect(modelePage.getUrl()).toEqual('/accueil');
        expect(modelePage.getTitreCourt()).toEqual('Titre court');
        expect(modelePage.getTitreLong()).toEqual('Titre long');
        expect(modelePage.getUrlCanonique()).toEqual('/urlcanonique');
        expect(modelePage.getMetaDescription()).toEqual('Meta description');
    });

    it('Création / consultation page avec hash', () => {
        const modelePage = new ModelePageSimple({
            url: `/accueil`,
            titreCourt: 'Titre court',
            titreLong: 'Titre long',
            urlCanonique: `/urlcanonique`
        });
        expect(modelePage.getUrl({ idElementCibleScroll: 'idhash' })).toEqual('/accueil#idhash');
    });
});
