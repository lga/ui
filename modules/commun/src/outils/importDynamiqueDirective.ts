import type { TemplateResult } from 'lit';
import { AsyncDirective, directive } from 'lit/async-directive.js';

class ImportDynamiqueDirective extends AsyncDirective {
    private importAFaire = true;

    render(importPromise: Promise<unknown>, htmlImportFait: TemplateResult, htmlImportEnCours: TemplateResult) {
        if (this.importAFaire) {
            importPromise.then(() => {
                this.importAFaire = false;
                this.setValue(htmlImportFait);
            });
            return htmlImportEnCours;
        } else {
            return htmlImportFait;
        }
    }
}

export const importDynamique = directive(ImportDynamiqueDirective);
