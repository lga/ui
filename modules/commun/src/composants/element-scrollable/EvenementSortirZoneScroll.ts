import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable';

export class EvenementSortirZoneScroll extends EvenementPersonnalisable<{ idElement: string }> {
    static ID = 'sortirZoneScroll';
    constructor(idElement: string) {
        super(EvenementSortirZoneScroll.ID, { idElement: idElement });
    }
}
