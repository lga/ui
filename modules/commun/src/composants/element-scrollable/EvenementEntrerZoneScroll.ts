import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable';

export class EvenementEntrerZoneScroll extends EvenementPersonnalisable<{ idElement: string }> {
    static ID = 'entrerZoneScroll';
    constructor(idElement: string) {
        super(EvenementEntrerZoneScroll.ID, { idElement: idElement });
    }
}
