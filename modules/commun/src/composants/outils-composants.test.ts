import { html } from 'lit';
import { describe, expect, it } from 'vitest';

import { formaterNombreEnEntierHtml, templateResultToString } from './outils-composants';

describe('Tests des fonctions de outils-composants.ts', () => {
    it('Test templateResultToString', () => {
        const nom = 'World';
        // on desactive le formatage prettier pour conserver espaces et \n dans les templates
        // prettier-ignore
        const tr1 = html`
        <div>
            Hello1 ${nom} !
        </div>`;
        // prettier-ignore
        const tr2 = html`
        <div>
            Hello2 ${nom} !${tr1}
        </div>`;
        expect(templateResultToString(tr2)).toEqual('<div> Hello2 World !<div> Hello1 World ! </div> </div>');
    });

    it('Fonction formaterNombreEnEntierHtml', () => {
        expect(templateResultToString(formaterNombreEnEntierHtml(null))).toEqual('-');
        expect(templateResultToString(formaterNombreEnEntierHtml(undefined))).toEqual('-');
        expect(templateResultToString(formaterNombreEnEntierHtml(5))).toEqual('<span class="chiffre">5</span>');
    });
});
