export interface SuggestionTerritoire {
    id: string;
    idTerritoireParcel: string;
    nom: string;
    libellePrincipal: string;
    libelleSecondaire: string;
    estSuggere: boolean;
}
