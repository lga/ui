import type { Context as ContextPageJS } from 'page';

import type { DonneesPageSimple } from './modeles/pages/donnees-pages';
import type { ModelePageSimple } from './modeles/pages/ModelePageSimple';

export function routePageSimple(page: ModelePageSimple): string {
    return page.getUrl();
}

export function creerDonneesInitialesPageSimple(contextPageJS: ContextPageJS): DonneesPageSimple {
    return {
        idElementCibleScroll: contextPageJS.hash
    };
}
