import { describe, expect, it } from 'vitest';

import { StyleCarteCerclesProportionnelsColores } from './StyleCarteCerclesProportionnelsColores';

describe('Test de la classe StyleCarteCerclesProportionnelsColores', () => {
    const calculerClasse = (valeur: number | null): string | null => {
        if (valeur === null) return null;
        if (valeur < 10) {
            return 'CLASSE_1';
        } else return 'CLASSE_2';
    };
    it('Tester les différentes fonctions ', () => {
        const style = new StyleCarteCerclesProportionnelsColores({
            facteurMultiplicatifTailleCercles: 2,
            classes: [
                { code: 'CLASSE_1', libelle: 'Classe 1', couleur: 'red' },
                { code: 'CLASSE_2', libelle: 'Classe 2', couleur: 'yellow' }
            ],
            fonctionNombreVersCodeClasse: calculerClasse
        });
        expect(style.calculerRayon(2)).toBeCloseTo(2.828, 2);
        expect(style.calculerRayon(-2)).toBeCloseTo(2.828, 2);
        expect(style.calculerCouleurCercle(null)).toEqual('#d7d7d7');
        expect(style.calculerCouleurCercle(9)).toEqual('red');
        expect(style.calculerCouleurCercle(10)).toEqual('yellow');
    });
});
