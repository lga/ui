import { formaterNombreEnNDecimalesString } from '@lga/base';

import { GradientCouleurs } from './GradientCouleurs';
import type { FonctionCalculerLibelleTooltip } from './style-carte-utils.js';
import { StyleCarteChoroplethe } from './StyleCarteChoroplethe';

export class StyleCarteChoropletheNotes extends StyleCarteChoroplethe {
    constructor() {
        super({
            gradientCouleurs: new GradientCouleurs([
                { valeur: 0, couleur: '#9b0000' },
                { valeur: 5, couleur: '#f2994a' },
                { valeur: 10, couleur: '#219653' }
            ]),
            couleurContours: 'white',
            labels: [
                { valeur: 0, label: '0' },
                { valeur: 2, label: '2' },
                { valeur: 4, label: '4' },
                { valeur: 6, label: '6' },
                { valeur: 8, label: '8' },
                { valeur: 10, label: '10' }
            ]
        });
    }

    calculerLibelleTooltip: FonctionCalculerLibelleTooltip = (valeur: number | string | null) => {
        if (valeur === null) return '-';
        return `Score : ${typeof valeur == 'string' ? valeur : formaterNombreEnNDecimalesString(valeur, 0)} / 10`;
    };
}
