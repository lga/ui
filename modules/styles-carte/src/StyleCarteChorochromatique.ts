import { COULEUR_VALEUR_NULL } from './couleurs-carte.js';
import type { FonctionCalculerLibelleTooltip, SeuilLegende } from './style-carte-utils.js';
import { type AttributsStyleCarte, StyleCarte } from './StyleCarte';

export interface ClasseCarteChorochromatique {
    code: string | null;
    libelle: string;
    couleur: string;
}

export type FonctionNombreVersCodeClasse = (valeur: number | null) => string | null;

interface AttributsStyleCarteChorochromatique extends AttributsStyleCarte {
    readonly couleurContours?: string;
    readonly classes: ClasseCarteChorochromatique[];
    readonly fonctionNombreVersCodeClasse?: FonctionNombreVersCodeClasse;
    readonly calculerLibelleTooltipPersonnalise?: FonctionCalculerLibelleTooltip;
}

export class StyleCarteChorochromatique extends StyleCarte {
    private classes: ClasseCarteChorochromatique[];
    readonly estIndicateurContinu = false;
    private readonly fonctionNombreVersCodeClasse?: FonctionNombreVersCodeClasse;

    constructor(attributs: AttributsStyleCarteChorochromatique) {
        super(attributs.couleurContours, attributs.calculerLibelleTooltipPersonnalise);
        this.classes = attributs.classes;
        this.fonctionNombreVersCodeClasse = attributs.fonctionNombreVersCodeClasse;
    }

    calculerCodeClasse(valeur: number | string | null): string | number | null {
        if (typeof valeur === 'number') {
            return this.fonctionNombreVersCodeClasse ? this.fonctionNombreVersCodeClasse(valeur) : null;
        }
        return valeur;
    }

    calculerCouleurFondFeature(valeur: number | string | null) {
        const classeCarte = this.classes.find((classe) => classe.code === this.calculerCodeClasse(valeur));
        if (classeCarte === undefined || classeCarte === null) {
            return COULEUR_VALEUR_NULL;
        } else {
            return classeCarte.couleur;
        }
    }

    calculerLibelleTooltipDefaut: FonctionCalculerLibelleTooltip = (valeur: number | string | null) => {
        const classeCarte = this.classes.find((classe) => classe.code === this.calculerCodeClasse(valeur));
        if (classeCarte === undefined || classeCarte === null) {
            return 'Valeur : Non disponible';
        } else {
            return `Valeur : ${classeCarte.libelle}`;
        }
    };

    get seuilsLegende(): SeuilLegende[] {
        return this.classes.map((classe) => {
            return { libelle: classe.libelle, couleur: classe.couleur };
        });
    }
}
