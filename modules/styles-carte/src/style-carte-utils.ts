import { formaterNombreSelonValeurString } from '@lga/base';

export interface SeuilLegende {
    couleur: string;
    libelle: string;
}

export type FonctionCalculerLibelleTooltip = (valeur: number | string | null, unite?: string) => string;

export const calculerLibelleTooltipGenerique: FonctionCalculerLibelleTooltip = (valeur: number | string | null, unite?: string) => {
    if (valeur === null) return 'Valeur : Non disponible';
    const formatUnite = unite === '%' ? '%' : unite === undefined || unite === '' ? '' : ` ${unite}`;
    return `Valeur : ${typeof valeur === 'string' ? valeur : formaterNombreSelonValeurString(valeur)}${formatUnite}`;
};
