import { COULEUR_CARTE_GRIS_CONTOURS } from './couleurs-carte.js';
import { calculerLibelleTooltipGenerique, type FonctionCalculerLibelleTooltip, type SeuilLegende } from './style-carte-utils.js';

export interface AttributsStyleCarte {
    readonly couleurContours?: string;
    readonly calculerLibelleTooltipPersonnalise?: FonctionCalculerLibelleTooltip;
    readonly unite?: string;
}

export class StyleCarte {
    readonly estIndicateurContinu: boolean;

    constructor(
        readonly couleurContours = COULEUR_CARTE_GRIS_CONTOURS,
        private readonly calculerLibelleTooltipPersonnalise?: FonctionCalculerLibelleTooltip,
        readonly unite?: string
    ) {
        this.estIndicateurContinu = true;
    }

    calculerCouleurFondFeature(valeur?: number | string | null): string {
        if (valeur !== undefined && valeur !== null) return 'white';
        return 'transparent';
    }

    calculerLibelleTooltipDefaut: FonctionCalculerLibelleTooltip = (valeur: number | string | null, unite?: string) => {
        return calculerLibelleTooltipGenerique(valeur, unite);
    };

    calculerLibelleTooltip: FonctionCalculerLibelleTooltip = (valeur: number | string | null, unite = this.unite) => {
        return this.calculerLibelleTooltipPersonnalise
            ? this.calculerLibelleTooltipPersonnalise(valeur, unite)
            : this.calculerLibelleTooltipDefaut(valeur, unite);
    };

    get seuilsLegende(): SeuilLegende[] | undefined {
        return undefined;
    }
}
