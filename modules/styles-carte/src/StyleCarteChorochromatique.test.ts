import { describe, expect, it } from 'vitest';

import { COULEUR_VALEUR_NULL } from './couleurs-carte.js';
import { StyleCarteChorochromatique } from './StyleCarteChorochromatique';

describe('Test de la classe StyleCarteChorochromatique', () => {
    const style = new StyleCarteChorochromatique({
        couleurContours: 'black',
        classes: [
            { code: 'CLASSE_1', libelle: 'Classe 1', couleur: 'red' },
            { code: 'CLASSE_2', libelle: 'Classe 2', couleur: 'yellow' }
        ]
    });

    it('Test des méthodes ', () => {
        expect(style.estIndicateurContinu).toBeFalsy();
        expect(style.calculerCouleurFondFeature('CLASSE_INCONNUE')).toEqual(COULEUR_VALEUR_NULL);
        expect(style.calculerLibelleTooltip('CLASSE_INCONNUE')).toEqual('Valeur : Non disponible');
        expect(style.calculerCouleurFondFeature('CLASSE_1')).toEqual('red');
        expect(style.calculerLibelleTooltip('CLASSE_1')).toEqual('Valeur : Classe 1');
    });
});

describe('Test de la classe StyleCarteChorochromatique avec fonction conversion valeurs en classes', () => {
    const style = new StyleCarteChorochromatique({
        couleurContours: 'black',
        classes: [
            { code: 'CLASSE_1', libelle: 'Classe 1', couleur: 'red' },
            { code: 'CLASSE_2', libelle: 'Classe 2', couleur: 'yellow' }
        ],
        fonctionNombreVersCodeClasse: (valeur) => {
            if (valeur == null) return null;
            if (valeur === 0) return 'CLASSE_NON_DEFINIE';
            return valeur > 0 ? 'CLASSE_1' : 'CLASSE_2';
        }
    });

    it('Test des méthodes', () => {
        expect(style.calculerCouleurFondFeature(1)).toEqual('red');
        expect(style.calculerLibelleTooltip(1)).toEqual('Valeur : Classe 1');
        expect(style.calculerCouleurFondFeature(-1)).toEqual('yellow');
        expect(style.calculerLibelleTooltip(-1)).toEqual('Valeur : Classe 2');
        expect(style.calculerCouleurFondFeature(0)).toEqual(COULEUR_VALEUR_NULL);
        expect(style.calculerLibelleTooltip(0)).toEqual('Valeur : Non disponible');
        expect(style.calculerCouleurFondFeature('CODE')).toEqual(COULEUR_VALEUR_NULL);
        expect(style.calculerLibelleTooltip('CODE')).toEqual('Valeur : Non disponible');
    });
});
