import { COULEUR_VALEUR_NULL } from './couleurs-carte.js';
import type { GradientCouleurs } from './GradientCouleurs';
import type { SeuilLegende } from './style-carte-utils.js';
import { type AttributsStyleCarte, StyleCarte } from './StyleCarte';

interface ValeurLabel {
    valeur: number;
    label: string;
}

export interface AttributsStyleCarteChoroplethe extends AttributsStyleCarte {
    readonly couleurContours?: string;
    readonly gradientCouleurs: GradientCouleurs;
    readonly labels: ValeurLabel[];
}

export class StyleCarteChoroplethe extends StyleCarte {
    private gradientCouleurs: GradientCouleurs;
    private labels: ValeurLabel[];

    constructor(attributs: AttributsStyleCarteChoroplethe) {
        super(attributs.couleurContours);
        this.gradientCouleurs = attributs.gradientCouleurs;
        this.labels = attributs.labels;
    }

    calculerCouleurFondFeature(valeur: number | null): string {
        return this.gradientCouleurs.calculerCouleur(valeur);
    }

    get seuilsLegende(): SeuilLegende[] {
        return this.labels.map((l, indice) => {
            let couleurIntervalle = COULEUR_VALEUR_NULL;
            if (indice === 0) {
                couleurIntervalle = this.gradientCouleurs.couleurBasse;
            } else if (indice >= this.labels.length - 1) {
                couleurIntervalle = this.gradientCouleurs.couleurHaute;
            } else {
                couleurIntervalle = this.calculerCouleurFondFeature(this.labels[indice].valeur);
            }
            return { libelle: l.label, couleur: couleurIntervalle };
        });
    }
}
