import { describe, expect, it } from 'vitest';

import { GradientCouleurs } from './GradientCouleurs';
import { StyleCarteChoroplethe } from './StyleCarteChoroplethe';

const couleurNote0 = '#9b0000';
const couleurNote2 = '#be3d1e';
const couleurNote5 = '#f2994a';
const couleurNote5_5 = '#dd994b';
const couleurNote10 = '#219653';
const seuilsCouleurs = [
    { valeur: 0, couleur: couleurNote0 },
    { valeur: 5, couleur: couleurNote5 },
    { valeur: 10, couleur: couleurNote10 }
];

describe('Test de la classe StyleCarteChoroplethe', () => {
    const style = new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs(seuilsCouleurs),
        couleurContours: 'red',
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 5.5, label: '5.5' },
            { valeur: 10, label: '10' }
        ]
    });

    it('Tester les différentes fonctions ', () => {
        expect(style.calculerCouleurFondFeature(2)).toEqual(couleurNote2);
        expect(style.calculerLibelleTooltip(2, 'm')).toEqual('Valeur : 2 m');
        expect(style.estIndicateurContinu).toBeTruthy();
        expect(style.seuilsLegende).toEqual([
            {
                couleur: couleurNote0,
                libelle: '0'
            },
            {
                couleur: couleurNote5_5,
                libelle: '5.5'
            },
            {
                couleur: couleurNote10,
                libelle: '10'
            }
        ]);
    });
});
