import type { CodeCategorieTerritoireApi, CodeSousCategorieTerritoireApi } from '@lga/specification-api';

import { CategorieTerritoire } from './CategorieTerritoire';
import { Territoire } from './Territoire.js';

export class HierarchieTerritoires {
    public readonly territoirePrincipal: Territoire;
    public readonly epci: Territoire | undefined;
    public readonly departement: Territoire | undefined;
    public readonly region: Territoire | undefined;
    public readonly pays: Territoire | undefined;

    constructor(
        private id: string,
        private nom: string,
        private codeCategorie: CodeCategorieTerritoireApi,
        private codeSousCategorie: CodeSousCategorieTerritoireApi | null,
        private idEpci: string | undefined,
        private nomEpci: string | undefined,
        private idDepartement: string | undefined,
        private nomDepartement: string | undefined,
        private idRegion: string | undefined,
        private nomRegion: string | undefined,
        private idPays: string | undefined,
        private nomPays: string | undefined
    ) {
        this.territoirePrincipal = new Territoire(id, nom, CategorieTerritoire.creer(codeCategorie, codeSousCategorie));

        if (idEpci !== undefined && nomEpci != undefined) {
            this.epci = new Territoire(idEpci, nomEpci, CategorieTerritoire.Epci);
        } else {
            this.epci = undefined;
        }
        if (idDepartement !== undefined && nomDepartement != undefined) {
            this.departement = new Territoire(idDepartement, nomDepartement, CategorieTerritoire.Departement);
        } else {
            this.departement = undefined;
        }
        if (idRegion !== undefined && nomRegion != undefined) {
            this.region = new Territoire(idRegion, nomRegion, CategorieTerritoire.Region);
        } else {
            this.region = undefined;
        }
        if (idPays !== undefined && nomPays != undefined) {
            this.pays = new Territoire(idPays, nomPays, CategorieTerritoire.Pays);
        } else if (this.territoirePrincipal.categorie === CategorieTerritoire.Pays) {
            this.pays = this.territoirePrincipal;
        } else {
            this.pays = undefined;
        }
    }
    //  TODO basculer dans cette classe la notion de territoire actif
    getListeTerritoires() {
        const hierarchie = new Set<Territoire>();
        hierarchie.add(this.territoirePrincipal);
        if (this.epci) hierarchie.add(this.epci);
        if (this.departement) hierarchie.add(this.departement);
        if (this.region) hierarchie.add(this.region);
        if (this.pays) hierarchie.add(this.pays);

        const hierarchieArray = Array.from(hierarchie.values());
        return hierarchieArray.sort(this.comparerTerritoiresParCategorie);
    }

    private comparerTerritoiresParCategorie(ta: Territoire, tb: Territoire) {
        return ta.categorie.comparer(tb.categorie);
    }
}
