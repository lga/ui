import { describe, expect, it } from 'vitest';

import { EchelleTerritoriale } from './EchelleTerritoriale';

describe("Test de l'énumération EchelleTerritoriale", () => {
    it('Vérification des catégories créées par défaut', () => {
        expect(EchelleTerritoriale.Epcis.categorieTerritoire.codeCategorie).toEqual('EPCI');
        expect(EchelleTerritoriale.listerToutes).toEqual([
            EchelleTerritoriale.Communes,
            EchelleTerritoriale.Epcis,
            EchelleTerritoriale.Departements,
            EchelleTerritoriale.Regions
        ]);
        expect(EchelleTerritoriale.listerTousLesCodeCategories).toEqual([
            EchelleTerritoriale.Communes.categorieTerritoire.codeCategorie,
            EchelleTerritoriale.Epcis.categorieTerritoire.codeCategorie,
            EchelleTerritoriale.Departements.categorieTerritoire.codeCategorie,
            EchelleTerritoriale.Regions.categorieTerritoire.codeCategorie
        ]);
        expect(EchelleTerritoriale.fromId('EPCI')?.categorieTerritoire.codeCategorie).toEqual('EPCI');
        expect(EchelleTerritoriale.fromNomGeojsonEtUrl('epcis')?.categorieTerritoire.codeCategorie).toEqual('EPCI');
    });
});
