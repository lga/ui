import { type HtmlString, htmlstring } from '@lga/base';
import type { CodeCategorieTerritoireApi, CodeSousCategorieTerritoireApi } from '@lga/specification-api';

const CODE_REGROUPEMENT_COMMUNES = 'REGROUPEMENT_COMMUNES';

export class CategorieTerritoire {
    static readonly Commune = new CategorieTerritoire('COMMUNE', null, 'commune', htmlstring`commune`, 1);
    static readonly Epci = new CategorieTerritoire('EPCI', null, 'intercommunalité', htmlstring`intercommunalité`, 2);
    static readonly RegroupementCommunes = {
        AutreTerritoire: new CategorieTerritoire(CODE_REGROUPEMENT_COMMUNES, 'AUTRE_TERRITOIRE', 'territoire', htmlstring`territoire`, 3),
        BassinDeVie: new CategorieTerritoire(CODE_REGROUPEMENT_COMMUNES, 'BASSIN_DE_VIE_2022', 'bassin de vie', htmlstring`bassin de vie`, 4),
        PaysPetr: new CategorieTerritoire(CODE_REGROUPEMENT_COMMUNES, 'PAYS_PETR', 'Pays / PETR', htmlstring`Pays / PETR`, 5),
        Pn: new CategorieTerritoire(
            CODE_REGROUPEMENT_COMMUNES,
            'PARC_NATIONAL',
            'Parc National',
            htmlstring`<abbr title="Parc National">PN</abbr>`,
            6
        ),
        Pnr: new CategorieTerritoire(
            CODE_REGROUPEMENT_COMMUNES,
            'PARC_NATUREL_REGIONAL',
            'Parc Naturel Régional',
            htmlstring`<abbr title="Parc Naturel Régional">PNR</abbr>`,
            7
        ),
        Scot: new CategorieTerritoire(
            CODE_REGROUPEMENT_COMMUNES,
            'SCHEMA_COHERENCE_TERRITORIAL',
            'Schéma de Cohérence Territoriale',
            htmlstring`<abbr title="Schéma de Cohérence Territoriale">SCoT</abbr>`,
            8
        ),
        Pat: new CategorieTerritoire(
            CODE_REGROUPEMENT_COMMUNES,
            'PROJET_ALIMENTAIRE_TERRITORIAL',
            'Projet alimentaire territorial',
            htmlstring`<abbr title="Projet alimentaire territorial">PAT</abbr>`,
            9
        ),
        Nd: new CategorieTerritoire(CODE_REGROUPEMENT_COMMUNES, 'NOUVEAU_DEPARTEMENT', 'département', htmlstring`département`, 10)
    };
    static readonly Departement = new CategorieTerritoire('DEPARTEMENT', null, 'département', htmlstring`département`, 11);
    static readonly Region = new CategorieTerritoire('REGION', null, 'région', htmlstring`région`, 12);
    static readonly Pays = new CategorieTerritoire('PAYS', null, 'pays', htmlstring`pays`, 13);

    private constructor(
        public readonly codeCategorie: CodeCategorieTerritoireApi,
        public readonly codeSousCategorie: CodeSousCategorieTerritoireApi | null,
        public readonly libelleCategorie: string,
        public readonly libelleCategorieHtml: HtmlString,
        private ordre: number
    ) {}

    static get listerToutes(): CategorieTerritoire[] {
        const listeCategories = Object.values(CategorieTerritoire).filter((c) => c instanceof CategorieTerritoire);
        const listeRegroupementsCommunes = Object.values(this.RegroupementCommunes);
        return listeCategories.concat(listeRegroupementsCommunes).sort((c1, c2) => c1.comparer(c2));
    }

    static creer(
        categorie: CodeCategorieTerritoireApi,
        sousCategorie?: CodeSousCategorieTerritoireApi | null,
        categorieTerritoireFallback?: CategorieTerritoire
    ): CategorieTerritoire {
        const categorieTerritoire = this.listerToutes.find((e) => this.verifierEgalite(e, categorie, sousCategorie));
        if (categorieTerritoire) return categorieTerritoire;
        if (categorieTerritoireFallback) return categorieTerritoireFallback;
        throw new RangeError(
            `Valeur incorrecte : le code catégorie "${categorie}" et/ou le code sous catégorie "${sousCategorie}" ne correspondent à aucun des codes de l'énumération`
        );
    }

    private static creerId(codeCategorie: CodeCategorieTerritoireApi, codeSousCategorie?: CodeSousCategorieTerritoireApi | null) {
        if (codeSousCategorie && codeCategorie === CODE_REGROUPEMENT_COMMUNES) {
            return codeCategorie.toUpperCase() + '_' + codeSousCategorie.toUpperCase();
        } else {
            return codeCategorie.toUpperCase();
        }
    }

    private static verifierEgalite(
        e: CategorieTerritoire,
        codeCategorie: CodeCategorieTerritoireApi,
        sousCategorie?: CodeSousCategorieTerritoireApi | null
    ) {
        return this.creerId(e.codeCategorie, e.codeSousCategorie) === this.creerId(codeCategorie, sousCategorie);
    }

    comparer(categorie: CategorieTerritoire) {
        return this.ordre - categorie.ordre;
    }

    estAuMoinsEgaleA(categorie: CategorieTerritoire): boolean {
        return this.ordre >= categorie.ordre;
    }
}
