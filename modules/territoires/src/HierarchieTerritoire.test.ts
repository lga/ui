import { describe, expect, it } from 'vitest';

import {
    creerHierarchieCommune,
    creerHierarchiePays,
    creerHierarchieRegion,
    creerHierarchieRegroupementCommunesPnr
} from './__test__/outils-tests-territoires';
import { CategorieTerritoire } from './CategorieTerritoire';
import type { Territoire } from './Territoire.js';

describe('Test creation hierarchie de territoires', () => {
    it("La hierarchie d'un pays doit etre vide", () => {
        const pays = creerHierarchiePays();
        expect(pays.territoirePrincipal).toEqual(pays.territoirePrincipal);
        expect(pays.epci).toBeUndefined();
        expect(pays.departement).toBeUndefined();
        expect(pays.region).toBeUndefined();
        expect(pays.pays).toEqual(pays.territoirePrincipal);
    });

    it("Une région ne doit pas avoir d'EPCI", () => {
        const region = creerHierarchieRegion();
        expect(region.territoirePrincipal.categorie).toEqual(CategorieTerritoire.Region);
        expect(region.epci).toBeUndefined();
        expect(region.pays?.id).toEqual('P-FR');
    });

    it("Un regroupement commune doit avoir un département et une région mais pas d'EPCI", () => {
        const region = creerHierarchieRegroupementCommunesPnr();
        expect(region.territoirePrincipal.categorie).toEqual(CategorieTerritoire.RegroupementCommunes.Pnr);
        expect(region.epci).toBeUndefined();
        expect(region.departement?.id).toEqual('D-1');
        expect(region.region?.id).toEqual('R-1');
        expect(region.pays?.id).toEqual('P-FR');
    });

    it('Une commune doit avoir un EPCI', () => {
        const commune = creerHierarchieCommune();
        expect(commune.territoirePrincipal.categorie).toEqual(CategorieTerritoire.Commune);
        expect(commune.epci?.id).toEqual('E-1');
    });
});

describe('Test de récupération de la hiérarchie des territoires', () => {
    it("La hiérarchie des territoires d'une commune doivent contenir tous les niveaux de la commune au pays", () => {
        const commune = creerHierarchieCommune();
        const hierarchie: Territoire[] = commune.getListeTerritoires();
        expect(hierarchie.map((t) => t.id)).toEqual(['C-12345', 'E-1', 'D-1', 'R-1', 'P-FR']);
        expect(hierarchie[0].categorie.codeCategorie).toEqual('COMMUNE');
    });

    it("La hiérarchie des territoires d'un regroupement de communes doit contenir le département, la région et le pays", () => {
        const region = creerHierarchieRegroupementCommunesPnr();
        const hierarchie: Territoire[] = region.getListeTerritoires();
        expect(hierarchie.map((t) => t.id)).toEqual(['PNR-1', 'D-1', 'R-1', 'P-FR']);
    });

    it("La hiérarchie des territoires d'une region doit contenir la région et le pays", () => {
        const region = creerHierarchieRegion();
        const hierarchie: Territoire[] = region.getListeTerritoires();
        expect(hierarchie.map((t) => t.id)).toEqual(['R-75', 'P-FR']);
    });

    it("La hiérarchie des territoires d'un pays ne doit contenir que le pays", () => {
        const pays = creerHierarchiePays();
        const hierarchie: Territoire[] = pays.getListeTerritoires();
        expect(hierarchie.map((t) => t.id)).toEqual(['P-FR']);
    });
});
