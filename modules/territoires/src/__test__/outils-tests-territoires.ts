import { HierarchieTerritoires } from '../HierarchieTerritoires';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function creerHierarchieTerritoires(territoire: any) {
    const territoirePrincipal = new HierarchieTerritoires(
        territoire.idTerritoire,
        territoire.nomTerritoire,
        territoire.categorieTerritoire,
        territoire.sousCategorieTerritoire,
        territoire.epci?.id,
        territoire.epci?.nom,
        territoire.departement?.id,
        territoire.departement?.nom,
        territoire.region?.id,
        territoire.region?.nom,
        territoire.pays?.id,
        territoire.pays?.nom
    );
    return territoirePrincipal;
}

export function creerHierarchiePays() {
    const regionJson = {
        idTerritoire: 'P-FR',
        nomTerritoire: 'France',
        categorieTerritoire: 'PAYS'
    };
    const hierarchieRegion = creerHierarchieTerritoires(regionJson);
    return hierarchieRegion;
}
export function creerHierarchieRegion() {
    const regionJson = {
        idTerritoire: 'R-75',
        nomTerritoire: 'Ma region',
        categorieTerritoire: 'REGION',
        pays: { id: 'P-FR', nom: 'France' }
    };
    const hierarchieRegion = creerHierarchieTerritoires(regionJson);
    return hierarchieRegion;
}

export function creerHierarchieRegroupementCommunesPnr() {
    const regroupementCommuneJson = {
        idTerritoire: 'PNR-1',
        nomTerritoire: 'PNR Test',
        categorieTerritoire: 'REGROUPEMENT_COMMUNES',
        sousCategorieTerritoire: 'PARC_NATUREL_REGIONAL',
        departement: { id: 'D-1', nom: 'MonDepartement' },
        region: { id: 'R-1', nom: 'MaRegion' },
        pays: { id: 'P-FR', nom: 'France' }
    };
    const hierarchieRegroupementCommunes = creerHierarchieTerritoires(regroupementCommuneJson);
    return hierarchieRegroupementCommunes;
}

export function creerHierarchieCommune() {
    const communeJson = {
        idTerritoire: 'C-12345',
        nomTerritoire: 'MaCommune',
        categorieTerritoire: 'COMMUNE',
        epci: { id: 'E-1', nom: 'MonEpci' },
        departement: { id: 'D-1', nom: 'MonDepartement' },
        region: { id: 'R-1', nom: 'MaRegion' },
        pays: { id: 'P-FR', nom: 'France' }
    };
    const hierarchieCommune = creerHierarchieTerritoires(communeJson);
    return hierarchieCommune;
}
