import type { CodeCategorieTerritoireApi } from '@lga/specification-api';
import { describe, expect, it } from 'vitest';

import { CategorieTerritoire } from './CategorieTerritoire';

describe("Test de l'énumération CategorieTerritoire", () => {
    it('Vérification des catégories créées par défaut', () => {
        expect(CategorieTerritoire.Region.codeCategorie).toEqual('REGION');
        expect(CategorieTerritoire.listerToutes).toEqual([
            CategorieTerritoire.Commune,
            CategorieTerritoire.Epci,
            CategorieTerritoire.RegroupementCommunes.AutreTerritoire,
            CategorieTerritoire.RegroupementCommunes.BassinDeVie,
            CategorieTerritoire.RegroupementCommunes.PaysPetr,
            CategorieTerritoire.RegroupementCommunes.Pn,
            CategorieTerritoire.RegroupementCommunes.Pnr,
            CategorieTerritoire.RegroupementCommunes.Scot,
            CategorieTerritoire.RegroupementCommunes.Pat,
            CategorieTerritoire.RegroupementCommunes.Nd,
            CategorieTerritoire.Departement,
            CategorieTerritoire.Region,
            CategorieTerritoire.Pays
        ]);
    });

    it('Vérification de la comparaison entre catégories', () => {
        expect(CategorieTerritoire.Region.comparer(CategorieTerritoire.Commune)).toBeGreaterThan(0);
        expect(CategorieTerritoire.Region.comparer(CategorieTerritoire.Region)).toEqual(0);
    });

    it("Vérification de la récupération d'une catégorie", () => {
        expect(CategorieTerritoire.creer('COMMUNE')).toEqual(CategorieTerritoire.Commune);
        expect(CategorieTerritoire.creer('AAA' as CodeCategorieTerritoireApi, undefined, CategorieTerritoire.Epci)).toEqual(CategorieTerritoire.Epci);
        expect(CategorieTerritoire.creer('COMMUNE', null)).toEqual(CategorieTerritoire.Commune);
        expect(CategorieTerritoire.creer('EPCI', 'METROPOLE')).toEqual(CategorieTerritoire.Epci);
        expect(CategorieTerritoire.creer('REGROUPEMENT_COMMUNES', 'SCHEMA_COHERENCE_TERRITORIAL')).toEqual(
            CategorieTerritoire.RegroupementCommunes.Scot
        );
        expect(CategorieTerritoire.creer('REGROUPEMENT_COMMUNES', 'PARC_NATUREL_REGIONAL')).toEqual(CategorieTerritoire.RegroupementCommunes.Pnr);
        expect(CategorieTerritoire.creer('REGROUPEMENT_COMMUNES', 'PROJET_ALIMENTAIRE_TERRITORIAL')).toEqual(
            CategorieTerritoire.RegroupementCommunes.Pat
        );
        expect(CategorieTerritoire.creer('REGROUPEMENT_COMMUNES', 'NOUVEAU_DEPARTEMENT')).toEqual(CategorieTerritoire.RegroupementCommunes.Nd);
    });

    it('Vérification de auMoinsEgaleA', () => {
        expect(CategorieTerritoire.Commune.estAuMoinsEgaleA(CategorieTerritoire.Departement)).toEqual(false);
        expect(CategorieTerritoire.Departement.estAuMoinsEgaleA(CategorieTerritoire.Departement)).toEqual(true);
        expect(CategorieTerritoire.Pays.estAuMoinsEgaleA(CategorieTerritoire.Departement)).toEqual(true);
    });
});
