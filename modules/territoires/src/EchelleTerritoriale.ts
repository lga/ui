import { CategorieTerritoire } from './CategorieTerritoire.js';

export class EchelleTerritoriale {
    static readonly Communes = new EchelleTerritoriale(CategorieTerritoire.Commune, 'Communes', 'communes', 12, 10);
    static readonly Epcis = new EchelleTerritoriale(CategorieTerritoire.Epci, 'Intercommunalités', 'epcis', 10, 5);
    static readonly Departements = new EchelleTerritoriale(CategorieTerritoire.Departement, 'Départements', 'departements', 8, 5);
    static readonly Regions = new EchelleTerritoriale(CategorieTerritoire.Region, 'Régions', 'regions', 7, 5);

    private constructor(
        public readonly categorieTerritoire: CategorieTerritoire,
        public readonly libelle: string,
        public readonly nomGeojsonEtUrl: string,
        public readonly maxZoom: number,
        public readonly minZoom: number,
        public readonly id = categorieTerritoire.codeCategorie
    ) {}

    static get listerToutes(): EchelleTerritoriale[] {
        return Object.values(EchelleTerritoriale).filter((c) => c instanceof EchelleTerritoriale);
    }

    static get listerTousLesCodeCategories(): string[] {
        return this.listerToutes.map((e) => e.id);
    }

    static fromId(id: string): EchelleTerritoriale | undefined {
        return EchelleTerritoriale.listerToutes.find((e: EchelleTerritoriale) => e.id === id);
    }

    static fromNomGeojsonEtUrl(nomGeojsonEtUrl: string): EchelleTerritoriale | undefined {
        return EchelleTerritoriale.listerToutes.find((e: EchelleTerritoriale) => e.nomGeojsonEtUrl === nomGeojsonEtUrl);
    }
}
