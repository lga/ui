import type { Parser } from 'csv-parse';
import { parse } from 'csv-parse/sync';
import fs from 'fs';

import { type HtmlString, htmlstring } from '../../base/build';
import type { IdConstatOuTheme } from '../../indicateurs/build';
import type { Initiative, TagProfil, TagSujet } from '../src';

const FICHIER_CSV_ENTREE = 'Initiatives TEF version Courante.csv';
const FICHIER_TS_RESULTAT = '../src/modeles/initiatives/initiatives-definitions.ts';

export interface InitiativeSansImage extends Omit<Initiative, 'image'> {
    idImage: string;
}

const extraireCelluleListe = (contenuCellule: string): string[] => {
    if (!contenuCellule.trim()) {
        return [];
    }
    return contenuCellule.split(',').map((elt) => {
        return elt.trim();
    });
};
const extraireCelluleString = (contenuCellule: string, valeurSiVide = 'null'): string => {
    if (!contenuCellule || contenuCellule.trim() === '') {
        return valeurSiVide;
    }
    return contenuCellule.trim();
};
const extraireCelluleHtmlString = (contenuCellule: string): HtmlString => {
    return htmlstring`${contenuCellule.trim()}`;
};
const extraireCelluleNumber = (contenuCellule: string, valeurSiAbsent: number): number => {
    // on force le type sans faire plus de vérification car le check est fait lors du build de initiatives-definitions.ts
    return contenuCellule ? (contenuCellule.trim() as unknown as number) : valeurSiAbsent;
};

function extraireListeInitiatives(lignesCsv: Parser): InitiativeSansImage[] {
    const listeInitiatives: InitiativeSansImage[] = lignesCsv
        .filter((r) => r.id !== '')
        .map((record): InitiativeSansImage => {
            const idImage = extraireCelluleString(record.idImage).toUpperCase();
            const initiative: InitiativeSansImage = {
                id: extraireCelluleString(record.id),
                titre: extraireCelluleString(record.titre),
                idTerritoire: extraireCelluleString(record.idTerritoire),
                localisation: extraireCelluleString(record.localisation),
                // on force le type sans plus de vérification dans les lignes suivantes car le check est fait lors du build de initiatives-definitions.ts
                idsConstatsThemes: extraireCelluleListe(record.idsConstatsThemes) as IdConstatOuTheme[],
                tagsProfilsScoreEleve: extraireCelluleListe(record.tagsProfilsScoreEleve) as TagProfil[],
                tagsProfilsScoreMoyen: extraireCelluleListe(record.tagsProfilsScoreMoyen) as TagProfil[],
                tagsProfilsScoreFaible: extraireCelluleListe(record.tagsProfilsScoreFaible) as TagProfil[],
                bonusScore: extraireCelluleNumber(record.bonusScore, 0),
                tagsSujets: extraireCelluleListe(record.tagsSujets) as unknown as TagSujet[],
                description: extraireCelluleHtmlString(record.description),
                informationSource: extraireCelluleString(record.informationSource, ''),
                intituleLienExterne: extraireCelluleString(record.intituleLienExterne),
                urlLienExterne: extraireCelluleString(record.urlLienExterne),
                idImage: idImage
            };
            return initiative;
        }) as unknown as InitiativeSansImage[]; // csv-parse n'offre pas de type generique, donc cast forcé (voir https://github.com/adaltas/node-csv/issues/407)
    return listeInitiatives;
}

const genererListeStringTs = (liste: string[]): string => {
    return `[${liste.map((elt) => `\`${elt}\``).join(', ')}]`;
};

const genererLigneImportImageDefinition = (idImage: string) => `import { ${idImage} } from '@lga/images-definitions';`;

const genererBlocInitiativeTs = (initiative: InitiativeSansImage) => `    {
        id: \`${initiative.id}\`,
        titre: \`${initiative.titre}\`,
        idTerritoire: \`${initiative.idTerritoire}\`,
        localisation: \`${initiative.localisation}\`,
        idsConstatsThemes: ${genererListeStringTs(initiative.idsConstatsThemes)},
        tagsProfilsScoreEleve: ${genererListeStringTs(initiative.tagsProfilsScoreEleve)},
        tagsProfilsScoreMoyen: ${genererListeStringTs(initiative.tagsProfilsScoreMoyen)},
        tagsProfilsScoreFaible: ${genererListeStringTs(initiative.tagsProfilsScoreFaible)},
        bonusScore: ${initiative.bonusScore},
        tagsSujets: ${genererListeStringTs(initiative.tagsSujets)},
        description: htmlstring\`${initiative.description}\`,
        informationSource: \`${initiative.informationSource}\`,
        intituleLienExterne: \`${initiative.intituleLienExterne}\`,
        urlLienExterne: \`${initiative.urlLienExterne}\`,
        image: ${initiative.idImage}
    }`;

const genererFichierTsDefinitionsInitiatives = (fichierTsResultat: string, listeInitiatives: InitiativeSansImage[]) => {
    const listeIdsImagesUniques = [...new Set(listeInitiatives.map((e) => e.idImage))];
    const importImageDefinition = `${listeIdsImagesUniques.map(genererLigneImportImageDefinition).join('\n')}`;

    const declarationConstDefinitionsInitiativesTs = `export const DEFINITIONS_INITIATIVES: Initiative[] = [
${listeInitiatives.map(genererBlocInitiativeTs).join(',\n')}
]; `;

    const contenuFichierTs = `import { htmlstring } from '@lga/base';
${importImageDefinition}

import type { Initiative } from './Initiatives';

${declarationConstDefinitionsInitiativesTs}
`;

    fs.writeFileSync(fichierTsResultat, contenuFichierTs);
    console.log(`Fichier créé : ${fichierTsResultat}`);
};

try {
    const contenuFichier = fs.readFileSync(FICHIER_CSV_ENTREE, 'utf8');
    const lignesCsv: Parser = parse(contenuFichier, {
        bom: true,
        columns: true,
        skip_empty_lines: true,
        delimiter: ';',
        quote: '"'
    });

    const listeInitiatives: InitiativeSansImage[] = extraireListeInitiatives(lignesCsv);

    genererFichierTsDefinitionsInitiatives(FICHIER_TS_RESULTAT, listeInitiatives);
} catch (erreur) {
    console.error('Erreur lors du traitement du csv:', erreur);
}
