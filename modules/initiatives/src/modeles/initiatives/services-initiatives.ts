import { deplacerElementEnPositionN, differenceListeAMoinsListeB } from '@lga/base';
import { type IdConstat, type IdTheme, MAP_ID_CONSTAT_ID_THEME } from '@lga/indicateurs';

import { creerListeInitiativesAvecScore, type ProfilSuggestion } from './calcul-scores';
import type { Initiative, InitiativeAvecScore, TagSujet } from './Initiatives';
import { DEFINITIONS_INITIATIVES } from './initiatives-definitions';
import { filtrerInitiatives, type OptionTri, trierInitiatives } from './initiatives-utils.js';

export function recupererListeInitiatives(listeInitiatives: Initiative[], idsThemes?: IdTheme[], tagsSujets?: TagSujet[]): Initiative[] {
    return filtrerInitiatives(listeInitiatives, idsThemes, tagsSujets);
}

export function recupererListeInitiativesAvecScore(
    listeInitiatives: Initiative[],
    profilSuggestion: ProfilSuggestion,
    optionTri: OptionTri = 'ScoreDecroissant',
    idsThemes?: IdTheme[],
    tagsSujets?: TagSujet[]
): InitiativeAvecScore[] {
    const initiatives = recupererListeInitiatives(listeInitiatives, idsThemes, tagsSujets);
    let initiativeResultats = creerListeInitiativesAvecScore(initiatives, profilSuggestion);
    initiativeResultats = trierInitiatives(initiativeResultats, optionTri);
    return initiativeResultats;
}

export function recupererListe3InitiativesParConstats(
    profilSuggestion: ProfilSuggestion,
    idsConstats: IdConstat[],
    listeInitiatives: Initiative[] = DEFINITIONS_INITIATIVES
): InitiativeAvecScore[] {
    const initiativesResultat = [];
    const listeInitiativesAvecScore = recupererListeInitiativesAvecScore(listeInitiatives, profilSuggestion);
    for (const idConstat of idsConstats) {
        initiativesResultat.push(...recupererListeTroisInitiativesPourUnConstat(listeInitiativesAvecScore, idConstat));
    }
    return initiativesResultat;
}

export const IDS_INITIATIVES_POUSSEES_MANUELLEMENT = {
    'Reseau Manger Bio': 'I-011'
};

export function recupererListeTroisInitiativesPourUnConstat(
    listeInitiativesAvecScore: InitiativeAvecScore[],
    idConstat: IdConstat
): InitiativeAvecScore[] {
    let initiativesResultat = [];
    const initiativesTriees = trierInitiatives(listeInitiativesAvecScore, 'ScoreDecroissant');

    const initiativesRelativesAuTheme = initiativesTriees.filter(
        (i) => i.idsConstatsThemes?.includes(idConstat) || i.idsConstatsThemes?.includes(MAP_ID_CONSTAT_ID_THEME[idConstat])
    );

    if (MAP_ID_CONSTAT_ID_THEME[idConstat] === 'terres-agricoles') {
        const initiativesProtectionTerres = initiativesTriees.filter((i) => i.tagsSujets?.includes('Protection des terres')).slice(0, 1);
        const initiativesSobrieteFonciere = initiativesTriees.filter((i) => i.tagsSujets?.includes('Sobriété foncière'));

        const initiativesTerresAgricolesRestantes = differenceListeAMoinsListeB<InitiativeAvecScore>(
            initiativesRelativesAuTheme,
            initiativesProtectionTerres.concat(initiativesSobrieteFonciere)
        );

        initiativesResultat = [...initiativesProtectionTerres, ...initiativesSobrieteFonciere, ...initiativesTerresAgricolesRestantes];
    } else {
        const initiativesConstat = initiativesRelativesAuTheme.filter((i) => i.idsConstatsThemes?.includes(idConstat));
        const initiativesHorsConstat = initiativesRelativesAuTheme.filter((i) => !i.idsConstatsThemes?.includes(idConstat));

        initiativesResultat = [...initiativesConstat, ...initiativesHorsConstat];
    }

    if (MAP_ID_CONSTAT_ID_THEME[idConstat] === 'autonomie-alimentaire') {
        initiativesResultat = deplacerElementEnPositionN(initiativesResultat, IDS_INITIATIVES_POUSSEES_MANUELLEMENT['Reseau Manger Bio'], 3);
    }
    return initiativesResultat.slice(0, 3);
}
