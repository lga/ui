import { describe, expect, it } from 'vitest';

import { calculerScore, creerListeInitiativesAvecScore, type ProfilSuggestion } from './calcul-scores';
import type { Initiative } from './Initiatives';
import { creerInitiative } from './initiatives-utils.test';

const INITIATIVES_TEST: Initiative[] = [
    creerInitiative('I-0001', ['COMMUNE_TOUS'], ['COMMUNE_URBAINE'], [], undefined, ['terres-agricoles']),
    creerInitiative('I-0002', ['AGRICULTURE_TOUS'], ['PAYS'], [], undefined, ['terres-agricoles']),
    creerInitiative('I-0003', ['AGRICULTURE_TOUS'], ['PAYS'], [], 5, ['consommation'])
];

describe('Test de la creation de liste initiatives avec score', () => {
    it('Test création liste initiatives suggérées', () => {
        // given
        const profilSuggestionFrance: ProfilSuggestion = { codeProfilCollectivite: 'PAYS', codeProfilAgriculture: 'NON_SPECIALISE' };
        // when
        const initiativeSuggerees = creerListeInitiativesAvecScore(INITIATIVES_TEST, profilSuggestionFrance);
        // then
        expect(initiativeSuggerees.map((i) => i.id)).toEqual(['I-0003', 'I-0002', 'I-0001']);
        expect(initiativeSuggerees.map((i) => i.score)).toEqual([55, 50, 0]);
    });
});

describe('Test du calcul du score d`une initiave', () => {
    const profilSuggestionCommune: ProfilSuggestion = { codeProfilCollectivite: 'PETITE_VILLE', codeProfilAgriculture: 'FRUITS_ET_LEGUMES' };

    it("Calculer le score d'une initiative sans Codes ou Tags", () => {
        const initiativeSansTags: Initiative = creerInitiative('I-0001', [], [], []);
        expect(calculerScore(profilSuggestionCommune, initiativeSansTags)).toEqual(0);

        const initiativeAvecCodesDifferentsDuProfilRecherche: Initiative = creerInitiative('I-0001', ['PAYS'], ['ELEVAGE_EXTENSIF'], ['VITICULTURE']);

        expect(calculerScore(profilSuggestionCommune, initiativeAvecCodesDifferentsDuProfilRecherche)).toEqual(0);
    });

    it("Calculer le score d'une initiative définie avec des Codes uniquement (sans TAGs)", () => {
        const initiativeScore30: Initiative = creerInitiative('I-0001', ['PETITE_VILLE'], [], []);
        expect(calculerScore(profilSuggestionCommune, initiativeScore30)).toEqual(30);

        const initiativeScore50: Initiative = creerInitiative('I-0001', ['PETITE_VILLE'], ['FRUITS_ET_LEGUMES'], []);
        expect(calculerScore(profilSuggestionCommune, initiativeScore50)).toEqual(50);

        const initiativeScore60: Initiative = creerInitiative('I-0001', ['PETITE_VILLE', 'FRUITS_ET_LEGUMES'], [], []);
        expect(calculerScore(profilSuggestionCommune, initiativeScore60)).toEqual(60);

        const initiativeScore20: Initiative = creerInitiative('I-0001', [], ['VITICULTURE'], ['PETITE_VILLE', 'FRUITS_ET_LEGUMES']);
        expect(calculerScore(profilSuggestionCommune, initiativeScore20)).toEqual(20);
    });

    it('Calculer un score avec des TAGs', () => {
        const initiativeScore20: Initiative = creerInitiative('I-0001', ['ELEVAGE_EXTENSIF'], ['COMMUNE_TOUS'], []);
        expect(calculerScore(profilSuggestionCommune, initiativeScore20)).toEqual(20);

        const initiativeScore60: Initiative = creerInitiative('I-0001', ['COMMUNE_TOUS', 'FRUITS_ET_LEGUMES'], ['AGRICULTURE_TOUS'], []);
        expect(calculerScore(profilSuggestionCommune, initiativeScore60)).toEqual(60);
    });

    it("Calculer le score d'une initiative, ne prendre en compte que le score le plus élevé pour un même code", () => {
        const initiativeScore30: Initiative = creerInitiative('I-0001', ['PETITE_VILLE', 'PETITE_VILLE'], ['COMMUNE_TOUS'], []);
        expect(calculerScore(profilSuggestionCommune, initiativeScore30)).toEqual(30);
    });

    it('Calculer un score avec bonus', () => {
        const profilSuggestionFrance: ProfilSuggestion = { codeProfilCollectivite: 'PAYS', codeProfilAgriculture: 'NON_SPECIALISE' };

        const initiativeScore30: Initiative = creerInitiative('I-0001', ['PAYS'], [], [], 5);
        expect(calculerScore(profilSuggestionFrance, initiativeScore30)).toEqual(35);
    });
});
