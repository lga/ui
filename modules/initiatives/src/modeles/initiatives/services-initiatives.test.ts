import { verifierListeAIncluseDansListeB } from '@lga/base';
import { describe, expect, it } from 'vitest';

import type { ProfilSuggestion } from './calcul-scores.js';
import type { Initiative, InitiativeAvecScore } from './Initiatives';
import { DEFINITIONS_INITIATIVES } from './initiatives-definitions';
import { creerInitiative, creerInitiativeAvecScore } from './initiatives-utils.test';
import {
    IDS_INITIATIVES_POUSSEES_MANUELLEMENT,
    recupererListeInitiativesAvecScore,
    recupererListeTroisInitiativesPourUnConstat
} from './services-initiatives';

describe('Test des fonctions de récupération des initiatives', () => {
    it('Test ids corrects dans IDS_INITIATIVES_POUSSEES_MANUELLEMENT', () => {
        expect(
            verifierListeAIncluseDansListeB(
                Object.values(IDS_INITIATIVES_POUSSEES_MANUELLEMENT),
                DEFINITIONS_INITIATIVES.map((i) => i.id)
            )
        ).toBe(true);
    });
    it('Test recupererListeInitiativesAvecScore', () => {
        // given
        const INITIATIVES: Initiative[] = [
            creerInitiative('I-001', [], [], [], 0, ['prelevements-eau'], []),
            creerInitiative('I-002', [], ['AGRICULTURE_TOUS'], [], 0, ['ressources'], ['Agroécologie']),
            creerInitiative('I-003', [], ['NON_SPECIALISE'], [], 0, ['prelevements-eau', 'alimentation-animale-importee'], []),
            creerInitiative('I-004', ['NON_SPECIALISE'], [], [], 0, ['ressources'], []),
            creerInitiative('I-005', ['PAYS'], [], [], 1, ['terres-agricoles'], [])
        ];
        const profilSuggestionFrance: ProfilSuggestion = { codeProfilCollectivite: 'PAYS', codeProfilAgriculture: 'NON_SPECIALISE' };
        // then
        expect(recupererListeInitiativesAvecScore(INITIATIVES, profilSuggestionFrance).map((i) => i.id)).toEqual([
            'I-005',
            'I-004',
            'I-003',
            'I-002',
            'I-001'
        ]);
        expect(recupererListeInitiativesAvecScore(INITIATIVES, profilSuggestionFrance, 'Theme').map((i) => i.id)).toEqual([
            'I-003',
            'I-001',
            'I-004',
            'I-002',
            'I-005'
        ]);
        expect(recupererListeInitiativesAvecScore(INITIATIVES, profilSuggestionFrance, 'ScoreDecroissant', ['ressources']).map((i) => i.id)).toEqual([
            'I-004',
            'I-002'
        ]);
        expect(
            recupererListeInitiativesAvecScore(INITIATIVES, profilSuggestionFrance, 'ScoreDecroissant', ['ressources'], ['Agroécologie']).map(
                (i) => i.id
            )
        ).toEqual(['I-002']);
    });

    it('Test récuperer les 3 initiatives pour le constat eau', () => {
        // given
        const INITIATIVES: InitiativeAvecScore[] = [
            creerInitiativeAvecScore('I-001', 1, ['prelevements-eau'], []),
            creerInitiativeAvecScore('I-002', 2, ['ressources'], []),
            creerInitiativeAvecScore('I-003', 3, ['dependances-ressources', 'alimentation-animale-importee'], []),
            creerInitiativeAvecScore('I-004', 4, ['ressources'], []),
            creerInitiativeAvecScore('I-005', 5, ['terres-agricoles'], [])
        ];
        // when
        const initiativeSuggerees = recupererListeTroisInitiativesPourUnConstat(INITIATIVES, 'prelevements-eau');
        // then
        expect(initiativeSuggerees.map((i) => i.id)).toEqual(['I-001', 'I-004', 'I-002']);
    });

    it('Test récuperer les 3 initiatives pour le theme terres-agricoles', () => {
        // given
        const INITIATIVES: InitiativeAvecScore[] = [
            creerInitiativeAvecScore('I-001', 1, ['terres-agricoles'], ['Sobriété foncière']),
            creerInitiativeAvecScore('I-002', 2, ['terres-agricoles'], []),
            creerInitiativeAvecScore('I-003', 3, ['terres-agricoles'], []),
            creerInitiativeAvecScore('I-004', 4, ['terres-agricoles'], ['Protection des terres']),
            creerInitiativeAvecScore('I-005', 5, ['terres-agricoles'], ['Protection des terres']),
            creerInitiativeAvecScore('I-006', 6, ['terres-agricoles'], []),
            creerInitiativeAvecScore('I-007', 7, ['ressources'], [])
        ]; // when
        const initiativeSuggerees = recupererListeTroisInitiativesPourUnConstat(INITIATIVES, 'terres-suffisantes');
        // then
        expect(initiativeSuggerees.map((i) => i.id)).toEqual(['I-005', 'I-001', 'I-006']);
    });

    it('Test récuperer les 3 initiatives pour le theme autonomie-alimentaire', () => {
        // given
        const INITIATIVES: InitiativeAvecScore[] = [
            creerInitiativeAvecScore('I-011', 1, ['autonomie-alimentaire']),
            creerInitiativeAvecScore('I-002', 2, ['autonomie-alimentaire']),
            creerInitiativeAvecScore('I-003', 3, ['autonomie-alimentaire']),
            creerInitiativeAvecScore('I-004', 4, ['autonomie-alimentaire']),
            creerInitiativeAvecScore('I-005', 5, ['autonomie-alimentaire']),
            creerInitiativeAvecScore('I-006', 6, ['autonomie-alimentaire']),
            creerInitiativeAvecScore('I-007', 7, ['ressources'])
        ]; // when
        const initiativeSuggerees = recupererListeTroisInitiativesPourUnConstat(INITIATIVES, 'distances');
        // then
        expect(initiativeSuggerees.map((i) => i.id)).toEqual(['I-006', 'I-005', 'I-011']);
    });
});
