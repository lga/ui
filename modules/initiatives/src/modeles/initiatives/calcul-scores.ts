import type { ProfilsApi } from '@lga/specification-api';

import { convertirTagsEnCodes } from '../tags-profils/tags-utils';
import type { Initiative, InitiativeAvecScore } from './Initiatives';
import { trierInitiatives } from './initiatives-utils.js';

const SCORE_ELEVE = 30;
const SCORE_MOYEN = 20;
const SCORE_FAIBLE = 10;

export type ProfilSuggestion = Omit<ProfilsApi, 'codeProfilDiagnostic'>;

export function creerListeInitiativesAvecScore(listeInitiatives: Initiative[], profilSuggestion: ProfilSuggestion): InitiativeAvecScore[] {
    const listeInitiativesSuggerees: InitiativeAvecScore[] = [];
    for (const initiative of listeInitiatives) {
        listeInitiativesSuggerees.push({
            ...initiative,
            score: calculerScore(profilSuggestion, initiative)
        });
    }
    return trierInitiatives(listeInitiativesSuggerees, 'ScoreDecroissant');
}

export function calculerScore(profilSuggestion: ProfilSuggestion, initiative: Initiative) {
    let score = 0;
    const codesProfilsScoreEleve = convertirTagsEnCodes(initiative.tagsProfilsScoreEleve);
    const codesProfilsScoreMoyen = convertirTagsEnCodes(initiative.tagsProfilsScoreMoyen);
    const codesProfilsScoreFaible = convertirTagsEnCodes(initiative.tagsProfilsScoreFaible);
    if (codesProfilsScoreEleve.includes(profilSuggestion.codeProfilCollectivite)) {
        score += SCORE_ELEVE;
    } else if (codesProfilsScoreMoyen.includes(profilSuggestion.codeProfilCollectivite)) {
        score += SCORE_MOYEN;
    } else if (codesProfilsScoreFaible.includes(profilSuggestion.codeProfilCollectivite)) {
        score += SCORE_FAIBLE;
    }
    if (codesProfilsScoreEleve.includes(profilSuggestion.codeProfilAgriculture)) {
        score += SCORE_ELEVE;
    } else if (codesProfilsScoreMoyen.includes(profilSuggestion.codeProfilAgriculture)) {
        score += SCORE_MOYEN;
    } else if (codesProfilsScoreFaible.includes(profilSuggestion.codeProfilAgriculture)) {
        score += SCORE_FAIBLE;
    }
    return score + (initiative.bonusScore ?? 0);
}
