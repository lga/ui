import { htmlstring } from '@lga/base';
import type { IdConstatOuTheme } from '@lga/indicateurs';
import { describe, expect, it } from 'vitest';

import type { TagProfil } from '../tags-profils';
import type { Initiative, InitiativeAvecScore, TagSujet } from './Initiatives';
import { filtrerInitiatives, trierInitiatives } from './initiatives-utils.js';

export function creerInitiative(
    id: string,
    tagsProfilsScoreEleve: TagProfil[],
    tagsProfilsScoreMoyen: TagProfil[],
    tagsProfilsScoreFaible: TagProfil[],
    bonusScore?: number,
    idsConstatsThemes?: IdConstatOuTheme[],
    tagsSujets?: TagSujet[]
): Initiative {
    return {
        id: id,
        titre: `Titre ${id}`,
        description: htmlstring`Description ${id}`,
        idTerritoire: 'T-xx',
        localisation: 'localisation',
        idsConstatsThemes: idsConstatsThemes ?? ['terres-agricoles'],
        tagsProfilsScoreEleve,
        tagsProfilsScoreMoyen,
        tagsProfilsScoreFaible,
        tagsSujets: tagsSujets ?? [],
        bonusScore: bonusScore ?? 0,
        informationSource: '',
        urlLienExterne: 'http://example.com',
        intituleLienExterne: 'Lien',
        image: { url: '', descriptionAlt: '', resolutionX: 0, resolutionY: 0 }
    };
}

export function creerInitiativeAvecScore(
    id: string,
    score: number,
    idsConstatsThemes: IdConstatOuTheme[],
    tagsSujets?: TagSujet[]
): InitiativeAvecScore {
    return {
        id: id,
        titre: `Titre ${id}`,
        description: htmlstring`Description ${id}`,
        idTerritoire: 'T-xx',
        localisation: 'localisation',
        idsConstatsThemes: idsConstatsThemes,
        tagsProfilsScoreEleve: [],
        tagsProfilsScoreMoyen: [],
        tagsProfilsScoreFaible: [],
        tagsSujets: tagsSujets ?? [],
        bonusScore: 0,
        score: score,
        informationSource: '',
        urlLienExterne: 'http://example.com',
        intituleLienExterne: 'Lien',
        image: { url: '', descriptionAlt: '', resolutionX: 0, resolutionY: 0 }
    };
}

describe('Test fonction de filtrage des initiatives', () => {
    it('Test filtrerParThemesOuSujets', () => {
        const liste: InitiativeAvecScore[] = [
            creerInitiativeAvecScore('I-01', 1, ['ressources'], []),
            creerInitiativeAvecScore('I-02', 2, ['ressources'], ['Agroécologie']),
            creerInitiativeAvecScore('I-03', 3, ['climat'], ['Protection des terres']),
            creerInitiativeAvecScore('I-04', 4, ['climat'], ['Protection des terres', 'Agroécologie']),
            creerInitiativeAvecScore('I-05', 5, ['accessibilite'], []),
            creerInitiativeAvecScore('I-06', 5, ['consommation'], [])
        ];

        expect(trierInitiatives(liste, 'ScoreDecroissant').map((i) => i.id)).toEqual(['I-05', 'I-06', 'I-04', 'I-03', 'I-02', 'I-01']);
        expect(trierInitiatives(liste, 'Theme').map((i) => i.id)).toEqual(['I-05', 'I-04', 'I-03', 'I-06', 'I-02', 'I-01']);
    });
    it('Test filtrerParThemesOuSujets', () => {
        const liste: Initiative[] = [
            creerInitiative('I-01', [], [], [], 0, ['ressources'], []),
            creerInitiative('I-02', [], [], [], 0, ['ressources'], ['Agroécologie']),
            creerInitiative('I-03', [], [], [], 0, ['climat'], ['Protection des terres']),
            creerInitiative('I-04', [], [], [], 0, ['climat'], ['Protection des terres', 'Agroécologie']),
            creerInitiative('I-05', [], [], [], 0, ['consommation'], [])
        ];

        const listeIds = liste.map((i) => i.id);
        expect(filtrerInitiatives(liste).map((i) => i.id)).toEqual(listeIds);
        expect(filtrerInitiatives(liste, [], []).map((i) => i.id)).toEqual(listeIds);
        expect(filtrerInitiatives(liste, ['climat'], []).map((i) => i.id)).toEqual(['I-03', 'I-04']);
        expect(filtrerInitiatives(liste, ['climat', 'ressources'], []).map((i) => i.id)).toEqual([]);
        expect(filtrerInitiatives(liste, [], ['Agroécologie']).map((i) => i.id)).toEqual(['I-02', 'I-04']);
        expect(filtrerInitiatives(liste, [], ['Protection des terres', 'Agroécologie']).map((i) => i.id)).toEqual(['I-04']);
        expect(filtrerInitiatives(liste, ['ressources'], ['Protection des terres', 'Agroécologie']).map((i) => i.id)).toEqual([]);
    });
});
