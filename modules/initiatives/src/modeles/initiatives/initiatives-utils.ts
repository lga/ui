import { verifierListeAIncluseDansListeB } from '@lga/base';
import type { IdTheme } from '@lga/indicateurs';

import type { Initiative, InitiativeAvecScore, TagSujet } from './Initiatives';

export type OptionTri = 'ScoreDecroissant' | 'Theme';

export function trierInitiatives(liste: InitiativeAvecScore[], option: OptionTri) {
    const listeTriee =
        option === 'ScoreDecroissant'
            ? liste.slice().sort((a, b) => {
                  if (a.score !== b.score) {
                      return b.score - a.score;
                  }
                  return a.idsConstatsThemes[0].localeCompare(b.idsConstatsThemes[0]);
              })
            : liste.slice().sort((a, b) => {
                  if (a.idsConstatsThemes[0] !== b.idsConstatsThemes[0]) {
                      return a.idsConstatsThemes[0].localeCompare(b.idsConstatsThemes[0]);
                  }
                  return b.score - a.score;
              });
    return listeTriee;
}

export function filtrerInitiatives(listeInitiatives: Initiative[], idsThemes?: IdTheme[], tagsSujets?: TagSujet[]) {
    let listeResultat = listeInitiatives.slice();
    if (idsThemes) listeResultat = listeResultat.filter((i) => verifierListeAIncluseDansListeB(idsThemes, i.idsConstatsThemes));
    if (tagsSujets) listeResultat = listeResultat.filter((i) => verifierListeAIncluseDansListeB(tagsSujets, i.tagsSujets));
    return listeResultat;
}
