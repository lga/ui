import type { CodeProfil } from './Codes';

export type JokerProfilAgriculture = 'AGRICULTURE_TOUS';
export type JokerProfilCollectivite =
    | 'INTERCO_TOUS'
    | 'INTERCO_URBAINE'
    | 'INTERCO_INTERMEDIAIRE'
    | 'INTERCO_RURALE'
    | 'EPCI_TOUS'
    | 'PARC_NATUREL_REGIONAL_TOUS'
    | 'COMMUNE_TOUS'
    | 'COMMUNE_URBAINE'
    | 'COMMUNE_INTERMEDIAIRE'
    | 'COMMUNE_RURALE';
export type JokerProfil = JokerProfilCollectivite | JokerProfilAgriculture;

export type DefinitionsJokersProfils = {
    [key in JokerProfil]: CodeProfil[];
};
